-- 2020.02.08 회원가입 테이블
CREATE TABLE `RESERVATE_APP`.`members` (
    `idx` INT NOT NULL AUTO_INCREMENT COMMENT '유저 인덱스' ,
    `member_id` VARCHAR(25) NOT NULL COMMENT '유저 아이디' ,
    `member_email` VARCHAR(255) NOT NULL COMMENT '유저 이메일' ,
    `member_phone` VARCHAR(12) NOT NULL COMMENT '유저 핸드폰' ,
    `member_password` VARCHAR(255) NOT NULL COMMENT '유저 비밀번호' ,
    `member_jointime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '가입 시간' ,
    PRIMARY KEY (`idx`)
) ENGINE = InnoDB COMMENT = '회원정보 테이블';

-- 2020.02.09 관리자 여부
ALTER TABLE `members` ADD `is_admin` TINYINT NOT NULL COMMENT '관리자 여부' AFTER `member_password`;

-- 2020.02.11 스튜디오 정보(deploy 완료)
CREATE TABLE `RESERVATE_APP`.`studios` (
    `idx` INT NOT NULL AUTO_INCREMENT COMMENT '인덱스' ,
    `delegate_phone` VARCHAR(20) NOT NULL COMMENT '대표 폰번호' ,
    `charge_phone` VARCHAR(20) NOT NULL COMMENT '담당자 폰번호' ,
    `studio_comment` TEXT NOT NULL COMMENT '스튜디오 설명' ,
    `studio_road_address` VARCHAR(100) NOT NULL COMMENT '스튜디오 도로명 주소' ,
    `studio_jibun_address` VARCHAR(100) NOT NULL COMMENT '스튜디오 지번 주소' ,
    `studio_city` VARCHAR(30) NOT NULL COMMENT '스튜디오 시' ,
    `studio_local` VARCHAR(30) NOT NULL COMMENT '스튜디오 군구' ,
    `user_idx` INT NOT NULL COMMENT '스튜디오 주인 인덱스' ,
    PRIMARY KEY (`idx`)
) ENGINE = InnoDB COMMENT = '스튜디오리스트';
ALTER TABLE `studios` ADD `studio_name` VARCHAR(100) NOT NULL COMMENT '스튜디오 이름' AFTER `user_idx`;
ALTER TABLE `studios` CHANGE `user_idx` `user_idx` INT(11) NOT NULL COMMENT '스튜디오 주인 인덱스' AFTER `idx`
ALTER TABLE `studios` ADD `studio_detail_local` VARCHAR(30) NOT NULL COMMENT '스튜디오 동' AFTER `studio_local`;
ALTER TABLE `studios` DROP `charge_phone`;
ALTER TABLE `studios` ADD `gps_x` VARCHAR(50) NOT NULL COMMENT 'x좌표' AFTER `studio_detail_local`, ADD `gps_y` VARCHAR(50) NOT NULL COMMENT 'y좌표' AFTER `gps_x`;

-- gps x좌표 주석 수정(deploy 완료)
ALTER TABLE `studios` CHANGE `gps_x` `gps_x` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'x좌표(latitude)';


-- 2020.02.02 스튜디오 예약(deploy 완료)
CREATE TABLE `RESERVATE_APP`.`reservate_list` (
    `idx` INT NOT NULL AUTO_INCREMENT COMMENT '인덱스' ,
    `studio_idx` INT NOT NULL COMMENT '스튜디오 인덱스' ,
    `reserver_idx` INT NOT NULL COMMENT '예약자 인덱스' ,
    `reserve_code` VARCHAR(30) NOT NULL COMMENT '예약 코드' ,
    `reserve_time` DATETIME NOT NULL COMMENT '예약 시간' ,
    PRIMARY KEY (`idx`)
) ENGINE = InnoDB COMMENT = '스튜디오 예약 테이블';

ALTER TABLE `reservate_list` CHANGE `reserve_time` `reserve_start_time` DATETIME NOT NULL COMMENT '예약 시작 시간';
ALTER TABLE `reservate_list` ADD `reserve_end_time` DATETIME NOT NULL COMMENT '예약 종료 시간' AFTER `reserve_start_time`;
ALTER TABLE `reservate_list` ADD `reserve_time` DATETIME NOT NULL COMMENT '예약 요청 시간' AFTER `reserve_code`;
ALTER TABLE `reservate_list` ADD `reserve_number` INT NOT NULL COMMENT '스튜디오 예약인원' AFTER `reserver_idx`;
ALTER TABLE `reservate_list` ADD `is_allow` TINYINT NOT NULL COMMENT '허가여부' AFTER `reserve_end_time`;
