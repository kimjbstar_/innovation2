<?php

use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\BadResponseException as BadResponse;
use GuzzleHttp\Client;

if (! class_exists('Deliver')) {
    class Deliver
    {
        private $baseUri = null;
        private $ci = null;
        private $baseArr = null;
        public function __construct()
        {
            $this->baseUri = getenv('BASE_URI');
            $this->baseArr = [
                'm_code' => getenv('M_CODE'),
                'cc_code' => getenv('CC_CODE'),
                'type' => 'json',
                'token' => null,
            ];
            $this->ci =& get_instance();
            $this->ci->load->library('session');
        }


        public function order(array $info)
        {
            /**
             * 오더 등록
             *
             *
             */



            //  m_code	회원사 고유번호	필수항목
            //  cc_code	 콜센터 고유번호	 필수항목
            //  user_id	 콜센터 가입 회원 아이디	 필수항목
            //  token	 OAuth 인증을 이후 액세스 토큰	 필수항목
            //  c_name	 접수자 이름ㆍ상호	 필수항목
            //  c_mobile	 접수자 연락처	 '-' 기호 제외
            //  c_dept_name	 접수자 부서명
            //  c_charge_name	 접수자 담당
            //  reason_desc	 배송사유
            //  s_start	 출발지 상호ㆍ이름	 필수항목
            //  start_telno	 출발지 전화번호	 '-' 기호 제외
            //  dept_name	 출발지 부서명
            //  charge_name	 출발지 담당
            //  start_sido	 출발지 시ㆍ도	 필수항목
            //  start_gugun	 출발지 구ㆍ군	 필수항목
            //  start_dong	 출발지 동명	 필수항목
            //  start_lon	 출발지 경도 좌표
            //  start_lat	 출발지 위도 좌표
            //  start_location	 출발지 상세주소
            //  s_dest	 도착지 상호ㆍ이름
            //  dest_telno	 도착지 전화번호	 '-' 기호 제외
            //  dest_dept	 도착지 부서명
            //  dest_charge	 도착지 담당
            //  dest_sido	 도착지 시ㆍ도	 필수항목
            //  dest_gugun	 도착지 구ㆍ군	 필수항목
            //  dest_dong	 도착지 동명	 필수항목
            //  dest_location	 도착지 상세주소
            //  dest_lon	 도착지 경도 좌표
            //  dest_lat	 도착지 위도 좌표
            //  kind	 배송수단(1:오토, 2:다마스, 5:라보, 4:밴, 6:지하철, 3:트럭)	 필수항목
            //  kind_etc	 배송수단 옵션 항목(차랑 톤수 선택)	 기타코드 API 참조
            //  pay_gbn	 지급방법(1:선불, 2:착불, 3:신용, 4:송금, 5:수금)	 필수항목
            //  doc	 배송방법(1:편도, 3:왕복, 5:경유)	 필수항목
            //  sfast	 배송선택(1:일반, 3:급송, 5:조조, 7:야간)	 필수항목
            //  item_type	 물품종류(1:서류봉투, 2:소박스, 3:중박스, 4:대박스)	 필수항목
            //  memo	 전달내용
            //  sms_telno	 SMS로 전달 받을 핸드폰번호	 '-' 기호 제외
            //  use_check	 예약시간 사용여부(체크시:3)
            //  pickup_date	 예약일(오늘 기준 최대 한달까지 선택가능)
            //  pick_hour	 예약일(시간단위)
            //  pick_min	 예약일(분단위)
            //  pick_sec	 예약일(초단위)
            //  price	 처리비용(정수형입력)
            //  s_c_code	 출발지 거래처코드
            //  d_c_code	 도착지 거래처코드
            //  type	 처리결과 구분(XMLㆍJSON)	 기본값 : xml


            // var_dump($this->addArr([
            //     'user_id' => $info['userId'],
            //     'c_name' => $info['orderName'],
            //     'c_mobile' => $info['orderMobile'],

            //     's_start' => $info['startName'],
            //     'start_telno' => $info['startTelNo'],
            //     'start_sido' => $info['startSido'],
            //     'start_gugun' => $info['startGugun'],
            //     'start_dong' => $info['startDong'],
            //     'start_location' => $info['startLocate'],

            //     's_dest' => $info['destName'],
            //     'dest_telno' => $info['destTelNo'],
            //     'dest_sido' => $info['destSido'],
            //     'dest_gugun' => $info['destGugun'],
            //     'dest_dong' => $info['destDong'],
            //     'dest_location' => $info['destLocal'],




            //     'kind' => $info['deliverType'],     //  kind	 배송수단(1:오토, 2:다마스, 5:라보, 4:밴, 6:지하철, 3:트럭)	 필수항목
            //     'pay_gbn' => $info['payType'],      //지급방법(1:선불, 2:착불, 3:신용, 4:송금, 5:수금, 6:카드)	 필수항목
            //     'doc' => $info['doc'],              //  doc	 배송방법(1:편도, 3:왕복, 5:경유)	 필수항목
            //     'sfast' => $info['sfast'],          //  sfast	 배송선택(1:일반, 3:급송, 5:조조, 7:야간)	 필수항목
            //     'item_type' => '1',



            //     'price' => $info['price'],
            //     'start_lon' => $info['start_lon'],
            //     'start_lat' => $info['start_lat'],
            //     'dest_lon' => $info['dest_lon'],
            //     'dest_lat' => $info['dest_lat'],


            //     'memo' => "",//$info['memo'],

            // ]));


            return $this->req('get', $this->baseUri . 'order_regist', $this->addArr([
                'user_id' => $info['userId'],
                'c_name' => $info['orderName'],
                'c_mobile' => $info['orderMobile'],

                's_start' => $info['startName'],
                'start_telno' => $info['startTelNo'],
                'start_sido' => $info['startSido'],
                'start_gugun' => $info['startGugun'],
                'start_dong' => $info['startDong'],
                'start_location' => $info['startLocate'],

                's_dest' => $info['destName'],
                'dest_telno' => $info['destTelNo'],
                'dest_sido' => $info['destSido'],
                'dest_gugun' => $info['destGugun'],
                'dest_dong' => $info['destDong'],
                'dest_location' => $info['destLocal'],




                'kind' => $info['deliverType'],     //  kind	 배송수단(1:오토, 2:다마스, 5:라보, 4:밴, 6:지하철, 3:트럭)	 필수항목
                'pay_gbn' => $info['payType'],      //지급방법(1:선불, 2:착불, 3:신용, 4:송금, 5:수금, 6:카드)	 필수항목
                'doc' => $info['doc'],              //  doc	 배송방법(1:편도, 3:왕복, 5:경유)	 필수항목
                'sfast' => $info['sfast'],          //  sfast	 배송선택(1:일반, 3:급송, 5:조조, 7:야간)	 필수항목
                'item_type' => '1',



                'price' => $info['price'],
                'start_lon' => $info['start_lon'],
                'start_lat' => $info['start_lat'],
                'dest_lon' => $info['dest_lon'],
                'dest_lat' => $info['dest_lat'],


                'memo' => $info['memo'],

            ]));
        }

        public function isNotExists(array $info)
        {
            /**
             * 아이디 중복 여부
             *
             * @example
             * $this->load->library('Deliver');
             * $this->deliver->isExists([
             *     'userId' => '가입할 유저 아이디',
             *     'token'  => 'oAuth발급받은 토큰',
             * ])
             */
            return $this->req('get', $this->baseUri . 'member_exist', $this->addArr([
                // 'token' => $info['token'],
                'user_id' => $info['userId'],
            ]))['message'][0]->code === '1000';
        }


        public function getOrderDetail(array $info)
        {
            /**
             * 주문정보 상세조회
             * @example
             * $this->load->library('Deliver');
             * $this->deliver->getOrderDetail([
             *     'userId' => '가입한 유저 아이디',
             *     'serial' => '오더 번호',
             * ]);
             */
            return $this->req('get', $this->baseUri . 'order_detail', $this->addArr([
                'user_id' => $info['userId'],
                'serial' => $info['serial'],
            ]));
        }

        public function cancel(array $info)
        {
            /**
             * 주문 취소 처리
             *
             * @example
             * $this->load->library('Deliver');
             * $this->deliver->isExists([
             *     'userId' => '가입한 유저 아이디',
             *     'serial' => '오더 번호',
             * ]);
             */
            return  $this->req('get', $this->baseUri . 'order_cancel', $this->addArr([
                'user_id' => $info['userId'],
                'serial' => $info['serial'],
            ]));
            //array(2) { ["stat"]=> bool(true) ["message"]=> array(1) { [0]=> object(stdClass)#53 (2) { ["code"]=> string(4) "1005" ["msg"]=> string(27) "RESULT:CURRENT-CANCEL-ORDER" } } }
                // array(2) { ["stat"]=> bool(true) ["message"]=> array(1) { [0]=> object(stdClass)#53 (2) { ["code"]=> string(4) "1004" ["msg"]=> string(27) "RESULT:SERIAL_NUMBER-FAILED" } } }

                //네트워크에 일시적으로 오류가 있습니다. 다시 시도 부탁드립니다.
                //return $result;
        }

        public function checkOrderSign(array $info)
        {
            /**
             * 오더고객사인
             *
             * @example
             * $this->load->library('Deliver');
             * $this->deliver->isExists([
             *     'userId' => '가입한 유저 아이디',
             *     'serial' => '오더 번호',
             * ]);
             */
            return $this->req('get', $this->baseUri . 'order_sign', $this->addArr([
                'user_id' => $info['userId'],
                'serial' => $info['serial'],
            ]));
        }


        public function getStart2EndCharge(array $info)
        {
            /**
             * 요금조회
             * @example
             * this->load->library('Deliver');
             * $this->deliver->getStart2EndCharge([
             *     'userId' => '가입한 유저 아이디',
             *     'kind'  => '배송수단', (1(오토) / 2(다마스) / 5(라보) / 4(밴) / 6(지하철) / 3(트럭)),
             *     'doc' => '배송방법' , (1(편도) / 3(왕복) / 5(경유)),
             *     'payType' => '지급방법' , (1.편도 / 3.왕복 / 5.경유),
             *     'startSi' => '출발지 시 도',
             *     'startGun' => '출발지 군구'
             *     'startDong' => '출벌지 동'
             *
             *     'endSi' => '도착지 시 도'
             *     'endGun' => '도착지 군구'
             *     'endDong' => '도착지 동',
             * ]);
             *
             */
            return $this->req('get', $this->baseUri . 'cost_search', $this->addArr([
                'user_id' => $info['userId'],
                'kind' => $info['kind'],
                'doc' => $info['doc'], // 퍈도 여부
                'pay_type' => $info['payType'],  // 결제 방법
                's_sido' => $info['startSi'],
                's_gugun' => $info['startGun'],
                's_dong' => $info['startDong'],

                'e_sido' => $info['endSi'],
                'e_gugun' => $info['endGun'],
                'e_dong' => $info['endDong'],
            ]));
        }





        public function map_axis_code(array $info)
        {
            /**
             * 주소기준 좌표 조회
             * @example
             * this->load->library('Deliver');
             * $this->deliver->getStart2EndCharge([
             *     'userId' => '가입한 유저 아이디',
             *     'kind'  => '배송수단', (1(오토) / 2(다마스) / 5(라보) / 4(밴) / 6(지하철) / 3(트럭)),
             *     'doc' => '배송방법' , (1(편도) / 3(왕복) / 5(경유)),
             *     'payType' => '지급방법' , (1.편도 / 3.왕복 / 5.경유),
             *     'startSi' => '출발지 시 도',
             *     'startGun' => '출발지 군구'
             *     'startDong' => '출벌지 동'
             *
             *     'endSi' => '도착지 시 도'
             *     'endGun' => '도착지 군구'
             *     'endDong' => '도착지 동',
             * ]);
             *
             *
             *
             *
             */



            $str = $this->randStr();

            return $this->req('get', $this->baseUri . 'map_axis_code', $this->addArr([

                'sido' => $info['sido'],
                'gugun' => $info['gugun'],
                'dong' => $info['dong'],


                'm_code' => getenv('M_CODE'),
                'cc_code' => getenv('CC_CODE'),
                // 'token' => $str.getenv('AUTH_TOKEN'),
                'type' => 'json',





            ]));
        }

        private function addArr(array $info): array
        {
            $token = $this->auth();
            $this->baseArr['token'] = $token['token'];
            return array_merge($this->baseArr, $info);
        }



        public function getDetail(array $info)
        {
            /**
             * 회원정보 조회
             *
             * @example
             * $this->load->library('Deliver');
             * $this->deliver->isExists([
             *     'userId' => '가입한 유저 아이디',
             *     'token'  => 'oAuth발급받은 토큰',
             * ])
             *
             */
            // return $this->req('get', $this->baseUri . 'member_detail', $this->addArr([
            //     // 'token' => $info['token'],
            //     'user_id' => $info['userId'],
            // ]))['message'][1] ?? null;
            return $this->req('get', $this->baseUri . 'member_detail', $this->addArr([
                // 'token' => $info['token'],
                'user_id' => $info['userId'],
            ]));
        }

        public function edit(array $info)
        {
            /**
             *
             * 회원정보 수정
             *
             * @example
             * $this->load->library('Deliver');
             * $this->deliver->isExists([
             *     'userId' => '가입할 유저 아이디',
             *     'token'  => 'oAuth발급받은 토큰',
             *     'password'  => '비밀번호',
             *     'passwordConfirm'  => '비밀번호 일치 여부',
             *     'customerName'  => '유저 이름',
             *     'dongName'  => '동 이름(독산동)',
             *     'telNo'  => '가입자 전화번호',
             * ]);
             *
             */
            return $this->req('get', $this->baseUri . 'member_modify', $this->addArr([
                // 'token' => $info['token'],
                'user_id' => $info['userId'],
                'password' => $info['password'],
                'password_confirm' => $info['passwordConfirm'],
                'cust_name' => $info['customerName'],
                'dong_name' => $info['dongName'],
                'tel_no' => str_replace('-', '', $info['telNo']),
            ]))['message'];
        }

        public function getOrderList(array $info)
        {
            return $this->req('get', $this->baseUri . 'order_list', $this->addArr([
                'user_id' => $info['userId'],
            ]));
        }


        public function register(array $info)
        {
            /**
             *
             * 회원등록
             *
             * @example
             * $this->load->library('Deliver');
             * $this->deliver->isExists([
             *     'userId' => '가입할 유저 아이디',
             *     'token'  => 'oAuth발급받은 토큰',
             *     'password'  => '비밀번호',
             *     'passwordConfirm'  => '비밀번호 일치 여부',
             *     'customerName'  => '유저 이름',
             *     'dongName'  => '동 이름(독산동)',
             *     'telNo'  => '가입자 전화번호',
             * ]);
             *
             */
            return $this->req('get', $this->baseUri . 'member_regist', $this->addArr([
                // 'token' => $info['token'],
                'user_id' => $info['userId'],
                'password' => $info['password'],
                'password_confirm' => $info['passwordConfirm'],
                'cust_name' => $info['customerName'],
                'dong_name' => $info['dongName'],
                'tel_no' => str_replace('-', '', $info['telNo']),
            ]))['message'];
        }

        public function getZipCodeByDongName(array $info)
        {
            // $str = $this->randStr();
            // $base = [
            //     'm_code'  => $str.getenv('M_CODE'),
            //     'cc_code' => $str.getenv('CC_CODE'),
            //     'type' => 'json',
            //     'page' => 1,
            //     'limit' => 5,
            // ];

            $reqRlt = $this->req('get', $this->baseUri . 'zipcode', $this->addArr([
                // 'dong_name' => $info['dongName'],
                'user_id' => $info['userId']
            ]))['message'];

            return $reqRlt;
            // return $reqRlt;
        }

        public function login(array $info)
        {
            // return $this->addArr([
            //     'user_id' => $info['userId'],
            //     'password' => $info['userPw'],
            // ]);
            return $this->req('get', $this->baseUri . 'login', $this->addArr([
                'user_id' => $info['userId'],
                'password' => $info['userPw'],
            ]));
        }
        public function auth()
        {
            $str = $this->randStr();
            $reqRlt = $this->req('get', $this->baseUri . 'oauth', [
                'm_code' => getenv('M_CODE'),
                'cc_code' => getenv('CC_CODE'),
                'ukey' => $str . getenv('AUTH_TOKEN'),
                'akey' => md5($str . getenv('AUTH_TOKEN')),
                'type' => 'json',
            ])['message'][0];

            if ($reqRlt->code === '1000') {
                return [
                    'stat' => true,
                    'token' => $reqRlt->token,
                ];
            }
            return [
                'stat' => false
            ];
        }

        private function randStr(): string
        {
            $pattern = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            return substr(str_shuffle($pattern), 0, 8);
            // return substr(str_shuffle($pattern), 0, 6);
        }

        private function req(string $method, string $url, array $param): array
        {
            // var_dump($param);
            // echo "<br><br><br><br><br><br>";
            return strtolower($method) === 'get' ? (new Client())->requestAsync('GET', $url, ['query' => $param])
                                                    ->then(
                                                        function (ResponseInterface $resp) {
                                                            try {
                                                                return [
                                                                    'stat' => true,
                                                                    'message' => json_decode($resp->getBody()->getContents()),
                                                                ];
                                                            } catch (Exception $e) {
                                                                return [
                                                                    'stat' => true,
                                                                    'message' => ($resp->getBody()->getContents())
                                                                ];
                                                            }
                                                        },
                                                        function (BadResponse $err) {
                                                            return [
                                                                'stat' => false,
                                                                'message' => $err->getResponse()->getBody()->getContents()
                                                            ];
                                                        }
                                                    )
                                                    ->wait() :
                                       (new Client())->requestAsync('POST', $url, [
                                                        'form_params' => $param
                                                     ])
                                                     ->then(
                                                         function (ResponseInterface $resp) {
                                                             try {
                                                                 return [
                                                                    'stat' => true,
                                                                    'message' => json_decode($resp->getBody()->getContents()),
                                                                ];
                                                             } catch (Exception $e) {
                                                                 return [
                                                                    'stat' => true,
                                                                    'message' => ($resp->getBody()->getContents())
                                                                ];
                                                             }
                                                         },
                                                         function (BadResponse $err) {
                                                             return [
                                                                'stat' => false,
                                                                'message' => $err->getResponse()->getBody()->getContents()
                                                            ];
                                                         }
                                                     )
                                                     ->wait();
        }
    }
}
