<?php

use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Client;

if (! class_exists('NaverGeo')) {
    class NaverGeo
    {
        private $id = null;
        private $password = null;
        public function __construct()
        {
            $this->id = getenv('NAVER_API_ID');
            $this->password = getenv('NAVER_API_PASSWORD');

            if (!($this->id && $this->password)) {
                throw new InvalidArgumentException('Apikey or Api Password is missing', 1);
            }
        }
        public function localeToXY(string $query): ?array
        {
            $url = 'https://naveropenapi.apigw.ntruss.com/map-geocode/v2/geocode';
            $reqRlt = $this->req('get', $url, [
                'query' => $query,
            ]);
            ['message'][0];

            // var_dump($reqRlt['message']);

            // exit();
            // log_message("error", "Error Message". $reqRlt['message']->meta->count);


            if ($reqRlt['message']->meta->count == 0) {
                $url = 'https://naveropenapi.apigw.ntruss.com/map-geocode/v2/geocode';
                $reqRlt = $this->req('get', $url, [
                    'query' => explode(' ', $query)[0] . ' ' . explode(' ', $query)[1],
                ]);
                ['message'][0];
                $reqRlt = $reqRlt['message']->addresses[0] ?? null;
                if ($reqRlt) {
                    return [

                        'si' => $reqRlt->addressElements[0]->shortName,
                        'gun' => $reqRlt->addressElements[1]->shortName,
                        'dong' => $reqRlt->addressElements[2]->shortName,
                        'address' => $reqRlt->jibunAddress,
                        'x' => $reqRlt->x,
                        'y' => $reqRlt->y
                    ];
                }
            } else {
                $reqRlt = $reqRlt['message']->addresses[0] ?? null;
                // var_dump($reqRlt->addressElements[1]->shortName);
                if ($reqRlt) {
                    return [

                        'si' => $reqRlt->addressElements[0]->shortName,
                        'gun' => $reqRlt->addressElements[1]->shortName,
                        'dong' => $reqRlt->addressElements[2]->shortName,
                        'address' => $reqRlt->jibunAddress,
                        'x' => $reqRlt->x,
                        'y' => $reqRlt->y
                    ];
                }
            }
            return null;
        }
        public function getDistanceFromLocale(string $start, string $end)
        {
            $url = 'https://naveropenapi.apigw.ntruss.com/map-direction/v1/driving';
            $startLocale = $this->localeToXY($start);
            $endLocale = $this->localeToXY($end);

            // var_dump($start);
            // echo '<br>';
            // var_dump($startLocale);
            // echo '<br>';
            // var_dump($end);
            // echo '<br>';
            // var_dump($endLocale);
            // echo '<br>';


            $startLocale_x = $startLocale['x'] ?? null;
            $endLocale_x = $endLocale['x'] ?? null;
            if ($startLocale_x && $endLocale_x) {
                $rlt_direction = $this->req('get', $url, [
                    'start' => $startLocale['x'] . ',' . $startLocale['y'],
                    'goal' => $endLocale['x'] . ',' . $endLocale['y'],
                ]);
                if ($rlt_direction) {
                    $rlt_all = [
                        'start' => $startLocale,
                        'dest' => $endLocale,
                        'distance' => $rlt_direction['message']->route->traoptimal[0]->summary->distance,
                        'duration' => $rlt_direction['message']->route->traoptimal[0]->summary->duration,
                    ];
                    // var_dump($rlt_all);
                    return $rlt_all;
                }
                return null;
            }
            return null;
        }
        private function req(string $method, string $url, array $param): array
        {
            return strtolower($method) === 'get' ? (new Client([
                'headers' => [
                    'X-NCP-APIGW-API-KEY-ID' => $this->id,
                    'X-NCP-APIGW-API-KEY' => $this->password,
                ],
            ]))->requestAsync('GET', $url . '?' . http_build_query($param))
                                                    ->then(
                                                        function (ResponseInterface $resp) {
                                                            try {
                                                                return [
                                                                    'stat' => true,
                                                                    'message' => json_decode($resp->getBody()->getContents()),
                                                                ];
                                                            } catch (Exception $e) {
                                                                return [
                                                                    'stat' => true,
                                                                    'message' => json_decode($resp->getBody()->getContents())
                                                                ];
                                                            }
                                                        },
                                                        function ($err) {
                                                            return [
                                                                'stat' => false,
                                                                'message' => $err->getResponse()->getBody()->getContents()
                                                            ];
                                                        }
                                                    )
                                                    ->wait() :
                                       (new Client())->requestAsync('POST', $url, [
                                                        'form_params' => $param
                                                     ])
                                                     ->then(
                                                         function (ResponseInterface $resp) {
                                                             try {
                                                                 return [
                                                                    'stat' => true,
                                                                    'message' => json_decode($resp->getBody()->getContents()),
                                                                ];
                                                             } catch (Exception $e) {
                                                                 return [
                                                                    'stat' => true,
                                                                    'message' => json_decode($resp->getBody()->getContents())
                                                                ];
                                                             }
                                                         },
                                                         function ($err) {
                                                             return [
                                                                'stat' => false,
                                                                'message' => $err->getResponse()->getBody()->getContents()
                                                            ];
                                                         }
                                                     )
                                                     ->wait();
        }
    }
}
