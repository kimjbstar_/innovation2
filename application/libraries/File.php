<?php

if (! class_exists('File')) {
    class File
    {
        /**
         * 파일 여부 채크,
         * url등 헬퍼 클래스 입니다.
         *
         * @copyright  2020 DBAD
         * @version    Release: @2.0.0@
         * @since      2.0.0
         */
        private $CI = null;
        private $config = null;
        private $basePath = null;
        public function __construct()
        {
            $this->CI =& get_instance();
            $this->basePath = '';
            $this->config = $this->CI->config;
        }
        public function getDefaultAssets(): array
        {
            return [
                'css' => [
                    'bootstrap' => $this->getCssUrl('lib/bootstrap.min.css'),
                    'fontAwesome' => $this->getCssUrl('lib/fontawesome.css'),
                    'main' => $this->getCssUrl('signUp/style.css'),
                ],
                'js' => [
                    'jquery' => $this->getJsUrl('lib/jquery.js'),
                    'popper' => $this->getJsUrl('lib/popper.min.js'),
                    'bootstrap' => $this->getJsUrl('lib/bootstrap.min.js'),
                    'swal' => $this->getJsUrl('lib/swal.js'),
                    'util' => $this->getJsUrl('lib/util.js'),
                    'main' => $this->getJsUrl('signUp/main.js'),
                ]
            ];
        }
        public function checkView(string $head, string $body, string $foot): bool
        {
            /**
             * 뷰 파일이 있는지를(헤드, 바디, 푸터) 존재 여부를
             * 체크하는 메소드 입니다.
             *
             * @param   string $head 헤드 경로
             * @param   string $body 바디 경로
             * @param   string $foot 푸터 경로
             *
             * @throws  throwClass throw 타입 설명
             * @used    \사용된\클래스\메소드
             * @author  dhtmdgkr123
             * @version 2.0.0
             * @return bool 파일 존재 여부
             */
            $ext = '.php';
            return file_exists(VIEWPATH . $head . $ext) && file_exists(VIEWPATH . $body . $ext) && file_exists(VIEWPATH . $foot . $ext);
        }
        private function buildUrl(string $path): string
        {
            /**
             * 절대경로에 해당하는
             * 파일에 맞게
             * 스태틱 url을 빌드하는 메소드 입니다
             *
             * @param   string $path 파일의 경로
             * @throws  Exception 경로가 올바르지 않을때
             * @used    \사용된\클래스\메소드
             * @author  dhtmdgkr123
             * @version 2.0.0
             * @return string 경로에 헤당하는 static url
             */
            if ($realPah = realpath(FCPATH . $path)) {
                return $this->config->site_url($path . $this->getLastModify($realPah));
            } else {
                throw new \Exception("path is invalid or file not found path : " . FCPATH . $path, 1);
            }
        }
        public function getCssUrl(string $cssPath): string
        {
            return $this->buildUrl($this->basePath . 'css/' . $cssPath);
        }
        public function getJsUrl(string $jsPath): string
        {
            return $this->buildUrl($this->basePath . 'js/' . $jsPath);
        }
        public function getImgUrl(string $imgPath): string
        {
            return $this->buildUrl($this->basePath . 'images/' . $imgPath);
        }
        public function getIconUrl(string $imgPath): string
        {
            return $this->buildUrl($this->basePath . 'icons/' . $imgPath);
        }
        private function getLastModify(string $path): string
        {
            return '?lastModify=' . md5(filemtime($path));
        }
        public function getAssetsUrl(string $imgPath): string
        {
            return $this->buildUrl($this->basePath . 'assets/' . $imgPath);
        }
    }
}
