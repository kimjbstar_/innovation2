<?php

defined('BASEPATH') or exit('No direct script access allowed');
$lang['userNumberIsNotRequire'] = '예약 인원을 입력 바랍니다.';
$lang['userNumberIsNotNumeric'] = '예약 인원은 숫자만 입력 가능합니다.';

$lang['reserveStartIsNotRequire'] = '스튜디오 사용 시작시간을 입력 바랍니다.';
$lang['reserveEndIsNotRequire'] = '스튜디오 사용 종료시간을 입력 바랍니다.';
$lang['timeError'] = '예약 시간을 확인하시기 바랍니다.';

$lang['success'] = '예약 성공했습니다.';
$lang['tryAgain'] = '다시 시도 바랍니다.';
$lang['alreadyReserve'] = '이미 예약 되어있습니다.';
$lang['sendSMSmessage'] = '%s(%s)님이 예약 했습니다 %s 로 접속하여 확인바랍니다.';

$lang['notExistsReserveUser'] = '예약건이 존재하지 않습니다.';
$lang['infoNotFound'] = '누락된 내용을 확인 바랍니다.';
$lang['complete'] = '처리 완료 됬습니다.';

$lang['reserveSMS'] = '예약 확정|%s 스튜디오에 예약 되었습니다.';
