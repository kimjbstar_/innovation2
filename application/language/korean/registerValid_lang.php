<?php

defined('BASEPATH') or exit('No direct script access allowed');
$lang['usernameIsNotRequire'] = '아이디를 입력 바랍니다.';
$lang['usernameIsNotUnique'] = '이미 존재하는 아이디 입니다.';
$lang['usernameIsNotMin'] = '아이디는 최소 6자리 이상 10자리 이하 입니다';
$lang['usernameIsNotMax'] = '아이디는 최소 6자리 이상 10자리 이하 입니다';

$lang['emailIsNotRequire'] = '이메일을 입력 바랍니다.';
$lang['emailIsNotemail'] = '올바르지 않은 이메일 형식 입니다.';
$lang['emailIsNotUnique'] = '이미 존재하는 이메일 입니다.';

$lang['phoneIsNotRequire'] = '전화번호를 입력 바랍니다.';
$lang['phoneIsNotNumeric'] = '전화번호는 숫자만 입력 가능합니다.';

$lang['passwordIsNotRequire'] = '비밀번호를 입력 바랍니다.';
$lang['passwordIsNotMin'] = '비밀번호는 최소 6자리 이상 10자리 이하 입니다';
$lang['passwordIsNotMax'] = '비밀번호는 최소 6자리 이상 10자리 이하 입니다';

$lang['passwordConfirmIsNotRequire'] = '비밀번호 재입력란을 입력 바랍니다.';
$lang['passwordConfirmIsNotSame'] = '비밀번호와 확인 비밀번호가 일치하지 않습니다.';
$lang['passwordConfirmIsNotMin'] = '비밀번호는 최소 6자리 이상 10자리 이하 입니다';
$lang['passwordConfirmIsNotMax'] = '비밀번호는 최소 6자리 이상 10자리 이하 입니다';

$lang['tryAgain'] = '다시 시도바랍니다.';

$lang['misMatch'] = '아이디 또는 비밀번호가 일치하지 않습니다.';
