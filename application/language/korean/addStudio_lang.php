<?php

$lang['studioNameIsNotRequire'] = '스튜디오 이름을 입력 바랍니다.';

$lang['chargePhoneIsNotRequire'] = '스튜디오 대표 번호를 입력 바랍니다.';
$lang['chargePhoneIsNotNumeric'] = '스튜디오 대표 번호는 숫자만 입력 가능합니다.';

$lang['explainIsNotRequire'] = '스튜디오 설명을 입력 바랍니다.';
$lang['enterpriseCodeIsNotRequire'] = '사업자 번호를 입력 바랍니다.';
$lang['addressCodeIsNotRequire'] = '주소를 입력 바랍니다.';

$lang['enterpriseCodeIsNotNumeric'] = '사업자 번호는 숫자만 입력 가능합니다.';

$lang['success'] = '스튜디오를 성공적으로 등록하였습니다.';
$lang['tryAgain'] = '다시 시도 바랍니다.';
