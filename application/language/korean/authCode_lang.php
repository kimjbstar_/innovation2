<?php

defined('BASEPATH') or exit('No direct script access allowed');
$lang['authSMSTitle'] = '회원가입 인증코드';
$lang['authSMSMessage'] = '회원가입 인증코드는 ';
$lang['authSMSMessageEnd'] = ' 입니다.';
$lang['invalidPhone'] = '핸드폰 번호를 입력하시지 않으셨거나 회원가입시 입력한 번호와 다릅니다.';
$lang['tryAgain'] = '다시 시도 바랍니다.';
$lang['authCodeNotMatch'] = '인증번호가 일치하지 않습니다.';
