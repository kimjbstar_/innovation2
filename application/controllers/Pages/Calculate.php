<?php

if (! class_exists('Calculate')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Calculate extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->load->model('Calculatemodel');
            $this->load->view(
                'm_common/head',
                [

                'static' => [
                    'assets' =>loadManagerCommonAssets(),

                    'url'=>loadCommonUrl()
                ]
            ]
            );
        }
        public function index(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/auth/login')) {
                $this->load->view('pages/calculate', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                    'data' => [
                        'calculate' => $this->Calculatemodel->get_calculate(),
                    ]
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
    }
}
