<?php

if (! class_exists('Advanced_ui')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Advanced_ui extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->load->view(
                'm_common/head',
                [

                'static' => [
                    'assets' =>loadManagerCommonAssets(),

                    'url'=>loadCommonUrl()
                ]
            ]
            );
        }
        public function cropper(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/advanced_ui/cropper')) {
                $this->load->view('pages/advanced_ui/cropper', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function owl_carousel(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/advanced_ui/owl_carousel')) {
                $this->load->view('pages/advanced_ui/owl_carousel', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function sweet_alert(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/advanced_ui/sweet_alert')) {
                $this->load->view('pages/advanced_ui/sweet_alert', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
    }
}
