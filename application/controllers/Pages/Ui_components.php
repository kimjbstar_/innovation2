<?php

if (! class_exists('Ui_components')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Ui_components extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->load->view(
                'm_common/head',
                [

                'static' => [
                    'assets' =>loadManagerCommonAssets(),

                    'url'=>loadCommonUrl()
                ]
            ]
            );
        }
        public function alerts(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/ui_components/alerts')) {
                $this->load->view('pages/ui_components/alerts', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function badges(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/ui_components/badges')) {
                $this->load->view('pages/ui_components/badges', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function breadcrumbs(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/ui_components/breadcrumbs')) {
                $this->load->view('pages/ui_components/breadcrumbs', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function button_group(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/ui_components/button_group')) {
                $this->load->view('pages/ui_components/button_group', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function buttons(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/ui_components/buttons')) {
                $this->load->view('pages/ui_components/buttons', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function cards(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/ui_components/cards')) {
                $this->load->view('pages/ui_components/cards', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function carousel(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/ui_components/carousel')) {
                $this->load->view('pages/ui_components/carousel', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function collapse(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/ui_components/collapse')) {
                $this->load->view('pages/ui_components/collapse', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function dropdowns(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/ui_components/dropdowns')) {
                $this->load->view('pages/ui_components/dropdowns', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function list_group(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/ui_components/list_group')) {
                $this->load->view('pages/ui_components/list_group', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function media_object(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/ui_components/media_object')) {
                $this->load->view('pages/ui_components/media_object', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function modal(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/ui_components/modal')) {
                $this->load->view('pages/ui_components/modal', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function navbar(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/ui_components/navbar')) {
                $this->load->view('pages/ui_components/navbar', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function navs(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/ui_components/navs')) {
                $this->load->view('pages/ui_components/navs', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function pagination(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/ui_components/pagination')) {
                $this->load->view('pages/ui_components/pagination', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function popover(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/ui_components/popover')) {
                $this->load->view('pages/ui_components/popover', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function progress(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/ui_components/progress')) {
                $this->load->view('pages/ui_components/progress', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function scrollbar(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/ui_components/scrollbar')) {
                $this->load->view('pages/ui_components/scrollbar', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function scrollspy(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/ui_components/scrollspy')) {
                $this->load->view('pages/ui_components/scrollspy', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function spinners(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/ui_components/spinners')) {
                $this->load->view('pages/ui_components/spinners', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function tooltips(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/ui_components/tooltips')) {
                $this->load->view('pages/ui_components/tooltips', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
    }
}
