<?php

if (! class_exists('Manage_setting')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Manage_setting extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->load->model('History/Money');
            $this->load->model('common/Account');
            $this->load->view(
                'm_common/head',
                [

                'static' => [
                    'assets' =>loadManagerCommonAssets(),

                    'url'=>loadCommonUrl()
                ]
            ]
            );
        }
        public function index(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/tables/data_table')) {
                $this->load->view('pages/manage_setting', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                    'data' => $this->Account->getSettingValues(),
                    'action' => [
                        'logout' => $this->config->site_url('Index/logout'),
                        'get_setting_value' => $this->config->site_url('Pages/Manage_setting/get_setting_value'),
                        'updateSettingValue' => $this->config->site_url('Pages/Manage_setting/updateSettingValue'),
                    ]
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }





        public function get_setting_value(): void
        {
            $own_index=trim($this->input->get('indexnumber', true));
            $this->load->model('common/Account');
            $history=$this->Account->getSpecificSettingValue((int)$own_index);
            // var_dump($history);

            $ans_text="";

            $value=$history[0];

            $ans_text.="<tr>";
            $ans_text.="<td>";
            $ans_text.=$value['value_description'];
            $ans_text.="</td>";
            $ans_text.="<td><input type='number' step='0.0001' name='setting_value' value='";
            $ans_text.=$value['setting_value'];
            $ans_text.="'></td>";
            $ans_text.="<input type='hidden'  name='setting_index' value='";
            $ans_text.=$value['setting_index'];
            $ans_text.="'>";
            $ans_text.="</tr>";


            // $ans_text.='<tr>';
            //     $ans_text.='<td>'.$value['value_description'].'</td>';
            //     $ans_text.='<td><input type="number" step="0.0001" name="setting_value" value="'.$value['setting_value'].'"></td>';
            //     $ans_text.='<input type="hidden"  name="setting_index" value="'.$value['setting_index'].'">';
            // $ans_text.='</tr>'   ;

            echo '{"answer":"' . $ans_text . '"}';
            exit();
        }




        public function updateSettingValue(): void
        {
            $setting_value=trim($this->input->post('setting_value', true));
            $setting_index=trim($this->input->post('setting_index', true));
            $this->load->model('common/Account');
            $history=$this->Account->updateSettingValue($setting_value, $setting_index);
            // var_dump($history);
            goBackAlert('값 설정 완료.', $this->config->site_url('Pages/Manage_setting'));
        }
    }
}
