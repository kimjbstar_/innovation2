<?php

if (! class_exists('Auth')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Auth extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->load->view(
                'm_common/head_without_menu',
                [

                'static' => [
                    'assets' =>loadManagerCommonAssets(),

                    'url'=>loadCommonUrl()
                ]
            ]
            );
        }
        public function login(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/auth/login')) {
                $this->load->view('pages/auth/login', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function register(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/auth/register')) {
                $this->load->view('pages/auth/register', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
    }
}
