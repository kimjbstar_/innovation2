<?php

if (! class_exists('Gifticon_setting')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Gifticon_setting extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->load->model('Order/Ordermodel');
            $this->load->view(
                'm_common/head',
                [

                'static' => [
                    'assets' =>loadManagerCommonAssets(),

                    'url'=>loadCommonUrl()
                ]
            ]
            );
        }

        public function save(): void
        {
            $condition = $this->input->get("c");

            $data['status'] = true;
            $this->Ordermodel->update_gifticon_condition($condition);
            header('Content-type: application/json');
            echo json_encode($data);
        }

        public function index(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/auth/login')) {
                $this->load->view('pages/gifticon_setting', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                    'data' => [
                        'check' => $this->Ordermodel->check_gifticon_send()
                    ]
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
    }
}
