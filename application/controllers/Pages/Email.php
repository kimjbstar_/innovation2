<?php

if (! class_exists('Email')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Email extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->load->view(
                'm_common/head',
                [

                'static' => [
                    'assets' =>loadManagerCommonAssets(),

                    'url'=>loadCommonUrl()
                ]
            ]
            );
        }
        public function inbox(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/email/inbox')) {
                $this->load->view('pages/email/inbox', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function read(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/email/read')) {
                $this->load->view('pages/email/read', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function compose(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/email/compose')) {
                $this->load->view('pages/email/compose', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
    }
}
