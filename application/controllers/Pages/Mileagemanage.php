<?php

if (! class_exists('Mileagemanage')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Mileagemanage extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->load->model('Mileage');
            $this->load->view(
                'm_common/head',
                [
                'static' => [
                    'assets' =>loadManagerCommonAssets(),
                    'url'=>loadCommonUrl()
                ]
            ]
            );
        }
        public function index(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/auth/login')) {
                $this->load->view('pages/mileage', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                    'data' => [
                        'mileage' => $this->Mileage->get_mileage_history()
                    ]
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
    }
}
