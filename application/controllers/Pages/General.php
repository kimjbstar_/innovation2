<?php

if (! class_exists('General')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class General extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->load->view(
                'm_common/head',
                [

                'static' => [
                    'assets' =>loadManagerCommonAssets(),

                    'url'=>loadCommonUrl()
                ]
            ]
            );
        }
        public function blank_page(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/general/blank_page')) {
                $this->load->view('pages/general/blank_page', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function faq(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/general/faq')) {
                $this->load->view('pages/general/faq', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function invoice(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/general/invoice')) {
                $this->load->view('pages/general/invoice', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function profile(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/general/profile')) {
                $this->load->view('pages/general/profile', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function pricing(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/general/pricing')) {
                $this->load->view('pages/general/pricing', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function timeline(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/general/timeline')) {
                $this->load->view('pages/general/timeline', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
    }
}
