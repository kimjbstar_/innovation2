<?php

if (! class_exists('Charts')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Charts extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->load->view(
                'm_common/head',
                [

                'static' => [
                    'assets' =>loadManagerCommonAssets(),

                    'url'=>loadCommonUrl()
                ]
            ]
            );
        }
        public function apex(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/charts/apex')) {
                $this->load->view('pages/charts/apex', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function chartjs(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/charts/chartjs')) {
                $this->load->view('pages/charts/chartjs', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function flot(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/charts/flot')) {
                $this->load->view('pages/charts/flot', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function morrisjs(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/charts/morrisjs')) {
                $this->load->view('pages/charts/morrisjs', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function peity(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/charts/peity')) {
                $this->load->view('pages/charts/peity', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function sparkline(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/charts/sparkline')) {
                $this->load->view('pages/charts/sparkline', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
    }
}
