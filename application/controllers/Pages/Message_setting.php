<?php

if (! class_exists('Message_setting')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Message_setting extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->load->model('Order/Ordermodel');
            $this->load->view(
                'm_common/head',
                [

                'static' => [
                    'assets' =>loadManagerCommonAssets(),

                    'url'=>loadCommonUrl()
                ]
            ]
            );
        }


        public function save(): void
        {
            //9, 12 15 18 21
            $s9 = $this->input->get("s9");
            $s12 = $this->input->get("s12");
            $s15 = $this->input->get("s15");
            $s18 = $this->input->get("s18");
            $s21 = $this->input->get("s21");

            $data['status'] = true;
            $this->Ordermodel->update_message_condition($s9, $s12, $s15, $s18, $s21);
            header('Content-type: application/json');
            echo json_encode($data);
        }


        public function index(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/auth/login')) {
                $this->load->view('pages/message_setting', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                    'data' => [
                        'condition' => $this->Ordermodel->get_message_condition()
                    ]
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
    }
}
