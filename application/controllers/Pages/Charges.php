<?php

if (! class_exists('Charges')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Charges extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->load->model('Chargesmodel');
            $this->load->view(
                'm_common/head',
                [

                'static' => [
                    'assets' =>loadManagerCommonAssets(),

                    'url'=>loadCommonUrl()
                ]
            ]
            );
        }


        private function getChargsByDate(string $startDate = null, string $endDate = null)
        {
            if (!($startDate && $endDate)) {
                $startDate = date('Y-m-d', strtotime(trim(
                    $startDate ?: date('Y-m-d')
                )));
                $endDate = date('Y-m-d', strtotime(trim(
                    $endDate ?: date('Y-m-d', strtotime('+1 days'))
                )));
            } else {
                $startDate = date('Y-m-d', strtotime(trim($startDate)));
                $endDate = date('Y-m-d', strtotime(trim($endDate)));
            }
            return $this->Chargesmodel->get_charges_all($startDate, $endDate);
        }

        public function index(): void
        {
            $start = trim($this->input->get('start', true));
            $end = trim($this->input->get('end', true));
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/auth/login')) {
                $this->load->view('pages/charges', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                    'data' => [
                        'all' => $this->getChargsByDate($start, $end), //$this->Chargesmodel->get_charges_all($start, $end),
                        // 'charge' => $this->Chargesmodel->get_charge(),
                        // 'message' => $this->Chargesmodel->get_charges_message(),
                        // 'gifticon' => $this->Chargesmodel->get_charges_gifticon()
                    ]
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
    }
}
