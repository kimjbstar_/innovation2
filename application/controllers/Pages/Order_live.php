<?php

if (! class_exists('Order_live')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Order_live extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->load->model('Order/Ordermodel');
            $this->load->view(
                'm_common/head',
                [

                'static' => [
                    'assets' =>loadManagerCommonAssets(),

                    'url'=>loadCommonUrl()
                ]
            ]
            );
        }



        public function cancel_order(): void
        {
            $this->load->library('Deliver');
            // var_dump(trim($this->input->get('userid', true)));
            // var_dump($this->Ordermodel->get_usercheck(trim($this->input->get('userid', true))));
            $cancel_result = $this->deliver->cancel([
                'userId' => trim($this->input->get('userid', true)),
                'serial' => trim($this->input->get('serial', true)),
            ]);
            // var_dump($cancel_result);

            //array(2) { ["stat"]=> bool(true) ["message"]=> array(1) { [0]=> object(stdClass)#53 (2) { ["code"]=> string(4) "1005" ["msg"]=> string(27) "RESULT:CURRENT-CANCEL-ORDER" } } }
            // array(2) { ["stat"]=> bool(true) ["message"]=> array(1) { [0]=> object(stdClass)#53 (2) { ["code"]=> string(4) "1004" ["msg"]=> string(27) "RESULT:SERIAL_NUMBER-FAILED" } } }

            // database update
            if ($cancel_result["message"][0]->code) {
                $this->Ordermodel->update_order_cancel(trim($this->input->get('userid', true)), trim($this->input->get('serial', true)));
            }
            echo "<br><script>location.href='/Pages/order_live';</script>";

            // goBackAlert( '주문을 취소했습니다.', $this->config->site_url('Pages/Order_live'));
        }

        private function getLiveOrder(string $startDate = null, string $endDate = null)
        {
            return $this->Ordermodel->getLiveOrder($startDate, $endDate);
        }

        public function index(): void
        {
            /**
             * 주문 취소 처리
             *
             * @example
             * $this->load->library('Deliver');
             * $this->deliver->isExists([
             *     'userId' => '가입한 유저 아이디',
             *     'serial' => '오더 번호',
             * ]);
             */

            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/tables/data_table')) {
                $this->load->view('pages/order_live', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                    'query' => [
                        'start' => trim($this->input->get('start', true)),
                        'end' => trim($this->input->get('end', true)),
                    ],
                    'data' => $this->getLiveOrder(
                        date('Y-m-d'),
                        date('Y-m-d', strtotime('+1 days'))
                    ),

                    'action' => [
                        'logout' => $this->config->site_url('Index/logout'),
                        'member_modify_detail' => $this->config->site_url('Pages/customer/member_modify_detail'),
                        'setRandomPassword' => $this->config->site_url('Pages/customer/setRandomPassword'),
                        'get_transaction' => $this->config->site_url('Pages/customer/get_transaction'),
                        'get_detail' => $this->config->site_url('Pages/customer/get_detail'),
                        // 'setStartLevel' => $this->config->site_url('Pages/customer/setStartLevel'),
                        // 'setLevelUp' => $this->config->site_url('Pages/customer/setLevelUp'),
                    ]
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }






        public function member_modify_detail(): void
        {
            $indexnumber=trim($this->input->get('indexnumber', true));
            $member_id=trim($this->input->get('member_id', true));
            $member_name=trim($this->input->get('member_name', true));
            $member_mail=trim($this->input->get('member_mail', true));

            $this->load->model('History/Money');

            $this->Money->member_modify($indexnumber, $member_id, $member_name, $member_mail);

            goBackAlert('변경 완료', $this->config->site_url('Pages/Customer'));
        }




        public function get_transaction(): void
        {
            $own_index=trim($this->input->get('indexnumber', true));
            $this->load->model('History/Money');
            $history=$this->Money->getTransactionHistoryByToIdx((int)$own_index);
            // var_dump($history);

            $ans_text="";
            foreach ($history as $value) {
                $ans_text.="<tr>";
                $ans_text.="<td>";
                $ans_text.=$value['transaction_hash'];
                $ans_text.="</td>";
                $ans_text.="<td>";
                $ans_text.=getIdByUserIdx((int)$value['to_idx'])->raw;
                $ans_text.="</td>";
                $ans_text.="<td>";
                $ans_text.=$value['transaction_amount'];
                $ans_text.="</td>";
                $ans_text.="<td>";
                $ans_text.=$value['description'];
                $ans_text.="</td>";
                $ans_text.="</tr>";
            }

            echo '{"answer":"' . $ans_text . '"}';
            exit();
        }

        public function get_detail(): void
        {
            $own_index=trim($this->input->get('indexnumber', true));
            $this->load->model('History/Money');
            $history=$this->Money->getMemberByMemberIdx((int)$own_index);
            // var_dump($history);

            $ans_text="";

            foreach ($history as $value) {
                $ans_text.="<tr>";
                $ans_text.="<th>ID</th>";
                $ans_text.="<td>" . $value['member_id'] . "</td>";
                $ans_text.="</tr>";
                $ans_text.="<tr>";
                $ans_text.="<th>Name</th>";
                $ans_text.="<td><input type=text  id='set_detail_member_name' value='" . $value['member_name'] . "'><input type=hidden  id='set_detail_member_index' value='" . $value['member_index'] . "'></td>";
                $ans_text.="</tr>";
                $ans_text.="<tr>";
                $ans_text.="<th>e-mail</th>";
                $ans_text.="<td><input type=text  id='set_detail_member_mail' value='" . $value['member_mail'] . "'></td>";
                $ans_text.="</tr>";
            }

            echo '{"answer":"' . $ans_text . '"}';
            exit();
        }



        public function setStartLevel(): void
        {
            $start_level=trim($this->input->post('start_level', true));
            $start_sales=trim($this->input->post('start_sales', true));
            $member_index=trim($this->input->post('member_index', true));
            $this->load->model('common/Account');
            $history=$this->Account->setStartLevel($start_level, $start_sales, $member_index);
            // var_dump($history);
            goBackAlert('시작 설정 완료.', $this->config->site_url('Pages/Customer'));
        }








        public function setRandomPassword(): void
        {
            $member_index=trim($this->input->post('member_index', true));
            $this->load->model('common/Account');
            $randomPass=$this->Account->setRandomPassword($member_index);
            // var_dump($history);
            goBackAlert('비밀번호를 [' . $randomPass . '] 로 설정하였습니다..', $this->config->site_url('Pages/Customer'));
        }

        public function setLevelUp(): void
        {
            $member_index=trim($this->input->post('member_index', true));
            $this->load->model('common/Account');
            $history=$this->Account->setLevelUp($member_index);
            // var_dump($history);
            goBackAlert('레벨업 완료.', $this->config->site_url('Pages/Customer'));
        }
    }
}
