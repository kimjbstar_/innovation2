<?php
if(! class_exists('Error')) {
	defined('BASEPATH') or exit('No direct script access allowed');
	class Error extends CI_Controller
	{
		public function __construct()
		{
            parent::__construct();    
            $this->load->helper('assetsLoader');  
            $this->load->view('m_common/head', [

                'static' => [
                    'assets' =>loadManagerCommonAssets(),
                    
                    'url'=>loadCommonUrl()  
                ]
            ]
            );      
		}
		public function 404() : void 
		{
			if ($this->file->checkView('m_common/head','m_common/footer', 'pages/error/404')) {

               
				$this->load->view('pages/error/404', [
					'url' => [
						// 'charge' => $this->config->site_url('Charge/step1'),
					],
				]);
				$this->load->view('m_common/footer', [
					'static' => [
						'assets' => loadFooterAssets(),
						'js' => [
							// 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
						]
					]
				]);
			} else {
				show_404();
			}
		}
		public function 500() : void 
		{
			if ($this->file->checkView('m_common/head','m_common/footer', 'pages/error/500')) {

               
				$this->load->view('pages/error/500', [
					'url' => [
						// 'charge' => $this->config->site_url('Charge/step1'),
					],
				]);
				$this->load->view('m_common/footer', [
					'static' => [
						'assets' => loadFooterAssets(),
						'js' => [
							// 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
						]
					]
				]);
			} else {
				show_404();
			}
		}
	}
}