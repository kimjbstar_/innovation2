<?php

if (! class_exists('Icons')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Icons extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->load->view(
                'm_common/head',
                [

                'static' => [
                    'assets' =>loadManagerCommonAssets(),

                    'url'=>loadCommonUrl()
                ]
            ]
            );
        }
        public function feather_icons(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/icons/feather_icons')) {
                $this->load->view('pages/icons/feather_icons', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function flag_icons(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/icons/flag_icons')) {
                $this->load->view('pages/icons/flag_icons', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function mdi_icons(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/icons/mdi_icons')) {
                $this->load->view('pages/icons/mdi_icons', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
    }
}
