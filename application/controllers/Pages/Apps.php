<?php

if (! class_exists('Apps')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Apps extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
        }
        public function chat(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/apps/chat')) {
                $this->load->view('pages/apps/chat', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function calendar(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/apps/calendar')) {
                $this->load->view(
                    'm_common/head',
                    [

                    'static' => [
                        'assets' =>loadManagerCommonAssets(),

                        'url'=>loadCommonUrl()
                    ]
                ]
                );


                $this->load->view('pages/apps/calendar', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $loadAssets = loadManagerCommonAssets();
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadManagerCommonAssets(),

                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
    }
}
