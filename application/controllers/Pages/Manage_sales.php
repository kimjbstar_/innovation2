<?php

if (! class_exists('Manage_sales')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Manage_sales extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->load->model('History/Money');
            $this->load->view(
                'm_common/head',
                [

                'static' => [
                    'assets' =>loadManagerCommonAssets(),

                    'url'=>loadCommonUrl()
                ]
            ]
            );
        }
        public function index(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/tables/data_table')) {
                $this->load->view('pages/manage_sales', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                    'data' => $this->Money->getAllOwnInfoHistory(),
                    'action' => [
                        'logout' => $this->config->site_url('Index/logout'),
                        'get_transaction' => $this->config->site_url('Pages/Manage_sales/get_transaction'),
                    ]
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }





        public function get_transaction(): void
        {
            $own_index=trim($this->input->get('indexnumber', true));
            $this->load->model('History/Money');
            $history=$this->Money->getTransactionHistoryByOwnIndex((int)$own_index);
            // var_dump($history);

            $ans_text="";
            foreach ($history as $value) {
                $ans_text.="<tr>";
                $ans_text.="<td>";
                $ans_text.=$value['transaction_hash'];
                $ans_text.="</td>";
                $ans_text.="<td>";
                $ans_text.=getIdByUserIdx((int)$value['to_idx'])->raw;
                $ans_text.="</td>";
                $ans_text.="<td>";
                $ans_text.=$value['transaction_amount'];
                $ans_text.="</td>";
                $ans_text.="<td>";
                $ans_text.=$value['description'];
                $ans_text.="</td>";
                $ans_text.="</tr>";
            }

            echo '{"answer":"' . $ans_text . '"}';
            exit();
        }
    }
}
