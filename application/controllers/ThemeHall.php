<?php

if (! class_exists('ThemeHall')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class ThemeHall extends CI_Controller
    {
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
        }



        public function index(): void
        {




            if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                $this->load->view('Seminar', [
                ]);
                $this->load->view('PC_attach', [
                ]);
                $this->load->view('ThemeHall/index', [
                    ]);                
            } else {
                show_404();
            }




        }






        public function FairRecruit(): void
        {

            // $this->load->view('ThemeHall/FairRecruit', [
            //     ]);


            //     $this->load->view('ThemeHall/ThemeAfter', [
            //     ]);


            $mobilechk = '/(iPod|iPhone|Android|BlackBerry|SymbianOS|SCH-M\d+|Opera Mini|Windows CE|Nokia|SonyEricsson|webOS|PalmOS)/i'; 

            // 모바일 접속인지 PC로 접속했는지 체크합니다.
            if(preg_match($mobilechk, $_SERVER['HTTP_USER_AGENT'])) {



                if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                    $this->load->view('ThemeHall/Mobile/Employ', [
                    ]);
                    $this->load->view('Mobile/Menu2', [
                    ]);
                } else {
                    show_404();
                }
            } else { 

                if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {

                    $this->load->view('ThemeHall/FairRecruit', [
                        ]);
        
        
                        $this->load->view('ThemeHall/ThemeAfter', [
                        ]);
                } else {
                    show_404();
                }
            }




        }

        public function ActiveAdministrationOffice(): void
        {


            // $this->load->view('ThemeHall/ActiveAdministrationOffice', [
            //     ]);

            //     $this->load->view('ThemeHall/ThemeAfter', [
            //     ]);       

            $mobilechk = '/(iPod|iPhone|Android|BlackBerry|SymbianOS|SCH-M\d+|Opera Mini|Windows CE|Nokia|SonyEricsson|webOS|PalmOS)/i'; 

            // 모바일 접속인지 PC로 접속했는지 체크합니다.
            if(preg_match($mobilechk, $_SERVER['HTTP_USER_AGENT'])) {



                if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                    $this->load->view('ThemeHall/Mobile/Positive', [
                    ]);
                    $this->load->view('Mobile/Menu2', [
                    ]);
                } else {
                    show_404();
                }
            } else { 

                if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {

                    $this->load->view('ThemeHall/ActiveAdministrationOffice', [
                        ]);
        
                        $this->load->view('ThemeHall/ThemeAfter', [
                        ]);       
                } else {
                    show_404();
                }
            }



        }

        public function FutureRecruitInnovation(): void
        {


            // $this->load->view('ThemeHall/Innovation', [
            //     ]);

            //     $this->load->view('ThemeHall/ThemeAfter', [
            //     ]);      


            $mobilechk = '/(iPod|iPhone|Android|BlackBerry|SymbianOS|SCH-M\d+|Opera Mini|Windows CE|Nokia|SonyEricsson|webOS|PalmOS)/i'; 

            // 모바일 접속인지 PC로 접속했는지 체크합니다.
            if(preg_match($mobilechk, $_SERVER['HTTP_USER_AGENT'])) {



                if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                    $this->load->view('ThemeHall/Mobile/Innovation', [
                    ]);
                    $this->load->view('Mobile/Menu2', [
                    ]);
                } else {
                    show_404();
                }
            } else { 

                if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {

                    $this->load->view('ThemeHall/Innovation', [
                        ]);
        
                        $this->load->view('ThemeHall/ThemeAfter', [
                        ]);              
                } else {
                    show_404();
                }
            }



        }





        public function Prevent(): void
        {


            // $this->load->view('ThemeHall/Prevent', []);       

            // $this->load->view('ThemeHall/ThemeAfter', [
            // ]);          


            $mobilechk = '/(iPod|iPhone|Android|BlackBerry|SymbianOS|SCH-M\d+|Opera Mini|Windows CE|Nokia|SonyEricsson|webOS|PalmOS)/i'; 

            // 모바일 접속인지 PC로 접속했는지 체크합니다.
            if(preg_match($mobilechk, $_SERVER['HTTP_USER_AGENT'])) {



                if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                    $this->load->view('ThemeHall/Mobile/Prevent', [
                    ]);
                    $this->load->view('Mobile/Menu2', [
                    ]);
                } else {
                    show_404();
                }
            } else { 

                if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {

                    $this->load->view('ThemeHall/Prevent', []);       

                    $this->load->view('ThemeHall/ThemeAfter', [
                    ]);          
                } else {
                    show_404();
                }
            }





        }

        public function Personnel(): void
        {


            // $this->load->view('ThemeHall/Balanced', []);  

            // $this->load->view('ThemeHall/ThemeAfter', [
            // ]);              

            $mobilechk = '/(iPod|iPhone|Android|BlackBerry|SymbianOS|SCH-M\d+|Opera Mini|Windows CE|Nokia|SonyEricsson|webOS|PalmOS)/i'; 

            // 모바일 접속인지 PC로 접속했는지 체크합니다.
            if(preg_match($mobilechk, $_SERVER['HTTP_USER_AGENT'])) {



                if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                    $this->load->view('ThemeHall/Mobile/Personnel', [
                    ]);
                    $this->load->view('Mobile/Menu2', [
                    ]);
                } else {
                    show_404();
                }
            } else { 

                if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {

                    $this->load->view('ThemeHall/Balanced', []);  

                    $this->load->view('ThemeHall/ThemeAfter', [
                    ]);              
                } else {
                    show_404();
                }
            }




        }


        public function Recruitment(): void
        {



            // $this->load->view('ThemeHall/Recruitment', []);   

            // $this->load->view('ThemeHall/ThemeAfter', [
            // ]);            

            $mobilechk = '/(iPod|iPhone|Android|BlackBerry|SymbianOS|SCH-M\d+|Opera Mini|Windows CE|Nokia|SonyEricsson|webOS|PalmOS)/i'; 

            // 모바일 접속인지 PC로 접속했는지 체크합니다.
            if(preg_match($mobilechk, $_SERVER['HTTP_USER_AGENT'])) {



                if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                    $this->load->view('ThemeHall/Mobile/Recruitment', [
                    ]);
                    $this->load->view('Mobile/Menu2', [
                    ]);
                } else {
                    show_404();
                }
            } else { 

                if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {

                    $this->load->view('ThemeHall/Recruitment', []);   

                    $this->load->view('ThemeHall/ThemeAfter', [
                    ]);            
                } else {
                    show_404();
                }
            }



        }


        public function History(): void
        {



            // $this->load->view('ThemeHall/History', []);   

            // $this->load->view('ThemeHall/ThemeAfter', [
            // ]);             

            $mobilechk = '/(iPod|iPhone|Android|BlackBerry|SymbianOS|SCH-M\d+|Opera Mini|Windows CE|Nokia|SonyEricsson|webOS|PalmOS)/i'; 

            // 모바일 접속인지 PC로 접속했는지 체크합니다.
            if(preg_match($mobilechk, $_SERVER['HTTP_USER_AGENT'])) {



                if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                    $this->load->view('ThemeHall/Mobile/History', [
                    ]);
                    $this->load->view('Mobile/Menu2', [
                    ]);
                } else {
                    show_404();
                }
            } else { 

                if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {

                    $this->load->view('ThemeHall/History', []);   

                    $this->load->view('ThemeHall/ThemeAfter', [
                    ]);             
                } else {
                    show_404();
                }
            }



        }









    }
}
