<?php

if (! class_exists('Profile')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Profile extends CI_Controller
    {
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->baseAssets = $this->file->getDefaultAssets();
            $this->lang->load('registerValid', 'korean');
        }

        public function save(): void
        {
            $this->load->helper(['valid', 'data']);
            $data = trimArray($this->input->post([
                'phone', 'email', 'password', 'passwordConfirm'
            ], true));
            $valiRlt = (new ValidPost($data))->setRules([
                'phone' => 'require|numeric',
                'email' => 'require|email',
                'password' => 'require|min:6|max:10',
                'passwordConfirm' => 'require|same:password|min:6|max:10',
            ])->run();
            if (! $valiRlt['stat']) {
                alert($this->lang->line($valiRlt['message']), $this->config->site_url('Profile'));
                return;
            }
            $this->load->model('User/User');
            if (! $this->User->update($this->preProcess($data), $this->session->userIdx)) {
                alert($this->lang->line('tryAgain'), $this->config->site_url('Main'));
                return;
            }
            setSession([
                'userPhone' => $data['phone'],
                'userEmail' => $data['email'],
            ]);
            redirect($this->config->site_url('Main'), 'refresh');
        }
        private function preProcess(array $data): array
        {
            return [
                'member_email' => $data['email'],
                'member_phone' => $data['phone'],
                'member_password' => password_hash($data['password'], PASSWORD_BCRYPT),
            ];
        }
        public function index(): void
        {
            $file = $this->file;
            $session = $this->session;
            $load = $this->load;

            $load->view('common/head', [
                'css' => $this->baseAssets['css']
            ]);
            $load->view('Profile/body', [
                'data' => [
                    'userId' => $session->userId,
                    'userEmail' => $session->userEmail,
                    'userPhone' => $session->userPhone,
                    'csrf' => [
                        'name' => $this->security->get_csrf_token_name(),
                        'hash' => $this->security->get_csrf_hash()
                    ]
                ],
                'img' => [
                    'back' => $file->getIconUrl('back.svg'),
                    'menu' => $file->getIconUrl('menu.svg'),
                    'avatar' => $file->getIconUrl('avatar.svg'),
                    'phone' => $file->getIconUrl('phone.svg'),
                    'envelope' => $file->getIconUrl('envelope.svg'),
                    'eye' => $file->getIconUrl('eye.svg'),
                    'lock' => $file->getIconUrl('lock.svg'),
                    'close' => $file->getIconUrl('close.svg'),
                ],
                'url' => [
                    'back' => $this->config->site_url('Main'),
                    'save' => $this->config->site_url('Profile/Save'),
                ],
            ]);
            $load->view('common/foot', [
                'js' => $this->baseAssets['js'],
            ]);
        }
    }
}
