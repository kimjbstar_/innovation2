<?php

if (! class_exists('Orderdetail')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Orderdetail extends CI_Controller
    {
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->baseAssets = $this->file->getDefaultAssets();
            $this->load->model('Studio/Studio');
        }
        public function logout(): void
        {
            $this->load->helper('data');
            clearSession();
            redirect($this->config->base_url(), 'refresh');
        }
        public function getDistanceByPosition(): void
        {
            $this->load->model('Studio/Studio');
            $this->json->echo($this->Studio->getByDistance([
                'lat' => trim($this->input->get('lon', true)),
                'lon' => trim($this->input->get('lat', true)),
            ]));
        }







        public function order_detail_at_detail(string $orderSerial): array
        {


            // $orderSerial=trim($this->input->get('v', true));


            $this->load->model('Order/Ordermodel');
            $db_orderdetail = $this->Ordermodel->get_order($orderSerial);


            if (count($db_orderdetail)==0) {
                return [];
            }


            $userId=$db_orderdetail[0]['userId'];

            $this->load->library('Deliver');
            $detail_result = $this->deliver->getOrderDetail([
                'userId' => $userId,
                'serial' => $orderSerial,
            ]);



            // 저장된 것과 가져온 배송 상태 코드가 다르면 처리
            if ($db_orderdetail[0]['state'] != $detail_result['message'][5]->state) {
                // state 등록
                $this->Ordermodel->order_state_update($orderSerial, $detail_result['message'][5]->state);
                // 알림 보내야 하는 시점이라면 알림 발송
            }


            // 저장된 것과 가져온 배당 기사 코드가 다르면 처리
            if ($db_orderdetail[0]['rider_code_no'] != $detail_result['message'][2]->rider_code_no) {
                // rider 등록
                $this->Ordermodel->order_rider_update($orderSerial, $detail_result['message'][2]->rider_code_no, $detail_result['message'][2]->rider_name, $detail_result['message'][2]->rider_tel_number);
            }


            return $detail_result['message'];
        }











        public function index(): void
        {
            if ($query = $this->input->get('query', true)) {
                redirect($this->config->site_url('Studio/Check?query=' . $query), 'refresh');
                return;
            }

            $load = $this->load;
            $file = $this->file;
            $load->view('common/head', [
                'css' => $this->baseAssets['css'],
                'title' => trim($this->input->get('v', true)) . ' 주문 상세 내역',
            ]);

            $load->view('Orderdetail/body', [
                'img' => [
                    'back' => $file->getIconUrl('back.svg'),
                    'menu' => $file->getIconUrl('menu.svg'),
                    'mapPos' => $file->getIconUrl('map-position.svg'),
                ],
                'url' => [
                    'back' => $this->config->site_url('Main'),
                    'search' => $this->config->site_url('Studio/Search'),
                ],
                'data' => [
                    'userId' => $this->session->userId,
                    'userMail' => $this->session->userEmail,
                    'orderVal' => $this->order_detail_at_detail(trim($this->input->get('v', true)))
                ],

            ]);

            $load->view('common/foot', [

                'js' => array_merge($this->baseAssets['js'], [
                    'codeLiv' => $file->getJsUrl('lib/postcode.v2.js'),
                    'postCode' => $file->getJsUrl('postCode/code.js'),

                    'mainJs' => $file->getJsUrl('main/main.js'),
                    // 'googleMap' => 'https://maps.googleapis.com/maps/api/js?'.http_build_query([
                    //     'key' => getenv('GOOGLE_MAP_KEY'),
                    //     'libraries' => 'places',
                    //     'callback' => 'initMap',
                    // ]),
                ])
            ]);
        }
    }
}
