<?php

if (! class_exists('Gifticonitem')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Gifticonitem extends CI_Controller
    {

        /**
         * Gifticon API 용도 컨트롤러로 생성함.
         */
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->baseAssets = $this->file->getDefaultAssets();
            $this->lang->load('addStudio', 'korean');
            $this->load->model('User/User');
        }


        /**
         * 사용 확인절차 구현하기
         */
        public function index(): void
        {
            $file = $this->file;
            $load = $this->load;
            $load->view('common/head', [
                'css' => $this->baseAssets['css'],
                'title' => '기프티콘 확인',
            ]);

            echo $this->input->get("c");
            echo $this->input->get("e");








            // header('Content-Type:text/plain');
            $ch = curl_init();
            $postvars = '"id":"' . $this->input->get("c") . '"';
            $postvars = '{' . $postvars . '}';      //JSON 데이터

            $url = "https://tara2.tarapay.net/ApiCheckGifticon?id=" . $this->input->get("c");         //URL
            echo $url;
            //헤더정보
            $headers = ["cache-control: no-cache","content-type: application/json; charset=utf-8"];

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $response = curl_exec($ch);
            // echo $response;
            $res = json_decode($response);
            // var_dump($res);

            // echo var_dump($res->ok);
            // echo $res->ok;   //1  ok
            // exit();
            $return_api_data = "success";
            if ($res->ok == 1) {
                echo "success";
                $return_api_data = "success";
            } else {
                //$this->session->userId 에 맞는 coupon_id 값의 사용된걸로 돌리기
                $this->User->update_gifticon_status($this->input->get("c"));
                // redirect($this->config->site_url('Gifticonlist'), 'refresh');
                $return_api_data = "fail";
            }
            // exit();


            $load->view('Gifticonitem/body', [
                'img' => [
                    'back' => $file->getIconUrl('back.svg'),
                    'address' => $file->getIconUrl('marker.svg'),
                    'phone' => $file->getIconUrl('phone.svg'),
                    'menu' => $file->getIconUrl('menu.svg'),
                    'avater' => $file->getIconUrl('avatar-dark.svg'),
                    'register' => $file->getIconUrl('driver-registration-dark.svg'),
                    'mapPos' => $file->getIconUrl('map-position.svg'),
                ],
                'url' => [
                    'back' => $this->config->site_url('Main'),
                    // 'save' => $this->config->site_url('Studio/Add/register')
                ],
                'data' => [
                    'userId' => $this->session->userId,
                    'userMail' => $this->session->userEmail,
                    'res' => $return_api_data,
                    // 'departure'=>trim($this->input->get('departure', true)),
                    // 'destination'=>trim($this->input->get('destination', true)),
                    // 'orderedlist' => $orderedlist,
                    'csrf' => [
                        'name' => $this->security->get_csrf_token_name(),
                        'hash' => $this->security->get_csrf_hash()
                    ],
                ]
            ]);

            $load->view('common/foot', [
                'js' => array_merge($this->baseAssets['js'], [
                    'postCode' => $file->getJsUrl('lib/postcode.v2.js'),
                    'addAddress' => $file->getJsUrl('addStudio/main.js')
                ])
            ]);

            $this->session->set_userdata('thisuser', trim($this->input->get('thisuser', true)));
        }
    }
}
