<?php

if (! class_exists('M_dashboard')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class M_dashboard extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
        }
        public function index(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'm_dashboard/body')) {
                $this->load->view(
                    'm_common/head',
                    [
                    'static' => [
                        'assets' =>loadManagerCommonAssets(),

                        'url'=>loadCommonUrl()
                    ]
                ]
                );
                $this->load->view('m_dashboard/body', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
    }
}
