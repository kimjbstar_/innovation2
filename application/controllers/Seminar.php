<?php

if (! class_exists('Seminar')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Seminar extends CI_Controller
    {
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
        }



        public function index(): void
        {







            $mobilechk = '/(iPod|iPhone|Android|BlackBerry|SymbianOS|SCH-M\d+|Opera Mini|Windows CE|Nokia|SonyEricsson|webOS|PalmOS)/i'; 

            // 모바일 접속인지 PC로 접속했는지 체크합니다.
            if(preg_match($mobilechk, $_SERVER['HTTP_USER_AGENT'])) {



                if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                    $this->load->view('Mobile/Seminar', [
                    ]);
                    $this->load->view('Mobile/Menu', [
                    ]);
                } else {
                    show_404();
                }
            } else { 

                if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                    $this->load->view('Seminar', [
                    ]);
                    $this->load->view('PC_attach', [
                    ]);
                } else {
                    show_404();
                }
            }









        }






        public function pretest(): void
        {




            if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                $this->load->view('SeminarPre', [
                ]);
                $this->load->view('PC_attach', [
                ]);
            } else {
                show_404();
            }



            // $mobilechk = '/(iPod|iPhone|Android|BlackBerry|SymbianOS|SCH-M\d+|Opera Mini|Windows CE|Nokia|SonyEricsson|webOS|PalmOS)/i'; 

            // // 모바일 접속인지 PC로 접속했는지 체크합니다.
            // if(preg_match($mobilechk, $_SERVER['HTTP_USER_AGENT'])) {



            //     if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
            //         $this->load->view('Mobile/Seminar', [
            //         ]);
            //         $this->load->view('Mobile/Menu', [
            //         ]);
            //     } else {
            //         show_404();
            //     }
            // } else { 

            //     if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
            //         $this->load->view('Seminar', [
            //         ]);
            //         $this->load->view('PC_attach', [
            //         ]);
            //     } else {
            //         show_404();
            //     }
            // }









        }





        public function live(): void
        {





            if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                $this->load->view('SeminarLive', [
                ]);
                $this->load->view('Mobile/Menu', [
                ]);
            } else {
                show_404();
            }






        }



        public function ChatSend()
        {


            $data = trimArray($this->input->get([
                'message'
            ], true));


            $this->load->model('Dbuse');

            if ($data['message']!=''){
                $this->Dbuse->insertChat($data['message']);
            }
            // echo "{'result':'success'}";
            // print_r( $data);

        }






        public function ChatGet()
        {


            $data = trimArray($this->input->get([
                'messageIndex'
            ], true));


            $this->load->model('Dbuse');

            $list = $this->Dbuse->getChat($data['messageIndex']);
            

            // echo "{'result':'success'}";

            echo json_encode($list);

            // print_r( $list);

        }





        public function Insert(): void
        {


            $data = trimArray($this->input->post([
                'var1'
            ], true));
            // // var_dump($data);

            $this->load->model('Dbuse');



            if ($data['var1']==""){

                echo "<script>alert('신청하셨던 연락처를 적어주세요.');location.href='http://publicservicefair.kr/Seminar/pretest';</script>";
            }
            else if (count($this->Dbuse->getSpecificSeminarApplier($data['var1']))<1){
                print_r( count($this->Dbuse->getSpecificSeminarApplier($data['var1'])));
                
                echo "<script>alert('세미나를 신청후 입장하실 수 있습니다.');location.href='http://publicservicefair.kr/Seminar/pretest';</script>";
            }
            else{
                echo "<script>alert('세미나 시청하기로 이동합니다.');location.href='https://youtu.be/3SdGz594FW0';</script>";
            }


            // $this->Dbuse->insertSeminar($data['var1'], $data['var2'], $data['var3'], $data['var4'], $data['var5']);
            // echo "<script>alert('시작 시간이 아닙니다.');location.href='http://publicservicefair.kr/Seminar';</script>";
            return;

        }





        public function Apply(): void
        {


            $data = trimArray($this->input->post([
                'var1', 'var2', 'var3', 'var4', 'var5', 'admitted'
            ], true));
            // var_dump($data);

            if ($data['admitted']!="yyy") {
                echo "<script>alert('개인정보의 수집·이용에 동의하지 않았습니다.');location.href='http://publicservicefair.kr/Seminar';</script>";
                alert("개인정보의 수집·이용에 동의하지 않았습니다.", $this->config->site_url('Seminar'));
                return;
            }

            if ($data['var1']=="" || $data['var2']==""|| $data['var3']==""|| $data['var4']=="") {
                echo "<script>alert('성명, 연락처, 이메일, 구분은 필수 입력사항입니다.');location.href='http://publicservicefair.kr/Seminar';</script>";
                alert("성명, 연락처, 이메일, 구분은 필수 입력사항입니다.", $this->config->site_url('Seminar'));
                return;
            }


            $this->load->model('Dbuse');


            $this->Dbuse->insertSeminar($data['var1'], $data['var2'], $data['var3'], $data['var4'], $data['var5']);
            echo "<script>alert('등록하였습니다.');location.href='http://publicservicefair.kr/Seminar';</script>";
            alert("등록하였습니다.", $this->config->site_url('Seminar'));
            return;

        }



        public function MentoringApply(): void
        {


            $data = trimArray($this->input->post([
                'var1', 'var2', 'var3', 'var4', 'var5', 'var6', 'var7','admitted'
            ], true));
            // var_dump($data);

            if ($data['admitted']!="yyy") {
                echo "<script>alert('개인정보의 수집·이용에 동의하지 않았습니다.');location.href='http://publicservicefair.kr/PracticeTest/OnlineMentoring';</script>";
                alert("개인정보의 수집·이용에 동의하지 않았습니다.", $this->config->site_url('PracticeTest/OnlineMentoring'));
                return;
            }

            if ($data['var1']=="" || $data['var2']==""|| $data['var3']==""|| $data['var4']==""|| $data['var6']==""|| $data['var7']=="") {
                echo "<script>alert('성명, 연락처, 이메일, 희망분야, 날짜, 시간은 필수 입력사항입니다.');location.href='http://publicservicefair.kr/PracticeTest/OnlineMentoring';</script>";
                alert("성명, 연락처, 이메일, 희망분야, 날짜, 시간은 필수 입력사항입니다.", $this->config->site_url('PracticeTest/OnlineMentoring'));
                return;
            }


            $this->load->model('Dbuse');

            $apply_limit =2;
            if ($data['var4']=='행정9급' &&  $data['var6']=="12월 8일"){
                $apply_limit = 4;
            }
            else if ($data['var4']=='소방직' &&  $data['var6']=="12월 8일"){
                $apply_limit = 4;
            }
            else if ($data['var4']=='소방직' &&  $data['var6']=="12월 9일"){
                $apply_limit = 4;
            }
            else if ($data['var4']=='해양경찰' &&  $data['var6']=="12월 8일"){
                $apply_limit = 4;
            }
            else if ($data['var4']=='군무원' &&  $data['var6']=="12월 8일"){
                $apply_limit = 4;
            }
            else if ($data['var4']=='군무원' &&  $data['var6']=="12월 9일"){
                $apply_limit = 4;
            }




            if (count($this->Dbuse->getSpecificMentoring($data['var4'], $data['var6'], $data['var7']))>=$apply_limit){
                
                echo "<script>alert('해당 시간은 접수가 마감되었습니다.');location.href='http://publicservicefair.kr/PracticeTest/OnlineMentoring';</script>";
                alert("해당 시간은 접수가 마감되었습니다.", $this->config->site_url('PracticeTest/OnlineMentoring'));
                return;
            }
            else if(count($this->Dbuse->getPhoneNumberMentoring($data['var2'], $data['var4']))>0){

                echo "<script>alert('이미 신청하신 내역이 존재합니다.');location.href='http://publicservicefair.kr/PracticeTest/OnlineMentoring';</script>";
                alert("이미 신청하신 내역이 존재합니다.", $this->config->site_url('PracticeTest/OnlineMentoring'));
                return;
            }
            else if(count($this->Dbuse->getEmailMentoring($data['var3'], $data['var4']))>0){

                echo "<script>alert('이미 신청하신 내역이 존재합니다!');location.href='http://publicservicefair.kr/PracticeTest/OnlineMentoring';</script>";
                alert("이미 신청하신 내역이 존재합니다!", $this->config->site_url('PracticeTest/OnlineMentoring'));
                return;
            }
            else{   

                $this->Dbuse->insertMentoring($data['var1'], $data['var2'], $data['var3'], $data['var4'], $data['var5'], $data['var6'], $data['var7']);
                echo "<script>alert('등록하였습니다.');location.href='http://publicservicefair.kr/PracticeTest/OnlineMentoring';</script>";
                alert("등록하였습니다.", $this->config->site_url('PracticeTest/OnlineMentoring'));
                return;
            }








        }


        

        public function InterviewApply(): void
        {


            $data = trimArray($this->input->post([
                'var1', 'var2', 'var3', 'var4', 'var5', 'var6','admitted'
            ], true));
            // var_dump($data);

            


            if ($data['var5']=="11월 20일 (금)") {
                echo "<script>alert('11월 20일은 모두 마감되었습니다.');location.href='http://publicservicefair.kr/PracticeTest/OnlineInterview';</script>";
                return;
            }

            if ($data['admitted']!="yyy") {
                echo "<script>alert('개인정보의 수집·이용에 동의하지 않았습니다.');location.href='http://publicservicefair.kr/PracticeTest/OnlineInterview';</script>";
                alert("개인정보의 수집·이용에 동의하지 않았습니다.", $this->config->site_url('PracticeTest/OnlineInterview'));
                return;
            }

            if ($data['var1']=="" || $data['var2']==""|| $data['var3']==""|| $data['var5']==""|| $data['var6']=="") {
                echo "<script>alert('성명, 연락처, 이메일, 날짜, 시간은 필수 입력사항입니다.');location.href='http://publicservicefair.kr/PracticeTest/OnlineInterview';</script>";
                alert("성명, 연락처, 이메일, 날짜, 시간은 필수 입력사항입니다.", $this->config->site_url('PracticeTest/OnlineInterview'));
                return;
            }


            $this->load->model('Dbuse');



            if (count($this->Dbuse->getSpecificInterview($data['var2'], $data['var6']))>=20){
                
                echo "<script>alert('해당 시간은 접수가 마감되었습니다.');location.href='http://publicservicefair.kr/PracticeTest/OnlineInterview';</script>";
                alert("해당 시간은 접수가 마감되었습니다.", $this->config->site_url('PracticeTest/OnlineInterview'));
                return;
            }

            if (count($this->Dbuse->getSpecificInterview($data['var2'], $data['var6']))>=20){
                
                echo "<script>alert('해당 시간은 접수가 마감되었습니다.');location.href='http://publicservicefair.kr/PracticeTest/OnlineInterview';</script>";
                alert("해당 시간은 접수가 마감되었습니다.", $this->config->site_url('PracticeTest/OnlineInterview'));
                return;
            }
            else if (count($this->Dbuse->getPhoneNumberInterview($data['var2']))>0){
                
                echo "<script>alert('이미 신청하신 내역이 존재합니다.');location.href='http://publicservicefair.kr/PracticeTest/OnlineInterview';</script>";
                alert("이미 신청하신 내역이 존재합니다.", $this->config->site_url('PracticeTest/OnlineInterview'));
                return;
            }
            else if (count($this->Dbuse->getEmailInterview($data['var3']))>0){
                
                echo "<script>alert('이미 신청하신 내역이 존재합니다!');location.href='http://publicservicefair.kr/PracticeTest/OnlineInterview';</script>";
                alert("이미 신청하신 내역이 존재합니다!", $this->config->site_url('PracticeTest/OnlineInterview'));
                return;
            }
            else{   

                $this->Dbuse->insertInterview($data['var1'], $data['var2'], $data['var3'], $data['var4'], $data['var5'], $data['var6']);
                echo "<script>alert('등록하였습니다.');location.href='http://publicservicefair.kr/PracticeTest/OnlineInterview';</script>";
                alert("등록하였습니다.", $this->config->site_url('PracticeTest/OnlineInterview'));
                return;
            }




        }
    }
}
