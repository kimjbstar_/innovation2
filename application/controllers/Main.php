<?php

if (! class_exists('Main')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Main extends CI_Controller
    {
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->baseAssets = $this->file->getDefaultAssets();
            $this->load->model('Studio/Studio');
        }
        public function logout(): void
        {
            $this->load->helper('data');
            clearSession();
            redirect($this->config->site_url('check'), 'refresh');
        }
        // public function getDistanceByPosition(): void
        // {
        //     $this->load->model('Studio/Studio');
        //     $this->json->echo($this->Studio->getByDistance([
        //         'lat' => trim($this->input->get('lon', true)),
        //         'lon' => trim($this->input->get('lat', true)),
        //     ]));
        // }

        public function index(): void
        {
            if ($query = $this->input->get('query', true)) {
                redirect($this->config->site_url('Studio/Check?query=' . $query), 'refresh');
                return;
            }

            if (is_null($this->session->main_access_try)) {
                $this->session->set_userdata('main_access_try', 1);
            }
            if ($this->session->main_access_try==1 && trim($this->input->get('access', true))!='application') {
                $this->session->set_userdata('main_access_try', $this->session->main_access_try+1);
                redirect($this->config->site_url('Companyinfo'), 'refresh');
            }

            $this->session->set_userdata('main_access_try', $this->session->main_access_try+1);


            // if (trim($this->input->get('access', true))=='application'){
            //     redirect($this->config->site_url('check'), 'refresh');
            // }

            $load = $this->load;
            $file = $this->file;
            $load->view('common/head', [
                'css' => $this->baseAssets['css'],
                'title' => '퀵카 앱 주문',
            ]);

            $load->view('Main/body', [
                'img' => [
                    'back' => $file->getIconUrl('back.svg'),
                    'menu' => $file->getIconUrl('menu.svg'),
                    'mapPos' => $file->getIconUrl('map-position.svg'),
                ],
                'url' => [
                    'back' => $this->config->site_url('Main'),
                    'search' => $this->config->site_url('Studio/Search'),
                ],
                'data' => [
                    'userId' => $this->session->userId,
                    'userMail' => $this->session->userEmail,
                    'access'=>trim($this->input->get('access', true)),
                ],

            ]);

            $load->view('common/foot', [

                'js' => array_merge($this->baseAssets['js'], [
                    'codeLiv' => $file->getJsUrl('lib/postcode.v2.js'),
                    'postCode' => $file->getJsUrl('postCode/code.js'),

                    'mainJs' => $file->getJsUrl('main/main.js'),
                    'googleMap' => 'https://maps.googleapis.com/maps/api/js?' . http_build_query([
                        'key' => getenv('GOOGLE_MAP_KEY'),
                        'libraries' => 'places',
                        'callback' => 'initMap',
                    ]),
                ])
            ]);
        }
    }
}
