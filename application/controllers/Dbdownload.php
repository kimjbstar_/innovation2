<?php

if (! class_exists('Dbdownload')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Dbdownload extends CI_Controller
    {
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->load->view(
                'm_common/head',
                [

                'static' => [
                    'assets' =>loadManagerCommonAssets(),

                    'url'=>loadCommonUrl()
                ]
            ]
            );
        }



        public function index(): void
        {



            $this->load->model('Dbuse');
            $my_db = $this->Dbuse->getMyUsingDB($this->session->userIdx);
            // var_dump($my_db);

            if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/dbdownload')) {
                $this->load->view('Dblist/dbdownload', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                    'text'=> [
                        'menu_1' => "DB 다운로드",
                        'menu_2' => "추출된 DB 다운로드",
                        'menu_3' => "",
                    ],
                    'db' => $my_db
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }


        }
    }
}
