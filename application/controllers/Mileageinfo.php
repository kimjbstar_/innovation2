<?php

if (! class_exists('Mileageinfo')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Mileageinfo extends CI_Controller
    {
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->baseAssets = $this->file->getDefaultAssets();
            $this->lang->load('addStudio', 'korean');
            $this->load->model(['User/User', 'Mileage']);
            // $this->load->model('Mileage');

            $this->load->helper('mileage');
        }
        
        public function index(): void
        {
            $file = $this->file;
            $load = $this->load;
            $currentMileage = $this->Mileage->get_current_mileage($this->session->userId);
            $currentMileage[0]->useAbleMileage = calcUseableMileage($currentMileage[0]->sum_mileage);

            $load->view('common/head', [
                'css' => $this->baseAssets['css'],
                'title' => '마일리지 확인',
            ]);
            $load->view('Mileageinfo/body', [
                'img' => [
                    'back' => $file->getIconUrl('back.svg'),
                    'address' => $file->getIconUrl('marker.svg'),
                    'phone' => $file->getIconUrl('phone.svg'),
                    'menu' => $file->getIconUrl('menu.svg'),
                    'avater' => $file->getIconUrl('avatar-dark.svg'),
                    'register' => $file->getIconUrl('driver-registration-dark.svg'),
                    'mapPos' => $file->getIconUrl('map-position.svg'),
                ],
                'url' => [
                    'back' => $this->config->site_url('Main'),
                ],
                'data' => [
                    'userId' => $this->session->userId,
                    'userMail' => $this->session->userEmail,
                    'csrf' => [
                        'name' => $this->security->get_csrf_token_name(),
                        'hash' => $this->security->get_csrf_hash()
                    ],
                    'mileage' => $currentMileage,
                ]
            ]);

            $load->view('common/foot', [
                'js' => array_merge($this->baseAssets['js'], [
                    'postCode' => $file->getJsUrl('lib/postcode.v2.js'),
                    'addAddress' => $file->getJsUrl('addStudio/main.js')
                ])
            ]);

            $this->session->set_userdata('thisuser', trim($this->input->get('thisuser', true)));
        }
    }
}
