<?php

if (! class_exists('OpenLinks')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class OpenLinks extends CI_Controller
    {
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->load->view(
                'm_common/head',
                [

                'static' => [
                    'assets' =>loadManagerCommonAssets(),

                    'url'=>loadCommonUrl()
                ]
            ]
            );
        }



        public function index(): void
        {



            $this->load->model('Dbuse');
            $my_db = $this->Dbuse->getMyDB($this->session->userIdx);
            // var_dump($my_db);

            $servernum = $this->Dbuse->getServerNum()[0]['serverNum'];

            $validKeywordsCount = $this->Dbuse->getValidKeywordsCount()[0]['validCount'];

            if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                $this->load->view('Dblist/keywords', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                    'text'=> [
                        'menu_1' => "키워드 관리",
                        'menu_2' => "키워드 그룹",
                        'menu_3' => "",
                    ],
                    'db' => $my_db,
                    'serverNum' =>$servernum, 
                    'validKeywordsCount' =>$validKeywordsCount
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }


        }
    }
}
