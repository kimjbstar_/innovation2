<?php

if (! class_exists('UseDB')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class UseDB extends CI_Controller
    {
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
        }



        public function getSpecificDB(): void
        {
            $this->load->model('Dbuse');
            $specific_db = $this->Dbuse->getSpecificButton($_GET['dbidx'])[0];
            // var_dump($specific_db);

            $dbform = '


                <form action="Keywordcontrol/updateButtons" method="post">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">링크 수정</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body">

                            <input type="hidden" value="'.$this->security->get_csrf_hash().'" name="'.$this->security->get_csrf_token_name().'">
                            <input type="hidden" class="form-control" name="link_index"  value="'.$_GET['dbidx'].'">
                            <div class="form-group">
                                <label>페이지명</label>
                                <input type="text" class="form-control" name="groupname" readonly="0" value="'.$specific_db['pageName'].'">
                            </div>
                            <div class="form-group">
                                <label>링크명</label>
                                <input type="text" class="form-control" name="groupname" readonly="0" value="'.$specific_db['buttonName'].'">
                            </div>                            
                            <div class="form-group">
                                <label>링크 주소</label>
                                <input type="text" class="form-control" name="link_address"  value="'.$specific_db['linkAddress'].'">
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">저장</button>
                    </div>
                </form>



            ';


            echo ($dbform);
        }



        public function getNewDB(): void
        {
            // $this->load->model('Dbuse');
            // $specific_db = $this->Dbuse->getSpecificDB($_GET['dbidx'])[0];
            // // var_dump($specific_db);

            $dbform = '

                <form action="Keywordcontrol/createNewKeywords" method="post">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">키워드 그룹 생성</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body">
                        
                            <input type="hidden" value="'.$this->security->get_csrf_hash().'" name="'.$this->security->get_csrf_token_name().'">
                            <div class="form-group">
                                <label>그룹명</label>
                                <input type="text" name="groupname" class="form-control"  value="" placeholder="키워드그룹명">
                            </div>
                            <div class="form-group">
                                <label>키워드&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;&nbsp;,로 구분해주세요. 컴마 이후 띄어쓰기 없이 바로 다음 키워드 입력해주세요.&nbsp;&nbsp;)</label>
                                <textarea class="form-control " name="keywords" rows="5" placeholder="키워드1,키워드2,키워드3,키워드4,키워드5"></textarea>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">생성</button>
                    </div>
                </form>




            ';


            echo ($dbform);
        }


    }
}
