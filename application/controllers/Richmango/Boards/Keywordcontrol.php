<?php

if (! class_exists('Keywordcontrol')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Keywordcontrol extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->lang->load('registerValid', 'korean');
        }

        public function createNewKeywords(): void
        {
            // $this->load->helper(['valid', 'data']);
            // $this->load->model('User/User');
            $data = trimArray($this->input->post([
                'groupname', 'keywords'
            ], true));
            if ($data['groupname']=="" || $data['keywords']=="") {
                alert("모든 내용을 채워주세요.", $this->config->site_url('Keywords'));
                return;
            }
            $this->load->model('Dbuse');
            $servernum = $this->Dbuse->getServerNum()[0]['serverNum'];
            $validKeywordsCount = $this->Dbuse->getValidKeywordsCount()[0]['validCount'];
            if ($servernum*15<$validKeywordsCount+count(explode(',',$data['keywords']))){
                alert("등록하시려는 전체 유효키워드의 수가 서버 가용범위 이상입니다.", $this->config->site_url('Keywords'));
                return;
            }
            $this->Dbuse->createNewKeywordGroup($data['groupname'], $data['keywords'], count(explode(',',$data['keywords'])));
            alert("등록하였습니다.", $this->config->site_url('Keywords'));
            return;

        }





        public function createNewFaq(): void
        {
            // $this->load->helper(['valid', 'data']);
            // $this->load->model('User/User');
            $data = trimArray($this->input->post([
                'question', 'answer'
            ], true));
            if ($data['question']=="" || $data['answer']=="") {
                alert("모든 내용을 채워주세요.", $this->config->site_url('Richmango/Contents/Faqs'));
                return;
            }
            $this->load->model('Dbuse');


            $this->Dbuse->createNewFaq($data['question'], $data['answer']);

            alert("등록하였습니다.", $this->config->site_url('Richmango/Contents/Faqs'));
            return;

        }



        public function createNewNotice(): void
        {
            // $this->load->helper(['valid', 'data']);
            // $this->load->model('User/User');
            $data = trimArray($this->input->post([
                'question', 'answer'
            ], true));
            if ($data['question']=="" || $data['answer']=="") {
                alert("모든 내용을 채워주세요.", $this->config->site_url('Richmango/Boards/Notices'));
                return;
            }
            $this->load->model('Dbuse');


            $this->Dbuse->createNewBoard($data['question'], $data['answer'],'notices');

            alert("등록하였습니다.", $this->config->site_url('Richmango/Boards/Notices'));
            return;

        }




        public function createNewInformation(): void
        {
            // $this->load->helper(['valid', 'data']);
            // $this->load->model('User/User');
            $data = trimArray($this->input->post([
                'question', 'answer'
            ], true));
            if ($data['question']=="" || $data['answer']=="") {
                alert("모든 내용을 채워주세요.", $this->config->site_url('Richmango/Boards/Informations'));
                return;
            }
            $this->load->model('Dbuse');


            $this->Dbuse->createNewBoard($data['question'], $data['answer'],'informations');

            alert("등록하였습니다.", $this->config->site_url('Richmango/Boards/Informations'));
            return;

        }



        public function updateFaq(): void
        {
            // $this->load->helper(['valid', 'data']);
            // $this->load->model('User/User');
            $data = trimArray($this->input->post([
                'question', 'answer', 'link_index'
            ], true));
            $this->load->model('Dbuse');
            $this->Dbuse->updateFaq($data['question'], $data['link_index'], $data['answer']);
            alert("수정하였습니다.", $this->config->site_url('Richmango/Contents/Faqs'));
            return;

        }



        public function updateBoard(): void
        {
            // $this->load->helper(['valid', 'data']);
            // $this->load->model('User/User');
            $data = trimArray($this->input->post([
                'question', 'answer', 'link_index'
            ], true));
            $this->load->model('Dbuse');
            $this->Dbuse->updateBoard($data['question'], $data['link_index'], $data['answer']);
            alert("수정하였습니다.", $this->config->site_url('Richmango/Boards/Notices'));
            return;

        }




        public function updateButtons(): void
        {
            // $this->load->helper(['valid', 'data']);
            // $this->load->model('User/User');
            $data = trimArray($this->input->post([
                'link_address', 'link_index'
            ], true));
            $this->load->model('Dbuse');
            $this->Dbuse->updateButton($data['link_address'], $data['link_index']);
            alert("수정하였습니다.", $this->config->site_url('Richmango/Links/Buttons'));
            return;

        }




        public function updateKeywords(): void
        {
            // $this->load->helper(['valid', 'data']);
            // $this->load->model('User/User');
            $data = trimArray($this->input->post([
                'groupname', 'keywords', 'dbindex'
            ], true));
            if ($data['groupname']=="" || $data['keywords']=="") {
                alert("모든 내용을 채워주세요.", $this->config->site_url('Keywords'));
                return;
            }
            $this->load->model('Dbuse');
            $servernum = $this->Dbuse->getServerNum()[0]['serverNum'];
            $validKeywordsCount = $this->Dbuse->getValidKeywordsCountExcept($data['dbindex'])[0]['validCount'];
            if ($servernum*15<$validKeywordsCount+count(explode(',',$data['keywords']))){
                alert("등록하시려는 전체 유효키워드의 수가 서버 가용범위 이상입니다.", $this->config->site_url('Keywords'));
                return;
            }
            $this->Dbuse->updateKeywordGroup($data['groupname'], $data['keywords'], count(explode(',',$data['keywords'])), $data['dbindex']);
            alert("수정하였습니다.", $this->config->site_url('Keywords'));
            return;

        }

        public function turnOffLink(): void
        {
            $data = trimArray($this->input->get([
                'groupnum'
            ], true));
            $this->load->model('Dbuse');
            $servernum = $this->Dbuse->turnOffLink($data['groupnum']);

            alert("Off 하였습니다.", $this->config->site_url('Richmango/Links/Buttons'));
            return;

        }


        public function downloadGroupToday(): void
        {
            
            
            ini_set("display_errors", 0);



            $data = trimArray($this->input->get([
                'groupnum', 'groupname'
            ], true));
            
            $this->load->model('Dbuse');
            $getdata = $this->Dbuse->getTodayDB($data['groupnum']);
            // var_dump($getdata);
            if (count($getdata[0])>0){


                require_once $_SERVER["DOCUMENT_ROOT"]."/phpexcel/Classes/PHPExcel.php"; // 엑셀 라이브러리 import

                //set the desired name of the excel file
                $fileName = date("Ymd")."_".$data['groupname']."_오늘자";
            
                // Create new PHPExcel object
                $objPHPExcel = new PHPExcel();
            
                // Set document properties
                $objPHPExcel->getProperties()->setCreator("Me")->setLastModifiedBy("Me")->setTitle("My Excel Sheet")->setSubject("My Excel Sheet")->setDescription("Excel Sheet")->setKeywords("Excel Sheet")->setCategory("Me");
            
            
                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $objPHPExcel->setActiveSheetIndex(0);
            
            
                // Add column headers
                $objPHPExcel->getActiveSheet()
                            ->setCellValue('A1', '번호')
                            ->setCellValue('B1', '시간')
                            ->setCellValue('C1', '키워드')
                            ->setCellValue('D1', '전화번호');;
            
                $ii = 2;
            
    
    
                foreach ($getdata as $getd) {
                    $objPHPExcel->getActiveSheet()->setCellValue('A'.$ii, $ii-1);
                    $objPHPExcel->getActiveSheet()->setCellValue('B'.$ii, $getd['dbSearched']);
                    $objPHPExcel->getActiveSheet()->setCellValue('C'.$ii, $getd['dbKeyword']);
                    $objPHPExcel->getActiveSheet()->setCellValue('D'.$ii, $getd['dbPhonenumber']);
                    $ii +=1;
                }

                // Set worksheet title
                $objPHPExcel->getActiveSheet()->setTitle($fileName);
            
                //save the file to the server (Excel2007)
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            
                $objWriter->save($_SERVER["DOCUMENT_ROOT"].'/temp/' . $fileName . '.xlsx'); // 저장될 파일 위치
            
            
                // 파일 다운로드 구현
                function mb_basename($path) { return end(explode('/',$path)); }
                function utf2euc($str) { return iconv("UTF-8","cp949//IGNORE", $str); }
                function is_ie() {
                if(!isset($_SERVER['HTTP_USER_AGENT']))return false;
            
                if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false) return true; // IE8
                if(strpos($_SERVER['HTTP_USER_AGENT'], 'Windows NT 6.1') !== false) return true; // IE11
                return false;
            
                }
            
            
                $filepath = $_SERVER["DOCUMENT_ROOT"].'/temp/'.$fileName.'.xlsx';
                $filesize = filesize($filepath);
                $filename = mb_basename($filepath);
            
                if( is_ie() ) $filename = utf2euc($filename);
            
            
                header("Pragma: public");
                header("Expires: 0");
                header("Content-Type: application/octet-stream");
                header("Content-Disposition: attachment; filename=\"$filename\"");
                header("Content-Transfer-Encoding: binary");
                header("Content-Length: $filesize");
    
                ob_clean();
                flush();
                readfile($filepath);
                unlink($_SERVER["DOCUMENT_ROOT"].'/temp/'.$fileName.'.xlsx'); // 생성된 excel 파일 삭제
            }
            else{

                alert("다운로드할 수 있는 추출 결과가 없습니다.", $this->config->site_url('Dbdownload'));
            }

        }



        public function downloadGroupYesterday(): void
        {
            
            
            ini_set("display_errors", 0);



            $data = trimArray($this->input->get([
                'groupnum', 'groupname'
            ], true));
            
            $this->load->model('Dbuse');
            $getdata = $this->Dbuse->getYesterdayDB($data['groupnum']);
            // var_dump($getdata);
            if (count($getdata[0])>0){


                require_once $_SERVER["DOCUMENT_ROOT"]."/phpexcel/Classes/PHPExcel.php"; // 엑셀 라이브러리 import

                //set the desired name of the excel file
                $fileName = date("Ymd")."_".$data['groupname']."_어제자";
            
                // Create new PHPExcel object
                $objPHPExcel = new PHPExcel();
            
                // Set document properties
                $objPHPExcel->getProperties()->setCreator("Me")->setLastModifiedBy("Me")->setTitle("My Excel Sheet")->setSubject("My Excel Sheet")->setDescription("Excel Sheet")->setKeywords("Excel Sheet")->setCategory("Me");
            
            
                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $objPHPExcel->setActiveSheetIndex(0);
            
            
                // Add column headers
                $objPHPExcel->getActiveSheet()
                            ->setCellValue('A1', '번호')
                            ->setCellValue('B1', '시간')
                            ->setCellValue('C1', '키워드')
                            ->setCellValue('D1', '전화번호');;
            
                $ii = 2;
            
    
    
                foreach ($getdata as $getd) {
                    $objPHPExcel->getActiveSheet()->setCellValue('A'.$ii, $ii-1);
                    $objPHPExcel->getActiveSheet()->setCellValue('B'.$ii, $getd['dbSearched']);
                    $objPHPExcel->getActiveSheet()->setCellValue('C'.$ii, $getd['dbKeyword']);
                    $objPHPExcel->getActiveSheet()->setCellValue('D'.$ii, $getd['dbPhonenumber']);
                    $ii +=1;
                }

                // Set worksheet title
                $objPHPExcel->getActiveSheet()->setTitle($fileName);
            
                //save the file to the server (Excel2007)
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            
                $objWriter->save($_SERVER["DOCUMENT_ROOT"].'/temp/' . $fileName . '.xlsx'); // 저장될 파일 위치
            
            
                // 파일 다운로드 구현
                function mb_basename($path) { return end(explode('/',$path)); }
                function utf2euc($str) { return iconv("UTF-8","cp949//IGNORE", $str); }
                function is_ie() {
                if(!isset($_SERVER['HTTP_USER_AGENT']))return false;
            
                if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false) return true; // IE8
                if(strpos($_SERVER['HTTP_USER_AGENT'], 'Windows NT 6.1') !== false) return true; // IE11
                return false;
            
                }
            
            
                $filepath = $_SERVER["DOCUMENT_ROOT"].'/temp/'.$fileName.'.xlsx';
                $filesize = filesize($filepath);
                $filename = mb_basename($filepath);
            
                if( is_ie() ) $filename = utf2euc($filename);
            
            
                header("Pragma: public");
                header("Expires: 0");
                header("Content-Type: application/octet-stream");
                header("Content-Disposition: attachment; filename=\"$filename\"");
                header("Content-Transfer-Encoding: binary");
                header("Content-Length: $filesize");
    
                ob_clean();
                flush();
                readfile($filepath);
                unlink($_SERVER["DOCUMENT_ROOT"].'/temp/'.$fileName.'.xlsx'); // 생성된 excel 파일 삭제
            }
            else{

                alert("다운로드할 수 있는 추출 결과가 없습니다.", $this->config->site_url('Dbdownload'));
            }

        }



        public function downloadGroupSevenDays(): void
        {
            
            
            ini_set("display_errors", 0);



            $data = trimArray($this->input->get([
                'groupnum', 'groupname'
            ], true));
            
            $this->load->model('Dbuse');
            $getdata = $this->Dbuse->getSevenDaysDB($data['groupnum']);
            // var_dump($getdata);
            if (count($getdata[0])>0){


                require_once $_SERVER["DOCUMENT_ROOT"]."/phpexcel/Classes/PHPExcel.php"; // 엑셀 라이브러리 import

                //set the desired name of the excel file
                $fileName = date("Ymd")."_".$data['groupname']."_7일간";
            
                // Create new PHPExcel object
                $objPHPExcel = new PHPExcel();
            
                // Set document properties
                $objPHPExcel->getProperties()->setCreator("Me")->setLastModifiedBy("Me")->setTitle("My Excel Sheet")->setSubject("My Excel Sheet")->setDescription("Excel Sheet")->setKeywords("Excel Sheet")->setCategory("Me");
            
            
                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $objPHPExcel->setActiveSheetIndex(0);
            
            
                // Add column headers
                $objPHPExcel->getActiveSheet()
                            ->setCellValue('A1', '번호')
                            ->setCellValue('B1', '시간')
                            ->setCellValue('C1', '키워드')
                            ->setCellValue('D1', '전화번호');;
            
                $ii = 2;
            
    
    
                foreach ($getdata as $getd) {
                    $objPHPExcel->getActiveSheet()->setCellValue('A'.$ii, $ii-1);
                    $objPHPExcel->getActiveSheet()->setCellValue('B'.$ii, $getd['dbSearched']);
                    $objPHPExcel->getActiveSheet()->setCellValue('C'.$ii, $getd['dbKeyword']);
                    $objPHPExcel->getActiveSheet()->setCellValue('D'.$ii, $getd['dbPhonenumber']);
                    $ii +=1;
                }

                // Set worksheet title
                $objPHPExcel->getActiveSheet()->setTitle($fileName);
            
                //save the file to the server (Excel2007)
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            
                $objWriter->save($_SERVER["DOCUMENT_ROOT"].'/temp/' . $fileName . '.xlsx'); // 저장될 파일 위치
            
            
                // 파일 다운로드 구현
                function mb_basename($path) { return end(explode('/',$path)); }
                function utf2euc($str) { return iconv("UTF-8","cp949//IGNORE", $str); }
                function is_ie() {
                if(!isset($_SERVER['HTTP_USER_AGENT']))return false;
            
                if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false) return true; // IE8
                if(strpos($_SERVER['HTTP_USER_AGENT'], 'Windows NT 6.1') !== false) return true; // IE11
                return false;
            
                }
            
            
                $filepath = $_SERVER["DOCUMENT_ROOT"].'/temp/'.$fileName.'.xlsx';
                $filesize = filesize($filepath);
                $filename = mb_basename($filepath);
            
                if( is_ie() ) $filename = utf2euc($filename);
            
            
                header("Pragma: public");
                header("Expires: 0");
                header("Content-Type: application/octet-stream");
                header("Content-Disposition: attachment; filename=\"$filename\"");
                header("Content-Transfer-Encoding: binary");
                header("Content-Length: $filesize");
    
                ob_clean();
                flush();
                readfile($filepath);
                unlink($_SERVER["DOCUMENT_ROOT"].'/temp/'.$fileName.'.xlsx'); // 생성된 excel 파일 삭제
            }
            else{

                alert("다운로드할 수 있는 추출 결과가 없습니다.", $this->config->site_url('Dbdownload'));
            }

        }

        public function deleteKeywordGroup(): void
        {
            $data = trimArray($this->input->get([
                'groupnum'
            ], true));
            $this->load->model('Dbuse');
            $servernum = $this->Dbuse->deleteKeywordGroup($data['groupnum']);

            alert("삭제 하였습니다.", $this->config->site_url('Keywords'));
            return;

        }



        public function turnOnLink(): void
        {
            $data = trimArray($this->input->get([
                'groupnum'
            ], true));
            $this->load->model('Dbuse');



            $servernum = $this->Dbuse->turnOnLink($data['groupnum']);
            alert("On 하였습니다.", $this->config->site_url('Richmango/Links/Buttons'));
            return;

        }

        public function turnOnNotices(): void
        {
            $data = trimArray($this->input->get([
                'groupnum'
            ], true));
            $this->load->model('Dbuse');



            $servernum = $this->Dbuse->turnOnBoard($data['groupnum']);
            alert("On 하였습니다.", $this->config->site_url('Richmango/Boards/Notices'));
            return;

        }

        public function turnOffNotices(): void
        {
            $data = trimArray($this->input->get([
                'groupnum'
            ], true));
            $this->load->model('Dbuse');



            $servernum = $this->Dbuse->turnOffBoard($data['groupnum']);
            alert("On 하였습니다.", $this->config->site_url('Richmango/Boards/Notices'));
            return;

        }




        public function turnOnInformations(): void
        {
            $data = trimArray($this->input->get([
                'groupnum'
            ], true));
            $this->load->model('Dbuse');



            $servernum = $this->Dbuse->turnOnBoard($data['groupnum']);
            alert("On 하였습니다.", $this->config->site_url('Richmango/Boards/Informations'));
            return;

        }

        public function turnOffInformations(): void
        {
            $data = trimArray($this->input->get([
                'groupnum'
            ], true));
            $this->load->model('Dbuse');



            $servernum = $this->Dbuse->turnOffBoard($data['groupnum']);
            alert("On 하였습니다.", $this->config->site_url('Richmango/Boards/Informations'));
            return;

        }







    }
}
