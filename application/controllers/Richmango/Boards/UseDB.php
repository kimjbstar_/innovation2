<?php

if (! class_exists('UseDB')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class UseDB extends CI_Controller
    {
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
        }



        public function getSpecificDB(): void
        {
            $this->load->model('Dbuse');
            $specific_db = $this->Dbuse->getSpecificBoard($_GET['dbidx'])[0];
            // var_dump($specific_db);

            $dbform = '


                <form action="Keywordcontrol/updateBoard" method="post">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">내용 수정</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body">

                            <input type="hidden" value="'.$this->security->get_csrf_hash().'" name="'.$this->security->get_csrf_token_name().'">
                            <input type="hidden" class="form-control" name="link_index"  value="'.$_GET['dbidx'].'">

                            <div class="form-group">
                                <label>제목</label>
                                <input type="text" class="form-control" name="question"  value="'.$specific_db['buttonName'].'">
                            </div>                            
                            <div class="form-group">
                                <label>내용</label>
                                <input type="text" class="form-control" name="answer"  value="'.$specific_db['linkAddress'].'">
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">저장</button>
                    </div>
                </form>



            ';


            echo ($dbform);
        }



        public function getNewDB(): void
        {
            // $this->load->model('Dbuse');
            // $specific_db = $this->Dbuse->getSpecificDB($_GET['dbidx'])[0];
            // // var_dump($specific_db);

            $dbform = '

                <form action="Keywordcontrol/createNewFaq" method="post">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">FAQ 생성</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body">
                        
                            <input type="hidden" value="'.$this->security->get_csrf_hash().'" name="'.$this->security->get_csrf_token_name().'">


                            <div class="form-group">
                                <label>질문</label>
                                <input type="text" class="form-control" name="question"  value="">
                            </div>                            
                            <div class="form-group">
                                <label>응답</label>
                                <input type="text" class="form-control" name="answer"  value="">
                            </div>



                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">생성</button>
                    </div>
                </form>




            ';


            echo ($dbform);
        }






        public function getNewNotice(): void
        {
            // $this->load->model('Dbuse');
            // $specific_db = $this->Dbuse->getSpecificDB($_GET['dbidx'])[0];
            // // var_dump($specific_db);

            $dbform = '

                <form action="Keywordcontrol/createNewNotice" method="post">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">공지 생성</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body">
                        
                            <input type="hidden" value="'.$this->security->get_csrf_hash().'" name="'.$this->security->get_csrf_token_name().'">


                            <div class="form-group">
                                <label>제목</label>
                                <input type="text" class="form-control" name="question"  value="">
                            </div>                            
                            <div class="form-group">
                                <label>내용</label>
                                <input type="text" class="form-control" name="answer"  value="">
                            </div>



                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">생성</button>
                    </div>
                </form>




            ';


            echo ($dbform);
        }




        public function getNewInformation(): void
        {
            // $this->load->model('Dbuse');
            // $specific_db = $this->Dbuse->getSpecificDB($_GET['dbidx'])[0];
            // // var_dump($specific_db);

            $dbform = '

                <form action="Keywordcontrol/createNewInformation" method="post">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">안내 생성</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body">
                        
                            <input type="hidden" value="'.$this->security->get_csrf_hash().'" name="'.$this->security->get_csrf_token_name().'">


                            <div class="form-group">
                                <label>제목</label>
                                <input type="text" class="form-control" name="question"  value="">
                            </div>                            
                            <div class="form-group">
                                <label>내용</label>
                                <input type="text" class="form-control" name="answer"  value="">
                            </div>



                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">생성</button>
                    </div>
                </form>




            ';


            echo ($dbform);
        }








    }
}
