<?php

if (! class_exists('Keywordcontrol')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Keywordcontrol extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->lang->load('registerValid', 'korean');
        }

        public function createNewKeywords(): void
        {
            // $this->load->helper(['valid', 'data']);
            // $this->load->model('User/User');
            $data = trimArray($this->input->post([
                'groupname', 'keywords'
            ], true));
            if ($data['groupname']=="" || $data['keywords']=="") {
                alert("모든 내용을 채워주세요.", $this->config->site_url('Keywords'));
                return;
            }
            $this->load->model('Dbuse');
            $servernum = $this->Dbuse->getServerNum()[0]['serverNum'];
            $validKeywordsCount = $this->Dbuse->getValidKeywordsCount()[0]['validCount'];
            if ($servernum*15<$validKeywordsCount+count(explode(',',$data['keywords']))){
                alert("등록하시려는 전체 유효키워드의 수가 서버 가용범위 이상입니다.", $this->config->site_url('Keywords'));
                return;
            }
            $this->Dbuse->createNewKeywordGroup($data['groupname'], $data['keywords'], count(explode(',',$data['keywords'])));
            alert("등록하였습니다.", $this->config->site_url('Keywords'));
            return;

        }





        public function createNewFaq(): void
        {
            // $this->load->helper(['valid', 'data']);
            // $this->load->model('User/User');
            $data = trimArray($this->input->post([
                'question', 'answer'
            ], true));
            if ($data['question']=="" || $data['answer']=="") {
                alert("모든 내용을 채워주세요.", $this->config->site_url('Richmango/Contents/Faqs'));
                return;
            }
            $this->load->model('Dbuse');


            $this->Dbuse->createNewFaq($data['question'], $data['answer']);

            alert("등록하였습니다.", $this->config->site_url('Richmango/Contents/Faqs'));
            return;

        }











        private function GenerateString($length)  
        {  
            $characters = "abcdefghijklmnopqrstuvwxyz";  
            $characters .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";  
            $characters .= "_";  
              
            $string_generated = "";  
              
            $nmr_loops = $length;  
            while ($nmr_loops--)  
            {  
                $string_generated .= $characters[mt_rand(0, strlen($characters) - 1)];  
            }  
              
            return $string_generated;  
        }  
        
        




        public function createNewHireInfo(): void
        {
            // $this->load->helper(['valid', 'data']);
            // $this->load->model('User/User');

            // echo $_SERVER["DOCUMENT_ROOT"];




            $data = trimArray($this->input->post([
                'company_name', 'location', 'company_category', 'company_tags','company_info','recruit_info',
                'faqs_q1','faqs_a1',
                'faqs_q2','faqs_a2',
                'faqs_q3','faqs_a3',
                'faqs_q4','faqs_a4',
                'faqs_q5','faqs_a5',
                'faqs_q6','faqs_a6',
                'faqs_q7','faqs_a7',
                'faqs_q8','faqs_a8',
                'faqs_q9','faqs_a9',
                'faqs_q10','faqs_a10'
            ], true));
            $faqs='';

            if ($data['company_name']=="" || $data['location']==""  || $data['company_category']=="") {
                alert("회사명, 지역, 카테고리는 필수입니다.", $this->config->site_url('Richmango/Contents/HireInfos'));
                return;
            }









            $uploaddir = $_SERVER["DOCUMENT_ROOT"].'/uploaded_images/';  // 업로드 할 위치 

            $logo_image="";
            $slide_images="";


            if ($_FILES['logo_image']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['logo_image']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['logo_image']['tmp_name'], $uploadfile);
                $logo_image.=$filename;
            }

            if ($_FILES['extra_image_one']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_one']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_one']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }
            if ($_FILES['extra_image_two']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_two']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_two']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }
            if ($_FILES['extra_image_three']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_three']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_three']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }
            if ($_FILES['extra_image_four']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_four']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_four']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }
            if ($_FILES['extra_image_five']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_five']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_five']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }



            // extra_image_one






            for ($i = 1; $i <= 10; $i++){
                if (!$data['faqs_q'.$i]=="" || !$data['faqs_a'.$i]=="") {
                    if ($faqs!=''){
                        $faqs.='%%%';
                    }
                    $faqs.='^Q^'.$data['faqs_q'.$i].'^A^'.$data['faqs_a'.$i];
                }                
            };

            // echo $faqs;


            $this->load->model('Dbuse');


            $this->Dbuse->createNewHireInfo($data['company_name'], $data['location'], $logo_image, $data['company_category'], $data['company_tags'], $slide_images, $data['company_info'], $data['recruit_info'], $faqs);

            // print_r($data);


            alert("등록하였습니다.", $this->config->site_url('Richmango/Contents/HireInfos'));
            return;

        }











        public function updateHireInfo(): void
        {
            // $this->load->helper(['valid', 'data']);
            // $this->load->model('User/User');

            // echo $_SERVER["DOCUMENT_ROOT"];


            // phpinfo();
            $this->load->model('Dbuse');


            $data = trimArray($this->input->post([
                'company_name', 'location', 'company_category', 'company_tags','company_info','recruit_info',
                'faqs_q1','faqs_a1',
                'faqs_q2','faqs_a2',
                'faqs_q3','faqs_a3',
                'faqs_q4','faqs_a4',
                'faqs_q5','faqs_a5',
                'faqs_q6','faqs_a6',
                'faqs_q7','faqs_a7',
                'faqs_q8','faqs_a8',
                'faqs_q9','faqs_a9',
                'faqs_q10','faqs_a10', 'company_index', 'logo_image_keep', 'slide_image_keep_0', 'slide_image_keep_0', 'slide_image_keep_1', 'slide_image_keep_2', 'slide_image_keep_3', 'slide_image_keep_4', 'slide_image_keep_5', 'slide_image_keep_6', 'slide_image_keep_7', 'slide_image_keep_8', 'slide_image_keep_9', 'slide_image_keep_10', 'slide_image_keep_11', 'slide_image_keep_12', 'slide_image_keep_13', 'slide_image_keep_14', 'slide_image_keep_15', 'slide_image_keep_16', 'slide_image_keep_17', 'slide_image_keep_18', 'slide_image_keep_19', 'slide_image_keep_20'
            ], true));
            $faqs='';


            // var_dump($data);


            if ($data['company_name']=="" || $data['location']==""  || $data['company_category']=="") {
                alert("회사명, 지역, 카테고리는 필수입니다.", $this->config->site_url('Richmango/Contents/HireInfos'));
                return;
            }









            $uploaddir = $_SERVER["DOCUMENT_ROOT"].'/uploaded_images/';  // 업로드 할 위치 

            $logo_image="";
            $slide_images="";


            // var_dump($_FILES);

            $specific_db = $this->Dbuse->getSpecificHireInfo($data['company_index'])[0];

            if ($data['logo_image_keep']==0){
                if ($_FILES['logo_image']['name'] !=""){
                    $filename = $this->GenerateString(7).$_FILES['logo_image']['name'];
                    $uploadfile = $uploaddir . basename($filename);
                    move_uploaded_file($_FILES['logo_image']['tmp_name'], $uploadfile);
                    $logo_image.=$filename;
                }
            }
            else if ($data['logo_image_keep']==1){
                $logo_image=$specific_db['logo_image'];
            }


            

            // var_dump($specific_db['slide_images']);

            $imagenum=0;
            foreach (explode("//",$specific_db['slide_images']) as $each_image){ 
                if ($data['slide_image_keep_'.$imagenum]==1){

                    if ($slide_images!=""){
                        $slide_images.="//";
                    }
                    $slide_images.=$each_image;
                }
                $imagenum+=1;
                
             }

            
            // $slide_images =$specific_db['slide_images'];


            if ($_FILES['extra_image_one']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_one']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_one']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }




            if ($_FILES['extra_image_one']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_one']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_one']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }

            if ($_FILES['extra_image_two']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_two']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_two']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }

            if ($_FILES['extra_image_three']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_three']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_three']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }

            if ($_FILES['extra_image_four']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_four']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_four']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }



            if ($_FILES['extra_image_five']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_five']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_five']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }


            // var_dump($slide_images);
            // extra_image_one






            for ($i = 1; $i <= 10; $i++){
                if (!$data['faqs_q'.$i]=="" || !$data['faqs_a'.$i]=="") {
                    if ($faqs!=''){
                        $faqs.='%%%';
                    }
                    $faqs.='^Q^'.$data['faqs_q'.$i].'^A^'.$data['faqs_a'.$i];
                }                
            };

            // echo $faqs;



            $this->Dbuse->updateHireInfo($data['company_name'], $data['location'], $logo_image, $data['company_category'], $data['company_tags'], $slide_images, $data['company_info'], $data['recruit_info'], $faqs, $data['company_index']);

            // print_r($data);


            alert("수정하였습니다.", $this->config->site_url('Richmango/Contents/HireInfos'));
            return;

        }

























































        public function createNewEventInfo(): void
        {
            // $this->load->helper(['valid', 'data']);
            // $this->load->model('User/User');

            // echo $_SERVER["DOCUMENT_ROOT"];




            $data = trimArray($this->input->post([
                'company_name', 'location', 'company_category', 'company_tags','company_info','recruit_info',
                'faqs_q1','faqs_a1',
                'faqs_q2','faqs_a2',
                'faqs_q3','faqs_a3',
                'faqs_q4','faqs_a4',
                'faqs_q5','faqs_a5',
                'faqs_q6','faqs_a6',
                'faqs_q7','faqs_a7',
                'faqs_q8','faqs_a8',
                'faqs_q9','faqs_a9',
                'faqs_q10','faqs_a10'
            ], true));
            $faqs='';

            if ($data['company_name']=="" || $data['location']=="" ) {
                alert("이벤트명, 링크는 필수입니다.", $this->config->site_url('Richmango/Contents/Events'));
                return;
            }









            $uploaddir = $_SERVER["DOCUMENT_ROOT"].'/uploaded_images/';  // 업로드 할 위치 

            $logo_image="";
            $slide_images="";



            if ($_FILES['extra_image_one']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_one']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_one']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }
            if ($_FILES['extra_image_two']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_two']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_two']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }
            if ($_FILES['extra_image_three']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_three']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_three']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }
            if ($_FILES['extra_image_four']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_four']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_four']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }
            if ($_FILES['extra_image_five']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_five']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_five']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }



            // extra_image_one






            $this->load->model('Dbuse');


            $this->Dbuse->createNewEventInfo($data['company_name'], $data['location'], $slide_images, $data['company_info']);

            // print_r($data);


            alert("등록하였습니다.", $this->config->site_url('Richmango/Contents/Events'));
            return;

        }






        public function updateEventInfo(): void
        {
            // $this->load->helper(['valid', 'data']);
            // $this->load->model('User/User');

            // echo $_SERVER["DOCUMENT_ROOT"];


            // phpinfo();
            $this->load->model('Dbuse');


            $data = trimArray($this->input->post([
                'company_name', 'location', 'company_category', 'company_tags','company_info','recruit_info',
                'faqs_q1','faqs_a1',
                'faqs_q2','faqs_a2',
                'faqs_q3','faqs_a3',
                'faqs_q4','faqs_a4',
                'faqs_q5','faqs_a5',
                'faqs_q6','faqs_a6',
                'faqs_q7','faqs_a7',
                'faqs_q8','faqs_a8',
                'faqs_q9','faqs_a9',
                'faqs_q10','faqs_a10', 'company_index', 'logo_image_keep', 'slide_image_keep_0', 'slide_image_keep_0', 'slide_image_keep_1', 'slide_image_keep_2', 'slide_image_keep_3', 'slide_image_keep_4', 'slide_image_keep_5', 'slide_image_keep_6', 'slide_image_keep_7', 'slide_image_keep_8', 'slide_image_keep_9', 'slide_image_keep_10', 'slide_image_keep_11', 'slide_image_keep_12', 'slide_image_keep_13', 'slide_image_keep_14', 'slide_image_keep_15', 'slide_image_keep_16', 'slide_image_keep_17', 'slide_image_keep_18', 'slide_image_keep_19', 'slide_image_keep_20'
            ], true));
            $faqs='';


            // var_dump($data);


            if ($data['company_name']=="" || $data['location']=="") {
                alert("이벤트명, 링크는 필수입니다.", $this->config->site_url('Richmango/Contents/Events'));
                return;
            }









            $uploaddir = $_SERVER["DOCUMENT_ROOT"].'/uploaded_images/';  // 업로드 할 위치 

            $logo_image="";
            $slide_images="";


            // var_dump($_FILES);

            $specific_db = $this->Dbuse->getSpecificEventInfo($data['company_index'])[0];


            

            // var_dump($specific_db['slide_images']);

            $imagenum=0;
            foreach (explode("//",$specific_db['slide_images']) as $each_image){ 
                if ($data['slide_image_keep_'.$imagenum]==1){

                    if ($slide_images!=""){
                        $slide_images.="//";
                    }
                    $slide_images.=$each_image;
                }
                $imagenum+=1;
                
             }

            
            // $slide_images =$specific_db['slide_images'];


            if ($_FILES['extra_image_one']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_one']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_one']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }




            if ($_FILES['extra_image_one']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_one']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_one']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }

            if ($_FILES['extra_image_two']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_two']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_two']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }

            if ($_FILES['extra_image_three']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_three']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_three']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }

            if ($_FILES['extra_image_four']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_four']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_four']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }



            if ($_FILES['extra_image_five']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_five']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_five']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }


            // var_dump($slide_images);
            // extra_image_one




            $this->Dbuse->updateEventInfo($data['company_name'], $data['location'],   $slide_images, $data['company_info'],  $data['company_index']);

            // print_r($data);


            alert("수정하였습니다.", $this->config->site_url('Richmango/Contents/Events'));
            return;

        }











        public function deleteEventInfo(): void
        {
            // $this->load->helper(['valid', 'data']);
            // $this->load->model('User/User');

            // echo $_SERVER["DOCUMENT_ROOT"];


            // phpinfo();
            $this->load->model('Dbuse');


            $data = trimArray($this->input->post([
                 'company_index'
            ], true));


            $this->Dbuse->deleteEventInfo($data['company_index']);



            alert("삭제하였습니다.", $this->config->site_url('Richmango/Contents/Events'));
            return;

        }























































































        public function createNewEventResultInfo(): void
        {
            // $this->load->helper(['valid', 'data']);
            // $this->load->model('User/User');

            // echo $_SERVER["DOCUMENT_ROOT"];




            $data = trimArray($this->input->post([
                'company_name', 'location', 'company_category', 'company_tags','company_info','recruit_info',
                'faqs_q1','faqs_a1',
                'faqs_q2','faqs_a2',
                'faqs_q3','faqs_a3',
                'faqs_q4','faqs_a4',
                'faqs_q5','faqs_a5',
                'faqs_q6','faqs_a6',
                'faqs_q7','faqs_a7',
                'faqs_q8','faqs_a8',
                'faqs_q9','faqs_a9',
                'faqs_q10','faqs_a10'
            ], true));
            $faqs='';

            if ($data['company_name']=="" || $data['location']=="" ) {
                alert("이벤트명, 링크는 필수입니다.", $this->config->site_url('Richmango/Contents/EventResult'));
                return;
            }









            $uploaddir = $_SERVER["DOCUMENT_ROOT"].'/uploaded_images/';  // 업로드 할 위치 

            $logo_image="";
            $slide_images="";



            if ($_FILES['extra_image_one']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_one']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_one']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }
            if ($_FILES['extra_image_two']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_two']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_two']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }
            if ($_FILES['extra_image_three']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_three']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_three']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }
            if ($_FILES['extra_image_four']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_four']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_four']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }
            if ($_FILES['extra_image_five']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_five']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_five']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }



            // extra_image_one






            $this->load->model('Dbuse');


            $this->Dbuse->createNewEventResultInfo($data['company_name'], $data['location'], $slide_images, $data['company_info']);

            // print_r($data);


            alert("등록하였습니다.", $this->config->site_url('Richmango/Contents/EventResult'));
            return;

        }






        public function updateEventResultInfo(): void
        {
            // $this->load->helper(['valid', 'data']);
            // $this->load->model('User/User');

            // echo $_SERVER["DOCUMENT_ROOT"];


            // phpinfo();
            $this->load->model('Dbuse');


            $data = trimArray($this->input->post([
                'company_name', 'location', 'company_category', 'company_tags','company_info','recruit_info',
                'faqs_q1','faqs_a1',
                'faqs_q2','faqs_a2',
                'faqs_q3','faqs_a3',
                'faqs_q4','faqs_a4',
                'faqs_q5','faqs_a5',
                'faqs_q6','faqs_a6',
                'faqs_q7','faqs_a7',
                'faqs_q8','faqs_a8',
                'faqs_q9','faqs_a9',
                'faqs_q10','faqs_a10', 'company_index', 'logo_image_keep', 'slide_image_keep_0', 'slide_image_keep_0', 'slide_image_keep_1', 'slide_image_keep_2', 'slide_image_keep_3', 'slide_image_keep_4', 'slide_image_keep_5', 'slide_image_keep_6', 'slide_image_keep_7', 'slide_image_keep_8', 'slide_image_keep_9', 'slide_image_keep_10', 'slide_image_keep_11', 'slide_image_keep_12', 'slide_image_keep_13', 'slide_image_keep_14', 'slide_image_keep_15', 'slide_image_keep_16', 'slide_image_keep_17', 'slide_image_keep_18', 'slide_image_keep_19', 'slide_image_keep_20'
            ], true));
            $faqs='';


            // var_dump($data);


            if ($data['company_name']=="" || $data['location']=="") {
                alert("이벤트명, 링크는 필수입니다.", $this->config->site_url('Richmango/Contents/EventResult'));
                return;
            }









            $uploaddir = $_SERVER["DOCUMENT_ROOT"].'/uploaded_images/';  // 업로드 할 위치 

            $logo_image="";
            $slide_images="";


            // var_dump($_FILES);

            $specific_db = $this->Dbuse->getSpecificEventResultInfo($data['company_index'])[0];


            

            // var_dump($specific_db['slide_images']);

            $imagenum=0;
            foreach (explode("//",$specific_db['slide_images']) as $each_image){ 
                if ($data['slide_image_keep_'.$imagenum]==1){

                    if ($slide_images!=""){
                        $slide_images.="//";
                    }
                    $slide_images.=$each_image;
                }
                $imagenum+=1;
                
             }

            
            // $slide_images =$specific_db['slide_images'];


            if ($_FILES['extra_image_one']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_one']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_one']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }




            if ($_FILES['extra_image_one']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_one']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_one']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }

            if ($_FILES['extra_image_two']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_two']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_two']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }

            if ($_FILES['extra_image_three']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_three']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_three']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }

            if ($_FILES['extra_image_four']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_four']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_four']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }



            if ($_FILES['extra_image_five']['name'] !=""){
                $filename = $this->GenerateString(7).$_FILES['extra_image_five']['name'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($_FILES['extra_image_five']['tmp_name'], $uploadfile);

                if ($slide_images!=""){
                    $slide_images.="//";
                }
                $slide_images.=$filename;
            }


            // var_dump($slide_images);
            // extra_image_one




            $this->Dbuse->updateEventResultInfo($data['company_name'], $data['location'],   $slide_images, $data['company_info'],  $data['company_index']);

            // print_r($data);


            alert("수정하였습니다.", $this->config->site_url('Richmango/Contents/EventResult'));
            return;

        }











        public function deleteEventResultInfo(): void
        {
            // $this->load->helper(['valid', 'data']);
            // $this->load->model('User/User');

            // echo $_SERVER["DOCUMENT_ROOT"];


            // phpinfo();
            $this->load->model('Dbuse');


            $data = trimArray($this->input->post([
                 'company_index'
            ], true));


            $this->Dbuse->deleteEventResultInfo($data['company_index']);



            alert("삭제하였습니다.", $this->config->site_url('Richmango/Contents/EventResult'));
            return;

        }









































































































































        public function updateFaq(): void
        {
            // $this->load->helper(['valid', 'data']);
            // $this->load->model('User/User');
            $data = trimArray($this->input->post([
                'question', 'answer', 'link_index'
            ], true));
            $this->load->model('Dbuse');
            $this->Dbuse->updateFaq($data['question'], $data['link_index'], $data['answer']);
            alert("수정하였습니다.", $this->config->site_url('Richmango/Contents/Faqs'));
            return;

        }




        public function updateButtons(): void
        {
            // $this->load->helper(['valid', 'data']);
            // $this->load->model('User/User');
            $data = trimArray($this->input->post([
                'link_address', 'link_index'
            ], true));
            $this->load->model('Dbuse');
            $this->Dbuse->updateButton($data['link_address'], $data['link_index']);
            alert("수정하였습니다.", $this->config->site_url('Richmango/Links/Buttons'));
            return;

        }




        public function updateKeywords(): void
        {
            // $this->load->helper(['valid', 'data']);
            // $this->load->model('User/User');
            $data = trimArray($this->input->post([
                'groupname', 'keywords', 'dbindex'
            ], true));
            if ($data['groupname']=="" || $data['keywords']=="") {
                alert("모든 내용을 채워주세요.", $this->config->site_url('Keywords'));
                return;
            }
            $this->load->model('Dbuse');
            $servernum = $this->Dbuse->getServerNum()[0]['serverNum'];
            $validKeywordsCount = $this->Dbuse->getValidKeywordsCountExcept($data['dbindex'])[0]['validCount'];
            if ($servernum*15<$validKeywordsCount+count(explode(',',$data['keywords']))){
                alert("등록하시려는 전체 유효키워드의 수가 서버 가용범위 이상입니다.", $this->config->site_url('Keywords'));
                return;
            }
            $this->Dbuse->updateKeywordGroup($data['groupname'], $data['keywords'], count(explode(',',$data['keywords'])), $data['dbindex']);
            alert("수정하였습니다.", $this->config->site_url('Keywords'));
            return;

        }

        public function turnOffLink(): void
        {
            $data = trimArray($this->input->get([
                'groupnum'
            ], true));
            $this->load->model('Dbuse');
            $servernum = $this->Dbuse->turnOffLink($data['groupnum']);

            alert("Off 하였습니다.", $this->config->site_url('Richmango/Links/Buttons'));
            return;

        }


        public function downloadGroupToday(): void
        {
            
            
            ini_set("display_errors", 0);



            $data = trimArray($this->input->get([
                'groupnum', 'groupname'
            ], true));
            
            $this->load->model('Dbuse');
            $getdata = $this->Dbuse->getTodayDB($data['groupnum']);
            // var_dump($getdata);
            if (count($getdata[0])>0){


                require_once $_SERVER["DOCUMENT_ROOT"]."/phpexcel/Classes/PHPExcel.php"; // 엑셀 라이브러리 import

                //set the desired name of the excel file
                $fileName = date("Ymd")."_".$data['groupname']."_오늘자";
            
                // Create new PHPExcel object
                $objPHPExcel = new PHPExcel();
            
                // Set document properties
                $objPHPExcel->getProperties()->setCreator("Me")->setLastModifiedBy("Me")->setTitle("My Excel Sheet")->setSubject("My Excel Sheet")->setDescription("Excel Sheet")->setKeywords("Excel Sheet")->setCategory("Me");
            
            
                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $objPHPExcel->setActiveSheetIndex(0);
            
            
                // Add column headers
                $objPHPExcel->getActiveSheet()
                            ->setCellValue('A1', '번호')
                            ->setCellValue('B1', '시간')
                            ->setCellValue('C1', '키워드')
                            ->setCellValue('D1', '전화번호');;
            
                $ii = 2;
            
    
    
                foreach ($getdata as $getd) {
                    $objPHPExcel->getActiveSheet()->setCellValue('A'.$ii, $ii-1);
                    $objPHPExcel->getActiveSheet()->setCellValue('B'.$ii, $getd['dbSearched']);
                    $objPHPExcel->getActiveSheet()->setCellValue('C'.$ii, $getd['dbKeyword']);
                    $objPHPExcel->getActiveSheet()->setCellValue('D'.$ii, $getd['dbPhonenumber']);
                    $ii +=1;
                }

                // Set worksheet title
                $objPHPExcel->getActiveSheet()->setTitle($fileName);
            
                //save the file to the server (Excel2007)
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            
                $objWriter->save($_SERVER["DOCUMENT_ROOT"].'/temp/' . $fileName . '.xlsx'); // 저장될 파일 위치
            
            
                // 파일 다운로드 구현
                function mb_basename($path) { return end(explode('/',$path)); }
                function utf2euc($str) { return iconv("UTF-8","cp949//IGNORE", $str); }
                function is_ie() {
                if(!isset($_SERVER['HTTP_USER_AGENT']))return false;
            
                if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false) return true; // IE8
                if(strpos($_SERVER['HTTP_USER_AGENT'], 'Windows NT 6.1') !== false) return true; // IE11
                return false;
            
                }
            
            
                $filepath = $_SERVER["DOCUMENT_ROOT"].'/temp/'.$fileName.'.xlsx';
                $filesize = filesize($filepath);
                $filename = mb_basename($filepath);
            
                if( is_ie() ) $filename = utf2euc($filename);
            
            
                header("Pragma: public");
                header("Expires: 0");
                header("Content-Type: application/octet-stream");
                header("Content-Disposition: attachment; filename=\"$filename\"");
                header("Content-Transfer-Encoding: binary");
                header("Content-Length: $filesize");
    
                ob_clean();
                flush();
                readfile($filepath);
                unlink($_SERVER["DOCUMENT_ROOT"].'/temp/'.$fileName.'.xlsx'); // 생성된 excel 파일 삭제
            }
            else{

                alert("다운로드할 수 있는 추출 결과가 없습니다.", $this->config->site_url('Dbdownload'));
            }

        }



        public function downloadGroupYesterday(): void
        {
            
            
            ini_set("display_errors", 0);



            $data = trimArray($this->input->get([
                'groupnum', 'groupname'
            ], true));
            
            $this->load->model('Dbuse');
            $getdata = $this->Dbuse->getYesterdayDB($data['groupnum']);
            // var_dump($getdata);
            if (count($getdata[0])>0){


                require_once $_SERVER["DOCUMENT_ROOT"]."/phpexcel/Classes/PHPExcel.php"; // 엑셀 라이브러리 import

                //set the desired name of the excel file
                $fileName = date("Ymd")."_".$data['groupname']."_어제자";
            
                // Create new PHPExcel object
                $objPHPExcel = new PHPExcel();
            
                // Set document properties
                $objPHPExcel->getProperties()->setCreator("Me")->setLastModifiedBy("Me")->setTitle("My Excel Sheet")->setSubject("My Excel Sheet")->setDescription("Excel Sheet")->setKeywords("Excel Sheet")->setCategory("Me");
            
            
                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $objPHPExcel->setActiveSheetIndex(0);
            
            
                // Add column headers
                $objPHPExcel->getActiveSheet()
                            ->setCellValue('A1', '번호')
                            ->setCellValue('B1', '시간')
                            ->setCellValue('C1', '키워드')
                            ->setCellValue('D1', '전화번호');;
            
                $ii = 2;
            
    
    
                foreach ($getdata as $getd) {
                    $objPHPExcel->getActiveSheet()->setCellValue('A'.$ii, $ii-1);
                    $objPHPExcel->getActiveSheet()->setCellValue('B'.$ii, $getd['dbSearched']);
                    $objPHPExcel->getActiveSheet()->setCellValue('C'.$ii, $getd['dbKeyword']);
                    $objPHPExcel->getActiveSheet()->setCellValue('D'.$ii, $getd['dbPhonenumber']);
                    $ii +=1;
                }

                // Set worksheet title
                $objPHPExcel->getActiveSheet()->setTitle($fileName);
            
                //save the file to the server (Excel2007)
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            
                $objWriter->save($_SERVER["DOCUMENT_ROOT"].'/temp/' . $fileName . '.xlsx'); // 저장될 파일 위치
            
            
                // 파일 다운로드 구현
                function mb_basename($path) { return end(explode('/',$path)); }
                function utf2euc($str) { return iconv("UTF-8","cp949//IGNORE", $str); }
                function is_ie() {
                if(!isset($_SERVER['HTTP_USER_AGENT']))return false;
            
                if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false) return true; // IE8
                if(strpos($_SERVER['HTTP_USER_AGENT'], 'Windows NT 6.1') !== false) return true; // IE11
                return false;
            
                }
            
            
                $filepath = $_SERVER["DOCUMENT_ROOT"].'/temp/'.$fileName.'.xlsx';
                $filesize = filesize($filepath);
                $filename = mb_basename($filepath);
            
                if( is_ie() ) $filename = utf2euc($filename);
            
            
                header("Pragma: public");
                header("Expires: 0");
                header("Content-Type: application/octet-stream");
                header("Content-Disposition: attachment; filename=\"$filename\"");
                header("Content-Transfer-Encoding: binary");
                header("Content-Length: $filesize");
    
                ob_clean();
                flush();
                readfile($filepath);
                unlink($_SERVER["DOCUMENT_ROOT"].'/temp/'.$fileName.'.xlsx'); // 생성된 excel 파일 삭제
            }
            else{

                alert("다운로드할 수 있는 추출 결과가 없습니다.", $this->config->site_url('Dbdownload'));
            }

        }



        public function downloadGroupSevenDays(): void
        {
            
            
            ini_set("display_errors", 0);



            $data = trimArray($this->input->get([
                'groupnum', 'groupname'
            ], true));
            
            $this->load->model('Dbuse');
            $getdata = $this->Dbuse->getSevenDaysDB($data['groupnum']);
            // var_dump($getdata);
            if (count($getdata[0])>0){


                require_once $_SERVER["DOCUMENT_ROOT"]."/phpexcel/Classes/PHPExcel.php"; // 엑셀 라이브러리 import

                //set the desired name of the excel file
                $fileName = date("Ymd")."_".$data['groupname']."_7일간";
            
                // Create new PHPExcel object
                $objPHPExcel = new PHPExcel();
            
                // Set document properties
                $objPHPExcel->getProperties()->setCreator("Me")->setLastModifiedBy("Me")->setTitle("My Excel Sheet")->setSubject("My Excel Sheet")->setDescription("Excel Sheet")->setKeywords("Excel Sheet")->setCategory("Me");
            
            
                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $objPHPExcel->setActiveSheetIndex(0);
            
            
                // Add column headers
                $objPHPExcel->getActiveSheet()
                            ->setCellValue('A1', '번호')
                            ->setCellValue('B1', '시간')
                            ->setCellValue('C1', '키워드')
                            ->setCellValue('D1', '전화번호');;
            
                $ii = 2;
            
    
    
                foreach ($getdata as $getd) {
                    $objPHPExcel->getActiveSheet()->setCellValue('A'.$ii, $ii-1);
                    $objPHPExcel->getActiveSheet()->setCellValue('B'.$ii, $getd['dbSearched']);
                    $objPHPExcel->getActiveSheet()->setCellValue('C'.$ii, $getd['dbKeyword']);
                    $objPHPExcel->getActiveSheet()->setCellValue('D'.$ii, $getd['dbPhonenumber']);
                    $ii +=1;
                }

                // Set worksheet title
                $objPHPExcel->getActiveSheet()->setTitle($fileName);
            
                //save the file to the server (Excel2007)
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            
                $objWriter->save($_SERVER["DOCUMENT_ROOT"].'/temp/' . $fileName . '.xlsx'); // 저장될 파일 위치
            
            
                // 파일 다운로드 구현
                function mb_basename($path) { return end(explode('/',$path)); }
                function utf2euc($str) { return iconv("UTF-8","cp949//IGNORE", $str); }
                function is_ie() {
                if(!isset($_SERVER['HTTP_USER_AGENT']))return false;
            
                if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false) return true; // IE8
                if(strpos($_SERVER['HTTP_USER_AGENT'], 'Windows NT 6.1') !== false) return true; // IE11
                return false;
            
                }
            
            
                $filepath = $_SERVER["DOCUMENT_ROOT"].'/temp/'.$fileName.'.xlsx';
                $filesize = filesize($filepath);
                $filename = mb_basename($filepath);
            
                if( is_ie() ) $filename = utf2euc($filename);
            
            
                header("Pragma: public");
                header("Expires: 0");
                header("Content-Type: application/octet-stream");
                header("Content-Disposition: attachment; filename=\"$filename\"");
                header("Content-Transfer-Encoding: binary");
                header("Content-Length: $filesize");
    
                ob_clean();
                flush();
                readfile($filepath);
                unlink($_SERVER["DOCUMENT_ROOT"].'/temp/'.$fileName.'.xlsx'); // 생성된 excel 파일 삭제
            }
            else{

                alert("다운로드할 수 있는 추출 결과가 없습니다.", $this->config->site_url('Dbdownload'));
            }

        }

        public function deleteKeywordGroup(): void
        {
            $data = trimArray($this->input->get([
                'groupnum'
            ], true));
            $this->load->model('Dbuse');
            $servernum = $this->Dbuse->deleteKeywordGroup($data['groupnum']);

            alert("삭제 하였습니다.", $this->config->site_url('Keywords'));
            return;

        }



        public function turnOnLink(): void
        {
            $data = trimArray($this->input->get([
                'groupnum'
            ], true));
            $this->load->model('Dbuse');



            $servernum = $this->Dbuse->turnOnLink($data['groupnum']);
            alert("On 하였습니다.", $this->config->site_url('Richmango/Links/Buttons'));
            return;

        }

        public function turnOnFaq(): void
        {
            $data = trimArray($this->input->get([
                'groupnum'
            ], true));
            $this->load->model('Dbuse');



            $servernum = $this->Dbuse->turnOnFaq($data['groupnum']);
            alert("On 하였습니다.", $this->config->site_url('Richmango/Contents/Faqs'));
            return;

        }

        public function turnOffFaq(): void
        {
            $data = trimArray($this->input->get([
                'groupnum'
            ], true));
            $this->load->model('Dbuse');



            $servernum = $this->Dbuse->turnOffFaq($data['groupnum']);
            alert("Off 하였습니다.", $this->config->site_url('Richmango/Contents/Faqs'));
            return;

        }



    }
}
