<?php

if (! class_exists('UseDB')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class UseDB extends CI_Controller
    {
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
        }



        public function getSpecificDB(): void
        {
            $this->load->model('Dbuse');
            $specific_db = $this->Dbuse->getSpecificFaq($_GET['dbidx'])[0];
            // var_dump($specific_db);

            $dbform = '


                <form action="Keywordcontrol/updateFaq" method="post">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">FAQ 수정</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body">

                            <input type="hidden" value="'.$this->security->get_csrf_hash().'" name="'.$this->security->get_csrf_token_name().'">
                            <input type="hidden" class="form-control" name="link_index"  value="'.$_GET['dbidx'].'">

                            <div class="form-group">
                                <label>질문</label>
                                <input type="text" class="form-control" name="question"  value="'.$specific_db['buttonName'].'">
                            </div>                            
                            <div class="form-group">
                                <label>응답</label>
                                <input type="text" class="form-control" name="answer"  value="'.$specific_db['linkAddress'].'">
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">저장</button>
                    </div>
                </form>



            ';


            echo ($dbform);
        }




        public function getHireInfoPopup(): void
        {
            $this->load->model('Dbuse');
            $specific_db = $this->Dbuse->getSpecificHireInfo($_GET['dbidx'])[0];
            // var_dump($specific_db);




            $company_info = preg_replace("/\r\n|\n/",'',stripslashes($specific_db['company_info']));
            $company_info = str_replace("'","\'",$company_info);
            $company_info = str_replace('"','\"',$company_info);
            
            $recruit_info = preg_replace("/\r\n|\n/",'',stripslashes($specific_db['recruit_info']));
            $recruit_info = str_replace("'","\'",$recruit_info);
            $recruit_info = str_replace('"','\"',$recruit_info);



            $dbform = '


                <script type="text/javascript" src="../../../../smarteditor2/js/HuskyEZCreator.js"></script>

                <form action="Keywordcontrol/updateHireInfo" method="post" enctype="multipart/form-data">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">회사 정보 수정</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body">

                            <input type="hidden" value="'.$this->security->get_csrf_hash().'" name="'.$this->security->get_csrf_token_name().'">
                            <input type="hidden" class="form-control" name="company_index"  value="'.$_GET['dbidx'].'">
                            <br/><h4>기본 정보</h4><br/>
                            <div class="form-group">
                                <label>회사명</label>
                                <input type="text" class="form-control" name="company_name"  value="'.$specific_db['company_name'].'">
                            </div>                            
                            <div class="form-group">
                                <label>지역</label>
                                <input type="text" class="form-control" name="location"  value="'.$specific_db['location'].'">
                            </div>          
                            <div class="form-group">
                                <label>카테고리</label>

                                <select class="form-control" name="company_category" >
                                    <option selected="" >'.$specific_db['company_category'].'</option>
                                    <option >중앙부처</option>
                                    <option >헌법기관</option>
                                    <option >지자체</option>
                                    <option >공공기관</option>
                                </select>

                            </div>                      
                            <div class="form-group">
                                <label>태그 ( 띄어쓰기 없이 , 로 구분하세요. )</label>
                                <input type="text" class="form-control" name="company_tags"  value="'.$specific_db['company_tags'].'">
                            </div>                    
                            
                            

                            <br/><h4>이미지 파일</h4><br/>

                            <div class="form-group">
                                <label>회사 로고</label>
                                <div>
                                </div>';
                                if ($specific_db['logo_image'] != Null){
                                    $dbform .='
                                        <img style="max-width:150px;" src="http://publicservicefair.kr/uploaded_images/'.$specific_db['logo_image'].'">
                                        삭제 : 
                                        <select class="form-control" style="max-width:150px;display:inline;" name="logo_image_keep" >
                                            <option value="1" selected="" >유지</option>
                                            <option value="0" >삭제</option>
                                        </select>                                     
                                    ';
                                }



                                $dbform .=
                                '
                                <input type="file" name="logo_image" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>    
                            

                            <div class="form-group">
                                <label>슬라이드 이미지</label>    <br>                        
                            ';

                            if ($specific_db['slide_images'] != Null){
                                $slide_images=explode('//',$specific_db['slide_images']);
                                $ii=0;
                                foreach ($slide_images as $slide_image) {
                                $dbform .='
                                    <img style="max-width:150px;" src="http://publicservicefair.kr/uploaded_images/'.$slide_image.'">
                                    삭제 : 
                                    <select class="form-control" style="max-width:150px;display:inline;" name="slide_image_keep_'.$ii.'" >
                                        <option value="1" selected="" >유지</option>
                                        <option value="0" >삭제</option>
                                    </select>                                     
                                
                                ';   
                                $ii+=1;                                     
                                }
                                
                            }




                            $dbform .='
                            </div>


                            <div class="form-group">
                                <input type="file" name="extra_image_one" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 1">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>


                            <div class="form-group">
                                <input type="file" name="extra_image_two" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 2">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>


                            <div class="form-group">
                                <input type="file" name="extra_image_three" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 3">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="file" name="extra_image_four" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 4">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>

                            
                            <div class="form-group">
                                <input type="file" name="extra_image_five" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 5">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>

                            <br/><h4>회사 설명</h4><br/>
                            <div class="form-group">
                                <label>기관 정보</label>
                                <textarea type="text" id="company_info" class="form-control" name="company_info" style="height:250px;"  >'.$specific_db['company_info'].'</textarea>
                            </div>                      
                            <div class="form-group">
                                <label>채용 정보</label>
                                <textarea type="text" id="recruit_info" class="form-control" name="recruit_info" style="height:250px;"  >'.$specific_db['recruit_info'].'</textarea>

                            </div>                      
                            <div class="form-group">
                                ';

                                if ($specific_db['faqs']!=""){
                                    $faqs = explode('%%%', $specific_db['faqs']);
                                }
                                

                                for ($i = 1; $i <= 10; $i++){
                                    
                                    if ($specific_db['faqs']!=""){
                                        if ($i-1<count($faqs)){
                                            $dbform .='
                                            <label>FAQ '.$i.'</label>
                                            <input type="text" class="form-control" name="faqs_q'.$i.'" placeholder="질문" value="'.explode('^A^',explode('^Q^',$faqs[$i-1])[1])[0].'">
    
                                            <textarea type="text" class="form-control" name="faqs_a'.$i.'" style="height:100px;" placeholder="답변" >'.explode('^A^',explode('^Q^',$faqs[$i-1])[1])[1].'</textarea>                                   
                                            ';
                                        }
                                        else{
                                            $dbform .='
                                            <label>FAQ '.$i.'</label>
                                            <input type="text" class="form-control" name="faqs_q'.$i.'" placeholder="질문" value="">
    
                                            <textarea type="text" class="form-control" name="faqs_a'.$i.'" style="height:100px;" placeholder="답변" ></textarea>    
                                            ';
                                        }
                                    }
                                    else{
                                        
                                        $dbform .='
                                        <label>FAQ '.$i.'</label>
                                        <input type="text" class="form-control" name="faqs_q'.$i.'" placeholder="질문" value="">

                                        <textarea type="text" class="form-control" name="faqs_a'.$i.'" style="height:100px;" placeholder="답변" ></textarea>    
                                        ';
                                    }
                                };





                                $dbform .='
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button onclick="submitContents();" class="btn btn-primary">저장</button>
                    </div>
                </form>












                <script type="text/javascript">

                var oEditors = [];
                var oEditors2 = [];
                
                
                
                var sLang = "ko_KR"; // 언어 (ko_KR/ en_US/ ja_JP/ zh_CN/ zh_TW), default = ko_KR
                
                // 추가 글꼴 목록
                //var aAdditionalFontSet = [["MS UI Gothic", "MS UI Gothic"], ["Comic Sans MS", "Comic Sans MS"],["TEST","TEST"]];
                
                
                
                nhn.husky.EZCreator.createInIFrame({
                 oAppRef: oEditors,
                 elPlaceHolder: "company_info",
                 sSkinURI: "../../../../smarteditor2/SmartEditor2Skin.html", 
                 htParams : {
                  bUseToolbar : true,    // 툴바 사용 여부 (true:사용/ false:사용하지 않음)
                  bUseVerticalResizer : true,  // 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
                  bUseModeChanger : true,   // 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
                  bSkipXssFilter : true,  // client-side xss filter 무시 여부 (true:사용하지 않음 / 그외:사용)
                  //aAdditionalFontList : aAdditionalFontSet,  // 추가 글꼴 목록
                  fOnBeforeUnload : function(){
                   //alert("완료!");
                  },
                  I18N_LOCALE : sLang
                 }, //boolean
                 fOnAppLoad : function(){
                  //예제 코드
                  oEditors.getById["company_info"].exec("PASTE_HTML", ["<?=$company_info?>"]);
                 },
                 fCreator: "createSEditor2"
                });
                
                
                nhn.husky.EZCreator.createInIFrame({
                 oAppRef: oEditors2,
                 elPlaceHolder: "recruit_info",
                 sSkinURI: "../../../../smarteditor2/SmartEditor2Skin.html", 
                 htParams : {
                  bUseToolbar : true,    // 툴바 사용 여부 (true:사용/ false:사용하지 않음)
                  bUseVerticalResizer : true,  // 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
                  bUseModeChanger : true,   // 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
                  bSkipXssFilter : true,  // client-side xss filter 무시 여부 (true:사용하지 않음 / 그외:사용)
                  //aAdditionalFontList : aAdditionalFontSet,  // 추가 글꼴 목록
                  fOnBeforeUnload : function(){
                   //alert("완료!");
                  },
                  I18N_LOCALE : sLang
                 }, //boolean
                 fOnAppLoad : function(){
                  //예제 코드
                  oEditors2.getById["recruit_info"].exec("PASTE_HTML", ["<?=$recruit_info?>"]);
                 },
                 fCreator: "createSEditor2"
                });
                
                
                
                function pasteHTML() {
                 var sHTML = "<span style=\'color:#FF0000;\'>이미지도 같은 방식으로 삽입합니다.<\/span>";
                 oEditors.getById["company_info"].exec("PASTE_HTML", [sHTML]);
                }
                
                function showHTML() {
                 var sHTML = oEditors.getById["company_info"].getIR();
                 alert(sHTML);
                }
                 
                function submitContents() {


                 oEditors.getById["company_info"].exec("UPDATE_CONTENTS_FIELD", []); // 에디터의 내용이 textarea에 적용됩니다.
                 oEditors2.getById["recruit_info"].exec("UPDATE_CONTENTS_FIELD", []); // 에디터의 내용이 textarea에 적용됩니다.
                 
                 // 에디터의 내용에 대한 값 검증은 이곳에서 document.getElementById("company_info").value를 이용해서 처리하면 됩니다.
                
                  $(\'form\').submit()




                }
                
                
                
                function setDefaultFont() {
                 var sDefaultFont = \'궁서\';
                 var nFontSize = 24;
                 oEditors.getById["company_info"].setDefaultFont(sDefaultFont, nFontSize);
                
                 oEditors.getById["recruit_info"].setDefaultFont(sDefaultFont, nFontSize);
                }
                
                
                function writeReset() {
                 document.f.reset();
                 oEditors.getById["company_info"].exec("SET_IR", ["'.$company_info.'"]);
                 oEditors.getById["recruit_info"].exec("SET_IR", ["'.$recruit_info.'"]);
                }
                
                </script>






            ';


            echo ($dbform);
        }






        public function getNewHireInfo(): void
        {
            // $this->load->model('Dbuse');
            // $specific_db = $this->Dbuse->getSpecificDB($_GET['dbidx'])[0];
            // // var_dump($specific_db);

            $dbform = '


                <script type="text/javascript" src="../../../../smarteditor2/js/HuskyEZCreator.js"></script>




                <form action="Keywordcontrol/createNewHireInfo" method="post"  enctype="multipart/form-data">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">회사 추가 생성</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body">

                            <input type="hidden" value="'.$this->security->get_csrf_hash().'" name="'.$this->security->get_csrf_token_name().'">
                            <br><h4>기본 정보</h4><br>
                            <div class="form-group">
                                <label>회사명</label>
                                <input type="text" class="form-control" name="company_name" value="">
                            </div>                            
                            <div class="form-group">
                                <label>지역</label>
                                <input type="text" class="form-control" name="location" value="">
                            </div>          
                            <div class="form-group">
                                <label>카테고리</label>

                                <select class="form-control" name="company_category">
                                    <option selected="" disabled="">선택안함</option>
                                    <option>중앙부처</option>
                                    <option>헌법기관</option>
                                    <option>지자체</option>
                                    <option>공공기관</option>
                                </select>

                            </div>                      
                            <div class="form-group">
                                <label>태그 ( 띄어쓰기 없이 , 로 구분하세요. )</label>
                                <input type="text" class="form-control" name="company_tags" value="">
                            </div>                    
                            
                            

                            <br><h4>이미지 파일</h4><br>

                            <div class="form-group">
                                <label>회사 로고</label>
                                <div>
                                </div>
                                    
                                <input type="file" name="logo_image" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>    
                            

                            <div class="form-group">
                                <label>슬라이드 이미지</label>    <br>                        
                            
                            </div>


                            <div class="form-group">
                                <input type="file" name="extra_image_one" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 1">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>


                            <div class="form-group">
                                <input type="file" name="extra_image_two" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 2">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>


                            <div class="form-group">
                                <input type="file" name="extra_image_three" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 3">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="file" name="extra_image_four" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 4">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>

                            
                            <div class="form-group">
                                <input type="file" name="extra_image_five" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 5">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>

                            <br><h4>회사 설명</h4><br>
                            <div class="form-group">
                                <label>기관 정보</label>
                                <textarea type="text" class="form-control" id="company_info" name="company_info" style="height:250px;"></textarea>
                            </div>                      
                            <div class="form-group">
                                <label>채용 정보</label>
                                <textarea type="text" class="form-control" id="recruit_info" name="recruit_info" style="height:250px;"></textarea>

                            </div>                      
                            <div class="form-group">
                                
                                        <label>FAQ 1</label>
                                        <input type="text" class="form-control" name="faqs_q1" placeholder="질문" value="">

                                        <textarea type="text" class="form-control" name="faqs_a1" style="height:100px;" placeholder="답변"></textarea>                                   
                                        
                                        <label>FAQ 2</label>
                                        <input type="text" class="form-control" name="faqs_q2" placeholder="질문" value="">

                                        <textarea type="text" class="form-control" name="faqs_a2" style="height:100px;" placeholder="답변"></textarea>                                   
                                        
                                        <label>FAQ 3</label>
                                        <input type="text" class="form-control" name="faqs_q3" placeholder="질문" value="">

                                        <textarea type="text" class="form-control" name="faqs_a3" style="height:100px;" placeholder="답변"></textarea>    
                                        
                                        <label>FAQ 4</label>
                                        <input type="text" class="form-control" name="faqs_q4" placeholder="질문" value="">

                                        <textarea type="text" class="form-control" name="faqs_a4" style="height:100px;" placeholder="답변"></textarea>    
                                        
                                        <label>FAQ 5</label>
                                        <input type="text" class="form-control" name="faqs_q5" placeholder="질문" value="">

                                        <textarea type="text" class="form-control" name="faqs_a5" style="height:100px;" placeholder="답변"></textarea>    
                                        
                                        <label>FAQ 6</label>
                                        <input type="text" class="form-control" name="faqs_q6" placeholder="질문" value="">

                                        <textarea type="text" class="form-control" name="faqs_a6" style="height:100px;" placeholder="답변"></textarea>    
                                        
                                        <label>FAQ 7</label>
                                        <input type="text" class="form-control" name="faqs_q7" placeholder="질문" value="">

                                        <textarea type="text" class="form-control" name="faqs_a7" style="height:100px;" placeholder="답변"></textarea>    
                                        
                                        <label>FAQ 8</label>
                                        <input type="text" class="form-control" name="faqs_q8" placeholder="질문" value="">

                                        <textarea type="text" class="form-control" name="faqs_a8" style="height:100px;" placeholder="답변"></textarea>    
                                        
                                        <label>FAQ 9</label>
                                        <input type="text" class="form-control" name="faqs_q9" placeholder="질문" value="">

                                        <textarea type="text" class="form-control" name="faqs_a9" style="height:100px;" placeholder="답변"></textarea>    
                                        
                                        <label>FAQ 10</label>
                                        <input type="text" class="form-control" name="faqs_q10" placeholder="질문" value="">

                                        <textarea type="text" class="form-control" name="faqs_a10" style="height:100px;" placeholder="답변"></textarea>    
                                        
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button onclick="submitContents();" class="btn btn-primary">저장</button>
                    </div>
                </form>



































                <script language="javascript">
                var oEditors = [];
                var oEditors2 = [];
                var sLang = "ko_KR"; // 언어 (ko_KR/ en_US/ ja_JP/ zh_CN/ zh_TW), default = ko_KR
                
                
                // 추가 글꼴 목록
                //var aAdditionalFontSet = [["MS UI Gothic", "MS UI Gothic"], ["Comic Sans MS", "Comic Sans MS"],["TEST","TEST"]];
                
                
                nhn.husky.EZCreator.createInIFrame({
                 oAppRef: oEditors,
                 elPlaceHolder: "company_info",
                 sSkinURI: "../../../../smarteditor2/SmartEditor2Skin.html", 
                 htParams : {
                  bUseToolbar : true,    // 툴바 사용 여부 (true:사용/ false:사용하지 않음)
                  bUseVerticalResizer : true,  // 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
                
                 
                
                  bUseModeChanger : true,   // 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
                
                 
                
                  bSkipXssFilter : true,  // client-side xss filter 무시 여부 (true:사용하지 않음 / 그외:사용)
                  //aAdditionalFontList : aAdditionalFontSet,  // 추가 글꼴 목록
                  fOnBeforeUnload : function(){
                   //alert("완료!");
                  },
                  I18N_LOCALE : sLang
                 }, //boolean
                 fOnAppLoad : function(){
                  //예제 코드
                  //oEditors.getById["company_info"].exec("PASTE_HTML", ["로딩이 완료된 후에 본문에 삽입되는 text입니다."]);
                 },
                 fCreator: "createSEditor2"
                });
                
                
                
                nhn.husky.EZCreator.createInIFrame({
                 oAppRef: oEditors2,
                 elPlaceHolder: "recruit_info",
                 sSkinURI: "../../../../smarteditor2/SmartEditor2Skin.html", 
                 htParams : {
                  bUseToolbar : true,    // 툴바 사용 여부 (true:사용/ false:사용하지 않음)
                  bUseVerticalResizer : true,  // 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
                  bUseModeChanger : true,   // 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
                  bSkipXssFilter : true,  // client-side xss filter 무시 여부 (true:사용하지 않음 / 그외:사용)
                  //aAdditionalFontList : aAdditionalFontSet,  // 추가 글꼴 목록
                  fOnBeforeUnload : function(){
                   //alert("완료!");
                  },
                  I18N_LOCALE : sLang
                 }, //boolean
                 fOnAppLoad : function(){
                  //예제 코드
                  //oEditors2.getById["recruit_info"].exec("PASTE_HTML", ["로딩이 완료된 후에 본문에 삽입되는 text입니다."]);
                 },
                 fCreator: "createSEditor2"
                });
                
                
                
                function pasteHTML(filepath) {
                // var sHTML = "<span style=\'color:#FF0000;\'>이미지도 같은 방식으로 삽입합니다.<\/span>";
                 var sHTML = \'<span style="color:#FF0000;"><img src="\'+filepath+\'"></span>\';
                 oEditors.getById["company_info"].exec("PASTE_HTML", [sHTML]);
                 oEditors2.getById["recruit_info"].exec("PASTE_HTML", [sHTML]);
                
                }
                
                
                
                function showHTML() {
                 var sHTML = oEditors.getById["company_info"].getIR();
                 alert(sHTML);
                }
                 
                function submitContents() {
                 oEditors.getById["company_info"].exec("UPDATE_CONTENTS_FIELD", []); // 에디터의 내용이 textarea에 적용됩니다.
                 oEditors2.getById["recruit_info"].exec("UPDATE_CONTENTS_FIELD", []); // 에디터의 내용이 textarea에 적용됩니다.
                 
                 // 에디터의 내용에 대한 값 검증은 이곳에서 document.getElementById("company_info").value를 이용해서 처리하면 됩니다.
                
                  $(\'form\').submit()


                }
                
                
                
                function setDefaultFont() {
                 var sDefaultFont = \'궁서\';
                 var nFontSize = 24;
                 oEditors.getById["company_info"].setDefaultFont(sDefaultFont, nFontSize);
                 oEditors.getById["recruit_info"].setDefaultFont(sDefaultFont, nFontSize);
                }
                
                
                function writeReset() {
                 document.f.reset();
                 oEditors.getById["company_info"].exec("SET_IR", [""]);
                 oEditors2.getById["recruit_info"].exec("SET_IR", [""]);
                }
                </script>












            ';


            echo ($dbform);
        }



















































































        public function getEventInfoPopup(): void
        {
            $this->load->model('Dbuse');
            $specific_db = $this->Dbuse->getSpecificEventInfo($_GET['dbidx'])[0];
            // var_dump($specific_db);




            $company_info = preg_replace("/\r\n|\n/",'',stripslashes($specific_db['company_info']));
            $company_info = str_replace("'","\'",$company_info);
            $company_info = str_replace('"','\"',$company_info);



            $dbform = '


                <script type="text/javascript" src="../../../../smarteditor2/js/HuskyEZCreator.js"></script>

                <form action="Keywordcontrol/updateEventInfo" method="post" enctype="multipart/form-data">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">이벤트 정보 수정</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body">

                            <input type="hidden" value="'.$this->security->get_csrf_hash().'" name="'.$this->security->get_csrf_token_name().'">
                            <input type="hidden" class="form-control" name="company_index"  value="'.$_GET['dbidx'].'">
                            <br/><h4>기본 정보</h4><br/>
                            <div class="form-group">
                                <label>이벤트명</label>
                                <input type="text" class="form-control" name="company_name"  value="'.$specific_db['company_name'].'">
                            </div>                            
                            <div class="form-group">
                                <label>링크</label>
                                <input type="text" class="form-control" name="location"  value="'.$specific_db['location'].'">
                            </div>        
                            
                            

                            <br/><h4>이미지 파일</h4><br/>


                            <div class="form-group">
                                <label>슬라이드 이미지</label>    <br>                        
                            ';

                            if ($specific_db['slide_images'] != Null){
                                $slide_images=explode('//',$specific_db['slide_images']);
                                $ii=0;
                                foreach ($slide_images as $slide_image) {
                                $dbform .='
                                    <img style="max-width:150px;" src="http://publicservicefair.kr/uploaded_images/'.$slide_image.'">
                                    삭제 : 
                                    <select class="form-control" style="max-width:150px;display:inline;" name="slide_image_keep_'.$ii.'" >
                                        <option value="1" selected="" >유지</option>
                                        <option value="0" >삭제</option>
                                    </select>                                     
                                
                                ';   
                                $ii+=1;                                     
                                }
                                
                            }




                            $dbform .='
                            </div>


                            <div class="form-group">
                                <input type="file" name="extra_image_one" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 1">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>


                            <div class="form-group">
                                <input type="file" name="extra_image_two" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 2">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>


                            <div class="form-group">
                                <input type="file" name="extra_image_three" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 3">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="file" name="extra_image_four" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 4">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>

                            
                            <div class="form-group">
                                <input type="file" name="extra_image_five" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 5">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>

                            <br/><h4>이벤트 설명</h4><br/>
                            <div class="form-group">
                                <textarea type="text" id="company_info" class="form-control" name="company_info" style="height:250px;"  >'.$specific_db['company_info'].'</textarea>
                            </div>       

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button onclick="submitContents();" class="btn btn-primary">저장</button>
                    </div>
                </form>


                <form action="Keywordcontrol/deleteEventInfo" method="post" enctype="multipart/form-data">


                    <input type="hidden" value="'.$this->security->get_csrf_hash().'" name="'.$this->security->get_csrf_token_name().'">
                    <input type="hidden" class="form-control" name="company_index"  value="'.$_GET['dbidx'].'">

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-urgent">삭제</button>
                    </div>
                </form>







                <script type="text/javascript">

                var oEditors = [];
                
                
                
                var sLang = "ko_KR"; // 언어 (ko_KR/ en_US/ ja_JP/ zh_CN/ zh_TW), default = ko_KR
                
                // 추가 글꼴 목록
                //var aAdditionalFontSet = [["MS UI Gothic", "MS UI Gothic"], ["Comic Sans MS", "Comic Sans MS"],["TEST","TEST"]];
                
                
                
                nhn.husky.EZCreator.createInIFrame({
                 oAppRef: oEditors,
                 elPlaceHolder: "company_info",
                 sSkinURI: "../../../../smarteditor2/SmartEditor2Skin.html", 
                 htParams : {
                  bUseToolbar : true,    // 툴바 사용 여부 (true:사용/ false:사용하지 않음)
                  bUseVerticalResizer : true,  // 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
                  bUseModeChanger : true,   // 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
                  bSkipXssFilter : true,  // client-side xss filter 무시 여부 (true:사용하지 않음 / 그외:사용)
                  //aAdditionalFontList : aAdditionalFontSet,  // 추가 글꼴 목록
                  fOnBeforeUnload : function(){
                   //alert("완료!");
                  },
                  I18N_LOCALE : sLang
                 }, //boolean
                 fOnAppLoad : function(){
                  //예제 코드
                  oEditors.getById["company_info"].exec("PASTE_HTML", ["<?=$company_info?>"]);
                 },
                 fCreator: "createSEditor2"
                });
                
                
                
                function pasteHTML() {
                 var sHTML = "<span style=\'color:#FF0000;\'>이미지도 같은 방식으로 삽입합니다.<\/span>";
                 oEditors.getById["company_info"].exec("PASTE_HTML", [sHTML]);
                }
                
                function showHTML() {
                 var sHTML = oEditors.getById["company_info"].getIR();
                 alert(sHTML);
                }
                 
                function submitContents() {


                 oEditors.getById["company_info"].exec("UPDATE_CONTENTS_FIELD", []); // 에디터의 내용이 textarea에 적용됩니다.
                 
                 // 에디터의 내용에 대한 값 검증은 이곳에서 document.getElementById("company_info").value를 이용해서 처리하면 됩니다.
                
                  $(\'form\').submit()




                }
                
                
                
                function setDefaultFont() {
                 var sDefaultFont = \'궁서\';
                 var nFontSize = 24;
                 oEditors.getById["company_info"].setDefaultFont(sDefaultFont, nFontSize);
                
                }
                
                
                function writeReset() {
                 document.f.reset();
                 oEditors.getById["company_info"].exec("SET_IR", ["'.$company_info.'"]);
                }
                
                </script>






            ';


            echo ($dbform);
        }






        public function getNewEventInfo(): void
        {
            // $this->load->model('Dbuse');
            // $specific_db = $this->Dbuse->getSpecificDB($_GET['dbidx'])[0];
            // // var_dump($specific_db);

            $dbform = '


                <script type="text/javascript" src="../../../../smarteditor2/js/HuskyEZCreator.js"></script>




                <form action="Keywordcontrol/createNewEventInfo" method="post"  enctype="multipart/form-data">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">이벤트 추가 생성</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body">

                            <input type="hidden" value="'.$this->security->get_csrf_hash().'" name="'.$this->security->get_csrf_token_name().'">
                            <br><h4>기본 정보</h4><br>
                            <div class="form-group">
                                <label>이벤트명</label>
                                <input type="text" class="form-control" name="company_name" value="">
                            </div>                            
                            <div class="form-group">
                                <label>링크</label>
                                <input type="text" class="form-control" name="location" value="">
                            </div>          
                            

                            <br><h4>이미지 파일</h4><br>


                            <div class="form-group">
                                <label>슬라이드 이미지</label>    <br>                        
                            
                            </div>


                            <div class="form-group">
                                <input type="file" name="extra_image_one" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 1">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>


                            <div class="form-group">
                                <input type="file" name="extra_image_two" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 2">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>


                            <div class="form-group">
                                <input type="file" name="extra_image_three" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 3">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="file" name="extra_image_four" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 4">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>

                            
                            <div class="form-group">
                                <input type="file" name="extra_image_five" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 5">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>

                            <br><h4>이벤트 설명</h4><br>
                            <div class="form-group">
                                <textarea type="text" class="form-control" id="company_info" name="company_info" style="height:250px;"></textarea>
                            </div>               

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button onclick="submitContents();" class="btn btn-primary">저장</button>
                    </div>
                </form>


















                <script language="javascript">
                var oEditors = [];
                var oEditors2 = [];
                var sLang = "ko_KR"; // 언어 (ko_KR/ en_US/ ja_JP/ zh_CN/ zh_TW), default = ko_KR
                
                
                // 추가 글꼴 목록
                //var aAdditionalFontSet = [["MS UI Gothic", "MS UI Gothic"], ["Comic Sans MS", "Comic Sans MS"],["TEST","TEST"]];
                
                
                nhn.husky.EZCreator.createInIFrame({
                 oAppRef: oEditors,
                 elPlaceHolder: "company_info",
                 sSkinURI: "../../../../smarteditor2/SmartEditor2Skin.html", 
                 htParams : {
                  bUseToolbar : true,    // 툴바 사용 여부 (true:사용/ false:사용하지 않음)
                  bUseVerticalResizer : true,  // 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
                
                 
                
                  bUseModeChanger : true,   // 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
                
                 
                
                  bSkipXssFilter : true,  // client-side xss filter 무시 여부 (true:사용하지 않음 / 그외:사용)
                  //aAdditionalFontList : aAdditionalFontSet,  // 추가 글꼴 목록
                  fOnBeforeUnload : function(){
                   //alert("완료!");
                  },
                  I18N_LOCALE : sLang
                 }, //boolean
                 fOnAppLoad : function(){
                  //예제 코드
                  //oEditors.getById["company_info"].exec("PASTE_HTML", ["로딩이 완료된 후에 본문에 삽입되는 text입니다."]);
                 },
                 fCreator: "createSEditor2"
                });
                
                
                
                function pasteHTML(filepath) {
                // var sHTML = "<span style=\'color:#FF0000;\'>이미지도 같은 방식으로 삽입합니다.<\/span>";
                 var sHTML = \'<span style="color:#FF0000;"><img src="\'+filepath+\'"></span>\';
                 oEditors.getById["company_info"].exec("PASTE_HTML", [sHTML]);
                
                }
                
                
                
                function showHTML() {
                 var sHTML = oEditors.getById["company_info"].getIR();
                 alert(sHTML);
                }
                 
                function submitContents() {
                 oEditors.getById["company_info"].exec("UPDATE_CONTENTS_FIELD", []); // 에디터의 내용이 textarea에 적용됩니다.
                 
                 // 에디터의 내용에 대한 값 검증은 이곳에서 document.getElementById("company_info").value를 이용해서 처리하면 됩니다.
                
                  $(\'form\').submit()


                }
                
                
                
                function setDefaultFont() {
                 var sDefaultFont = \'궁서\';
                 var nFontSize = 24;
                 oEditors.getById["company_info"].setDefaultFont(sDefaultFont, nFontSize);
                }
                
                
                function writeReset() {
                 document.f.reset();
                 oEditors.getById["company_info"].exec("SET_IR", [""]);
                }
                </script>












            ';


            echo ($dbform);
        }












































































        








































































        public function getEventResultInfoPopup(): void
        {
            $this->load->model('Dbuse');
            $specific_db = $this->Dbuse->getSpecificEventResultInfo($_GET['dbidx'])[0];
            // var_dump($specific_db);




            $company_info = preg_replace("/\r\n|\n/",'',stripslashes($specific_db['company_info']));
            $company_info = str_replace("'","\'",$company_info);
            $company_info = str_replace('"','\"',$company_info);



            $dbform = '


                <script type="text/javascript" src="../../../../smarteditor2/js/HuskyEZCreator.js"></script>

                <form action="Keywordcontrol/updateEventResultInfo" method="post" enctype="multipart/form-data">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">이벤트 결과 정보 수정</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body">

                            <input type="hidden" value="'.$this->security->get_csrf_hash().'" name="'.$this->security->get_csrf_token_name().'">
                            <input type="hidden" class="form-control" name="company_index"  value="'.$_GET['dbidx'].'">
                            <br/><h4>기본 정보</h4><br/>
                            <div class="form-group">
                                <label>이벤트명</label>
                                <input type="text" class="form-control" name="company_name"  value="'.$specific_db['company_name'].'">
                            </div>                            
                            <div class="form-group">
                                <label>링크</label>
                                <input type="text" class="form-control" name="location"  value="'.$specific_db['location'].'">
                            </div>        
                            
                            

                            <br/><h4>이미지 파일</h4><br/>


                            <div class="form-group">
                                <label>슬라이드 이미지</label>    <br>                        
                            ';

                            if ($specific_db['slide_images'] != Null){
                                $slide_images=explode('//',$specific_db['slide_images']);
                                $ii=0;
                                foreach ($slide_images as $slide_image) {
                                $dbform .='
                                    <img style="max-width:150px;" src="http://publicservicefair.kr/uploaded_images/'.$slide_image.'">
                                    삭제 : 
                                    <select class="form-control" style="max-width:150px;display:inline;" name="slide_image_keep_'.$ii.'" >
                                        <option value="1" selected="" >유지</option>
                                        <option value="0" >삭제</option>
                                    </select>                                     
                                
                                ';   
                                $ii+=1;                                     
                                }
                                
                            }




                            $dbform .='
                            </div>


                            <div class="form-group">
                                <input type="file" name="extra_image_one" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 1">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>


                            <div class="form-group">
                                <input type="file" name="extra_image_two" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 2">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>


                            <div class="form-group">
                                <input type="file" name="extra_image_three" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 3">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="file" name="extra_image_four" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 4">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>

                            
                            <div class="form-group">
                                <input type="file" name="extra_image_five" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 5">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>

                            <br/><h4>이벤트 설명</h4><br/>
                            <div class="form-group">
                                <textarea type="text" id="company_info" class="form-control" name="company_info" style="height:250px;"  >'.$specific_db['company_info'].'</textarea>
                            </div>       

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button onclick="submitContents();" class="btn btn-primary">저장</button>
                    </div>
                </form>


                <form action="Keywordcontrol/deleteEventResultInfo" method="post" enctype="multipart/form-data">


                    <input type="hidden" value="'.$this->security->get_csrf_hash().'" name="'.$this->security->get_csrf_token_name().'">
                    <input type="hidden" class="form-control" name="company_index"  value="'.$_GET['dbidx'].'">

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-urgent">삭제</button>
                    </div>
                </form>







                <script type="text/javascript">

                var oEditors = [];
                
                
                
                var sLang = "ko_KR"; // 언어 (ko_KR/ en_US/ ja_JP/ zh_CN/ zh_TW), default = ko_KR
                
                // 추가 글꼴 목록
                //var aAdditionalFontSet = [["MS UI Gothic", "MS UI Gothic"], ["Comic Sans MS", "Comic Sans MS"],["TEST","TEST"]];
                
                
                
                nhn.husky.EZCreator.createInIFrame({
                 oAppRef: oEditors,
                 elPlaceHolder: "company_info",
                 sSkinURI: "../../../../smarteditor2/SmartEditor2Skin.html", 
                 htParams : {
                  bUseToolbar : true,    // 툴바 사용 여부 (true:사용/ false:사용하지 않음)
                  bUseVerticalResizer : true,  // 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
                  bUseModeChanger : true,   // 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
                  bSkipXssFilter : true,  // client-side xss filter 무시 여부 (true:사용하지 않음 / 그외:사용)
                  //aAdditionalFontList : aAdditionalFontSet,  // 추가 글꼴 목록
                  fOnBeforeUnload : function(){
                   //alert("완료!");
                  },
                  I18N_LOCALE : sLang
                 }, //boolean
                 fOnAppLoad : function(){
                  //예제 코드
                  oEditors.getById["company_info"].exec("PASTE_HTML", ["<?=$company_info?>"]);
                 },
                 fCreator: "createSEditor2"
                });
                
                
                
                function pasteHTML() {
                 var sHTML = "<span style=\'color:#FF0000;\'>이미지도 같은 방식으로 삽입합니다.<\/span>";
                 oEditors.getById["company_info"].exec("PASTE_HTML", [sHTML]);
                }
                
                function showHTML() {
                 var sHTML = oEditors.getById["company_info"].getIR();
                 alert(sHTML);
                }
                 
                function submitContents() {


                 oEditors.getById["company_info"].exec("UPDATE_CONTENTS_FIELD", []); // 에디터의 내용이 textarea에 적용됩니다.
                 
                 // 에디터의 내용에 대한 값 검증은 이곳에서 document.getElementById("company_info").value를 이용해서 처리하면 됩니다.
                
                  $(\'form\').submit()




                }
                
                
                
                function setDefaultFont() {
                 var sDefaultFont = \'궁서\';
                 var nFontSize = 24;
                 oEditors.getById["company_info"].setDefaultFont(sDefaultFont, nFontSize);
                
                }
                
                
                function writeReset() {
                 document.f.reset();
                 oEditors.getById["company_info"].exec("SET_IR", ["'.$company_info.'"]);
                }
                
                </script>






            ';


            echo ($dbform);
        }






        public function getNewEventResultInfo(): void
        {
            // $this->load->model('Dbuse');
            // $specific_db = $this->Dbuse->getSpecificDB($_GET['dbidx'])[0];
            // // var_dump($specific_db);

            $dbform = '


                <script type="text/javascript" src="../../../../smarteditor2/js/HuskyEZCreator.js"></script>




                <form action="Keywordcontrol/createNewEventResultInfo" method="post"  enctype="multipart/form-data">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">이벤트 결과 추가 생성</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body">

                            <input type="hidden" value="'.$this->security->get_csrf_hash().'" name="'.$this->security->get_csrf_token_name().'">
                            <br><h4>기본 정보</h4><br>
                            <div class="form-group">
                                <label>이벤트명</label>
                                <input type="text" class="form-control" name="company_name" value="">
                            </div>                            
                            <div class="form-group">
                                <label>링크</label>
                                <input type="text" class="form-control" name="location" value="">
                            </div>          
                            

                            <br><h4>이미지 파일</h4><br>


                            <div class="form-group">
                                <label>슬라이드 이미지</label>    <br>                        
                            
                            </div>


                            <div class="form-group">
                                <input type="file" name="extra_image_one" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 1">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>


                            <div class="form-group">
                                <input type="file" name="extra_image_two" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 2">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>


                            <div class="form-group">
                                <input type="file" name="extra_image_three" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 3">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="file" name="extra_image_four" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 4">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>

                            
                            <div class="form-group">
                                <input type="file" name="extra_image_five" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="슬라이드 이미지 추가 5">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                            </div>

                            <br><h4>이벤트 설명</h4><br>
                            <div class="form-group">
                                <textarea type="text" class="form-control" id="company_info" name="company_info" style="height:250px;"></textarea>
                            </div>               

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button onclick="submitContents();" class="btn btn-primary">저장</button>
                    </div>
                </form>


















                <script language="javascript">
                var oEditors = [];
                var oEditors2 = [];
                var sLang = "ko_KR"; // 언어 (ko_KR/ en_US/ ja_JP/ zh_CN/ zh_TW), default = ko_KR
                
                
                // 추가 글꼴 목록
                //var aAdditionalFontSet = [["MS UI Gothic", "MS UI Gothic"], ["Comic Sans MS", "Comic Sans MS"],["TEST","TEST"]];
                
                
                nhn.husky.EZCreator.createInIFrame({
                 oAppRef: oEditors,
                 elPlaceHolder: "company_info",
                 sSkinURI: "../../../../smarteditor2/SmartEditor2Skin.html", 
                 htParams : {
                  bUseToolbar : true,    // 툴바 사용 여부 (true:사용/ false:사용하지 않음)
                  bUseVerticalResizer : true,  // 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
                
                 
                
                  bUseModeChanger : true,   // 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
                
                 
                
                  bSkipXssFilter : true,  // client-side xss filter 무시 여부 (true:사용하지 않음 / 그외:사용)
                  //aAdditionalFontList : aAdditionalFontSet,  // 추가 글꼴 목록
                  fOnBeforeUnload : function(){
                   //alert("완료!");
                  },
                  I18N_LOCALE : sLang
                 }, //boolean
                 fOnAppLoad : function(){
                  //예제 코드
                  //oEditors.getById["company_info"].exec("PASTE_HTML", ["로딩이 완료된 후에 본문에 삽입되는 text입니다."]);
                 },
                 fCreator: "createSEditor2"
                });
                
                
                
                function pasteHTML(filepath) {
                // var sHTML = "<span style=\'color:#FF0000;\'>이미지도 같은 방식으로 삽입합니다.<\/span>";
                 var sHTML = \'<span style="color:#FF0000;"><img src="\'+filepath+\'"></span>\';
                 oEditors.getById["company_info"].exec("PASTE_HTML", [sHTML]);
                
                }
                
                
                
                function showHTML() {
                 var sHTML = oEditors.getById["company_info"].getIR();
                 alert(sHTML);
                }
                 
                function submitContents() {
                 oEditors.getById["company_info"].exec("UPDATE_CONTENTS_FIELD", []); // 에디터의 내용이 textarea에 적용됩니다.
                 
                 // 에디터의 내용에 대한 값 검증은 이곳에서 document.getElementById("company_info").value를 이용해서 처리하면 됩니다.
                
                  $(\'form\').submit()


                }
                
                
                
                function setDefaultFont() {
                 var sDefaultFont = \'궁서\';
                 var nFontSize = 24;
                 oEditors.getById["company_info"].setDefaultFont(sDefaultFont, nFontSize);
                }
                
                
                function writeReset() {
                 document.f.reset();
                 oEditors.getById["company_info"].exec("SET_IR", [""]);
                }
                </script>












            ';


            echo ($dbform);
        }
































































        public function getNewDB(): void
        {
            // $this->load->model('Dbuse');
            // $specific_db = $this->Dbuse->getSpecificDB($_GET['dbidx'])[0];
            // // var_dump($specific_db);

            $dbform = '

                <form action="Keywordcontrol/createNewFaq" method="post">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">FAQ 생성</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body">
                        
                            <input type="hidden" value="'.$this->security->get_csrf_hash().'" name="'.$this->security->get_csrf_token_name().'">


                            <div class="form-group">
                                <label>질문</label>
                                <input type="text" class="form-control" name="question"  value="">
                            </div>                            
                            <div class="form-group">
                                <label>응답</label>
                                <input type="text" class="form-control" name="answer"  value="">
                            </div>



                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">생성</button>
                    </div>
                </form>




            ';


            echo ($dbform);
        }


    }
}
echo '<script src="https://quickcar.co.kr/assets/js/file-upload.js?lastModify=ae3750c786116d3ed3d1fea87bf66c5d"></script>';
