<?php

if (! class_exists('EventResult')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class EventResult extends CI_Controller
    {
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->load->view(
                'm_common/head',
                [

                'static' => [
                    'assets' =>loadManagerCommonAssets(),

                    'url'=>loadCommonUrl()
                ]
            ]
            );
        }



        public function index(): void
        {



            $this->load->model('Dbuse');
            $my_db = $this->Dbuse->getEventResultInfos();
            // var_dump($my_db);
            $servernum = $this->Dbuse->getServerNum()[0]['serverNum'];

            $validKeywordsCount = $this->Dbuse->getValidKeywordsCount()[0]['validCount'];

            if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                $this->load->view('Admin/Contents/EventResults', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                    'text'=> [
                        'menu_1' => "컨텐츠 관리",
                        'menu_2' => "이벤트 결과 관리",
                        'menu_3' => "",
                    ],
                    'db' => $my_db,
                    'serverNum' =>$servernum, 
                    'validKeywordsCount' =>$validKeywordsCount
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }

        }



        public function Result(): void
        {


            $this->load->model('Dbuse');
            $my_db = $this->Dbuse->getEventResultInfos();
            // var_dump($my_db);
            $servernum = $this->Dbuse->getServerNum()[0]['serverNum'];

            $validKeywordsCount = $this->Dbuse->getValidKeywordsCount()[0]['validCount'];

            if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                $this->load->view('Admin/Contents/EventResults', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                    'text'=> [
                        'menu_1' => "컨텐츠 관리",
                        'menu_2' => "이벤트 결과 관리",
                        'menu_3' => "",
                    ],
                    'db' => $my_db,
                    'serverNum' =>$servernum, 
                    'validKeywordsCount' =>$validKeywordsCount
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }

        }


    }
}
