<?php

if (! class_exists('PracticeTest')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class PracticeTest extends CI_Controller
    {
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
        }



        public function index(): void
        {

            if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                $this->load->view('Interview', [
                ]);
                $this->load->view('PC_attach', [
                ]);
            } else {
                show_404();
            }

        }






        public function PSAT(): void
        {

            if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                $this->load->view('PSAT', [
                ]);
                $this->load->view('PC_attach', [
                ]);
            } else {
                show_404();
            }

        }




        public function OnlineInterview(): void
        {



            $mobilechk = '/(iPod|iPhone|Android|BlackBerry|SymbianOS|SCH-M\d+|Opera Mini|Windows CE|Nokia|SonyEricsson|webOS|PalmOS)/i'; 

            // 모바일 접속인지 PC로 접속했는지 체크합니다.
            if(preg_match($mobilechk, $_SERVER['HTTP_USER_AGENT'])) {



                if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                    $this->load->view('Mobile/Interview', [
                    ]);
                    $this->load->view('Mobile/Menu', [
                    ]);
                } else {
                    show_404();
                }
            } else { 

                if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                    $this->load->view('Interview', [
                    ]);
                    $this->load->view('PC_attach', [
                    ]);
                } else {
                    show_404();
                }
            }





        }

        public function OnlineMentoring(): void
        {





            $mobilechk = '/(iPod|iPhone|Android|BlackBerry|SymbianOS|SCH-M\d+|Opera Mini|Windows CE|Nokia|SonyEricsson|webOS|PalmOS)/i'; 

            // 모바일 접속인지 PC로 접속했는지 체크합니다.
            if(preg_match($mobilechk, $_SERVER['HTTP_USER_AGENT'])) {



                if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                    $this->load->view('Mobile/Mentoring', [
                    ]);
                    $this->load->view('Mobile/Menu', [
                    ]);
                    $this->load->view('MentoringFooter', [
                        ]);         
                } else {
                    show_404();
                }
            } else { 

                if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                    $this->load->view('Mentoring', [
                    ]);
                    $this->load->view('MentoringFooter', [
                        ]);   
                        $this->load->view('PC_attach', [
                        ]);      
                } else {
                    show_404();
                }
            }





        }





        public function MentoringApply(): void
        {


            $data = trimArray($this->input->get([
                'name', 'tel', 'email', 'who', 'from', 'admitted'
            ], true));
            // var_dump($data);

            if ($data['admitted']!="yyy") {
                alert("개인정보의 수집·이용에 동의하지 않았습니다.", $this->config->site_url('PracticeTest/Mentoring'));
                return;
            }

            if ($data['name']=="" || $data['tel']==""|| $data['email']=="") {
                alert("성명, 연락처, 이메일은 필수 입력사항입니다.", $this->config->site_url('PracticeTest/Mentoring'));
                return;
            }


            $this->load->model('Dbuse');


            // $this->Dbuse->createNewKeywordGroup($data['groupname'], $data['keywords'], count(explode(',',$data['keywords'])));
            // alert("등록하였습니다.", $this->config->site_url('Seminar'));
            return;

        }


        

        public function InterviewApply(): void
        {


            $data = trimArray($this->input->get([
                'name', 'tel', 'email', 'who', 'from', 'admitted'
            ], true));
            // var_dump($data);

            if ($data['admitted']!="yyy") {
                alert("개인정보의 수집·이용에 동의하지 않았습니다.", $this->config->site_url('PracticeTest/Interview'));
                return;
            }

            if ($data['name']=="" || $data['tel']==""|| $data['email']=="") {
                alert("성명, 연락처, 이메일은 필수 입력사항입니다.", $this->config->site_url('PracticeTest/Interview'));
                return;
            }


            $this->load->model('Dbuse');


            // $this->Dbuse->createNewKeywordGroup($data['groupname'], $data['keywords'], count(explode(',',$data['keywords'])));
            // alert("등록하였습니다.", $this->config->site_url('Seminar'));
            return;

        }





    }
}
