<?php

if (! class_exists('RecruitInfo')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class RecruitInfo extends CI_Controller
    {
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
        }



        public function index(): void
        {



            $data = trimArray($this->input->post([
                'category', 'keyword','orderby'
            ], true));

            if ($data['orderby']==""){$data['orderby']="최신순";}




            $this->load->model('Dbuse');
            $my_db = $this->Dbuse->getHireInfoSearch($data['category'],$data['keyword'],$data['orderby'], 1);


            // var_dump($my_db);
            // var_dump($data);

            $mobilechk = '/(iPod|iPhone|Android|BlackBerry|SymbianOS|SCH-M\d+|Opera Mini|Windows CE|Nokia|SonyEricsson|webOS|PalmOS)/i'; 



            if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                $this->load->view('RecruitInfo', [
                ]);
                $this->load->view('PC_attach', [
                ]);
                $this->load->view('Recruit_attach', [
                    'data2' => $data,
                    'searched' =>$my_db
                ]);
            } else {
                show_404();
            }



        }





        public function detail(): void
        {



            $data = trimArray($this->input->get([
                'idx'
            ], true));



            $this->load->model('Dbuse');


            // echo "aaaaa";

            $my_db = $this->Dbuse->getSpecificHireInfoSearch($data['idx']);



            if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                $this->load->view('RecruitInfoDetail', [
                    'data' => $my_db,
                ]);
                $this->load->view('PC_attach', [
                ]);
                if ($data['idx']==35 || $data['idx']==54 || $data['idx']==59){
                    $this->load->view('RecruitInfoEvent');
                }
                if ($data['idx']==35){
                    


                    
                }
            } else {
                show_404();
            }



        }




        public function Event(): void
        {


            if ($_GET['idx']==35){
                    

                $this->load->model('Dbuse');
                $all_db = $this->Dbuse->eventGetToday($_GET['idx']);
                $my_db = $this->Dbuse->eventGetIp($_GET['idx'],$_SERVER["REMOTE_ADDR"]);
                
                if ($all_db[0]['todayCount']>=40){
                    echo "<script>alert('오늘의 이벤트 참여가 마감되었습니다.');location.href='http://publicservicefair.kr/RecruitInfo/Detail?idx=".$_GET['idx']."';</script>";
                }
                else if ($my_db[0]['ipCount']>=1){
                    echo "<script>alert('이미 참여한 이벤트입니다.');location.href='http://publicservicefair.kr/RecruitInfo/Detail?idx=".$_GET['idx']."';</script>";
                }
                else{
                    $this->Dbuse->eventInvited($_GET['idx'],$_SERVER["REMOTE_ADDR"]);
                    echo "<script>alert('이벤트 페이지로 이동합니다.');location.href='https://docs.google.com/forms/d/1jDwm-5ltGfSc1C05O9T27IXScW_3Mx9Zo-j3mNR5pcI/viewform?edit_requested=true';</script>";

                    // echo "  ".$my_db[0]['ipCount'];
                }
    
                    
            }



        }







        public function getmore(): void
        {





            $data = trimArray($this->input->get([
                'category', 'keyword','orderby', 'page'
            ], true));



            $this->load->model('Dbuse');
            $my_db = $this->Dbuse->getHireInfoSearch($data['category'],$data['keyword'],$data['orderby'], $data['page']);

            echo json_encode($my_db);
            // var_dump($my_db);
            // print_r($my_db,true);



        }





    }
}
