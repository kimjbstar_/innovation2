<?php

if (! class_exists('RequestLinks')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class RequestLinks extends CI_Controller
    {
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();

            header('Access-Control-Allow-Origin: *');

            header('Content-type: application/json'); 

        }
        public function index(): void
        {

            $file = $this->file;
            $load = $this->load;



            $data = trimArray($this->input->get([
                'page'
            ], true));
            // echo $data['page'];

            $this->load->model('Dbuse');
            $specific_db = $this->Dbuse->getSpecificButtonLinks($data['page']);
            
            print_r( json_encode( $specific_db ) );


        }
    }
}
