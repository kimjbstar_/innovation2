<?php

if (! class_exists('Gifticonlist')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Gifticonlist extends CI_Controller
    {

        /**
         * Gifticon API 용도 컨트롤러로 생성함.
         */
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->baseAssets = $this->file->getDefaultAssets();
            $this->lang->load('addStudio', 'korean');
            $this->load->model('User/User');
        }



        public function index(): void
        {
            $file = $this->file;
            $load = $this->load;
            $load->view('common/head', [
                'css' => $this->baseAssets['css'],
                'title' => '기프티콘 확인',
            ]);
            $load->view('Gifticonlist/body', [
                'img' => [
                    'back' => $file->getIconUrl('back.svg'),
                    'address' => $file->getIconUrl('marker.svg'),
                    'phone' => $file->getIconUrl('phone.svg'),
                    'menu' => $file->getIconUrl('menu.svg'),
                    'avater' => $file->getIconUrl('avatar-dark.svg'),
                    'register' => $file->getIconUrl('driver-registration-dark.svg'),
                    'mapPos' => $file->getIconUrl('map-position.svg'),
                ],
                'url' => [
                    'back' => $this->config->site_url('Main'),
                    // 'save' => $this->config->site_url('Studio/Add/register')
                ],
                'data' => [
                    'userId' => $this->session->userId,
                    'userMail' => $this->session->userEmail,
                    'list' => $this->User->get_gifticon_list($this->session->userId),
                    // 'departure'=>trim($this->input->get('departure', true)),
                    // 'destination'=>trim($this->input->get('destination', true)),
                    // 'orderedlist' => $orderedlist,
                    'csrf' => [
                        'name' => $this->security->get_csrf_token_name(),
                        'hash' => $this->security->get_csrf_hash()
                    ],
                ]
            ]);

            $load->view('common/foot', [
                'js' => array_merge($this->baseAssets['js'], [
                    'postCode' => $file->getJsUrl('lib/postcode.v2.js'),
                    'addAddress' => $file->getJsUrl('addStudio/main.js')
                ])
            ]);

            $this->session->set_userdata('thisuser', trim($this->input->get('thisuser', true)));
        }
    }
}
