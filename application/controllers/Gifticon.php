<?php

if (! class_exists('Gifticon')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Gifticon extends CI_Controller
    {

        /**
         * Gifticon API 용도 컨트롤러로 생성함.
         */
        // private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            // $this->baseAssets = $this->file->getDefaultAssets();
            // $this->lang->load('addStudio', 'korean');
        }


        // public function talk_list() : bool
        // {
        //     // $phonenumber = trim($this->input->get('phonenumber', true));
        //     // $this->load->library('Deliver');
        //     // $detail_result = $this->deliver->isNotExists([
        //     //     'userId' => $phonenumber,
        //     //     'token'  => 'oAuth발급받은 토큰',
        //     // ]);
        //     // // var_dump($detail_result);



        //     $username = "jackkor";                //필수입력
        //     $key = "yjFmfSusrGeOX1S";           //필수입력

        //     /* 여기까지 수정해주시기 바랍니다. */

        //     $postvars = '{"username":"'.$username.'","key":"'.$key.'","template_type":"3"}';        //JSON 데이터

        //     $url = "https://directsend.co.kr/index.php/api_kakao/get/template";        //URL

        //     $ch = curl_init();
        //     $headers = array("cache-control: no-cache","content-type: application/json; charset=utf-8");      //헤더정보

        //     curl_setopt($ch,CURLOPT_URL, $url);
        //     curl_setopt($ch,CURLOPT_POST, true);
        //     curl_setopt($ch,CURLOPT_POSTFIELDS, $postvars);
        //     curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        //     curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
        //     curl_setopt($ch,CURLOPT_TIMEOUT, 20);
        //     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //     $response = curl_exec($ch);

        //     var_dump(json_decode($response)->data->list);
        //     curl_close ($ch);

        //     return true;
        // }


        // public function talk_send_test_9() : bool
        // {

        //     // 8:9
        //     // 고객님 #{비고1}에서 #{비고2} 요청하신 퀵서비스 #{비고3}기사님이 배정되었습니다.
        //     // 아래 링크로 가시면 배송 현황과 기사님 연락처를 확인하실 수 있습니다.
        //     // #{비고4}
        //     // 퀵카 1661-2482


        //     $ch = curl_init();

        //     $username = "jackkor";                //필수입력
        //     $key = "yjFmfSusrGeOX1S";           //필수입력

        //     $kakao_plus_id = "@qc2482";            //필수입력
        //     $user_template_no = "9";            //필수입력 (하단 259 라인 API 이용하여 확인)
        //     $receiver = '{"name":"고승일","mobile":"01025349557","note1":"독산동","note2":"신설동","note3":"홍길동","note4":"https://quickcar.co.kr/orderdetail?v=192987659"}';
        //     $receiver = '['.$receiver.']';
        //     // 대체문자 정보 추가
        //     $kakao_faild_type = "1";          // 1 : 대체문자(SMS) / 2 : 대체문자(LMS) / 3 : 대체문자(MMS) 대체문자 사용시 필수 입력
        //     $title = "대체문자 MMS/LMS 제목입니다.";
        //     $message = '[$NAME]님 알림 문자 입니다. 전화번호 : [$MOBILE] 비고1 : [$NOTE1] 비고2 : [$NOTE2] 비고3 : [$NOTE3] 비고4 : [$NOTE4] 비고5 : [$NOTE5]';             //대체문자 사용시 필수입력
        //     $sender = "발신자번호";                    //대체문자 사용시 필수입력
        //     // 예약발송 정보 추가
        //     $reserve_type = 'NORMAL'; // NORMAL - 즉시발송 / ONETIME - 1회예약 / WEEKLY - 매주정기예약 / MONTHLY - 매월정기예약
        //     $start_reserve_time = date('Y-m-d H:i:s'); //  발송하고자 하는 시간(시,분단위까지만 가능) (동일한 예약 시간으로는 200회 이상 API 호출을 할 수 없습니다.)
        //     $end_reserve_time = date('Y-m-d H:i:s'); //  발송이 끝나는 시간 1회 예약일 경우 $start_reserve_time = $end_reserve_time
        //     $remained_count = 1;
        //     // 예약 수정/취소 API는 소스 하단을 참고 해주시기 바랍니다.

        //     // 실제 발송성공실패 여부를 받기 원하실 경우 아래 주석을 해제하신 후, 사이트에 등록한 URL 번호를 입력해 주시기 바랍니다.
        //     $return_url_yn = TRUE;        //return_url 사용시 필수 입력
        //     $return_url = 0;

        //     /* 여기까지 수정해주시기 바랍니다. */

        //     $message = str_replace(' ', ' ', $message);  //유니코드 공백문자 치환, 대체문자 발송시 주석 해제


        //     $postvars = '"username":"'.$username.'"';
        //     $postvars = $postvars.', "key":"'.$key.'"';
        //     $postvars = $postvars.', "kakao_plus_id":"'.$kakao_plus_id.'"';
        //     $postvars = $postvars.', "user_template_no":"'.$user_template_no.'"';
        //     $postvars = $postvars.', "receiver":'.$receiver;
        //     $postvars = '{'.$postvars.'}';      //JSON 데이터

        //     $url = "https://directsend.co.kr/index.php/api_v2/kakao_notice";         //URL

        //     //헤더정보
        //     $headers = array("cache-control: no-cache","content-type: application/json; charset=utf-8");

        //     curl_setopt($ch,CURLOPT_URL, $url);
        //     curl_setopt($ch,CURLOPT_POST, true);
        //     curl_setopt($ch,CURLOPT_POSTFIELDS, $postvars);
        //     curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        //     curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
        //     curl_setopt($ch,CURLOPT_TIMEOUT, 20);
        //     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //     $response = curl_exec($ch);


        //     return true;
        // }




        // public function talk_send_test_21() : bool
        // {





        //     // 4 : 21
        //     // 오늘 주문하신 어플 우측상단 메뉴에서 로그인 하시면 발급된 #{아메리카노} 쿠폰을 확인하실 수 있습니다.
        //     // 감사합니다
        //     // 퀵카 1661-2482




        //     $ch = curl_init();

        //     $username = "jackkor";                //필수입력
        //     $key = "yjFmfSusrGeOX1S";           //필수입력

        //     $kakao_plus_id = "@qc2482";            //필수입력
        //     $user_template_no = "21";            //필수입력 (하단 259 라인 API 이용하여 확인)
        //     $receiver = '{"name":"고승일","mobile":"01025349557","note1":"아메리카노"}';
        //     $receiver = '['.$receiver.']';
        //     // 대체문자 정보 추가
        //     $kakao_faild_type = "1";          // 1 : 대체문자(SMS) / 2 : 대체문자(LMS) / 3 : 대체문자(MMS) 대체문자 사용시 필수 입력
        //     $title = "대체문자 MMS/LMS 제목입니다.";
        //     $message = '[$NAME]님 알림 문자 입니다. 전화번호 : [$MOBILE] 비고1 : [$NOTE1] 비고2 : [$NOTE2] 비고3 : [$NOTE3] 비고4 : [$NOTE4] 비고5 : [$NOTE5]';             //대체문자 사용시 필수입력
        //     $sender = "발신자번호";                    //대체문자 사용시 필수입력
        //     // 예약발송 정보 추가
        //     $reserve_type = 'NORMAL'; // NORMAL - 즉시발송 / ONETIME - 1회예약 / WEEKLY - 매주정기예약 / MONTHLY - 매월정기예약
        //     $start_reserve_time = date('Y-m-d H:i:s'); //  발송하고자 하는 시간(시,분단위까지만 가능) (동일한 예약 시간으로는 200회 이상 API 호출을 할 수 없습니다.)
        //     $end_reserve_time = date('Y-m-d H:i:s'); //  발송이 끝나는 시간 1회 예약일 경우 $start_reserve_time = $end_reserve_time
        //     $remained_count = 1;
        //     // 예약 수정/취소 API는 소스 하단을 참고 해주시기 바랍니다.

        //     // 실제 발송성공실패 여부를 받기 원하실 경우 아래 주석을 해제하신 후, 사이트에 등록한 URL 번호를 입력해 주시기 바랍니다.
        //     $return_url_yn = TRUE;        //return_url 사용시 필수 입력
        //     $return_url = 0;

        //     /* 여기까지 수정해주시기 바랍니다. */

        //     $message = str_replace(' ', ' ', $message);  //유니코드 공백문자 치환, 대체문자 발송시 주석 해제


        //     $postvars = '"username":"'.$username.'"';
        //     $postvars = $postvars.', "key":"'.$key.'"';
        //     $postvars = $postvars.', "kakao_plus_id":"'.$kakao_plus_id.'"';
        //     $postvars = $postvars.', "user_template_no":"'.$user_template_no.'"';
        //     $postvars = $postvars.', "receiver":'.$receiver;
        //     $postvars = '{'.$postvars.'}';      //JSON 데이터

        //     $url = "https://directsend.co.kr/index.php/api_v2/kakao_notice";         //URL

        //     //헤더정보
        //     $headers = array("cache-control: no-cache","content-type: application/json; charset=utf-8");

        //     curl_setopt($ch,CURLOPT_URL, $url);
        //     curl_setopt($ch,CURLOPT_POST, true);
        //     curl_setopt($ch,CURLOPT_POSTFIELDS, $postvars);
        //     curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        //     curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
        //     curl_setopt($ch,CURLOPT_TIMEOUT, 20);
        //     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //     $response = curl_exec($ch);


        //     return true;
        // }




        // public function talk_send_test_18() : bool
        // {


        //     // #{이름} 고객님 #{비고1}를 이용해주셔서 감사합니다.
        //     // #{비고2} 주문 이벤트로 #{비고3} 쿠폰을 보내드립니다.
        //     // 맛있게 드시고 친구소개 부탁드립니다.
        //     // 선물링크 : #{비고4}
        //     // 퀵카 1661-2482

        //     $ch = curl_init();

        //     $username = "jackkor";                //필수입력
        //     $key = "yjFmfSusrGeOX1S";           //필수입력

        //     $kakao_plus_id = "@qc2482";            //필수입력
        //     $user_template_no = "18";            //필수입력 (하단 259 라인 API 이용하여 확인)
        //     $receiver = '{"name":"고승일","mobile":"01025349557","note1":"퀵카","note2":"10번째","note3":"아메리카노","note4":"https://quickcar.co.kr/orderdetail?v=192987659"}';
        //     $receiver = '['.$receiver.']';
        //     // 대체문자 정보 추가
        //     $kakao_faild_type = "1";          // 1 : 대체문자(SMS) / 2 : 대체문자(LMS) / 3 : 대체문자(MMS) 대체문자 사용시 필수 입력
        //     $title = "대체문자 MMS/LMS 제목입니다.";
        //     $message = '[$NAME]님 알림 문자 입니다. 전화번호 : [$MOBILE] 비고1 : [$NOTE1] 비고2 : [$NOTE2] 비고3 : [$NOTE3] 비고4 : [$NOTE4] 비고5 : [$NOTE5]';             //대체문자 사용시 필수입력
        //     $sender = "발신자번호";                    //대체문자 사용시 필수입력
        //     // 예약발송 정보 추가
        //     $reserve_type = 'NORMAL'; // NORMAL - 즉시발송 / ONETIME - 1회예약 / WEEKLY - 매주정기예약 / MONTHLY - 매월정기예약
        //     $start_reserve_time = date('Y-m-d H:i:s'); //  발송하고자 하는 시간(시,분단위까지만 가능) (동일한 예약 시간으로는 200회 이상 API 호출을 할 수 없습니다.)
        //     $end_reserve_time = date('Y-m-d H:i:s'); //  발송이 끝나는 시간 1회 예약일 경우 $start_reserve_time = $end_reserve_time
        //     $remained_count = 1;
        //     // 예약 수정/취소 API는 소스 하단을 참고 해주시기 바랍니다.

        //     // 실제 발송성공실패 여부를 받기 원하실 경우 아래 주석을 해제하신 후, 사이트에 등록한 URL 번호를 입력해 주시기 바랍니다.
        //     $return_url_yn = TRUE;        //return_url 사용시 필수 입력
        //     $return_url = 0;

        //     /* 여기까지 수정해주시기 바랍니다. */

        //     $message = str_replace(' ', ' ', $message);  //유니코드 공백문자 치환, 대체문자 발송시 주석 해제


        //     $postvars = '"username":"'.$username.'"';
        //     $postvars = $postvars.', "key":"'.$key.'"';
        //     $postvars = $postvars.', "kakao_plus_id":"'.$kakao_plus_id.'"';
        //     $postvars = $postvars.', "user_template_no":"'.$user_template_no.'"';
        //     $postvars = $postvars.', "receiver":'.$receiver;
        //     $postvars = '{'.$postvars.'}';      //JSON 데이터

        //     $url = "https://directsend.co.kr/index.php/api_v2/kakao_notice";         //URL

        //     //헤더정보
        //     $headers = array("cache-control: no-cache","content-type: application/json; charset=utf-8");

        //     curl_setopt($ch,CURLOPT_URL, $url);
        //     curl_setopt($ch,CURLOPT_POST, true);
        //     curl_setopt($ch,CURLOPT_POSTFIELDS, $postvars);
        //     curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        //     curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
        //     curl_setopt($ch,CURLOPT_TIMEOUT, 20);
        //     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //     $response = curl_exec($ch);


        //     return true;
        // }




        // public function talk_send_test_15() : bool
        // {


        //     // 6: 15
        //     // 고객님 요청하신 퀵서비스 완료되었습니다.
        //     // 감사합니다.
        //     // 퀵카 1661-2482




        //     $ch = curl_init();

        //     $username = "jackkor";                //필수입력
        //     $key = "yjFmfSusrGeOX1S";           //필수입력

        //     $kakao_plus_id = "@qc2482";            //필수입력
        //     $user_template_no = "15";            //필수입력 (하단 259 라인 API 이용하여 확인)
        //     $receiver = '{"name":"고승일","mobile":"01025349557"}';
        //     $receiver = '['.$receiver.']';
        //     // 대체문자 정보 추가
        //     $kakao_faild_type = "1";          // 1 : 대체문자(SMS) / 2 : 대체문자(LMS) / 3 : 대체문자(MMS) 대체문자 사용시 필수 입력
        //     $title = "대체문자 MMS/LMS 제목입니다.";
        //     $message = '[$NAME]님 알림 문자 입니다. 전화번호 : [$MOBILE] 비고1 : [$NOTE1] 비고2 : [$NOTE2] 비고3 : [$NOTE3] 비고4 : [$NOTE4] 비고5 : [$NOTE5]';             //대체문자 사용시 필수입력
        //     $sender = "발신자번호";                    //대체문자 사용시 필수입력
        //     // 예약발송 정보 추가
        //     $reserve_type = 'NORMAL'; // NORMAL - 즉시발송 / ONETIME - 1회예약 / WEEKLY - 매주정기예약 / MONTHLY - 매월정기예약
        //     $start_reserve_time = date('Y-m-d H:i:s'); //  발송하고자 하는 시간(시,분단위까지만 가능) (동일한 예약 시간으로는 200회 이상 API 호출을 할 수 없습니다.)
        //     $end_reserve_time = date('Y-m-d H:i:s'); //  발송이 끝나는 시간 1회 예약일 경우 $start_reserve_time = $end_reserve_time
        //     $remained_count = 1;
        //     // 예약 수정/취소 API는 소스 하단을 참고 해주시기 바랍니다.

        //     // 실제 발송성공실패 여부를 받기 원하실 경우 아래 주석을 해제하신 후, 사이트에 등록한 URL 번호를 입력해 주시기 바랍니다.
        //     $return_url_yn = TRUE;        //return_url 사용시 필수 입력
        //     $return_url = 0;

        //     /* 여기까지 수정해주시기 바랍니다. */

        //     $message = str_replace(' ', ' ', $message);  //유니코드 공백문자 치환, 대체문자 발송시 주석 해제


        //     $postvars = '"username":"'.$username.'"';
        //     $postvars = $postvars.', "key":"'.$key.'"';
        //     $postvars = $postvars.', "kakao_plus_id":"'.$kakao_plus_id.'"';
        //     $postvars = $postvars.', "user_template_no":"'.$user_template_no.'"';
        //     $postvars = $postvars.', "receiver":'.$receiver;
        //     $postvars = '{'.$postvars.'}';      //JSON 데이터

        //     $url = "https://directsend.co.kr/index.php/api_v2/kakao_notice";         //URL

        //     //헤더정보
        //     $headers = array("cache-control: no-cache","content-type: application/json; charset=utf-8");

        //     curl_setopt($ch,CURLOPT_URL, $url);
        //     curl_setopt($ch,CURLOPT_POST, true);
        //     curl_setopt($ch,CURLOPT_POSTFIELDS, $postvars);
        //     curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        //     curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
        //     curl_setopt($ch,CURLOPT_TIMEOUT, 20);
        //     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //     $response = curl_exec($ch);


        //     return true;
        // }





        // public function talk_send_test_12() : bool
        // {


        //     // 7:12
        //     // #{이름} 고객님께서 요청하신 퀵서비스 주문에 #{비고1} 기사님이 배정되었습니다.
        //     // 아래 링크로 가시면 배송 현황과 기사님 연락처를 확인하실 수 있습니다.
        //     // #{비고2}
        //     // 퀵카 1661-2482




        //     $ch = curl_init();

        //     $username = "jackkor";                //필수입력
        //     $key = "yjFmfSusrGeOX1S";           //필수입력

        //     $kakao_plus_id = "@qc2482";            //필수입력
        //     $user_template_no = "12";            //필수입력 (하단 259 라인 API 이용하여 확인)
        //     $receiver = '{"name":"고승일","mobile":"01025349557","note1":"김덕현","note2":"https://quickcar.co.kr/orderdetail?v=192987659"}';
        //     $receiver = '['.$receiver.']';
        //     // 대체문자 정보 추가
        //     $kakao_faild_type = "1";          // 1 : 대체문자(SMS) / 2 : 대체문자(LMS) / 3 : 대체문자(MMS) 대체문자 사용시 필수 입력
        //     $title = "대체문자 MMS/LMS 제목입니다.";
        //     $message = '[$NAME]님 알림 문자 입니다. 전화번호 : [$MOBILE] 비고1 : [$NOTE1] 비고2 : [$NOTE2] 비고3 : [$NOTE3] 비고4 : [$NOTE4] 비고5 : [$NOTE5]';             //대체문자 사용시 필수입력
        //     $sender = "발신자번호";                    //대체문자 사용시 필수입력
        //     // 예약발송 정보 추가
        //     $reserve_type = 'NORMAL'; // NORMAL - 즉시발송 / ONETIME - 1회예약 / WEEKLY - 매주정기예약 / MONTHLY - 매월정기예약
        //     $start_reserve_time = date('Y-m-d H:i:s'); //  발송하고자 하는 시간(시,분단위까지만 가능) (동일한 예약 시간으로는 200회 이상 API 호출을 할 수 없습니다.)
        //     $end_reserve_time = date('Y-m-d H:i:s'); //  발송이 끝나는 시간 1회 예약일 경우 $start_reserve_time = $end_reserve_time
        //     $remained_count = 1;
        //     // 예약 수정/취소 API는 소스 하단을 참고 해주시기 바랍니다.

        //     // 실제 발송성공실패 여부를 받기 원하실 경우 아래 주석을 해제하신 후, 사이트에 등록한 URL 번호를 입력해 주시기 바랍니다.
        //     $return_url_yn = TRUE;        //return_url 사용시 필수 입력
        //     $return_url = 0;

        //     /* 여기까지 수정해주시기 바랍니다. */

        //     $message = str_replace(' ', ' ', $message);  //유니코드 공백문자 치환, 대체문자 발송시 주석 해제


        //     $postvars = '"username":"'.$username.'"';
        //     $postvars = $postvars.', "key":"'.$key.'"';
        //     $postvars = $postvars.', "kakao_plus_id":"'.$kakao_plus_id.'"';
        //     $postvars = $postvars.', "user_template_no":"'.$user_template_no.'"';
        //     $postvars = $postvars.', "receiver":'.$receiver;
        //     $postvars = '{'.$postvars.'}';      //JSON 데이터

        //     $url = "https://directsend.co.kr/index.php/api_v2/kakao_notice";         //URL

        //     //헤더정보
        //     $headers = array("cache-control: no-cache","content-type: application/json; charset=utf-8");

        //     curl_setopt($ch,CURLOPT_URL, $url);
        //     curl_setopt($ch,CURLOPT_POST, true);
        //     curl_setopt($ch,CURLOPT_POSTFIELDS, $postvars);
        //     curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        //     curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
        //     curl_setopt($ch,CURLOPT_TIMEOUT, 20);
        //     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //     $response = curl_exec($ch);


        //     return true;
        // }





        // public function index() : void {



        //     $file = $this->file;
        //     $load = $this->load;
        //     $load->view('common/head', [
        //         'css' => $this->baseAssets['css'],
        //         'title' => '주문 진행',
        //     ]);

        //     $load->view('Order1/body', [
        //         'img' => [
        //             'back' => $file->getIconUrl('back.svg'),
        //             'address' => $file->getIconUrl('marker.svg'),
        //             'phone' => $file->getIconUrl('phone.svg'),
        //             'menu' => $file->getIconUrl('menu.svg'),
        //             'avater' => $file->getIconUrl('avatar-dark.svg'),
        //             'register' => $file->getIconUrl('driver-registration-dark.svg'),
        //             'mapPos' => $file->getIconUrl('map-position.svg'),
        //         ],
        //         'url' => [
        //             'back' => $this->config->site_url('Main'),
        //             'save' => $this->config->site_url('Studio/Add/register')
        //         ],
        //         'data' => [
        //             'userId' => $this->session->userId,
        //             'userMail' => $this->session->userEmail,
        //             'departure'=>trim($this->input->get('departure', true)),
        //             'destination'=>trim($this->input->get('destination', true)),

        //             'dep_name'=>trim($this->input->get('dep_name', true)),
        //             'dep_number'=>trim($this->input->get('dep_number', true)),
        //             'dest_name'=>trim($this->input->get('dest_name', true)),
        //             'dest_number'=>trim($this->input->get('dest_number', true)),

        //             'csrf' => [
        //                 'name' => $this->security->get_csrf_token_name(),
        //                 'hash' => $this->security->get_csrf_hash()
        //             ],
        //         ]
        //     ]);

        //     $load->view('common/foot', [
        //         'js' => array_merge($this->baseAssets['js'], [
        //             'postCode' => $file->getJsUrl('lib/postcode.v2.js'),
        //             'addAddress' => $file->getJsUrl('addStudio/main.js')
        //         ])
        //     ]);

        //     $this->session->set_userdata('thisuser', trim($this->input->get('thisuser', true)));

        // }



        // public function order2() : void
        // {

        //     $this->session->set_userdata('count_p', trim($this->input->get('count_p', true)));
        //     $this->session->set_userdata('etc_p', trim($this->input->get('etc_p', true)));
        //     $this->session->set_userdata('memo', trim($this->input->get('memo', true)));



        //     $this->session->set_userdata('start_name', trim($this->input->get('start_name', true)));
        //     $this->session->set_userdata('start_telno', trim($this->input->get('start_telno', true)));
        //     $this->session->set_userdata('dest_telno', trim($this->input->get('dest_telno', true)));
        //     $this->session->set_userdata('dest_name', trim($this->input->get('dest_name', true)));





        //     $file = $this->file;
        //     $load = $this->load;
        //     $load->view('common/head', [
        //         'css' => $this->baseAssets['css'],
        //         'title' => '결제 방법 선택',
        //     ]);

        //     $load->view('Order2/body', [
        //         'img' => [
        //             'back' => $file->getIconUrl('back.svg'),
        //             'address' => $file->getIconUrl('marker.svg'),
        //             'phone' => $file->getIconUrl('phone.svg'),
        //             'menu' => $file->getIconUrl('menu.svg'),
        //             'avater' => $file->getIconUrl('avatar-dark.svg'),
        //             'register' => $file->getIconUrl('driver-registration-dark.svg'),
        //             'mapPos' => $file->getIconUrl('map-position.svg'),
        //         ],
        //         'url' => [
        //             'back' => $this->config->site_url('Main'),
        //             'save' => $this->config->site_url('Studio/Add/register')
        //         ],
        //         'data' => [
        //             'userId' => $this->session->userId,
        //             'userMail' => $this->session->userEmail,


        //             'start'=>trim($this->input->get('start', true)),
        //             'end'=>trim($this->input->get('end', true)),
        //             'count_p'=>trim($this->input->get('count_p', true)),
        //             'etc_p'=>trim($this->input->get('etc_p', true)),
        //             'memo'=>trim($this->input->get('memo', true)),
        //             'price'=>trim($this->input->get('price', true)),
        //             'add_price'=>trim($this->input->get('add_price', true)),



        //             'departure'=>trim($this->input->get('departure', true)),
        //             'destination'=>trim($this->input->get('destination', true)),
        //             'csrf' => [
        //                 'name' => $this->security->get_csrf_token_name(),
        //                 'hash' => $this->security->get_csrf_hash()
        //             ],
        //         ]
        //     ]);

        //     $load->view('common/foot', [
        //         'js' => array_merge($this->baseAssets['js'], [
        //             'postCode' => $file->getJsUrl('lib/postcode.v2.js'),
        //             'addAddress' => $file->getJsUrl('addStudio/main.js')
        //         ])
        //     ]);
        // }






        // // public function is_user_not_exist(string $phonenumber) : bool

        // public function is_user_not_exist(string $phonenumber) : bool
        // {
        //     $phonenumber = trim($this->input->get('phonenumber', true));
        //     $this->load->library('Deliver');
        //     $detail_result = $this->deliver->isNotExists([
        //         'userId' => $phonenumber,
        //         'token'  => 'oAuth발급받은 토큰',
        //     ]);
        //     // var_dump($detail_result);
        //     return $detail_result;
        // }

        // public function cancel_order() : void
        // {

        //     $this->load->library('Deliver');
        //     $cancel_result = $this->deliver->cancel([
        //         'userId' => trim($this->input->get('userid', true)),
        //         'serial' => trim($this->input->get('serial', true)),
        //     ]);
        //     var_dump($cancel_result);

        //     // goBackAlert( '주문을 취소했습니다.', $this->config->site_url('Orderlist'));
        // }







        // public function order_list() : void
        // {

        //     $this->load->library('Deliver');
        //     $detail_result = $this->deliver->getOrderList([
        //         'userId' => 'jackkor2',
        //     ]);
        //     // print_r($detail_result);
        //     foreach ($detail_result['message'] as $value) {
        //         # code...
        //         echo '<br>';
        //         echo '<br>';
        //         // var_dump($value);
        //         foreach ($value as $vv) {
        //             # code...
        //             var_dump($vv);
        //             echo '<br>';
        //         }
        //     }
        // }


        // public function order_detail(string $orderSerial) : array
        // {

        //     $userId='jackkor2';
        //     // $orderSerial='192986537';

        //     $this->load->library('Deliver');
        //     $detail_result = $this->deliver->getOrderDetail([
        //         'userId' => $userId,
        //         'serial' => $orderSerial,
        //     ]);

        //     $this->load->model('Order/Ordermodel');
        //     $db_orderdetail = $this->Ordermodel->get_order($orderSerial);


        //     // 저장된 것과 가져온 배송 상태 코드가 다르면 처리
        //     if($db_orderdetail[0]['state'] != $detail_result['message'][5]->state ){
        //         // state 등록
        //         $this->Ordermodel->order_state_update($orderSerial,$detail_result['message'][5]->state);
        //         // 알림 보내야 하는 시점이라면 알림 발송
        //     }


        //     // 저장된 것과 가져온 배당 기사 코드가 다르면 처리
        //     if($db_orderdetail[0]['rider_code_no'] != $detail_result['message'][2]->rider_code_no ){
        //         // rider 등록
        //         $this->Ordermodel->order_rider_update($orderSerial,$detail_result['message'][2]->rider_code_no, $detail_result['message'][2]->rider_name, $detail_result['message'][2]->rider_tel_number);
        //     }


        //     return $detail_result['message'];

        // }





        // public function register(string $phonenumber,string $customerName, string $dongName) : array
        // {



        //     // $phonenumber = trim($this->input->get('phonenumber', true));
        //     // $customerName = trim($this->input->get('customerName', true));
        //     // $dongName = trim($this->input->get('dongName', true));


        //     $this->load->model('Order/Ordermodel');



        //     if (count($this->Ordermodel->get_usercheck($phonenumber))!=0) {
        //         var_dump("there is already that user");
        //         var_dump($this->Ordermodel->get_usercheck($phonenumber)[0]['user_id']);
        //     }
        //     else{


        //         $this->load->helper('string');
        //         $randomtext = random_string('alnum',16);

        //         while (!$this->is_user_not_exist($phonenumber.$randomtext)
        //         ){
        //             $randomtext = random_string('alnum',16);
        //         }


        //         $register_array=[
        //             'userId' => $phonenumber.$randomtext,
        //             'token'  => getenv('AUTH_TOKEN'),
        //             'password'  => 'password',
        //             'passwordConfirm'  => 'password',
        //             'customerName'  => $customerName,
        //             'dongName'  => $dongName,
        //             'telNo'  => $phonenumber,
        //             'user_id'  => $phonenumber.$randomtext,
        //         ];


        //         $this->load->library('Deliver');
        //         $answer = $this->deliver->register($register_array);

        //         $this->Ordermodel->add_user_register($register_array);


        //         return $answer;

        //     }





        // }


        // public function int_element(int $get_number, int $element) : int
        // {

        //     return floor( $get_number /$element )*$element;



        // }



        // public function save_weather_list() : bool
        // {





        //     $this->load->library('NaverGeo');


        //     $adr_1 = trim($this->input->get('adr_1', true));
        //     $adr_2 = trim($this->input->get('adr_2', true));
        //     $adr_3 = trim($this->input->get('adr_3', true));
        //     $nx = trim($this->input->get('nx', true));
        //     $ny = trim($this->input->get('ny', true));

        //     $requested=$this->navergeo->localeToXY($adr_1." ".$adr_2." ".$adr_3);

        //     var_dump($requested);

        //     $this->load->model('User/User');

        //     if ($this->User->add_weather_grid($adr_1." ".$adr_2." ".$adr_3,$adr_1,$adr_2,$adr_3,$requested['x'],$requested['y'],$nx,$ny)){
        //         echo "inserted";
        //     }


        //     return true;



        // }



        // public function get_weather(string $gps_x, string $gps_y) : int
        // {


        //     // $this->load->library('NaverGeo');

        //     // $requested=$this->navergeo->localeToXY($find_addr);

        //     // var_dump($requested);

        //     $this->load->model('User/User');

        //     $getnxy = $this->User->get_nxy($gps_x,$gps_y);
        //     // var_dump($getnxy);


        //     // SELECT nx,ny,abs(gpsx-126.8955818) + abs(gpsy-37.4645478) as 'distance' FROM `weather_grid` ORDER BY `distance` ASC limit 1


        //     $ch = curl_init();
        //     $url = 'http://apis.data.go.kr/1360000/VilageFcstInfoService/getUltraSrtFcst'; /*URL*/
        //     $queryParams = '?' . urlencode('ServiceKey') . '=%2FeU%2FY%2F%2FrNBokLkGmOD9GyVv8SuuO9UjhA2rQUVXBmob5C9M3Jr7cKj5rHFVdrnL9yZ5dhuAOGHp5gVHUTkD7dA%3D%3D'; /*Service Key*/
        //     $queryParams .= '&' . urlencode('pageNo') . '=' . urlencode('1'); /*페이지번호*/
        //     $queryParams .= '&' . urlencode('numOfRows') . '=' . urlencode('10'); /*한 페이지 결과 수*/
        //     $queryParams .= '&' . urlencode('dataType') . '=' . urlencode('JSON'); /*요청자료형식(XML/JSON)Default: XML*/
        //     $queryParams .= '&' . urlencode('base_date') . '='.date("Ymd"); /*각각의 base_time 로 검색 참고자료 참조*/
        //     $queryParams .= '&' . urlencode('base_time') . '='.date("Hi"); /*각각의 base_time 로 검색 참고자료 참조*/
        //     $queryParams .= '&' . urlencode('nx') . '='.(string)(int)$getnxy[0]['nx']; /*각각의 base_time 로 검색 참고자료 참조*/
        //     $queryParams .= '&' . urlencode('ny') . '='.(string)(int)$getnxy[0]['ny']; /*각각의 base_time 로 검색 참고자료 참조*/
        //     // echo $url . $queryParams;
        //     curl_setopt($ch, CURLOPT_URL, $url . $queryParams);
        //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //     curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //     curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        //     $response = curl_exec($ch);
        //     curl_close($ch);


        //     if (json_decode($response)->response->header->resultCode=="00"){
        //         return json_decode($response)->response->body->items->item[0]->fcstValue;
        //     }
        //     else{
        //         return 0;
        //     }

        // }




        // public function get_weather_2() : int
        // {


        //     var_dump( is_null($this->session->start ));
        //     $this->load->library('NaverGeo');
        //     $requested=$this->navergeo->localeToXY("신설동 18");

        //     $this->load->model('User/User');
        //     $getnxy = $this->User->get_nxy($requested['x'],$requested['y']);

        //     var_dump($getnxy);

        //     $ch = curl_init();
        //     $url = 'http://apis.data.go.kr/1360000/VilageFcstInfoService/getUltraSrtFcst'; /*URL*/
        //     $queryParams = '?' . urlencode('ServiceKey') . '=%2FeU%2FY%2F%2FrNBokLkGmOD9GyVv8SuuO9UjhA2rQUVXBmob5C9M3Jr7cKj5rHFVdrnL9yZ5dhuAOGHp5gVHUTkD7dA%3D%3D'; /*Service Key*/
        //     $queryParams .= '&' . urlencode('pageNo') . '=' . urlencode('1'); /*페이지번호*/
        //     $queryParams .= '&' . urlencode('numOfRows') . '=' . urlencode('10'); /*한 페이지 결과 수*/
        //     $queryParams .= '&' . urlencode('dataType') . '=' . urlencode('JSON'); /*요청자료형식(XML/JSON)Default: XML*/
        //     $queryParams .= '&' . urlencode('base_date') . '='.date("Ymd"); /*각각의 base_time 로 검색 참고자료 참조*/
        //     $queryParams .= '&' . urlencode('base_time') . '='.date("Hi"); /*각각의 base_time 로 검색 참고자료 참조*/
        //     $queryParams .= '&' . urlencode('nx') . '='.(string)(int)$getnxy[0]['nx']; /*각각의 base_time 로 검색 참고자료 참조*/
        //     $queryParams .= '&' . urlencode('ny') . '='.(string)(int)$getnxy[0]['ny']; /*각각의 base_time 로 검색 참고자료 참조*/
        //     // echo $url . $queryParams;
        //     curl_setopt($ch, CURLOPT_URL, $url . $queryParams);
        //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //     curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //     curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        //     $response = curl_exec($ch);
        //     curl_close($ch);

        //     if (json_decode($response)->response->header->resultCode=="00"){
        //         var_dump(json_decode($response)->response->body->items->item[0]->fcstValue);
        //         echo '<br>';
        //         var_dump("정상으로 받아옴");
        //         return json_decode($response)->response->body->items->item[0]->fcstValue;
        //     }
        //     else{
        //         var_dump("응답 못 받아옴");
        //         return 0;
        //     }


        // }



        // private function distance_price(string $startsi, string $destsi, int $kind_class, float $distance) : int
        // {
        //     $max_fee=array(99999999);

        //     //  kind	 배송수단(1:오토, 2:다마스, 5:라보, 4:밴, 6:지하철, 3:트럭)	 필수항목
        //     if ($kind_class==1){
        //         $max_fee[]=90000;
        //     }
        //     if ($kind_class==2 || $kind_class==5){
        //         $max_fee[]=200000;
        //     }
        //     if ($kind_class==3){
        //         $max_fee[]=250000;
        //     }

        //     if ($startsi==$destsi){    // 출발지, 도착지 시,도가 같을 때

        //         if ($kind_class!=3){    // 트럭 아니면


        //             if (strpos($startsi,"서울")!==false){   //서울이면 최대 요금 설정
        //                 $this_max_fee=20000;
        //             }else{$this_max_fee=9999999999;}

        //             if ($distance<=4){$price_0 = 8000;}
        //             else if ($distance<=8){$price_0 = 10000;}
        //             else if ($distance<=10){$price_0 = 12000;}
        //             else if ($distance<=15){$price_0 = 13000;}
        //             else if ($distance<=20){$price_0 = 15000;}
        //             else if ($distance<=25){$price_0 = 18000;}
        //             else {
        //                 $price_0 = ((int)$distance-25)*1000 + 18000;
        //             }
        //             $price_0 = min($this_max_fee, $price_0);


        //         }
        //         else{//트럭이면

        //             if (strpos($startsi,"서울")!==false){//서울이면 최대 요금 설정
        //                 $this_max_fee=45000;
        //             }else{$this_max_fee=9999999999;}

        //             if ($distance<=10){$price_0 = 35000;}
        //             else if ($distance<=20){$price_0 = 40000;}
        //             else {
        //                 $price_0 = $this->int_element(((int)$distance-20),10)*5000 + 40000;

        //             }
        //             $price_0 = min($this_max_fee, $price_0);

        //         }

        //     }
        //     else{ // 출발지, 도착지 시,도가 다를 때


        //         if ($kind_class!=3){ // 트럭 아니면



        //             if ($distance<=4){$price_0 = 10000;}
        //             else if ($distance<=10){$price_0 = 15000;}
        //             else if ($distance<=20){$price_0 = 18000;}
        //             else if ($distance<=25){$price_0 = 20000;}
        //             else if ($distance<=35){$price_0 = 25000;}
        //             else if ($distance<=45){$price_0 = 30000;}
        //             else {
        //                 $price_0 = $this->int_element(((int)$distance-45),3)*1000 + 30000;
        //             }


        //         }
        //         else{//트럭이면

        //             if ($distance<=10){$price_0 = 35000;}
        //             else if ($distance<=20){$price_0 = 40000;}
        //             else if ($distance<=35){$price_0 = 45000;}
        //             else if ($distance<=45){$price_0 = 50000;}
        //             else if ($distance<=55){$price_0 = 55000;}
        //             else if ($distance<=65){$price_0 = 60000;}
        //             else if ($distance<=75){$price_0 = 65000;}
        //             else if ($distance<=80){$price_0 = 70000;}
        //             else if ($distance<=90){$price_0 = 80000;}
        //             else if ($distance<=100){$price_0 = 90000;}
        //             else {
        //                 $price_0 = $this->int_element(((int)$distance-100),10)*5000 + 90000;
        //             }

        //         }
        //     }
        //     if ($kind_class==5){$price_0+=15000;}
        //     else if ($kind_class==2){$price_0+=10000;}


        //     $max_fee[]=$price_0;

        //     return min($max_fee);

        // }




        // public function special_price(string $start, string $dest, int $kind_class) : int
        // {
        //     $price_0=0;


        //     //// CASE 1
        //         $filter_case1=[
        //             ["강남구","강남구",1,7000],
        //             ["강남구","서초구",1,8000],
        //             ["서초구","서초구",1,7000],
        //             ["강남구","여의도동",1,10000],
        //             ["서초구","여의도동",1,10000],
        //             ["여의도동","여의도동",1,7000],

        //             ["강남구","강남구",2,20000],
        //             ["강남구","서초구",2,20000],
        //             ["서초구","서초구",2,7000],
        //             ["강남구","여의도동",2,25000],
        //             ["서초구","여의도동",2,25000],

        //             ["강남구","여의도동",5,30000],
        //             ["서초구","여의도동",5,30000],
        //         ];
        //         $non_filter_case1=["내곡동","자곡동","원지동","방배동"];

        //         foreach ($filter_case1 as $vv) {
        //             if ( ( ( strpos($start,$vv[0])!==false && strpos($dest,$vv[1])!==false ) || ( strpos($start,$vv[1])!==false && strpos($dest,$vv[0])!==false ) ) && $kind_class==$vv[2] ){
        //                 $price_0=$vv[3];
        //             }
        //         }
        //         foreach ($non_filter_case1 as $vv) {
        //             if ( strpos($start,$vv)!==false || strpos($dest,$vv)!==false ){
        //                 $price_0=0;
        //             }
        //         }


        //     //// CASE 2

        //         $filter_case2=[
        //             ["강남구","종로구",1,10000],
        //             ["강남구","중구",1,10000],
        //             ["서초구","종로구",1,10000],
        //             ["서초구","즁구",1,10000],
        //             ["여의도","종로구",1,10000],
        //             ["여의도","중구",1,10000],

        //             ["강남구","종로구",2,25000],
        //             ["강남구","중구",2,25000],
        //             ["서초구","종로구",2,25000],
        //             ["서초구","중구",2,25000],
        //             ["여의도","종로구",2,25000],
        //             ["여의도","중구",2,25000],

        //             ["강남구","종로구",5,30000],
        //             ["강남구","중구",5,30000],
        //             ["서초구","종로구",5,30000],
        //             ["서초구","중구",5,30000],
        //             ["여의도","종로구",5,30000],
        //             ["여의도","중구",5,30000],
        //         ];
        //         $non_filter_case2=["부암동","평창동","명륜","연건동","혜화동","무악동","청운동","삼청동","가희동"];


        //         foreach ($filter_case2 as $vv) {
        //             if ( ( ( strpos($start,$vv[0])!==false && strpos($dest,$vv[1])!==false ) || ( strpos($start,$vv[1])!==false && strpos($dest,$vv[0])!==false ) ) && $kind_class==$vv[2] ){
        //                 $price_0=$vv[3];
        //             }
        //         }
        //         foreach ($non_filter_case2 as $vv) {
        //             if ( strpos($start,$vv)!==false || strpos($dest,$vv)!==false ){
        //                 $price_0=0;
        //             }
        //         }

        //     return $price_0;

        // }




        // public function getprice() : void
        // {


        //     // https://quickcar.co.kr/Order/getprice?kind=1&doc=1&startSi=서울시&startGun=금천구&startDong=독산동&endSi=서울시&endGun=관악구&endDong=봉천동
        //     // 링크접속




        //     $this->load->library('Deliver');
        //     $this->load->library('NaverGeo');

        //     //입력 데이터 정리
        //     $this->session->set_userdata('kind', trim($this->input->get('kind', true)));
        //     $this->session->set_userdata('doc', trim($this->input->get('doc', true)));
        //     $this->session->set_userdata('count_p', trim($this->input->get('count_p', true)));
        //     $this->session->set_userdata('etc_p', trim($this->input->get('etc_p', true)));
        //     $this->session->set_userdata('memo', trim($this->input->get('memo', true)));
        //     $this->session->set_userdata('product', trim($this->input->get('product', true)));


        //     $this->session->set_userdata('start_detail', trim($this->input->get('start_detail', true)));
        //     $this->session->set_userdata('dest_detail', trim($this->input->get('dest_detail', true)));


        //     $kind = trim($this->input->get('kind', true));
        //     $count_p = trim($this->input->get('count_p', true));
        //     $product = trim($this->input->get('product', true));


        //     // $this->session->set_userdata('start', trim($this->input->get('start', true)));
        //     // $this->session->set_userdata('end', trim($this->input->get('end', true)));
        //     // $requested=$this->navergeo->getDistanceFromLocale($this->session->start,$this->session->end);
        //     // $start_weather=$this->get_weather($requested['start']['x'],$requested['start']['y']);
        //     // $dest_weather=$this->get_weather($requested['dest']['x'],$requested['dest']['y']);
        //     // $this->session->set_userdata('get_price_requested', $requested);
        //     // $this->session->set_userdata('start_weather', $start_weather);
        //     // $this->session->set_userdata('dest_weather', $dest_weather);


        //     if ( is_null($this->session->get_price_requested) || is_null($this->session->start_weather) || is_null($this->session->start) || is_null($this->session->end)  ||  $this->session->start!=trim($this->input->get('start', true)) || $this->session->end!=trim($this->input->get('end', true))  )
        //     {

        //         $this->session->set_userdata('start', trim($this->input->get('start', true)));
        //         $this->session->set_userdata('end', trim($this->input->get('end', true)));
        //         $requested=$this->navergeo->getDistanceFromLocale($this->session->start,$this->session->end);
        //         $start_weather=$this->get_weather($requested['start']['x'],$requested['start']['y']);
        //         $dest_weather=$this->get_weather($requested['dest']['x'],$requested['dest']['y']);
        //         $this->session->set_userdata('get_price_requested', $requested);
        //         $this->session->set_userdata('start_weather', $start_weather);
        //         $this->session->set_userdata('dest_weather', $dest_weather);

        //     }
        //     else{
        //         $requested=$this->session->get_price_requested;
        //         $start_weather=$this->session->start_weather;
        //         $dest_weather=$this->session->dest_weather;
        //     }


        //     //거리 연산
        //     $distance = $requested['distance']/1000;
        //     $this->session->set_userdata('searched', $requested);


        //     if ($requested === null){
        //         $price_0=0;
        //     }
        //     else if (   $this->special_price($requested['start']['address'],$requested['dest']['address'], $kind)!=0   ){
        //         $price_0=$this->special_price($requested['start']['address'],$requested['dest']['address'], $kind);
        //     }
        //     else{
        //         //거리 요금 산정

        //         $price_0 = $this->distance_price($requested['start']['si'], $requested['dest']['si'], $kind , $distance);

        //         if ($kind==3){
        //             if ($price_0<=$this->distance_price($requested['start']['si'], $requested['dest']['si'], 5 , $distance)){
        //                 $price_0=$this->distance_price($requested['start']['si'], $requested['dest']['si'], 5 , $distance);
        //             }
        //         }

        //     }


        //     ////  추가요금 산정
        //     $price_1=0;
        //     $add_max=9999999;

        //     $add_ratio=array();

        //     if ($kind==1 && (  ( $product=="쇼핑백" && $count_p==3 )   || ( $product=="소박스" && $count_p==3 ) || $product=="대박스" )){//오토바이 쇼핑백 3개, 소박스 3개, 대박스일경우
        //         $price_1=3000;
        //     }
        //     if (1830<=date("Hi") && date("Hi")<1930){
        //         array_push($add_ratio,0.3);
        //         $add_max=10000;
        //     }
        //     if (1930<=date("Hi") && date("Hi")<2030){
        //         array_push($add_ratio,0.5);
        //         $add_max=20000;
        //     }
        //     if (2030<=date("Hi") && date("Hi")<2400){
        //         array_push($add_ratio,1);
        //         $add_max=30000;
        //     }
        //     if (date("Hi")<600){
        //         array_push($add_ratio,1);
        //         $add_max=30000;
        //     }
        //     if (600<=date("Hi") && date("Hi")<730){
        //         array_push($add_ratio,0.5);
        //         $add_max=20000;
        //     }


        //     if ( $start_weather!=0 ||  $dest_weather!=0 ) {
        //         array_push($add_ratio,0.5);
        //     }


        //     if (count($add_ratio)!=0){
        //         $price_1=(int)(($price_0+$price_1)*max($add_ratio))+$price_1;
        //     }

        //     $price_1 = floor( $price_1 /1000 )*1000;
        //     $price_1 = min ($price_1,$add_max);


        //     $this->session->set_userdata('price', $price_0);
        //     $this->session->set_userdata('add_price', $price_1);

        //     $answerdata=[
        //         "price"=>$price_0,
        //         "add_price"=>$price_1,
        //         "distance" => $distance,
        //     ];



        //     header('Content-type: application/json');
        //     echo json_encode($answerdata);


        // }













        // public function do_order() : void
        // {



        //     $this->load->model('Order/Ordermodel');
        //     $this->load->library('Deliver');

        //     $price_multiply=1;


        //     if (trim($this->input->get('doc', true))==1){
        //         $price_multiply=1;
        //     }
        //     if (trim($this->input->get('doc', true))==3){
        //         $price_multiply=2;
        //     }

        //     $memo="";

        //     if ($this->session->product=="기타")
        //         {
        //             $memo.="전화먼저 / ".$this->session->etc_p;
        //         }
        //     else
        //         {

        //             $memo.="전화먼저 / ".$this->session->product;
        //             if ($this->session->count_p!=0){
        //                 $memo.=" ".$this->session->count_p." 개";
        //             }
        //         }
        //     if ($this->session->memo!=""){
        //         $memo.=" / ".$this->session->memo;
        //     }


        //     if($this->session->thisuser==1 || $this->session->thisuser==0){
        //         $orderName = 'Q-'.$this->session->start_name.'-'.$this->session->start_telno;
        //         $orderMobile = $this->session->start_telno;
        //         $orderDongName = $this->session->searched['start']['dong'];
        //         $orderJoinName = $this->session->start_name;
        //     }
        //     else{
        //         $orderName = 'Q-'.$this->session->dest_name.'-'.$this->session->dest_telno;
        //         $orderMobile = $this->session->dest_telno;
        //         $orderDongName = $this->session->searched['dest']['dong'];
        //         $orderJoinName = $this->session->dest_name;
        //     }


        //     while (count($this->Ordermodel->get_usercheck($orderMobile))==0){
        //         $this->register($orderMobile,$orderJoinName,$orderDongName);
        //     }

        //     // var_dump($this->register($orderMobile,$orderName,$orderDongName));
        //     // var_dump($orderMobile);
        //     // var_dump($this->Ordermodel->get_usercheck($orderMobile));

        //     $order_insung_id = $this->Ordermodel->get_usercheck($orderMobile)[0]['user_id'];

        //     $orderresult = $this->deliver->order([



        //         'userId' => $order_insung_id,
        //         'orderName' => $orderName,
        //         'orderMobile' => $orderMobile,

        //         'startLocate' => $this->session->start.' '.$this->session->start_detail,
        //         'startTelNo' => $this->session->start_telno,
        //         'startSido' => $this->session->searched['start']['si'],
        //         'startGugun' => $this->session->searched['start']['gun'],
        //         'startDong' => $this->session->searched['start']['dong'],
        //         'startName' => $this->session->start_name,

        //         'destLocal' => $this->session->end.' '.$this->session->dest_detail,
        //         'destTelNo' => $this->session->dest_telno,
        //         'destSido' => $this->session->searched['dest']['si'],
        //         'destGugun' => $this->session->searched['dest']['gun'],
        //         'destDong' => $this->session->searched['dest']['dong'],
        //         'destName' => $this->session->dest_name,


        //         'deliverType' => $this->session->kind,     //  kind	 배송수단(1:오토, 2:다마스, 5:라보, 4:밴, 6:지하철, 3:트럭)	 필수항목
        //         'payType' => trim($this->input->get('payType', true)),      //지급방법(1:선불, 2:착불, 3:신용, 4:송금, 5:수금)	 필수항목
        //         'doc' => trim($this->input->get('doc', true)),              //  doc	 배송방법(1:편도, 3:왕복, 5:경유)	 필수항목
        //         'sfast' => trim($this->input->get('sfast', true)),          //  sfast	 배송선택(1:일반, 3:급송, 5:조조, 7:야간)	 필수항목



        //         'price' => trim($this->input->get('want_price', true)),
        //         'start_lon' => (int)($this->session->searched['start']['x']*360000),
        //         'start_lat' => (int)($this->session->searched['start']['y']*360000),
        //         'dest_lon' => (int)($this->session->searched['dest']['x']*360000),
        //         'dest_lat' => (int)($this->session->searched['dest']['y']*360000),


        //         'memo' => $memo,


        //         'card_number' => trim($this->input->get('card_number', true)),
        //         'card_year' => trim($this->input->get('card_year', true)),
        //         'card_month' => trim($this->input->get('card_month', true)),
        //         'card_email' => trim($this->input->get('card_email', true)),


        //      ]);



        //      $orderArray = [



        //         'userId' => $order_insung_id,
        //         'orderName' => $orderName,
        //         'orderMobile' => $orderMobile,

        //         'startLocal' => $this->session->start,
        //         'startTelNo' => $this->session->start_telno,
        //         'startSido' => $this->session->searched['start']['si'],
        //         'startGugun' => $this->session->searched['start']['gun'],
        //         'startDong' => $this->session->searched['start']['dong'],
        //         'startName' => $this->session->start_name,

        //         'destLocal' => $this->session->end,
        //         'destTelNo' => $this->session->dest_telno,
        //         'destSido' => $this->session->searched['dest']['si'],
        //         'destGugun' => $this->session->searched['dest']['gun'],
        //         'destDong' => $this->session->searched['dest']['dong'],
        //         'destName' => $this->session->dest_name,


        //         'deliverType' => $this->session->kind,     //  kind	 배송수단(1:오토, 2:다마스, 5:라보, 4:밴, 6:지하철, 3:트럭)	 필수항목
        //         'payType' => trim($this->input->get('payType', true)),      //지급방법(1:선불, 2:착불, 3:신용, 4:송금, 5:수금)	 필수항목
        //         'doc' => trim($this->input->get('doc', true)),              //  doc	 배송방법(1:편도, 3:왕복, 5:경유)	 필수항목
        //         'sfast' => trim($this->input->get('sfast', true)),          //  sfast	 배송선택(1:일반, 3:급송, 5:조조, 7:야간)	 필수항목



        //         'price' => trim($this->input->get('want_price', true)),
        //         'start_lon' => ($this->session->searched['start']['x']),
        //         'start_lat' => ($this->session->searched['start']['y']),
        //         'dest_lon' => ($this->session->searched['dest']['x']),
        //         'dest_lat' => ($this->session->searched['dest']['y']),


        //         'memo' => $memo,

        //         'orderNum' => $orderresult['message'][1]->serial_number,



        //         'card_number' => trim($this->input->get('card_number', true)),
        //         'card_year' => trim($this->input->get('card_year', true)),
        //         'card_month' => trim($this->input->get('card_month', true)),
        //         'card_email' => trim($this->input->get('card_email', true)),



        //      ];


        //     $this->session->set_userdata('orderId', $order_insung_id );





        //     $this->Ordermodel->add_order($orderArray);

        //     if(is_null($this->session->orderlist)){
        //         $this->session->set_userdata('orderlist', array($orderresult['message'][1]->serial_number) );
        //     }
        //     else{

        //         $orderlist = $this->session->orderlist;
        //         $orderlist[]=$orderresult['message'][1]->serial_number;
        //         $this->session->set_userdata('orderlist', $orderlist );
        //         // $this->session->set_userdata('orderlist', array_push($this->session->orderlist, $orderresult['message'][1]->serial_number));
        //     }
        //     // var_dump(array($orderresult['message'][1]->serial_number));

        //     //  var_dump($this->session->orderlist);


        //     $path = realpath(__DIR__ . '/../../public/order');
        //     $json_data = json_encode($orderArray);

        //     $json_data = iconv("UTF-8","CP949", $json_data);


        //     file_put_contents($path."/order_".date("Ymd-His")."_".uniqid()."_".$orderName.".txt", $json_data);


        //     // var_dump($orderresult['message'][1]->serial_number);

        //      if ($orderresult['message'][0]->code == '1000' ){
        //         goBackAlert( '주문을 성공적으로 입력하였습니다. 배차되는 과정을 별도로 안내드리겠습니다.', $this->config->site_url('Orderlist'));
        //      }
        //      else{
        //         goBackAlert('네트워크에 일시적으로 오류가 있습니다. 다시 시도 부탁드립니다.', $this->config->site_url('Main'));
        //      }



        // }
    }
}
