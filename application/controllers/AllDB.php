<?php

if (! class_exists('AllDB')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class AllDB extends CI_Controller
    {
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->load->view(
                'm_common/head',
                [

                'static' => [
                    'assets' =>loadManagerCommonAssets(),

                    'url'=>loadCommonUrl()
                ]
            ]
            );
        }



        public function index(): void
        {




            if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/mydb')) {
                $this->load->view('Dblist/alldb', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                    'text'=> [
                        'menu_1' => "DB관리",
                        'menu_2' => "유입 DB 분배",
                        'menu_3' => "",
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }


        }
    }
}
