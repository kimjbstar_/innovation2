<?php

if (! class_exists('Order1')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Order1 extends CI_Controller
    {
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->baseAssets = $this->file->getDefaultAssets();
            $this->lang->load('addStudio', 'korean');
        }
        public function index(): void
        {
            $file = $this->file;
            $load = $this->load;
            $load->view('common/head', [
                'css' => $this->baseAssets['css']
            ]);

            $load->view('Order1/body', [
                'img' => [
                    'back' => $file->getIconUrl('back.svg'),
                    'address' => $file->getIconUrl('marker.svg'),
                    'phone' => $file->getIconUrl('phone.svg'),
                    'menu' => $file->getIconUrl('menu.svg'),
                    'avater' => $file->getIconUrl('avatar-dark.svg'),
                    'register' => $file->getIconUrl('driver-registration-dark.svg'),
                    'mapPos' => $file->getIconUrl('map-position.svg'),
                ],
                'url' => [
                    'back' => $this->config->site_url('Main'),
                    'save' => $this->config->site_url('Studio/Add/register')
                ],
                'data' => [
                    'userId' => $this->session->userId,
                    'userMail' => $this->session->userEmail,
                    'csrf' => [
                        'name' => $this->security->get_csrf_token_name(),
                        'hash' => $this->security->get_csrf_hash()
                    ],
                ]
            ]);

            $load->view('common/foot', [
                'js' => array_merge($this->baseAssets['js'], [
                    'postCode' => $file->getJsUrl('lib/postcode.v2.js'),
                    'addAddress' => $file->getJsUrl('addStudio/main.js')
                ])
            ]);
        }
    }
}
