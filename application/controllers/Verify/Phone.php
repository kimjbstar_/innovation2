<?php

if (! class_exists('Phone')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Phone extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->baseAssets = $this->file->getDefaultAssets();
            $this->lang->load('authCode', 'korean');
        }
        public function get(): void
        {
            if (isset($this->session->registerInfo)) {
                $load = $this->load;
                $load->view('common/head', [
                    'css' => array_merge($this->baseAssets['css'], [
                        'intlTel' => 'https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/css/intlTelInput.css',
                    ])
                ]);
                $load->view('Verify/GetPhoneNum/body', [
                    'data' => [
                        'userPhone' => $this->session->registerInfo['phone'],
                        'csrf' => [
                            'name' => $this->security->get_csrf_token_name(),
                            'hash' => $this->security->get_csrf_hash()
                        ],
                    ],
                    'url' => [
                        'sendMessage' => $this->config->site_url('Verify/Phone/set'),
                    ],
                    'img' => [
                        'back' => $this->file->getIconUrl('back.svg'),
                    ]
                ]);
                $load->view('common/foot', [
                    'js' => insertAfter($this->baseAssets['js'], [
                        'util' => $this->file->getJsUrl('lib/util.js'),
                        'inputNum' => $this->file->getJsUrl('getPhone/intlTelInput.min.js')
                    ], 'swal')
                ]);
            } else {
                redirect($this->config->base_url(), 'refresh');
            }
        }

        private function buildMessage(int $code): string
        {
            return $this->lang->line('authSMSMessage') . $code . $this->lang->line('authSMSMessageEnd');
        }

        public function set(): void
        {
            $phoneNum = trim($this->input->post('phone', true));
            if ($phoneNum && ($phoneNum === $this->session->registerInfo['phone'])) {
                $this->load->helper('sms');
                try {
                    $sendSMS = sendSMS(
                        $this->lang->line('authSMSTitle'),
                        $this->buildMessage($authCode = random_int(1000, 9999)),
                        [
                            $this->session->registerInfo['phone']
                        ]
                    );
                    if ($sendSMS->status === '0') {
                        $this->session->set_userdata('authCode', $authCode);
                        redirect($this->config->site_url('Verify/Main'), 'refresh');
                    } else {
                        alert($this->lang->line('tryAgain'), $this->config->base_url());
                    }
                } catch (Exception $e) {
                    $this->load->helper('data');
                    clearSession();
                    alert($this->lang->line('tryAgain'), $this->config->base_url());
                }
            } else {
                alert($this->lang->line('invalidPhone'), $this->config->base_url());
            }
        }
    }
}
