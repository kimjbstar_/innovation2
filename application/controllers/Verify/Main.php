<?php

if (! class_exists('Main')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Main extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->baseAssets = $this->file->getDefaultAssets();
            $this->lang->load('authCode', 'korean');
        }
        public function index(): void
        {
            $file = $this->file;
            $load = $this->load;
            $load->view('common/head', [
                'css' => $this->baseAssets['css']
            ]);
            $load->view('Verify/Auth/body', [
                'img' => [
                    'back' => $file->getIconUrl('back.svg'),
                    'verified' => $file->getIconUrl('verified.svg'),
                    'back' => $file->getIconUrl('back.svg'),
                ],
                'url' => [
                    'back' => $this->config->site_url('Main'),
                    'verifyCode' => $this->config->site_url('Verify/Main/Auth')
                ],
                'data' => [
                    'userId' => $this->session->registerInfo['phone'],
                    'csrf' => [
                        'name' => $this->security->get_csrf_token_name(),
                        'hash' => $this->security->get_csrf_hash()
                    ],
                ]
            ]);
            $load->view('common/foot', [
                'js' => $this->baseAssets['js']
            ]);
        }
        public function Auth(): void
        {
            $authCode = abs(intval(trim($this->input->post('authCode', true))));
            if (isset($this->session->registerInfo) && $this->session->authCode === $authCode) {
                $this->load->model('User/User');
                $this->load->helper(['user', 'valid', 'data']);

                $valid = (new ValidPost($this->session->registerInfo))->setRules([
                    'username' => 'require|unique:member_id,members|min:6|max:18|contain:number,alpha',
                    // 'email' => 'require|email|unique:member_email,members',
                    // 'phone' => 'require|numeric',
                ])->run();

                if ($valid['stat'] && $this->User->add($this->session->registerInfo)) {
                    redirect($this->config->site_url('Verify/success'), 'refresh');
                } else {
                    clearSession();
                    alert($this->lang->line('tryAgain'), $this->config->base_url());
                }
            } else {
                alert($this->lang->line('authCodeNotMatch'), $this->config->base_url('Verify/Main'));
            }
        }
    }
}
