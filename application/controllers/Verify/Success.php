<?php

if (! class_exists('Success')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Success extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->baseAssets = $this->file->getDefaultAssets();
        }
        public function set(): Type
        {
            if (isset($this->session->registerInfo)) {
                $this->load->helper(['data', 'user']);
                setSession([
                    'isLogin' => true,
                    'userId' => $this->session->registerInfo['username'],
                    'userIdx' => getUserInfoByUserId($this->session->registerInfo['username'])->userIdx,
                    'isAdmin' => false,
                ]);
                $this->session->unset_userdata('registerInfo');
                $this->session->unset_userdata('authCode');
                redirect($this->config->site_url('Main'), 'refresh');
            } else {
                redirect($this->config->base_url(), 'refresh');
            }
        }

        public function index(): void
        {
            $load = $this->load;
            $file = $this->file;
            if (isset($this->session->registerInfo)) {
                $load->view('common/head', [
                    'css' => $this->baseAssets['css'],
                ]);
                $load->view('Verify/Success/body', [
                    'img' => [
                        'back' => $file->getIconUrl('back.svg'),
                        'verified' => $file->getIconUrl('verified.svg'),
                    ],
                    'url' => [
                        'main' => $this->config->site_url('Verify/Success/set'),
                    ]
                ]);
                $load->view('common/foot', [
                    'js' => $this->baseAssets['js'],
                ]);
            } else {
                redirect($this->config->base_url(), 'refresh');
            }
        }
    }
}
