<?php

if (! class_exists('Auth')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Auth extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->lang->load('registerValid', 'korean');
            $this->load->view(
                'm_common/head_without_menu',
                [

                'static' => [
                    'assets' =>loadManagerCommonAssets(),

                    'url'=>loadCommonUrl()
                ]
            ]
            );
        }
        public function login(): void
        {

            var_dump($_SERVER["REMOTE_ADDR"]);

            $fruits = ['125.128.73.193', '125.128.73.194','125.128.73.199','192.168.81.93', '10.118.80.58', '175.198.109.209','106.101.2.204','121.130.243.173','211.114.186.60','108.162.215.224','172.69.34.66','39.7.46.125','118.235.9.196','110.70.16.102','121.130.210.92','118.127.201.27','121.130.210.92','59.15.209.191', '61.72.43.11', '125.128.73.193','125.128.73.194','125.128.73.199','192.168.81.93', '118.235.3.79','122.36.21.26','39.7.231.66', '172.30.1.43', '39.7.230.49','121.131.215.227','192.168.219.138'];

            

            if (!in_array($_SERVER["REMOTE_ADDR"], $fruits))
            {

                alert("접속 허용되지 않은 네트워크입니다.", $this->config->site_url('Home'));
                return;
            }




            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/auth/login')) {
                $this->load->view('Auth/login', [
                    'url' => [
                        'login' => $this->config->site_url('Auth/attempt'),
                        'signUp' => $this->config->site_url('Auth/valid'),
                    ],
                    'data' => [
                        'csrf' => [
                            'name' => $this->security->get_csrf_token_name(),
                            'hash' => $this->security->get_csrf_hash()
                        ],
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }
        public function register(): void
        {
            if ($this->file->checkView('m_common/head', 'm_common/footer', 'pages/auth/register')) {
                $this->load->view('pages/auth/register', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }
        }


        public function attempt(): void
        {


            $fruits = ['125.128.73.193', '125.128.73.194','125.128.73.199','192.168.81.93', '10.118.80.58', '175.198.109.209','106.101.2.204','121.130.243.173','211.114.186.60','108.162.215.224','172.69.34.66','39.7.46.125','118.235.9.196','110.70.16.102','121.130.210.92','118.127.201.27','121.130.210.92','59.15.209.191', '61.72.43.11', '125.128.73.193','125.128.73.194','125.128.73.199','192.168.81.93', '118.235.3.79','122.36.21.26','39.7.231.66', '172.30.1.43', '39.7.230.49','121.131.215.227','192.168.219.138'];


            if (!in_array($_SERVER["REMOTE_ADDR"], $fruits))
            {

                alert("접속 허용되지 않은 네트워크입니다.", $this->config->site_url('Auth/login'));
                return;
            }



            $this->load->helper(['valid', 'data']);
            $this->load->model('User/User');
            $data = trimArray($this->input->post([
                'username', 'password', 'query'
            ], true));

            $validRlt = (new ValidPost($data))->setRules([
                'username' => 'require',
                'password' => 'require'
            ])->run();

            if (! $validRlt['stat']) {
                alert($this->lang->line($validRlt['message']), $this->config->site_url('Check'));
                return;
            }

            $attemptRlt = $this->User->attempt($data);
            if ($attemptRlt['stat']) {
                setSession($attemptRlt['data']);
                redirect($this->config->site_url('Richmango/Links/Buttons'));
            } else {
                // var_dump($this->session);
                alert($this->lang->line($attemptRlt['message']), $this->config->site_url('Auth/login'));
            }
        }

        public function valid(): void
        {
            $data = trimArray($this->input->post([
                'username',  'password', 'passwordConfirm'
            ], true));

            $this->load->helper('valid');
            $valid = (new ValidPost($data))->setRules([
                'username' => 'require|unique:member_id,members|min:6|max:18|contain:number,alpha',
                // 'email' => 'require|email|unique:member_email,members',
                // 'phone' => 'require|numeric',
                'password' => 'require|min:4|max:4',
                'passwordConfirm' => 'require|same:password|min:4|max:4',
            ])->run();

            if ($valid['stat']) {
                $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
                unset($data['passwordConfirm']);
                $this->session->set_userdata('registerInfo', $data);
                // redirect($this->config->site_url('Verify/Phone/get'), 'refresh');


                $this->load->model('User/User');
                $this->load->helper(['user', 'valid', 'data']);

                $valid = (new ValidPost($this->session->registerInfo))->setRules([
                    'username' => 'require|unique:member_id,members|min:6|max:18|contain:number,alpha',
                    // 'email' => 'require|email|unique:member_email,members',
                    // 'phone' => 'require|numeric',
                ])->run();

                if ($valid['stat'] && $this->User->add($this->session->registerInfo)) {
                    redirect($this->config->site_url('MyDB'), 'refresh');
                } else {
                    clearSession();
                    alert($this->lang->line('tryAgain'), $this->config->base_url());
                }
            } else {
                alert($this->lang->line($valid['message']), $this->config->base_url());
            }
        }
        public function logout(): void
        {
            $this->load->helper('data');
            clearSession();
            redirect($this->config->site_url('Auth/login'), 'refresh');
        }

    }
}
