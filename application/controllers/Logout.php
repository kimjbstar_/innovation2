<?php

if (! class_exists('Logout')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Logout extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->lang->load('registerValid', 'korean');
        }
        public function logout(): void
        {
            $this->load->helper('data');
            clearSession();
            redirect($this->config->site_url('Auth/login'), 'refresh');
        }

    }
}
