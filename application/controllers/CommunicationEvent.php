<?php

if (! class_exists('CommunicationEvent')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class CommunicationEvent extends CI_Controller
    {
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
        }


        public function Ask(): void
        {



            
            $data = trimArray($this->input->post([
                'var1', 'var2', 'var3', 'var4'
            ], true));
            // var_dump($data);

            if ($data['var1']=="" || $data['var2']==""|| $data['var3']==""|| $data['var4']=="") {
                echo "<script>alert('성명, 이메일, 질문분야, 상담내용은 필수 입력사항입니다.');location.href='http://publicservicefair.kr/CommunicationEvent';</script>";
                return;
            }
            else{

                $ch = curl_init();
            
            
                /* 여기서부터 수정해주시기 바랍니다. */
                $subject = '[온라인공직박람회- '.$data['var3'].' 문의] '.$data['var1'].'님';   //필수입력(템플릿 미사용시), 템플릿 사용시 공백을 입력 하시기 바랍니다.
                $body = '<p>문의 사항 : '.$data['var4'].'</p>';                 //필수입력(템플릿 미사용시), 템플릿 사용시 공백을 입력 하시기 바랍니다.
                $sender = $data['var2'];          //필수입력
                $sender_name = $data['var1'];
                $username = "insider9";                //필수입력
                $key = "DtG1gG0QCCdNpB8";           //필수입력
                
                //수신자 정보 추가 - 필수 입력(주소록 미사용시), 치환문자 미사용시 치환문자 데이터를 입력하지 않고 사용할수 있습니다.
                //치환문자 미사용시 {"email":"aaaa@directsend.co.kr"} 이메일만 입력 해주시기 바랍니다.

                if ($data['var3']=="적극행정관"){$receiver = '{"name":"적극행정지원과", "email":"powershimba@korea.kr"}'.',{"name":"취합용", "email":"insdrcr.psf@gmail.com"}';}
                else if ($data['var3']=="공정채용관"){$receiver = '{"name":"인재정책과", "email":"kjh5218@korea.kr"}'.',{"name":"취합용", "email":"insdrcr.psf@gmail.com"}';}
                else if ($data['var3']=="공개경력채용관"){$receiver = '{"name":"공개채용1과", "email":"yjkim13@korea.kr"}'.',{"name":"취합용", "email":"insdrcr.psf@gmail.com"}';}
                else if ($data['var3']=="K-시험방역관"){$receiver = '{"name":"공개채용1과", "email":"yjkim13@korea.kr"}'.',{"name":"취합용", "email":"insdrcr.psf@gmail.com"}';}
                else if ($data['var3']=="균형인사관"){$receiver = '{"name":"균형인사과", "email":"bovs133@korea.kr"}'.',{"name":"취합용", "email":"insdrcr.psf@gmail.com"}';}
                else if ($data['var3']=="공직역사가치관"){$receiver = '{"name":"복무과", "email":"pluslsm@korea.kr"}'.',{"name":"취합용", "email":"insdrcr.psf@gmail.com"}';}
                else if ($data['var3']=="미래인사혁신관"){$receiver = '{"name":"인재정책과", "email":"dotheright@korea.kr"}'.',{"name":"취합용", "email":"insdrcr.psf@gmail.com"}';}



                
                $receiver = '['.$receiver.']';      //JSON 데이터
                
                $bodytag = '1';  //HTML이 기본값 입니다. 메일 내용을 텍스트로 보내실 경우 주석을 해제 해주시기 바랍니다.
                $return_url = 0;
                $option_return_url = 0;
                $open = 1;	// open 결과를 받으려면 아래 주석을 해제 해주시기 바랍니다.
                $click = 1;	// click 결과를 받으려면 아래 주석을 해제 해주시기 바랍니다.
                $check_period = 3;	// 트래킹 기간을 지정하며 3 / 7 / 10 / 15 일을 기준으로 지정하여 발송해 주시기 바랍니다. (단, 지정을 하지 않을 경우 결과를 받을 수 없습니다.)
                // 예약발송 정보 추가
                $mail_type = 'NORMAL'; // NORMAL - 즉시발송 / ONETIME - 1회예약 / WEEKLY - 매주정기예약 / MONTHLY - 매월정기예약
                $start_reserve_time = date('Y-m-d H:i:s'); //  발송하고자 하는 시간(시,분단위까지만 가능) (동일한 예약 시간으로는 200회 이상 API 호출을 할 수 없습니다.)
                $end_reserve_time = date('Y-m-d H:i:s'); //  발송이 끝나는 시간 1회 예약일 경우 $start_reserve_time = $end_reserve_time
                // WEEKLY | MONTHLY 일 경우에 시작 시간부터 끝나는 시간까지 발송되는 횟수 Ex) type = WEEKLY, start_reserve_time = '2017-05-17 13:00:00', end_reserve_time = '2017-05-24 13:00:00' 이면 remained_count = 2 로 되어야 합니다.
                $remained_count = 1;
                // 예약 수정/취소 API는 소스 하단을 참고 해주시기 바랍니다.
                
                //필수안내문구 추가
                $agreement_text = '본메일은 [$NOW_DATE] 기준, 회원님의 수신동의 여부를 확인한 결과 회원님께서 수신동의를 하셨기에 발송되었습니다.';
                $deny_text = "메일 수신을 원치 않으시면 [" . '$DENY_LINK' . "]를 클릭하세요.\\nIf you don't want this type of information or e-mail, please click the [".'$EN_DENY_LINK'."]";
                $sender_info_text = "사업자 등록번호:-- 소재지:ㅇㅇ시(도) ㅇㅇ구(군) ㅇㅇ동 ㅇㅇㅇ번지 TEL:--\\nEmail: <a href='mailto:test@directsend.co.kr'>test@directsend.co.kr</a>";
                $logo_state = 1; // logo 사용시 1 / 사용안할 시 0
                $logo_path = 'http://logoimage.com/image.png';  //사용하실 로고 이미지를 입력하시기 바랍니다.
                $logo_sort = 'CENTER';      //로고 정렬 LEFT - 왼쪽 정렬 / CENTER - 가운데 정렬 / RIGHT - 오른쪽 정렬
                $footer_sort = 'CENTER';      //메일내용, 풋터(수신옵션) 정렬 LEFT - 왼쪽 정렬 / CENTER - 가운데 정렬 / RIGHT - 오른쪽 정렬
                
                // 첨부파일의 URL을 보내면 DirectSend에서 파일을 download 받아 발송처리를 진행합니다. 첨부파일은 전체 10MB 이하로 발송을 해야 하며, 파일의 구분자는 '|(shift+\)'로 사용하며 5개까지만 첨부가 가능합니다.
                $file_url = '';
                // 첨부파일의 이름을 지정할 수 있도록 합니다.
                // 첨부파일의 이름은 순차적(https://directsend.co.kr/test.png - image.png, https://directsend.co.kr/test1.png - image2.png) 와 같이 적용이 되며, file_name을 지정하지 않은 경우 마지막의 파일의 이름으로 메일에 보여집니다.
                $file_name = '';
                
                /* 여기까지 수정해주시기 바랍니다. */
                
                $postvars = '"subject":"'.$subject.'"';
                $postvars = $postvars.', "body":"'.$body.'"';
                $postvars = $postvars.', "sender":"'.$sender.'"';
                $postvars = $postvars.', "sender_name":"'.$sender_name.'"';
                $postvars = $postvars.', "username":"'.$username.'"';
                $postvars = $postvars.', "receiver":'.$receiver;
                $postvars = $postvars.', "key":"'.$key.'"';
                $postvars = '{'.$postvars.'}';      //JSON 데이터
                
                // URL
                $url = "https://directsend.co.kr/index.php/api_v2/mail_change_word";
                
                //헤더정보
                $headers = array(
                    "cache-control: no-cache",
                    "content-type: application/json; charset=utf-8"
                );
                
                curl_setopt($ch,CURLOPT_URL, $url);
                curl_setopt($ch,CURLOPT_POST, true);
                curl_setopt($ch,CURLOPT_POSTFIELDS, $postvars);		//JSON 데이터
                curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
                curl_setopt($ch,CURLOPT_TIMEOUT, 60);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                $response = curl_exec($ch);
                // var_dump($response);
                // var_dump(json_encode($response));
                
                /*
                    status code
                    0   : 정상발송 (성공코드는 다이렉트센드 DB서버에 정상수신됨을 뜻하며 발송성공(실패)의 결과는 발송완료 이후 확인 가능합니다.)
                    100 : POST validation 실패
                    101 : 회원정보가 일치하지 않음
                    102 : Subject, Body 정보가 없습니다.
                    103 : Sender 이메일이 유효하지 않습니다.
                    104 : receiver 이메일이 유효하지 않습니다.
                    105 : 본문에 포함되면 안되는 확장자가 있습니다.
                    106 : body validation 실패
                    107 : 받는사람이 없습니다.
                    108 : 예약정보가 유효하지 않습니다.
                    109 : return_url이 없습니다.
                    110 : 첨부파일이 없습니다.
                    111 : 첨부파일의 개수가 5개를 초과합니다.
                    112 : 파일의 총Size가 10 MB를 넘어갑니다.
                    113 : 첨부파일이 다운로드 되지 않았습니다.
                    114 : utf-8 인코딩 에러 발생
                    115 : 템플릿 validation 실패
                    200 : 동일 예약시간으로는 200회 이상 API 호출을 할 수 없습니다.
                    201 : 분당 300회 이상 API 호출을 할 수 없습니다.
                    202 : 발송자명이 최대길이를 초과 하였습니다.
                    205 : 잔액부족
                    999 : Internal Error.
                 */
                
                //curl 에러 확인
                if(curl_errno($ch)){
                    // echo 'Curl error: ' . curl_error($ch);
    
                    echo "<script>alert('잠시 후 다시 시도해주세요.');location.href='http://publicservicefair.kr/CommunicationEvent';</script>";
                    curl_close ($ch);
                    return;                
                }else{
                    // print_R($response);
                    // var_dump(json_decode($response)->status);
    
                    if (json_decode($response)->status==0){
    
                        echo "<script>alert('문의를 발송하였습니다.');location.href='http://publicservicefair.kr/CommunicationEvent';</script>";
                        curl_close ($ch);
                        return;      
                    }
                    else if (json_decode($response)->status==103){
    
                        echo "<script>alert('정상적인 메일주소를 입력하여주세요.');location.href='http://publicservicefair.kr/CommunicationEvent';</script>";
                        curl_close ($ch);
                        return;      
                    }
                    else{
    
                        echo "<script>alert('잠시 후 다시 시도해주세요.');location.href='http://publicservicefair.kr/CommunicationEvent';</script>";
                        curl_close ($ch);
                        return;      
                    }
    
    
                }
            }



            
            

        }


        public function index(): void
        {



            $this->load->model('Dbuse');
            $Faqs = $this->Dbuse->getFaqs();
            $Notices = $this->Dbuse->getBoards('notices');
            $Events = $this->Dbuse->getEventInfos();
            $EventResults = $this->Dbuse->getEventResultInfos();
            // var_dump($Events);



            if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                $this->load->view('CommunicationEvent.php', [
                    'Faqs' => $Faqs,
                    'Notices' => $Notices,
                    'Events' => $Events,
                    'EventResults' => $EventResults,
                ]);
                $this->load->view('ThemeHall/ThemeAfter', [
                ]);
            } else {
                show_404();
            }


            // $mobilechk = '/(iPod|iPhone|Android|BlackBerry|SymbianOS|SCH-M\d+|Opera Mini|Windows CE|Nokia|SonyEricsson|webOS|PalmOS)/i'; 

            // // 모바일 접속인지 PC로 접속했는지 체크합니다.
            // if(preg_match($mobilechk, $_SERVER['HTTP_USER_AGENT'])) {



            //     if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
            //         $this->load->view('Mobile/Seminar', [
            //         ]);
            //         $this->load->view('Mobile/Menu', [
            //         ]);
            //     } else {
            //         show_404();
            //     }
            // } else { 

            //     if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
            //         $this->load->view('CommunicationEvent.php', [
            //             'Faqs' => $Faqs,
            //             'Notices' => $Notices,
            //             'Events' => $Events,
            //         ]);
            //         $this->load->view('ThemeHall/ThemeAfter', [
            //         ]);
            //     } else {
            //         show_404();
            //     }
            // }









        }









        public function EventDetail(): void
        {




            $this->load->model('Dbuse');
            $Faqs = $this->Dbuse->getFaqs();
            $Notices = $this->Dbuse->getBoards('notices');
            $Event = $this->Dbuse->getEventSpecificInfos($_GET['idx']);


            if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                $this->load->view('CommunicationEventDetail', [
                    'Faqs' => $Faqs,
                    'Notices' => $Notices,
                    'Event' => $Event,
                ]);
                $this->load->view('ThemeHall/ThemeAfter', [
                ]);
            } else {
                show_404();
            }


            // $mobilechk = '/(iPod|iPhone|Android|BlackBerry|SymbianOS|SCH-M\d+|Opera Mini|Windows CE|Nokia|SonyEricsson|webOS|PalmOS)/i'; 

            // // 모바일 접속인지 PC로 접속했는지 체크합니다.
            // if(preg_match($mobilechk, $_SERVER['HTTP_USER_AGENT'])) {



            //     if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
            //         $this->load->view('Mobile/Seminar', [
            //         ]);
            //         $this->load->view('Mobile/Menu', [
            //         ]);
            //     } else {
            //         show_404();
            //     }
            // } else { 

            //     if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
            //         $this->load->view('CommunicationEvent.php', [
            //             'Faqs' => $Faqs,
            //             'Notices' => $Notices,
            //             'Events' => $Events,
            //         ]);
            //         $this->load->view('ThemeHall/ThemeAfter', [
            //         ]);
            //     } else {
            //         show_404();
            //     }
            // }









        }









        public function EventResultDetail(): void
        {




            $this->load->model('Dbuse');
            $Faqs = $this->Dbuse->getFaqs();
            $Notices = $this->Dbuse->getBoards('notices');
            $Event = $this->Dbuse->getEventResultSpecificInfos($_GET['idx']);


            if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                $this->load->view('CommunicationEventResultDetail', [
                    'Faqs' => $Faqs,
                    'Notices' => $Notices,
                    'Event' => $Event,
                ]);
                $this->load->view('ThemeHall/ThemeAfter', [
                ]);
            } else {
                show_404();
            }


            // $mobilechk = '/(iPod|iPhone|Android|BlackBerry|SymbianOS|SCH-M\d+|Opera Mini|Windows CE|Nokia|SonyEricsson|webOS|PalmOS)/i'; 

            // // 모바일 접속인지 PC로 접속했는지 체크합니다.
            // if(preg_match($mobilechk, $_SERVER['HTTP_USER_AGENT'])) {



            //     if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
            //         $this->load->view('Mobile/Seminar', [
            //         ]);
            //         $this->load->view('Mobile/Menu', [
            //         ]);
            //     } else {
            //         show_404();
            //     }
            // } else { 

            //     if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
            //         $this->load->view('CommunicationEvent.php', [
            //             'Faqs' => $Faqs,
            //             'Notices' => $Notices,
            //             'Events' => $Events,
            //         ]);
            //         $this->load->view('ThemeHall/ThemeAfter', [
            //         ]);
            //     } else {
            //         show_404();
            //     }
            // }









        }





        public function NoticeDetail(): void
        {


            

            $this->load->model('Dbuse');
            $Notice = $this->Dbuse->getSpecificNotice($_GET['idx']);



            if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                $this->load->view('CommunicationEventNotice', [
                    'Notice' => $Notice,
                ]);
                $this->load->view('ThemeHall/ThemeAfter', [
                ]);
            } else {
                show_404();
            }


            // $mobilechk = '/(iPod|iPhone|Android|BlackBerry|SymbianOS|SCH-M\d+|Opera Mini|Windows CE|Nokia|SonyEricsson|webOS|PalmOS)/i'; 

            // // 모바일 접속인지 PC로 접속했는지 체크합니다.
            // if(preg_match($mobilechk, $_SERVER['HTTP_USER_AGENT'])) {



            //     if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
            //         $this->load->view('Mobile/Seminar', [
            //         ]);
            //         $this->load->view('Mobile/Menu', [
            //         ]);
            //     } else {
            //         show_404();
            //     }
            // } else { 

            //     if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
            //         $this->load->view('CommunicationEvent.php', [
            //             'Faqs' => $Faqs,
            //             'Notices' => $Notices,
            //             'Events' => $Events,
            //         ]);
            //         $this->load->view('ThemeHall/ThemeAfter', [
            //         ]);
            //     } else {
            //         show_404();
            //     }
            // }









        }







    }
}
