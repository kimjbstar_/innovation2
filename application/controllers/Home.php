<?php

if (! class_exists('Home')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Home extends CI_Controller
    {
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
        }



        public function index(): void
        {


            if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
                $this->load->view('Home2', [
                    ]);
                    $this->load->view('PC_attach', [
                    ]);      
                    $this->load->view('OpenEvent2', [
                        ]);                 
            } else {
                show_404();
            }


            // $mobilechk = '/(iPod|iPhone|Android|BlackBerry|SymbianOS|SCH-M\d+|Opera Mini|Windows CE|Nokia|SonyEricsson|webOS|PalmOS)/i'; 

            // // 모바일 접속인지 PC로 접속했는지 체크합니다.
            // if(preg_match($mobilechk, $_SERVER['HTTP_USER_AGENT'])) {

            //     if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
            //         $this->load->view('Mobile/Home', [
            //         ]);
            //         $this->load->view('Mobile/Menu', [
            //         ]);
            //     } else {
            //         show_404();
            //     }
            // } else { 

            //     if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/keywords')) {
            //         $this->load->view('Home', [
            //         ]);
            //         $this->load->view('PC_attach', [
            //         ]);
            //         $this->load->view('PC_main_attach', [
            //         ]);
            //     } else {
            //         show_404();
            //     }
            // }



        }
    }
}
