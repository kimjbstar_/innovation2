<?php

if (! class_exists('MyDB')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class MyDB extends CI_Controller
    {
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('assetsLoader');
            $this->load->view(
                'm_common/head',
                [

                'static' => [
                    'assets' =>loadManagerCommonAssets(),

                    'url'=>loadCommonUrl()
                ]
            ]
            );
        }



        public function index(): void
        {



            $this->load->model('Dbuse');
            $my_db = $this->Dbuse->getMyDB($this->session->userIdx);
            // var_dump($my_db);

            if ($this->file->checkView('m_common/head', 'm_common/footer', 'Dblist/mydb')) {
                $this->load->view('Dblist/mydb', [
                    'url' => [
                        // 'charge' => $this->config->site_url('Charge/step1'),
                    ],
                    'text'=> [
                        'menu_1' => "DB관리",
                        'menu_2' => "나의 DB",
                        'menu_3' => "",
                    ],
                    'db' => $my_db
                ]);
                $this->load->view('m_common/footer', [
                    'static' => [
                        'assets' => loadFooterAssets(),
                        'js' => [
                            // 'swiper.js' => $this->file->getJsUrl('swiper.min.js'),
                        ]
                    ]
                ]);
            } else {
                show_404();
            }


        }
    }
}
