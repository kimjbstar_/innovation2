<?php

if (! class_exists('Search')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Search extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->baseAssets = $this->file->getDefaultAssets();
        }
        public function index(): void
        {
            if ($query = trim($this->input->get('query', true))) {
                $this->load->model('Studio/Studio');

                $queryRlt = array_map(function ($e) {
                    return [
                        'studioName' => $e->studioName,
                        'comment' => $e->comment,
                        'address' => $e->address,
                        'reserveUrl' => $this->config->site_url('Studio/Reservate?' . http_build_query([
                            'studioId' => $e->idx,
                        ])),
                    ];
                }, $this->Studio->searchByKeyWord($query));
                $this->load->view('common/head', [
                    'css' => $this->baseAssets['css'],
                ]);
                $this->load->view('Studio/Search/body', [
                    'data' => [
                        'searchRlts' => $queryRlt,
                        'searchCount' => count($queryRlt),
                    ],
                    'url' => [
                        'back' => $this->config->site_url('Main')
                    ]
                ]);
                $this->load->view('common/foot', [
                    'js' => $this->baseAssets['js']
                ]);
            } else {
                redirect($this->config->site_url('Main'), 'refresh');
            }
            // 모든컬럼 or 처리
            // 리스트 누를 시 sms발송 스튜디오 주인한테
            // 지도상에 스튜디오 표시
        }
    }
}
