<?php

if (! class_exists('Add')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Add extends CI_Controller
    {
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->baseAssets = $this->file->getDefaultAssets();
            $this->lang->load('addStudio', 'korean');
        }
        public function register(): void
        {
            $this->load->helper(['valid', 'data']);
            $data = trimArray($this->input->post([
                'studioName', 'chargePhone',
                'address', 'explain',
                'enterpriseCode', 'studio_jibun_address',
                'studio_city', 'studio_local',
                'studio_bname',
            ], true));

            $validRlt = (new ValidPost($data))->setRules([
                'studioName' => 'require',
                'chargePhone' => 'require|numeric',
                'explain' => 'require',
                'enterpriseCode' => 'require|numeric',
                'address' => 'require'
            ])->run();

            if ($validRlt['stat'] === false) {
                alert($this->lang->line($validRlt['message']), $this->config->site_url('Studio/Add'));
                return;
            }

            $getGpsInfoByAddress = _req('get', getenv('KAKO_API_ADDRESS_URL'), [
                'query' => $data['address']
            ], [
                'headers' => [
                    'Authorization' => getenv('KAKO_API_KEY')
                ]
            ]);

            if ($getGpsInfoByAddress['stat']) {
                $getGpsInfoByAddress = json_decode($getGpsInfoByAddress['message'])->documents[0];
                $data['x'] = $getGpsInfoByAddress->x;
                $data['y'] = $getGpsInfoByAddress->y;
            } else {
                $data['x'] = '';
                $data['y'] = '';
            }

            $data['user_idx'] = $this->session->userIdx;

            $this->load->model('Studio/Studio');
            if ($this->Studio->add($data)) {
                alert($this->lang->line('success'), $this->config->site_url('Main'));
                return;
            }
            alert($this->config->site_url('tryAgain'), $this->config->site_url('Main'));
            return;
        }
        public function index(): void
        {
            $file = $this->file;
            $load = $this->load;
            $load->view('common/head', [
                'css' => $this->baseAssets['css']
            ]);

            $load->view('Studio/Add/body', [
                'img' => [
                    'back' => $file->getIconUrl('back.svg'),
                    'address' => $file->getIconUrl('marker.svg'),
                    'phone' => $file->getIconUrl('phone.svg'),
                    'menu' => $file->getIconUrl('menu.svg'),
                    'avater' => $file->getIconUrl('avatar-dark.svg'),
                    'register' => $file->getIconUrl('driver-registration-dark.svg'),
                    'mapPos' => $file->getIconUrl('map-position.svg'),
                ],
                'url' => [
                    'back' => $this->config->site_url('Main'),
                    'save' => $this->config->site_url('Studio/Add/register')
                ],
                'data' => [
                    'userId' => $this->session->userId,
                    'userMail' => $this->session->userEmail,
                    'csrf' => [
                        'name' => $this->security->get_csrf_token_name(),
                        'hash' => $this->security->get_csrf_hash()
                    ],
                ]
            ]);

            $load->view('common/foot', [
                'js' => array_merge($this->baseAssets['js'], [
                    'postCode' => $file->getJsUrl('lib/postcode.v2.js'),
                    'addAddress' => $file->getJsUrl('addStudio/main.js')
                ])
            ]);
        }
    }
}
