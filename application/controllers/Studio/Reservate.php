<?php

if (! class_exists('Reservate')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Reservate extends CI_Controller
    {
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->baseAssets = $this->file->getDefaultAssets();
            $this->load->helper('sms');
            $this->lang->load('reservate', 'korean');
        }

        public function index(): void
        {
            $query = trim($this->input->get('studioId', true));
            if ($query !== '') {
                if (0 <= abs(intval($query))) {
                    $file = $this->file;
                    $this->load->view('common/head', [
                        'css' => $this->baseAssets['css']
                    ]);

                    $this->load->view('Studio/Reserve/body', [
                        'now' => date('Y-m-d H:i:s'),
                        'csrf' => [
                            'name' => $this->security->get_csrf_token_name(),
                            'hash' => $this->security->get_csrf_hash()
                        ],
                        'studioIdx' => $query,
                        'reserve' => $this->config->site_url('Studio/Reservate/reserveUser'),
                    ]);

                    $this->load->view('common/foot', [
                        'js' => array_merge($this->baseAssets['js'], [
                            'main' => $file->getJsUrl('reserve/main.js')
                        ]),
                    ]);
                } else {
                    redirect($this->config->site_url('Main'), 'refresh');
                }
            } else {
                redirect($this->config->site_url('Main'), 'refresh');
            }
        }

        public function reserveUser(): void
        {
            $this->load->model('Studio/Studio');
            $data = trimArray($this->input->post([
                'userNumber', 'reserveStart', 'reserveEnd',
                'studioId',
            ], true));
            $studioInfo = $this->Studio->getInfoByIdx($data['studioId']);
            if ($studioInfo === null) {
                alert($this->lang->line('tryAgain'), $this->config->site_url('Main'));
                return;
            }

            $this->load->helper('valid');
            $validRlt = (new ValidPost($data))->setRules([
                'userNumber' => 'require|numeric|min:1',
                'reserveStart' => 'require',
                'reserveEnd' => 'require',
                'studioId' => 'require|numeric',
            ])->run();

            if ($validRlt['stat'] === false) {
                alert($this->lang->line($validRlt['message']), $this->config->site_url('Studio/Reservate?studioId=' . $data['studioId']));
                return;
            }

            $data = [
                'studioIdx' => $data['studioId'],
                'userIdx' => $this->session->userIdx,
                'reserveUser' => $data['userNumber'],
                'reserveCode' => bin2hex(random_bytes(10)),
                'now' => date('Y-m-d H:i:s'),
                'start' => date('Y-m-d H:00:00', strtotime($data['reserveStart'])),
                'end' => date('Y-m-d H:00:00', strtotime($data['reserveEnd'])),
            ];

            $redUrl = $this->config->site_url('Studio/Reservate?studioId=' . $data['studioIdx']);
            if (strtotime($data['start']) >= strtotime($data['end'])) {
                alert($this->lang->line('timeError'), $redUrl);
            } elseif ($this->Studio->alreadyReserve($data)) {
                alert($this->lang->line('alreadyReserve'), $redUrl);
            } elseif ($this->Studio->reserve($data)) {
                sendSMS('Studio Search', sprintf(
                    $this->lang->line('sendSMSmessage'),
                    $this->session->userPhone,
                    $this->session->userId,
                    $this->config->base_url('?' . http_build_query([
                        'query' => $data['reserveCode'],
                    ])),
                ), [$studioInfo->deletatePhone]);
                alert($this->lang->line('success'), $this->config->site_url('Main'));
            } else {
                alert($this->lang->line('tryAgain'), $redUrl);
            }
        }
    }
}
