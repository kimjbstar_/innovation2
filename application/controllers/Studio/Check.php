<?php

if (! class_exists('Check')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Check extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->baseAssets = $this->file->getDefaultAssets();
            $this->load->model('Studio/Studio');
            $this->lang->load('reservate', 'korean');
        }
        public function index(): void
        {
            $getReserveList = $this->Studio->getReserveListByOwnerIdx($query = trim($this->input->get('query', true)));
            if (count($getReserveList) === 0) {
                alert($this->lang->line('notExistsReserveUser'), $this->config->site_url('Main'));
                return;
            }

            $this->load->view('common/head', [
                'css' => $this->baseAssets['css'],
            ]);
            $this->load->view('Studio/Check/body', [
                'data' => [
                    'info' => $getReserveList[0],
                    'isAllow' => $this->config->site_url('Studio/Check/setReserveStatus'),
                    'query' => $query,
                    'csrf' => [
                        'name' => $this->security->get_csrf_token_name(),
                        'hash' => $this->security->get_csrf_hash()
                    ],
                ]
            ]);
            $this->load->view('common/foot', [
                'js' => $this->baseAssets['js'],
            ]);
        }

        public function setReserveStatus(): void
        {
            $this->load->helper('valid');
            $data = trimArray($this->input->post([
                'isReserve', 'query',
            ], true));
            $validRlt = (new ValidPost($data))->setRules([
                'isReserve' => 'require',
                'query' => 'require',
            ])->run();

            if ($validRlt['stat'] == false) {
                alert($this->lang->line('infoNotFound'), $this->config->site_url('Studio/Check?query=' . $data['query']));
                return;
            }

            $data['isReserve'] = strtolower($data['isReserve']) === 'on';
            if ($data['isReserve']) {
                if ($this->Studio->setReservateStatus($data['query'])) {
                    $this->load->helper('sms');
                    $studioInfo = $this->Studio->getReserveListByOwnerIdx($data['query'])[0];
                    $data = explode('|', sprintf($this->lang->line('reserveSMS'), $studioInfo['studioName']));
                    sendSMS($data[0], $data[1], [
                        $studioInfo['userPhone']
                    ]);
                }
            }
            alert($this->lang->line('complete'), $this->config->site_url('Main'));
        }
    }
}
