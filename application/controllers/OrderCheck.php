<?php

if (! class_exists('OrderCheck')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class OrderCheck extends CI_Controller
    {
        private $baseAssets = null;
        public function __construct()
        {
            parent::__construct();
            $this->baseAssets = $this->file->getDefaultAssets();
            $this->load->model('Studio/Studio');
        }
        public function logout(): void
        {
            $this->load->helper('data');
            clearSession();
            redirect($this->config->base_url(), 'refresh');
        }
        public function getDistanceByPosition(): void
        {
            $this->load->model('Studio/Studio');
            $this->json->echo($this->Studio->getByDistance([
                'lat' => trim($this->input->get('lon', true)),
                'lon' => trim($this->input->get('lat', true)),
            ]));
        }




        public function talk_start_other(): bool
        {


            // 7:12
            // #{이름} 고객님께서 요청하신 퀵서비스 주문에 #{비고1} 기사님이 배정되었습니다.
            // 아래 링크로 가시면 배송 현황과 기사님 연락처를 확인하실 수 있습니다.
            // #{비고2}
            // 퀵카 1661-2482




            $ch = curl_init();

            $username = "jackkor";                //필수입력
            $key = "yjFmfSusrGeOX1S";           //필수입력

            $kakao_plus_id = "@qc2482";            //필수입력
            $user_template_no = "12";            //필수입력 (하단 259 라인 API 이용하여 확인)
            $receiver = '{"name":"고승일","mobile":"01025349557","note1":"김덕현","note2":"https://quickcar.co.kr/orderdetail?v=192987659"}';
            $receiver = '[' . $receiver . ']';
            // 대체문자 정보 추가
            $kakao_faild_type = "1";          // 1 : 대체문자(SMS) / 2 : 대체문자(LMS) / 3 : 대체문자(MMS) 대체문자 사용시 필수 입력
            $title = "대체문자 MMS/LMS 제목입니다.";
            $message = '[$NAME]님 알림 문자 입니다. 전화번호 : [$MOBILE] 비고1 : [$NOTE1] 비고2 : [$NOTE2] 비고3 : [$NOTE3] 비고4 : [$NOTE4] 비고5 : [$NOTE5]';             //대체문자 사용시 필수입력
            $sender = "발신자번호";                    //대체문자 사용시 필수입력
            // 예약발송 정보 추가
            $reserve_type = 'NORMAL'; // NORMAL - 즉시발송 / ONETIME - 1회예약 / WEEKLY - 매주정기예약 / MONTHLY - 매월정기예약
            $start_reserve_time = date('Y-m-d H:i:s'); //  발송하고자 하는 시간(시,분단위까지만 가능) (동일한 예약 시간으로는 200회 이상 API 호출을 할 수 없습니다.)
            $end_reserve_time = date('Y-m-d H:i:s'); //  발송이 끝나는 시간 1회 예약일 경우 $start_reserve_time = $end_reserve_time
            $remained_count = 1;
            // 예약 수정/취소 API는 소스 하단을 참고 해주시기 바랍니다.

            // 실제 발송성공실패 여부를 받기 원하실 경우 아래 주석을 해제하신 후, 사이트에 등록한 URL 번호를 입력해 주시기 바랍니다.
            $return_url_yn = true;        //return_url 사용시 필수 입력
            $return_url = 0;

            /* 여기까지 수정해주시기 바랍니다. */

            $message = str_replace(' ', ' ', $message);  //유니코드 공백문자 치환, 대체문자 발송시 주석 해제


            $postvars = '"username":"' . $username . '"';
            $postvars = $postvars . ', "key":"' . $key . '"';
            $postvars = $postvars . ', "kakao_plus_id":"' . $kakao_plus_id . '"';
            $postvars = $postvars . ', "user_template_no":"' . $user_template_no . '"';
            $postvars = $postvars . ', "receiver":' . $receiver;
            $postvars = '{' . $postvars . '}';      //JSON 데이터

            $url = "https://directsend.co.kr/index.php/api_v2/kakao_notice";         //URL

            //헤더정보
            $headers = ["cache-control: no-cache","content-type: application/json; charset=utf-8"];

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $response = curl_exec($ch);


            return true;
        }
        public function talk_start_self(): bool
        {

            // 8:9
            // 고객님 #{비고1}에서 #{비고2} 요청하신 퀵서비스 #{비고3}기사님이 배정되었습니다.
            // 아래 링크로 가시면 배송 현황과 기사님 연락처를 확인하실 수 있습니다.
            // #{비고4}
            // 퀵카 1661-2482


            $ch = curl_init();

            $username = "jackkor";                //필수입력
            $key = "yjFmfSusrGeOX1S";           //필수입력

            $kakao_plus_id = "@qc2482";            //필수입력
            $user_template_no = "9";            //필수입력 (하단 259 라인 API 이용하여 확인)
            $receiver = '{"name":"고승일","mobile":"01025349557","note1":"독산동","note2":"신설동","note3":"홍길동","note4":"https://quickcar.co.kr/orderdetail?v=192987659"}';
            $receiver = '[' . $receiver . ']';
            // 대체문자 정보 추가
            $kakao_faild_type = "1";          // 1 : 대체문자(SMS) / 2 : 대체문자(LMS) / 3 : 대체문자(MMS) 대체문자 사용시 필수 입력
            $title = "대체문자 MMS/LMS 제목입니다.";
            $message = '[$NAME]님 알림 문자 입니다. 전화번호 : [$MOBILE] 비고1 : [$NOTE1] 비고2 : [$NOTE2] 비고3 : [$NOTE3] 비고4 : [$NOTE4] 비고5 : [$NOTE5]';             //대체문자 사용시 필수입력
            $sender = "발신자번호";                    //대체문자 사용시 필수입력
            // 예약발송 정보 추가
            $reserve_type = 'NORMAL'; // NORMAL - 즉시발송 / ONETIME - 1회예약 / WEEKLY - 매주정기예약 / MONTHLY - 매월정기예약
            $start_reserve_time = date('Y-m-d H:i:s'); //  발송하고자 하는 시간(시,분단위까지만 가능) (동일한 예약 시간으로는 200회 이상 API 호출을 할 수 없습니다.)
            $end_reserve_time = date('Y-m-d H:i:s'); //  발송이 끝나는 시간 1회 예약일 경우 $start_reserve_time = $end_reserve_time
            $remained_count = 1;
            // 예약 수정/취소 API는 소스 하단을 참고 해주시기 바랍니다.

            // 실제 발송성공실패 여부를 받기 원하실 경우 아래 주석을 해제하신 후, 사이트에 등록한 URL 번호를 입력해 주시기 바랍니다.
            $return_url_yn = true;        //return_url 사용시 필수 입력
            $return_url = 0;

            /* 여기까지 수정해주시기 바랍니다. */

            $message = str_replace(' ', ' ', $message);  //유니코드 공백문자 치환, 대체문자 발송시 주석 해제


            $postvars = '"username":"' . $username . '"';
            $postvars = $postvars . ', "key":"' . $key . '"';
            $postvars = $postvars . ', "kakao_plus_id":"' . $kakao_plus_id . '"';
            $postvars = $postvars . ', "user_template_no":"' . $user_template_no . '"';
            $postvars = $postvars . ', "receiver":' . $receiver;
            $postvars = '{' . $postvars . '}';      //JSON 데이터

            $url = "https://directsend.co.kr/index.php/api_v2/kakao_notice";         //URL

            //헤더정보
            $headers = ["cache-control: no-cache","content-type: application/json; charset=utf-8"];

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $response = curl_exec($ch);


            return true;
        }


        public function talk_finsih_first(): bool   ///// 재검수 요청 중
        {





            // 4 : 21
            // 오늘 주문하신 어플 우측상단 메뉴에서 로그인 하시면 발급된 #{아메리카노} 쿠폰을 확인하실 수 있습니다.
            // 감사합니다
            // 퀵카 1661-2482




            $ch = curl_init();

            $username = "jackkor";                //필수입력
            $key = "yjFmfSusrGeOX1S";           //필수입력

            $kakao_plus_id = "@qc2482";            //필수입력
            $user_template_no = "21";            //필수입력 (하단 259 라인 API 이용하여 확인)
            $receiver = '{"name":"고승일","mobile":"01025349557","note1":"아메리카노"}';
            $receiver = '[' . $receiver . ']';
            // 대체문자 정보 추가
            $kakao_faild_type = "1";          // 1 : 대체문자(SMS) / 2 : 대체문자(LMS) / 3 : 대체문자(MMS) 대체문자 사용시 필수 입력
            $title = "대체문자 MMS/LMS 제목입니다.";
            $message = '[$NAME]님 알림 문자 입니다. 전화번호 : [$MOBILE] 비고1 : [$NOTE1] 비고2 : [$NOTE2] 비고3 : [$NOTE3] 비고4 : [$NOTE4] 비고5 : [$NOTE5]';             //대체문자 사용시 필수입력
            $sender = "발신자번호";                    //대체문자 사용시 필수입력
            // 예약발송 정보 추가
            $reserve_type = 'NORMAL'; // NORMAL - 즉시발송 / ONETIME - 1회예약 / WEEKLY - 매주정기예약 / MONTHLY - 매월정기예약
            $start_reserve_time = date('Y-m-d H:i:s'); //  발송하고자 하는 시간(시,분단위까지만 가능) (동일한 예약 시간으로는 200회 이상 API 호출을 할 수 없습니다.)
            $end_reserve_time = date('Y-m-d H:i:s'); //  발송이 끝나는 시간 1회 예약일 경우 $start_reserve_time = $end_reserve_time
            $remained_count = 1;
            // 예약 수정/취소 API는 소스 하단을 참고 해주시기 바랍니다.

            // 실제 발송성공실패 여부를 받기 원하실 경우 아래 주석을 해제하신 후, 사이트에 등록한 URL 번호를 입력해 주시기 바랍니다.
            $return_url_yn = true;        //return_url 사용시 필수 입력
            $return_url = 0;

            /* 여기까지 수정해주시기 바랍니다. */

            $message = str_replace(' ', ' ', $message);  //유니코드 공백문자 치환, 대체문자 발송시 주석 해제


            $postvars = '"username":"' . $username . '"';
            $postvars = $postvars . ', "key":"' . $key . '"';
            $postvars = $postvars . ', "kakao_plus_id":"' . $kakao_plus_id . '"';
            $postvars = $postvars . ', "user_template_no":"' . $user_template_no . '"';
            $postvars = $postvars . ', "receiver":' . $receiver;
            $postvars = '{' . $postvars . '}';      //JSON 데이터

            $url = "https://directsend.co.kr/index.php/api_v2/kakao_notice";         //URL

            //헤더정보
            $headers = ["cache-control: no-cache","content-type: application/json; charset=utf-8"];

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $response = curl_exec($ch);


            return true;
        }
        public function talk_finish_with_gift(): bool
        {


            // #{이름} 고객님 #{비고1}를 이용해주셔서 감사합니다.
            // #{비고2} 주문 이벤트로 #{비고3} 쿠폰을 보내드립니다.
            // 맛있게 드시고 친구소개 부탁드립니다.
            // 선물링크 : #{비고4}
            // 퀵카 1661-2482

            $ch = curl_init();

            $username = "jackkor";                //필수입력
            $key = "yjFmfSusrGeOX1S";           //필수입력

            $kakao_plus_id = "@qc2482";            //필수입력
            $user_template_no = "18";            //필수입력 (하단 259 라인 API 이용하여 확인)
            $receiver = '{"name":"고승일","mobile":"01025349557","note1":"퀵카","note2":"10번째","note3":"아메리카노","note4":"https://quickcar.co.kr/orderdetail?v=192987659"}';
            $receiver = '[' . $receiver . ']';
            // 대체문자 정보 추가
            $kakao_faild_type = "1";          // 1 : 대체문자(SMS) / 2 : 대체문자(LMS) / 3 : 대체문자(MMS) 대체문자 사용시 필수 입력
            $title = "대체문자 MMS/LMS 제목입니다.";
            $message = '[$NAME]님 알림 문자 입니다. 전화번호 : [$MOBILE] 비고1 : [$NOTE1] 비고2 : [$NOTE2] 비고3 : [$NOTE3] 비고4 : [$NOTE4] 비고5 : [$NOTE5]';             //대체문자 사용시 필수입력
            $sender = "발신자번호";                    //대체문자 사용시 필수입력
            // 예약발송 정보 추가
            $reserve_type = 'NORMAL'; // NORMAL - 즉시발송 / ONETIME - 1회예약 / WEEKLY - 매주정기예약 / MONTHLY - 매월정기예약
            $start_reserve_time = date('Y-m-d H:i:s'); //  발송하고자 하는 시간(시,분단위까지만 가능) (동일한 예약 시간으로는 200회 이상 API 호출을 할 수 없습니다.)
            $end_reserve_time = date('Y-m-d H:i:s'); //  발송이 끝나는 시간 1회 예약일 경우 $start_reserve_time = $end_reserve_time
            $remained_count = 1;
            // 예약 수정/취소 API는 소스 하단을 참고 해주시기 바랍니다.

            // 실제 발송성공실패 여부를 받기 원하실 경우 아래 주석을 해제하신 후, 사이트에 등록한 URL 번호를 입력해 주시기 바랍니다.
            $return_url_yn = true;        //return_url 사용시 필수 입력
            $return_url = 0;

            /* 여기까지 수정해주시기 바랍니다. */

            $message = str_replace(' ', ' ', $message);  //유니코드 공백문자 치환, 대체문자 발송시 주석 해제


            $postvars = '"username":"' . $username . '"';
            $postvars = $postvars . ', "key":"' . $key . '"';
            $postvars = $postvars . ', "kakao_plus_id":"' . $kakao_plus_id . '"';
            $postvars = $postvars . ', "user_template_no":"' . $user_template_no . '"';
            $postvars = $postvars . ', "receiver":' . $receiver;
            $postvars = '{' . $postvars . '}';      //JSON 데이터

            $url = "https://directsend.co.kr/index.php/api_v2/kakao_notice";         //URL

            //헤더정보
            $headers = ["cache-control: no-cache","content-type: application/json; charset=utf-8"];

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $response = curl_exec($ch);


            return true;
        }
        public function talk_finish(): bool
        {


            // 6: 15
            // 고객님 요청하신 퀵서비스 완료되었습니다.
            // 감사합니다.
            // 퀵카 1661-2482




            $ch = curl_init();

            $username = "jackkor";                //필수입력
            $key = "yjFmfSusrGeOX1S";           //필수입력

            $kakao_plus_id = "@qc2482";            //필수입력
            $user_template_no = "15";            //필수입력 (하단 259 라인 API 이용하여 확인)
            $receiver = '{"name":"고승일","mobile":"01025349557"}';
            $receiver = '[' . $receiver . ']';
            // 대체문자 정보 추가
            $kakao_faild_type = "1";          // 1 : 대체문자(SMS) / 2 : 대체문자(LMS) / 3 : 대체문자(MMS) 대체문자 사용시 필수 입력
            $title = "대체문자 MMS/LMS 제목입니다.";
            $message = '[$NAME]님 알림 문자 입니다. 전화번호 : [$MOBILE] 비고1 : [$NOTE1] 비고2 : [$NOTE2] 비고3 : [$NOTE3] 비고4 : [$NOTE4] 비고5 : [$NOTE5]';             //대체문자 사용시 필수입력
            $sender = "발신자번호";                    //대체문자 사용시 필수입력
            // 예약발송 정보 추가
            $reserve_type = 'NORMAL'; // NORMAL - 즉시발송 / ONETIME - 1회예약 / WEEKLY - 매주정기예약 / MONTHLY - 매월정기예약
            $start_reserve_time = date('Y-m-d H:i:s'); //  발송하고자 하는 시간(시,분단위까지만 가능) (동일한 예약 시간으로는 200회 이상 API 호출을 할 수 없습니다.)
            $end_reserve_time = date('Y-m-d H:i:s'); //  발송이 끝나는 시간 1회 예약일 경우 $start_reserve_time = $end_reserve_time
            $remained_count = 1;
            // 예약 수정/취소 API는 소스 하단을 참고 해주시기 바랍니다.

            // 실제 발송성공실패 여부를 받기 원하실 경우 아래 주석을 해제하신 후, 사이트에 등록한 URL 번호를 입력해 주시기 바랍니다.
            $return_url_yn = true;        //return_url 사용시 필수 입력
            $return_url = 0;

            /* 여기까지 수정해주시기 바랍니다. */

            $message = str_replace(' ', ' ', $message);  //유니코드 공백문자 치환, 대체문자 발송시 주석 해제


            $postvars = '"username":"' . $username . '"';
            $postvars = $postvars . ', "key":"' . $key . '"';
            $postvars = $postvars . ', "kakao_plus_id":"' . $kakao_plus_id . '"';
            $postvars = $postvars . ', "user_template_no":"' . $user_template_no . '"';
            $postvars = $postvars . ', "receiver":' . $receiver;
            $postvars = '{' . $postvars . '}';      //JSON 데이터

            $url = "https://directsend.co.kr/index.php/api_v2/kakao_notice";         //URL

            //헤더정보
            $headers = ["cache-control: no-cache","content-type: application/json; charset=utf-8"];

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $response = curl_exec($ch);


            return true;
        }



        public function talk_increase_fee(): bool  ///// 재검수 요청 중
        {


            // 고객님 #{비고1}에서 #{비고2}로 요청하신 주문이 배차 지연되고 있습니다.
            // 배차가 지연되는 원인은 #{비고3}입니다.
            // #{비고4}원 증액 후 배차 시도를 계속해봐도 될까요?

            // 아래 선택지 중 신중하게 선택 부탁드립니다.




            // 증액후 계속 시도 : #{비고5}


            // 현재대로 계속 시도 : #{비고6}


            // 주문 취소 : #{비고7}

            // 퀵카 1661-2482




            $ch = curl_init();

            $username = "jackkor";                //필수입력
            $key = "yjFmfSusrGeOX1S";           //필수입력

            $kakao_plus_id = "@qc2482";            //필수입력
            $user_template_no = "33";            //필수입력 (하단 259 라인 API 이용하여 확인)
            $receiver = '{"name":"고승일","mobile":"01025349557","note1":"독산동","note2":"신설동","note3":"우천 ","note4":"3,000","note5":"https://quickcar.co.kr/orderincrease?v=192987659&ans=yes","note6":"https://quickcar.co.kr/orderincrease?v=192987659&ans=no","note7":"https://quickcar.co.kr/orderincrease?v=192987659&ans=cancel"}';
            $receiver = '[' . $receiver . ']';
            // 대체문자 정보 추가
            $kakao_faild_type = "1";          // 1 : 대체문자(SMS) / 2 : 대체문자(LMS) / 3 : 대체문자(MMS) 대체문자 사용시 필수 입력
            $title = "대체문자 MMS/LMS 제목입니다.";
            $message = '[$NAME]님 알림 문자 입니다. 전화번호 : [$MOBILE] 비고1 : [$NOTE1] 비고2 : [$NOTE2] 비고3 : [$NOTE3] 비고4 : [$NOTE4] 비고5 : [$NOTE5]';             //대체문자 사용시 필수입력
            $sender = "발신자번호";                    //대체문자 사용시 필수입력
            // 예약발송 정보 추가
            $reserve_type = 'NORMAL'; // NORMAL - 즉시발송 / ONETIME - 1회예약 / WEEKLY - 매주정기예약 / MONTHLY - 매월정기예약
            $start_reserve_time = date('Y-m-d H:i:s'); //  발송하고자 하는 시간(시,분단위까지만 가능) (동일한 예약 시간으로는 200회 이상 API 호출을 할 수 없습니다.)
            $end_reserve_time = date('Y-m-d H:i:s'); //  발송이 끝나는 시간 1회 예약일 경우 $start_reserve_time = $end_reserve_time
            $remained_count = 1;
            // 예약 수정/취소 API는 소스 하단을 참고 해주시기 바랍니다.

            // 실제 발송성공실패 여부를 받기 원하실 경우 아래 주석을 해제하신 후, 사이트에 등록한 URL 번호를 입력해 주시기 바랍니다.
            $return_url_yn = true;        //return_url 사용시 필수 입력
            $return_url = 0;

            /* 여기까지 수정해주시기 바랍니다. */

            $message = str_replace(' ', ' ', $message);  //유니코드 공백문자 치환, 대체문자 발송시 주석 해제


            $postvars = '"username":"' . $username . '"';
            $postvars = $postvars . ', "key":"' . $key . '"';
            $postvars = $postvars . ', "kakao_plus_id":"' . $kakao_plus_id . '"';
            $postvars = $postvars . ', "user_template_no":"' . $user_template_no . '"';
            $postvars = $postvars . ', "receiver":' . $receiver;
            $postvars = '{' . $postvars . '}';      //JSON 데이터

            $url = "https://directsend.co.kr/index.php/api_v2/kakao_notice";         //URL

            //헤더정보
            $headers = ["cache-control: no-cache","content-type: application/json; charset=utf-8"];

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $response = curl_exec($ch);


            return true;
        }



        public function index(): void
        {


            // $orderSerial=trim($this->input->get('v', true));


            $this->load->model('Order/Ordermodel');
            $db_orderdetail = $this->Ordermodel->get_order_ing();

            $check_ok = true;  // 기프티콘 및 메세지 전송조건에 대해서 문제가 났을시에 false로 변경하여 알림톡 전송을 막을 수 있다.


            if (count($db_orderdetail)==0) {
                // return array();
                // var_dump("변동 사항 없음");
            } else {
                foreach ($db_orderdetail as $checkdb) {
                    # code...

                    // var_dump($checkdb);
                    // echo '<br><br><br>';


                    $userId=$checkdb['userId'];

                    $this->load->library('Deliver');
                    $detail_result = $this->deliver->getOrderDetail([
                        'userId' => $userId,
                        'serial' => $checkdb['orderNum'],
                    ]);

                    // echo "<br>";
                    // echo "<br>";echo "<br>";
                    // echo "<br>";echo "<br>";echo "<br>";echo "<br>";echo "<br>";echo "<br>";echo "<br>";

                    // var_dump($detail_result);
                    // echo "<br>";
                    // echo "<br>";echo "<br>";
                    // echo "<br>";echo "<br>";echo "<br>";echo "<br>";echo "<br>";echo "<br>";echo "<br>";

                    // 저장된 것과 가져온 배송 상태 코드가 다르면 처리
                    if ($checkdb['state'] != $detail_result['message'][5]->state) {


                        // 기사님 위치값 저장
                        // id
                        //$checkdb['orderNum']
                        //$detail_result['message'][5]->state
                        //$detail_result['message'][9]->start_lat/360000   $detail_result['message'][9]->start_lon/360000
                        //$detail_result['message'][9]->dest_lat/360000   $detail_result['message'][9]->dest_lon/360000
                        //$detail_result['message'][9]->rider_lat/360000   $detail_result['message'][9]->rider_lon/360000
                        $this->Ordermodel->set_rider_info($checkdb['orderNum'], $detail_result['message'][5]->state, $detail_result['message'][9]->start_lat/360000, $detail_result['message'][9]->start_lon/360000, $detail_result['message'][9]->dest_lat/360000, $detail_result['message'][9]->dest_lon/360000, $detail_result['message'][9]->rider_lat/360000, $detail_result['message'][9]->rider_lon/360000);






                        // state 등록
                        $this->Ordermodel->order_state_update($checkdb['orderNum'], $detail_result['message'][5]->state);
                        // 알림 보내야 하는 시점이라면 알림 발송
                        $ch = curl_init();
                        $username = "jackkor";                //필수입력
                        $key = "yjFmfSusrGeOX1S";           //필수입력
                        $kakao_plus_id = "@qc2482";            //필수입력
                        // template_no는 하단에 조건마다 셋팅
                        //$user_template_no = "9";            //필수입력 (하단 259 라인 API 이용하여 확인)
                        // 대체문자 정보 추가
                        $kakao_faild_type = "0";          // 1 : 대체문자(SMS) / 2 : 대체문자(LMS) / 3 : 대체문자(MMS) 대체문자 사용시 필수 입력
                        $title = "대체문자 MMS/LMS 제목입니다.";
                        $sender = "";                    //대체문자 사용시 필수입력
                        // 예약발송 정보 추가
                        $reserve_type = 'NORMAL'; // NORMAL - 즉시발송 / ONETIME - 1회예약 / WEEKLY - 매주정기예약 / MONTHLY - 매월정기예약
                        $start_reserve_time = date('Y-m-d H:i:s'); //  발송하고자 하는 시간(시,분단위까지만 가능) (동일한 예약 시간으로는 200회 이상 API 호출을 할 수 없습니다.)
                        $end_reserve_time = date('Y-m-d H:i:s'); //  발송이 끝나는 시간 1회 예약일 경우 $start_reserve_time = $end_reserve_time
                        $message = '[$NAME]님 알림 문자 입니다. 전화번호 : [$MOBILE] 비고1 : [$NOTE1] 비고2 : [$NOTE2] 비고3 : [$NOTE3] 비고4 : [$NOTE4] 비고5 : [$NOTE5]';             //대체문자 사용시 필수입력
                        $remained_count = 1;
                        // 예약 수정/취소 API는 소스 하단을 참고 해주시기 바랍니다.
                        // 실제 발송성공실패 여부를 받기 원하실 경우 아래 주석을 해제하신 후, 사이트에 등록한 URL 번호를 입력해 주시기 바랍니다.
                        $return_url_yn = true;        //return_url 사용시 필수 입력
                        $return_url = 0;

                        $message_condition = $this->Ordermodel->get_message_condition();
                        
                        // $detail_result['message'][2]->rider_code_no, $detail_result['message'][2]->rider_name, $detail_result['message'][2]->rider_tel_number
                        //array(37) { ["orderIndex"]=> string(2) "97" ["userId"]=> string(27) "01025349557NoZ3gFf7TtMqQCp4" ["orderName"]=> string(25) "Q-01025349557-01025349557"
                        /*
                         ["orderMobile"]=> string(11) "01025349557" ["startLocal"]=> string(37) "서울 강동구 양재대로123길 7" ["startTelNo"]=> string(11) "01025349557"
                          ["startSido"]=> string(15) "서울특별시" ["StartGugun"]=> string(9) "강동구" ["StartDong"]=> string(9) "천호동" ["StartName"]=> string(11) "01025349557"
                          ["destLocal"]=> string(37) "서울 강동구 양재대로123길 7" ["destTelNo"]=> string(11) "01025349557" ["destSido"]=> string(15) "서울특별시" ["destGugun"]=> string(9) "강동구"
                           ["destDong"]=> string(9) "천호동" ["destName"]=> string(11) "01025349557" ["deliverType"]=> string(1) "3" ["payType"]=> string(1) "1" ["doc"]=> string(1) "1"
                            ["sfast"]=> string(1) "1" ["price"]=> string(5) "65000" ["start_lon"]=> string(14) "127.1415110000" ["start_lat"]=> string(13) "37.5432442000" ["dest_lon"]=> string(14) "127.1415110000"
                             ["dest_lat"]=> string(13) "37.5432442000" ["memo"]=> string(31) "전화먼저 / 쇼핑백 / test" ["orderNum"]=> string(9) "194658008" ["state"]=> string(6) "접수" ["checked"]=> string(1) "0"
                             ["rider_code_no"]=> string(0) "" ["rider_name"]=> string(0) "" ["rider_tel_number"]=> string(0) "" ["card_number"]=> string(0) "" ["card_year"]=> string(0) "" ["card_month"]=> string(0) ""
                              ["card_email"]=> string(0) "" ["order_datetime"]=> string(19) "2020-05-15 05:47:54" }
                              */
                        /*
                        {
[
      "orderIndex"
   ]   => string(2)   "97"[
      "userId"
   ]   => string(27)   "01025349557NoZ3gFf7TtMqQCp4"[
      "orderName"
   ]   => string(25)   "Q-01025349557-01025349557"[
      "orderMobile"
   ]   => string(11)   "01025349557"[
      "startLocal"
   ]   => string(37)   "서울 강동구 양재대로123길 7"[
      "startTelNo"
   ]   => string(11)   "01025349557"[
      "startSido"
   ]   => string(15)   "서울특별시"[
      "StartGugun"
   ]   => string(9)   "강동구"[
      "StartDong"
   ]   => string(9)   "천호동"[
      "StartName"
   ]   => string(11)   "01025349557"[
      "destLocal"
   ]   => string(37)   "서울 강동구 양재대로123길 7"[
      "destTelNo"
   ]   => string(11)   "01025349557"[
      "destSido"
   ]   => string(15)   "서울특별시"[
      "destGugun"
   ]   => string(9)   "강동구"[
      "destDong"
   ]   => string(9)   "천호동"[
      "destName"
   ]   => string(11)   "01025349557"[
      "deliverType"
   ]   => string(1)   "3"[
      "payType"
   ]   => string(1)   "1"[
      "doc"
   ]   => string(1)   "1"[
      "sfast"
   ]   => string(1)   "1"[
      "price"
   ]   => string(5)   "65000"[
      "start_lon"
   ]   => string(14)   "127.1415110000"[
      "start_lat"
   ]   => string(13)   "37.5432442000"[
      "dest_lon"
   ]   => string(14)   "127.1415110000"[
      "dest_lat"
   ]   => string(13)   "37.5432442000"[
      "memo"
   ]   => string(31)   "전화먼저 / 쇼핑백 / test"[
      "orderNum"
   ]   => string(9)   "194658008"[
      "state"
   ]   => string(6)   "접수"[
      "checked"
   ]   => string(1)   "0"[
      "rider_code_no"
   ]   => string(0)   ""[
      "rider_name"
   ]   => string(0)   ""[
      "rider_tel_number"
   ]   => string(0)   ""[
      "card_number"
   ]   => string(0)   ""[
      "card_year"
   ]   => string(0)   ""[
      "card_month"
   ]   => string(0)   ""[
      "card_email"
   ]   => string(0)   ""[
      "order_datetime"
   ]   => string(19)   "2020-05-15 05:47:54"
}
                         */
                        $receiverMobile = is_null($checkdb['message_receiver_info']) ? $checkdb['message_receiver_info'] : $checkdb['startTelNo'];

                        if ($detail_result['message'][5]->state=='배차') {
                        } elseif ($detail_result['message'][5]->state=='취소') {
                        } elseif ($detail_result['message'][5]->state=='접수') {
                        } elseif ($detail_result['message'][5]->state=='배송') {
                            echo '<br>배송<br>';
                            // 배차처리 주문자에게
                            // public function talk_send_test_9() : bool
                            //$receiver = '{"name":"고승일","mobile":"01025349557","note1":"독산동","note2":"신설동","note3":"홍길동","note4":"https://quickcar.co.kr/orderdetail?v=192987659"}';
                            $receiver = '{"name":"' . $checkdb['StartName'] . '","mobile":"' . $receiverMobile . '","note1":"' . $checkdb['startLocal'] . '","note2":"' . $checkdb['destLocal'] . '","note3":"' . $detail_result['message'][2]->rider_name . '","note4":"https://quickcar.co.kr/orderdetail?v="' . $checkdb['orderNum'] . '"}';
                            $receiver = '[' . $receiver . ']';
                            $user_template_no = "9";

                            $message = str_replace(' ', ' ', $message);  //유니코드 공백문자 치환, 대체문자 발송시 주석 해제

                            $postvars = '"username":"' . $username . '"';
                            $postvars = $postvars . ', "key":"' . $key . '"';
                            $postvars = $postvars . ', "kakao_plus_id":"' . $kakao_plus_id . '"';
                            $postvars = $postvars . ', "user_template_no":"' . $user_template_no . '"';
                            $postvars = $postvars . ', "receiver":' . $receiver;
                            $postvars = '{' . $postvars . '}';      //JSON 데이터
                            $url = "https://directsend.co.kr/index.php/api_v2/kakao_notice";         //URL
                            //헤더정보
                            $headers = ["cache-control: no-cache","content-type: application/json; charset=utf-8"];

                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_POST, true);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                            // 보내전에 값이 있는지 없는지 체크해서 전송하기
                            // member_id $checkdb['StartName'],
                            // orderNum 194658008   $checkdb['orderNum']
                            // message_type = user_template_no
                            // is_send = 1
                            if ($message_condition[0]["s9"] === 'N') {
                                $check_ok = false;
                            }
                            // 메세지 전송여부
                            $is_send = $this->Ordermodel->check_order_message_send($checkdb['StartName'], $checkdb['orderNum'], $user_template_no, '1');
                            if ($is_send && $check_ok) {
                                $response = curl_exec($ch);
                            } else {
                                $check_ok = true;
                            }




                            // 배차처리 상대방에게
                            //public function talk_send_test_12() : bool
                            //$receiver = '{"name":"고승일","mobile":"01025349557","note1":"김덕현","note2":"https://quickcar.co.kr/orderdetail?v=192987659"}';
                            $receiver = '{"name":"' . $checkdb['destName'] . '","mobile":"' . $receiverMobile . '","note1":"' . $detail_result['message'][2]->rider_name . '","note2":"https://quickcar.co.kr/orderdetail?v=' . $checkdb['orderNum'] . '"}';
                            $receiver = '[' . $receiver . ']';
                            $user_template_no = "12";

                            $message = str_replace(' ', ' ', $message);  //유니코드 공백문자 치환, 대체문자 발송시 주석 해제

                            $postvars = '"username":"' . $username . '"';
                            $postvars = $postvars . ', "key":"' . $key . '"';
                            $postvars = $postvars . ', "kakao_plus_id":"' . $kakao_plus_id . '"';
                            $postvars = $postvars . ', "user_template_no":"' . $user_template_no . '"';
                            $postvars = $postvars . ', "receiver":' . $receiver;
                            $postvars = '{' . $postvars . '}';      //JSON 데이터
                            $url = "https://directsend.co.kr/index.php/api_v2/kakao_notice";         //URL
                            //헤더정보
                            $headers = ["cache-control: no-cache","content-type: application/json; charset=utf-8"];

                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_POST, true);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                            // 보내전에 값이 있는지 없는지 체크해서 전송하기
                            // member_id $checkdb['StartName'],
                            // orderNum 194658008   $checkdb['orderNum']
                            // message_type = user_template_no
                            // is_send = 1

                            if ($message_condition[0]["s12"] === 'N') {
                                $check_ok = false;
                            }

                            // 메세지 전송여부
                            $is_send = $this->Ordermodel->check_order_message_send($checkdb['StartName'], $checkdb['orderNum'], $user_template_no, '1');
                            if ($is_send && $check_ok) {
                                $response = curl_exec($ch);
                            } else {
                                $check_ok = true;
                            }
                        } elseif ($detail_result['message'][5]->state=='완료') {
                            // 배송완료 기존고객
                            // public function talk_send_test_15() : bool
                            //$receiver = '{"name":"고승일","mobile":"01025349557"}';
                            $receiver = '{"name":"' . $checkdb['StartName'] . '","mobile":"' . $receiverMobile . '"}';
                            $receiver = '[' . $receiver . ']';
                            $user_template_no = "15";

                            $message = str_replace(' ', ' ', $message);  //유니코드 공백문자 치환, 대체문자 발송시 주석 해제

                            $postvars = '"username":"' . $username . '"';
                            $postvars = $postvars . ', "key":"' . $key . '"';
                            $postvars = $postvars . ', "kakao_plus_id":"' . $kakao_plus_id . '"';
                            $postvars = $postvars . ', "user_template_no":"' . $user_template_no . '"';
                            $postvars = $postvars . ', "receiver":' . $receiver;
                            $postvars = '{' . $postvars . '}';      //JSON 데이터
                            $url = "https://directsend.co.kr/index.php/api_v2/kakao_notice";         //URL
                            //헤더정보
                            $headers = ["cache-control: no-cache","content-type: application/json; charset=utf-8"];

                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_POST, true);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                            // 보내전에 값이 있는지 없는지 체크해서 전송하기
                            // member_id $checkdb['StartName'],
                            // orderNum 194658008   $checkdb['orderNum']
                            // message_type = user_template_no
                            // is_send = 1
                            if ($message_condition[0]["s15"] === 'N') {
                                $check_ok = false;
                            }
                            // 메세지 전송여부
                            $is_send = $this->Ordermodel->check_order_message_send($checkdb['StartName'], $checkdb['orderNum'], $user_template_no, '1');
                            if ($is_send && $check_ok) {
                                $response = curl_exec($ch);
                            } else {
                                $check_ok = true;
                            }



                            /**
                             * order count test 194658008
                             * $checkdb['orderNum']
                             *   $checkdb['startTelNo']
                             *  member id 로 조회해야함.
                            */
                            $order_count = $this->Ordermodel->get_order_count($checkdb['startTelNo'])[0]["total"];

                            if ($order_count > 1) {

                                //  public function check_gifticon_send():
                                $condition = $this->Ordermodel->check_gifticon_send()['condition'];
                                $condition_explode = explode('/', $condition);

                                foreach ($condition_explode as $value) {
                                    if ($order_count == $value) {
                                        $check_ok = true;
                                    } else {
                                        $check_ok = false;
                                    }
                                }
                                //완료조건인데 선물보내주는 옵션이 있을때
                                // 선물보낼때 member_id 참고
                                //public function talk_send_test_18() : bool
                                $receiver = '{"name":"' . $checkdb['StartName'] . '","mobile":"' .  $receiverMobile  . '","note1":"퀵카","note2":"10번째","note3":"아메리카노","note4":"https://quickcar.co.kr/orderdetail?v=' . $checkdb['orderNum'] . '"}';
                                $receiver = '[' . $receiver . ']';
                                $user_template_no = "18";

                                $message = str_replace(' ', ' ', $message);  //유니코드 공백문자 치환, 대체문자 발송시 주석 해제

                                $postvars = '"username":"' . $username . '"';
                                $postvars = $postvars . ', "key":"' . $key . '"';
                                $postvars = $postvars . ', "kakao_plus_id":"' . $kakao_plus_id . '"';
                                $postvars = $postvars . ', "user_template_no":"' . $user_template_no . '"';
                                $postvars = $postvars . ', "receiver":' . $receiver;
                                $postvars = '{' . $postvars . '}';      //JSON 데이터
                                $url = "https://directsend.co.kr/index.php/api_v2/kakao_notice";         //URL
                                //헤더정보
                                $headers = ["cache-control: no-cache","content-type: application/json; charset=utf-8"];

                                curl_setopt($ch, CURLOPT_URL, $url);
                                curl_setopt($ch, CURLOPT_POST, true);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
                                curl_setopt($ch, CURLOPT_TIMEOUT, 20);
                                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                                // 보내전에 값이 있는지 없는지 체크해서 전송하기
                                // member_id $checkdb['StartName'],
                                // orderNum 194658008   $checkdb['orderNum']
                                // message_type = user_template_no
                                // is_send = 1
                                if ($message_condition[0]["s18"] === 'N') {
                                    $check_ok = false;
                                }
                                // 메세지 전송여부
                                $is_send = $this->Ordermodel->check_order_message_send($checkdb['StartName'], $checkdb['orderNum'], $user_template_no, '1');
                                if ($is_send && $check_ok) {
                                    $response = curl_exec($ch);







                                    // $checkdb['StartName']  - member_id

                                    $ch = curl_init();
                                    $postvars = '';
                                    $postvars = '{' . $postvars . '}';      //JSON 데이터

                                    $url = "https://tara2.tarapay.net/ApiBuyGifticon";         //URL
                                    echo $url;
                                    //헤더정보
                                    $headers = ["cache-control: no-cache","content-type: application/json; charset=utf-8"];

                                    curl_setopt($ch, CURLOPT_URL, $url);
                                    curl_setopt($ch, CURLOPT_POST, true);
                                    curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
                                    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
                                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                                    $response = curl_exec($ch);
                                    // echo $response;
                                    $res = json_decode($response);
                                    // echo var_dump($res->ok);
                                    // echo $res->ok;   //1  ok
                                    // exit();
                                    //if($res->ok == 1){
                                    // echo "success";
                                    //}else{
                                    // $this->session->userId 에 맞는 coupon_id 값의 사용된걸로 돌리기
                                    //}
                                    // exit();
                                    //public function send_gifticon(string $member_id, string $coupon_id, string $item_id, string $buy_date, string $expire_date,
                                    // string $is_present, string $receiver, string $sender, string $coupon_status): bool{
                                    $resData = $res->data;
                                    // $this->Ordermodel->send_gifticon($checkdb['StartName'], $resData->coupon_id, $resData->item_id, $resData->buy_date, $resData->expire_date, $resData->is_present, $resData->receiver, $resData->sender, $resData->coupon_status);
                                } else {
                                    $check_ok = true;
                                }
                            } else {
                                // 배송완료 미고객 member_id 참고   첫 주문 고객
                                // /public function talk_send_test_21() : bool
                                $receiver = '{"name":"' . $checkdb['StartName'] . '","mobile":"' .  $receiverMobile . '","note1":"아메리카노"}';
                                $receiver = '[' . $receiver . ']';
                                $user_template_no = "21";

                                $message = str_replace(' ', ' ', $message);  //유니코드 공백문자 치환, 대체문자 발송시 주석 해제

                                $postvars = '"username":"' . $username . '"';
                                $postvars = $postvars . ', "key":"' . $key . '"';
                                $postvars = $postvars . ', "kakao_plus_id":"' . $kakao_plus_id . '"';
                                $postvars = $postvars . ', "user_template_no":"' . $user_template_no . '"';
                                $postvars = $postvars . ', "receiver":' . $receiver;
                                $postvars = '{' . $postvars . '}';      //JSON 데이터
                                $url = "https://directsend.co.kr/index.php/api_v2/kakao_notice";         //URL
                                //헤더정보
                                $headers = ["cache-control: no-cache","content-type: application/json; charset=utf-8"];

                                curl_setopt($ch, CURLOPT_URL, $url);
                                curl_setopt($ch, CURLOPT_POST, true);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
                                curl_setopt($ch, CURLOPT_TIMEOUT, 20);
                                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                                // 보내전에 값이 있는지 없는지 체크해서 전송하기
                                // member_id $checkdb['StartName'],
                                // orderNum 194658008   $checkdb['orderNum']
                                // message_type = user_template_no
                                // is_send = 1

                                if ($message_condition[0]["s21"] === 'N') {
                                    $check_ok = false;
                                }

                                // 메세지 전송여부
                                $is_send = $this->Ordermodel->check_order_message_send($checkdb['StartName'], $checkdb['orderNum'], $user_template_no, '1');
                                if ($is_send && $check_ok) {
                                    $response = curl_exec($ch);


                                    // $checkdb['StartName']  - member_id

                                    $ch = curl_init();
                                    $postvars = '';
                                    $postvars = '{' . $postvars . '}';      //JSON 데이터

                                    $url = "https://tara2.tarapay.net/ApiBuyGifticon";         //URL
                                    echo $url;
                                    //헤더정보
                                    $headers = ["cache-control: no-cache","content-type: application/json; charset=utf-8"];

                                    curl_setopt($ch, CURLOPT_URL, $url);
                                    curl_setopt($ch, CURLOPT_POST, true);
                                    curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
                                    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
                                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                                    $response = curl_exec($ch);
                                    // echo $response;
                                    $res = json_decode($response);
                                    // echo var_dump($res->ok);
                                    // echo $res->ok;   //1  ok
                                    // exit();
                                    //if($res->ok == 1){
                                    // echo "success";
                                    //}else{
                                    // $this->session->userId 에 맞는 coupon_id 값의 사용된걸로 돌리기
                                    //}
                                    // exit();
                                    //public function send_gifticon(string $member_id, string $coupon_id, string $item_id, string $buy_date, string $expire_date,
                                    // string $is_present, string $receiver, string $sender, string $coupon_status): bool{
                                    $resData = $res->data;
                                    $this->Ordermodel->send_gifticon($checkdb['StartName'], $resData->coupon_id, $resData->item_id, $resData->buy_date, $resData->expire_date, $resData->is_present, $resData->receiver, $resData->sender, $resData->coupon_status);
                                } else {
                                    $check_ok = true;
                                }
                            }
                        }
                    }


                    // 저장된 것과 가져온 배당 기사 코드가 다르면 처리
                    if ($checkdb['rider_code_no'] != $detail_result['message'][2]->rider_code_no) {
                        // rider 등록
                        $this->Ordermodel->order_rider_update($checkdb['orderNum'], $detail_result['message'][2]->rider_code_no, $detail_result['message'][2]->rider_name, $detail_result['message'][2]->rider_tel_number);
                    }
                    // return $detail_result['message'];
                }
            }
        }
    }
}
