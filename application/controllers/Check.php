<?php

if (! class_exists('Check')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Check extends CI_Controller
    {
        private $baseAssets = null;

        public function __construct()
        {
            parent::__construct();
            $this->baseAssets = $this->file->getDefaultAssets();
            $this->lang->load('registerValid', 'korean');
        }

        public function index(): void
        {
            $load = $this->load;
            $file = $this->file;
            $load->view('common/head', [
                'css' => $this->baseAssets['css'],
                'title' => '로그인 및 회원가입',
            ]);

            // 지도 터치 이동, 축적 변경시 값 다시가져오기
            // 화면상 지도 끝의 gps좌표 가져오기
            // 검색 리스트 클릭 시 input(인원(몇명 예약할지), 시작시간(0시 ~ 24시), 끝시간(0시 ~ 24시), 일자) 폼
            // 폼 넘어갈 시 예약 요청 일련번호 랜덤 생성 생성한 랜덤번호로 문자 발송 링크만 발송

            // 스튜디오 화면(훅에서 빼야함)
            // 예약 인원, 시간 일자
            // 버튼
            // 예약한게 잇으면 바로 예약 페이지 가기
            // 예약 허가또는 거부 시 시간 표시
            // 예약 요청자한테 승인 거절 여뷰 표시

            // 나의 예약 요청 리스트(예약요청한 리스트 var_dump)
            // 오늘 해야 하는것
            // 메뉴에서 안되는것들 다 빼기
            // 한글화
            $load->view('Check/body', [
                'img' => [
                    'avatarLight' => $file->getIconUrl('avatar-light.svg'),
                    'lock' => $file->getIconUrl('lock.svg'),
                    'eye' => $file->getIconUrl('eye.svg'),
                    'envelope' => $file->getIconUrl('envelope.svg'),
                    'phone' => $file->getIconUrl('phone.svg'),
                ],
                'url' => [
                    'login' => $this->config->site_url('Check/attempt'),
                    'signUp' => $this->config->site_url('Check/valid'),
                ],
                'data' => [
                    'csrf' => [
                        'name' => $this->security->get_csrf_token_name(),
                        'hash' => $this->security->get_csrf_hash()
                    ],
                    'query' => trim($this->input->get('query', true)),
                ],
            ]);

            $load->view('common/foot', [
                'js' => $this->baseAssets['js']
            ]);
        }

        public function attempt(): void
        {
            $this->load->helper(['valid', 'data']);
            $this->load->model('User/User');
            $data = trimArray($this->input->post([
                'username', 'password', 'query'
            ], true));

            $validRlt = (new ValidPost($data))->setRules([
                'username' => 'require',
                'password' => 'require'
            ])->run();

            if (! $validRlt['stat']) {
                alert($this->lang->line($validRlt['message']), $this->config->site_url('Check'));
                return;
            }

            $attemptRlt = $this->User->attempt($data);
            if ($attemptRlt['stat']) {
                setSession($attemptRlt['data']);
                redirect($this->config->site_url($data['query'] ?'Main?query=' . $data['query'] :'Main'), 'refresh');
            } else {
                alert($this->lang->line($attemptRlt['message']), $this->config->site_url('Check'));
            }
        }

        public function valid(): void
        {
            $data = trimArray($this->input->post([
                'username',  'password', 'passwordConfirm'
            ], true));

            $this->load->helper('valid');
            $valid = (new ValidPost($data))->setRules([
                'username' => 'require|unique:member_id,members|min:6|max:18|contain:number,alpha',
                // 'email' => 'require|email|unique:member_email,members',
                // 'phone' => 'require|numeric',
                'password' => 'require|min:4|max:4',
                'passwordConfirm' => 'require|same:password|min:4|max:4',
            ])->run();

            if ($valid['stat']) {
                $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
                unset($data['passwordConfirm']);
                $this->session->set_userdata('registerInfo', $data);
                // redirect($this->config->site_url('Verify/Phone/get'), 'refresh');


                $this->load->model('User/User');
                $this->load->helper(['user', 'valid', 'data']);

                $valid = (new ValidPost($this->session->registerInfo))->setRules([
                    'username' => 'require|unique:member_id,members|min:6|max:18|contain:number,alpha',
                    // 'email' => 'require|email|unique:member_email,members',
                    // 'phone' => 'require|numeric',
                ])->run();

                if ($valid['stat'] && $this->User->add($this->session->registerInfo)) {
                    redirect($this->config->site_url('Verify/success'), 'refresh');
                } else {
                    clearSession();
                    alert($this->lang->line('tryAgain'), $this->config->base_url());
                }
            } else {
                alert($this->lang->line($valid['message']), $this->config->base_url());
            }
        }
    }
}
