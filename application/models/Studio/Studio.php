<?php

if (! class_exists('Studio')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Studio extends CI_Model
    {
        public function __construct()
        {
            parent::__construct();
        }

        public function setReservateStatus(string $query): bool
        {
            $this->db->trans_begin();
            $this->db->where('reserve_code', $query)
                     ->update('reservate_list', [
                       'is_allow' => 1,
                       'reserve_confirm' => date('Y-m-d H:i:s'),
                    ]);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }
        }

        private function _insert(string $tableName, array $info): bool
        {
            $this->db->trans_begin();
            $this->db->insert($tableName, $info);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }
        }

        public function reserve(array $info): bool
        {
            return $this->_insert('reservate_list', [
                'studio_idx' => $info['studioIdx'],
                'reserve_number' => $info['reserveUser'],
                'reserver_idx' => $info['userIdx'],
                'reserve_code' => $info['reserveCode'],
                'reserve_time' => $info['now'],
                'reserve_start_time' => $info['start'],
                'reserve_end_time' => $info['end'],
            ]);
        }

        public function alreadyReserve(array $info): bool
        {
            return $this->db->select('idx')
                            ->where([
                                'studio_idx' => $info['studioIdx'],
                                'reserve_start_time >= ' => $info['start'],
                                'reserve_end_time <= ' => $info['end'],
                            ])->get('reservate_list')->num_rows() >= 1;
        }

        public function add(array $data): bool
        {
            return $this->_insert('studios', [
                'studio_name' => $data['studioName'],
                'delegate_phone' => $data['chargePhone'],
                'user_idx' => $data['user_idx'],
                'studio_comment' => $data['explain'],
                'studio_road_address' => $data['address'],
                'studio_jibun_address' => $data['studio_jibun_address'],
                'studio_city' => $data['studio_city'],
                'studio_local' => $data['studio_local'],
                'studio_detail_local' => $data['studio_bname'],
                'gps_x' => $data['x'],
                'gps_y' => $data['y'],
            ]);
        }

        public function getByDistance(array $data)
        {
            $lat = $this->db->escape($data['lat']);
            $lon = $this->db->escape($data['lon']);
            $distanceQuery = "FLOOR((
                6371 * acos(cos(radians(" . $lat . ")) * cos(radians(gps_x)) * cos(radians(gps_y)
                    - radians(" . $lon . ")) + sin(radians(" . $lat . ")) * sin(radians(gps_x)))
            )) AS distance";

            return $this->db->select('studio_name AS `studioName`')
                            ->select('gps_y AS `lat`')
                            ->select('gps_x AS `lon`')
                            ->select($distanceQuery)
                            ->having('distance >= ', 0)
                            ->having('distance <= ', 100)
                            ->get('studios')
                            ->result_array();
        }

        public function getInfoByIdx(string $idx): ?object
        {
            return $this->db->select('studio_name AS `studioName`')
                            ->select('delegate_phone AS `deletatePhone`')
                            ->select('studio_road_address AS `studioAddress`')
                            ->where('idx', $idx)
                            ->get('studios')
                            ->result()[0] ?? null;
        }

        public function searchByKeyWord(string $query)
        {
            return $this->db->select('studio_name AS `studioName`')
                            ->select('studio_comment AS `comment`')
                            ->select('idx')
                            ->select('studio_road_address AS `address`')
                            ->like('studio_name', $query, 'left')
                            ->or_like('studio_name', $query, 'right')
                            ->or_like('studio_jibun_address', $query, 'left')
                            ->or_like('studio_jibun_address', $query, 'right')
                            ->or_like('studio_detail_local', $query, 'left')
                            ->or_like('studio_detail_local', $query, 'right')
                            ->get('studios')
                            ->result();
        }

        public function getReserveListByOwnerIdx(string $query): array
        {
            return $this->db->select('members.member_id AS `userId`')
                            ->select('members.member_phone AS `userPhone`')
                            ->select('studios.studio_name AS `studioName`')
                            ->join('reservate_list AS `reserveList`', 'studios.idx = reserveList.studio_idx')
                            ->join('members', 'members.idx = reserveList.reserver_idx')
                            ->where('reserve_code', $query)
                            ->get('studios')
                            ->result_array();
        }
    }
}
