
<?php

if (! class_exists('Dbuse')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Dbuse extends CI_Model
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->database();
        }



        public function getFaqs(): array
        {
            return $this->db->select('faq_list.link_idx AS `linkIdx`')
                            ->select('faq_list.link_address AS `linkAddress`')
                            ->select('faq_list.link_name AS `linkName`')
                            ->select('faq_list.page_name AS `pageName`')
                            ->select('faq_list.button_name AS `buttonName`')
                            ->select('faq_list.created_when AS `createdWhen`')
                            ->select('faq_list.onoff AS `onOff`')
                            ->get('faq_list')
                            ->result_array();
        }



        public function getBoards(string $whereboard): array
        {
            return $this->db->select('boards.link_idx AS `linkIdx`')
                            ->select('boards.link_address AS `linkAddress`')
                            ->select('boards.link_name AS `linkName`')
                            ->select('boards.page_name AS `pageName`')
                            ->select('boards.button_name AS `buttonName`')
                            ->select('boards.created_when AS `createdWhen`')
                            ->select('boards.onoff AS `onOff`')
                            ->select('boards.hit AS `hit`')
                            ->where('page_name', $whereboard)
                            ->get('boards')
                            ->result_array();
        }


        

        public function getSpecificNotice(string $idx): array
        {


            $query = "update `boards` set hit=hit+1 where link_idx=".$idx;
            $query = $this->db->query($query);

            return $this->db->select('boards.link_idx AS `linkIdx`')
                            ->select('boards.link_address AS `linkAddress`')
                            ->select('boards.link_name AS `linkName`')
                            ->select('boards.page_name AS `pageName`')
                            ->select('boards.button_name AS `buttonName`')
                            ->select('boards.created_when AS `createdWhen`')
                            ->select('boards.onoff AS `onOff`')
                            ->select('boards.hit AS `hit`')
                            ->where('link_idx', $idx)
                            ->get('boards')
                            ->result_array();
        }



        public function eventGetToday(string $whichevent): array
        {
            return $this->db->select('count(*) as `todayCount`')
                            ->where('date(invited_when) = date(now())')
                            ->where('which_event', $whichevent)
                            ->get('event_invited')
                            ->result_array();
        }

        public function eventInvited(string $whichevent, string $myIp)
        {

            $this->db->trans_begin();
            $this->db->insert('event_invited', [
                'which_event' => $whichevent,
                'invited_ip' => $myIp,
            ]);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
            } else {
                $this->db->trans_rollback();
            }
        }



        public function eventGetIp(string $whichevent, string $myIp): array
        {
            return $this->db->select('count(*) as `ipCount`')
                            ->where('date(invited_when) = date(now())')
                            ->where('which_event', $whichevent)
                            ->where('invited_ip', $myIp)
                            ->get('event_invited')
                            ->result_array();
        }




        public function getHireInfoSearch($category ,  $keyword,  $orderby, $page): array
        {


            if ($orderby=="가나다순"){$orderby="company_name asc";}
            else if ($orderby=="최신순"){$orderby="company_inserted desc ";}
            else if ($orderby=="인기순"){$orderby="hit desc";}


            // $query = "select company_name, company_index,location,logo_image,company_category FROM `reqruit_info` where company_name like '%" . $keyword . "%' and company_category like '%".$category."%' order by force_orderby asc";
            // // if (!is_null($keyword)){$query.=" order by ".$orderby;}
            // // if (!is_null($category)){$query.=" order by ".$orderby;}



            $query = "select company_name, company_index,location,logo_image,company_category FROM `reqruit_info` where company_name like '%" . $keyword . "%' and company_category like '%".$category."%' order by ".$orderby;
            // if (!is_null($keyword)){$query.=" order by ".$orderby;}
            // if (!is_null($category)){$query.=" order by ".$orderby;}
            // if (!is_null($orderby) and $orderby !=""){$query.=" ,".$orderby;}
            $query.=" limit ".(($page-1)*6).", 6 ";

            $query = $this->db->query($query)->result_array();

            return $query;


        }
        


        public function getSpecificHireInfoSearch($idx): array
        {


            $query = "update `reqruit_info` set hit=hit+1 where company_index=".$idx;
            $query = $this->db->query($query);

            $query = "select * FROM `reqruit_info` where company_index=".$idx;
            $query = $this->db->query($query)->result_array();

            return $query;


        }










        
        public function getHireInfos(): array
        {
            return $this->db->select('reqruit_info.company_name AS `company_name`')
                            ->select('reqruit_info.location AS `location`')
                            ->select('reqruit_info.logo_image AS `logo_image`')
                            ->select('reqruit_info.company_category AS `company_category`')
                            ->select('reqruit_info.hit AS `hit`')
                            ->select('reqruit_info.company_tags AS `company_tags`')
                            ->select('reqruit_info.slide_images AS `slide_images`')
                            ->select('reqruit_info.company_info AS `company_info`')
                            ->select('reqruit_info.recruit_info AS `recruit_info`')
                            ->select('reqruit_info.faqs AS `faqs`')
                            ->select('reqruit_info.company_inserted AS `company_inserted`')
                            ->select('reqruit_info.company_index AS `company_index`')
                            ->get('reqruit_info')
                            ->result_array();
        }


        public function getEventInfos(): array
        {
            return $this->db->select('event_info.company_name AS `company_name`')
                            ->select('event_info.location AS `location`')
                            ->select('event_info.hit AS `hit`')
                            ->select('event_info.slide_images AS `slide_images`')
                            ->select('event_info.company_inserted AS `company_inserted`')
                            ->select('event_info.company_index AS `company_index`')
                            ->get('event_info')
                            ->result_array();
        }


        public function getEventSpecificInfos(string $idxnum): array
        {



            $query = "update `event_info` set hit=hit+1 where company_index=".$idxnum;
            $query = $this->db->query($query);

            return $this->db->select('event_info.company_name AS `company_name`')
                            ->select('event_info.location AS `location`')
                            ->select('event_info.hit AS `hit`')
                            ->select('event_info.slide_images AS `slide_images`')
                            ->select('event_info.company_info AS `company_info`')
                            ->select('event_info.company_inserted AS `company_inserted`')
                            ->where('company_index', $idxnum)
                            ->get('event_info')
                            ->result_array();
        }
























        public function getEventResultInfos(): array
        {
            return $this->db->select('event_result_info.company_name AS `company_name`')
                            ->select('event_result_info.location AS `location`')
                            ->select('event_result_info.hit AS `hit`')
                            ->select('event_result_info.slide_images AS `slide_images`')
                            ->select('event_result_info.company_inserted AS `company_inserted`')
                            ->select('event_result_info.company_index AS `company_index`')
                            ->get('event_result_info')
                            ->result_array();
        }


        public function getEventResultSpecificInfos(string $idxnum): array
        {



            $query = "update `event_result_info` set hit=hit+1 where company_index=".$idxnum;
            $query = $this->db->query($query);

            return $this->db->select('event_result_info.company_name AS `company_name`')
                            ->select('event_result_info.location AS `location`')
                            ->select('event_result_info.hit AS `hit`')
                            ->select('event_result_info.slide_images AS `slide_images`')
                            ->select('event_result_info.company_info AS `company_info`')
                            ->select('event_result_info.company_inserted AS `company_inserted`')
                            ->where('company_index', $idxnum)
                            ->get('event_result_info')
                            ->result_array();
        }

























        public function getButtonLinks(): array
        {
            return $this->db->select('service_link.link_idx AS `linkIdx`')
                            ->select('service_link.link_address AS `linkAddress`')
                            ->select('service_link.link_name AS `linkName`')
                            ->select('service_link.page_name AS `pageName`')
                            ->select('service_link.button_name AS `buttonName`')
                            ->select('service_link.created_when AS `createdWhen`')
                            ->select('service_link.onoff AS `onOff`')
                            ->get('service_link')
                            ->result_array();
        }


        public function getSpecificButtonLinks(string $pagename): array
        {
            return $this->db->select('service_link.link_idx AS `linkIdx`')
                            ->select('service_link.link_address AS `linkAddress`')
                            ->select('service_link.link_name AS `linkName`')
                            ->select('service_link.page_name AS `pageName`')
                            ->select('service_link.button_name AS `buttonName`')
                            ->select('service_link.created_when AS `createdWhen`')
                            ->select('service_link.onoff AS `onOff`')
                            ->where('page_name', $pagename)
                            ->get('service_link')
                            ->result_array();
        }



        public function getSpecificFaq(string $dbIdx): array
        {
            return $this->db->select('faq_list.link_idx AS `linkIdx`')
                            ->select('faq_list.link_address AS `linkAddress`')
                            ->select('faq_list.link_name AS `linkName`')
                            ->select('faq_list.page_name AS `pageName`')
                            ->select('faq_list.button_name AS `buttonName`')
                            ->select('faq_list.created_when AS `createdWhen`')
                            ->select('faq_list.onoff AS `onOff`')
                            ->where('link_idx', $dbIdx)
                            ->get('faq_list')
                            ->result_array();

        }



        public function getSpecificHireInfo(string $dbIdx): array
        {
            return $this->db->select('reqruit_info.company_name AS `company_name`')
                            ->select('reqruit_info.location AS `location`')
                            ->select('reqruit_info.logo_image AS `logo_image`')
                            ->select('reqruit_info.company_category AS `company_category`')
                            ->select('reqruit_info.hit AS `hit`')
                            ->select('reqruit_info.company_tags AS `company_tags`')
                            ->select('reqruit_info.slide_images AS `slide_images`')
                            ->select('reqruit_info.company_info AS `company_info`')
                            ->select('reqruit_info.recruit_info AS `recruit_info`')
                            ->select('reqruit_info.faqs AS `faqs`')
                            ->select('reqruit_info.company_inserted AS `company_inserted`')
                            ->where('company_index', $dbIdx)
                            ->get('reqruit_info')
                            ->result_array();
        }




        public function getSpecificEventInfo(string $dbIdx): array
        {
            return $this->db->select('event_info.company_name AS `company_name`')
                            ->select('event_info.location AS `location`')
                            ->select('event_info.logo_image AS `logo_image`')
                            ->select('event_info.company_category AS `company_category`')
                            ->select('event_info.hit AS `hit`')
                            ->select('event_info.company_tags AS `company_tags`')
                            ->select('event_info.slide_images AS `slide_images`')
                            ->select('event_info.company_info AS `company_info`')
                            ->select('event_info.recruit_info AS `recruit_info`')
                            ->select('event_info.faqs AS `faqs`')
                            ->select('event_info.company_inserted AS `company_inserted`')
                            ->where('company_index', $dbIdx)
                            ->get('event_info')
                            ->result_array();
        }





        public function getSpecificEventResultInfo(string $dbIdx): array
        {
            return $this->db->select('event_result_info.company_name AS `company_name`')
                            ->select('event_result_info.location AS `location`')
                            ->select('event_result_info.logo_image AS `logo_image`')
                            ->select('event_result_info.company_category AS `company_category`')
                            ->select('event_result_info.hit AS `hit`')
                            ->select('event_result_info.company_tags AS `company_tags`')
                            ->select('event_result_info.slide_images AS `slide_images`')
                            ->select('event_result_info.company_info AS `company_info`')
                            ->select('event_result_info.recruit_info AS `recruit_info`')
                            ->select('event_result_info.faqs AS `faqs`')
                            ->select('event_result_info.company_inserted AS `company_inserted`')
                            ->where('company_index', $dbIdx)
                            ->get('event_result_info')
                            ->result_array();
        }







        public function getSpecificBoard(string $dbIdx): array
        {
            return $this->db->select('boards.link_idx AS `linkIdx`')
                            ->select('boards.link_address AS `linkAddress`')
                            ->select('boards.link_name AS `linkName`')
                            ->select('boards.page_name AS `pageName`')
                            ->select('boards.button_name AS `buttonName`')
                            ->select('boards.created_when AS `createdWhen`')
                            ->select('boards.onoff AS `onOff`')
                            ->where('link_idx', $dbIdx)
                            ->get('boards')
                            ->result_array();

        }





        public function getSpecificButton(string $dbIdx): array
        {
            return $this->db->select('service_link.link_idx AS `linkIdx`')
                            ->select('service_link.link_address AS `linkAddress`')
                            ->select('service_link.link_name AS `linkName`')
                            ->select('service_link.page_name AS `pageName`')
                            ->select('service_link.button_name AS `buttonName`')
                            ->select('service_link.created_when AS `createdWhen`')
                            ->select('service_link.onoff AS `onOff`')
                            ->where('link_idx', $dbIdx)
                            ->get('service_link')
                            ->result_array();

        }




        public function getMyDB(string $userIdx): array
        {
            return $this->db->select('db_get.name AS `dbName`')
                            ->select('db_get.keyword_group_name AS `dbLandName`')
                            ->select('db_get.db_history AS `dbHistory`')
                            ->select('db_get.get_datetime AS `dbGet`')
                            ->select('db_get.keyword_count AS `keywordCount`')
                            ->select('db_get.db_index AS `dbIdx`')
                            ->select('db_get.onoff AS `onOff`')
                            ->select('DBFLAG.flag_color AS `dbColor`')
                            ->join('db_flag AS `DBFLAG`', 'db_get.flag_index = DBFLAG.flag_index')
                            ->get('db_get')
                            ->result_array();
        }


        public function getMyUsingDB(string $userIdx): array
        {
            return $this->db->select('db_get.name AS `dbName`')
                            ->select('db_get.keyword_group_name AS `dbLandName`')
                            ->select('db_get.db_history AS `dbHistory`')
                            ->select('db_get.get_datetime AS `dbGet`')
                            ->select('db_get.keyword_count AS `keywordCount`')
                            ->select('db_get.found_today AS `foundToday`')
                            ->select('db_get.found_total AS `foundTotal`')
                            ->select('db_get.db_index AS `dbIdx`')
                            ->select('db_get.onoff AS `onOff`')
                            ->select('DBFLAG.flag_color AS `dbColor`')
                            ->join('db_flag AS `DBFLAG`', 'db_get.flag_index = DBFLAG.flag_index')
                            ->where('onoff', 1)
                            ->get('db_get')
                            ->result_array();
        }


        public function getSpecificDB(string $dbIdx): array
        {
            return $this->db->select('db_get.name AS `dbName`')
                            ->select('db_get.keyword_group_name AS `dbLandName`')
                            ->select('db_get.get_datetime AS `dbGet`')
                            ->select('db_get.db_history AS `dbHistory`')
                            ->select('db_get.keyword_count AS `keywordCount`')
                            ->select('db_get.db_index AS `dbIdx`')
                            ->select('db_get.onoff AS `onOff`')
                            ->select('DBFLAG.flag_color AS `dbColor`')
                            ->join('db_flag AS `DBFLAG`', 'db_get.flag_index = DBFLAG.flag_index')
                            ->where('db_index', $dbIdx)
                            ->get('db_get')
                            ->result_array();

        }



        public function getSevenDaysDB(string $dbIdx): array
        {
            return $this->db->select('searched_db.phonenumber AS `dbPhonenumber`')
                            ->select('searched_db.keyword AS `dbKeyword`')
                            ->select('searched_db.searched AS `dbSearched`')
                            ->where('db_index', $dbIdx)
                            ->where('date(searched)>=date(DATE_ADD(NOW(), INTERVAL -7 DAY))')
                            ->get('searched_db')
                            ->result_array();
        }


        public function getYesterdayDB(string $dbIdx): array
        {
            return $this->db->select('searched_db.phonenumber AS `dbPhonenumber`')
                            ->select('searched_db.keyword AS `dbKeyword`')
                            ->select('searched_db.searched AS `dbSearched`')
                            ->where('db_index', $dbIdx)
                            ->where('date(searched)=date(DATE_ADD(NOW(), INTERVAL -24 HOUR))')
                            ->get('searched_db')
                            ->result_array();
        }


        public function getTodayDB(string $dbIdx): array
        {
            return $this->db->select('searched_db.phonenumber AS `dbPhonenumber`')
                            ->select('searched_db.keyword AS `dbKeyword`')
                            ->select('searched_db.searched AS `dbSearched`')
                            ->where('db_index', $dbIdx)
                            ->where('date(searched)=date(now())')
                            ->get('searched_db')
                            ->result_array();
        }



        public function getValidKeywordsCount(): array
        {
            return $this->db->select('sum(keyword_count) as `validCount`')
                            ->where('onoff', "1")
                            ->get('db_get')
                            ->result_array();
        }



        public function getValidKeywordsCountExcept(int $groupNum): array
        {
            return $this->db->select('sum(keyword_count) as `validCount`')
                            ->where('onoff', "1")
                            ->where('db_index!=', $groupNum)
                            ->get('db_get')
                            ->result_array();
        }


        
        public function wantTurnOnKeywordsCount(int $groupNum): array
        {
            return $this->db->select('sum(keyword_count) as `keywordsCount`')
                            ->where('db_index', $groupNum)
                            ->get('db_get')
                            ->result_array();
        }



        public function deleteDB(int $apply_index): bool
        {

            $query = $this->db->query("DELETE FROM `apply_list` where apply_index='" . $apply_index . "'");

            return true;
        }


        public function deleteKeywordGroup(int $groupNum): bool
        {

            $query = $this->db->query("DELETE FROM `db_get` where db_index='" . $groupNum . "'");

            return true;
        }


        public function turnOffLink(int $groupNum): bool
        {
            $this->db->trans_begin();
            $this->db
                ->set('onoff', 0)
                ->where('link_idx', $groupNum)
                ->update('service_link');
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }


        }



        public function turnOnLink(int $groupNum): bool
        {
            $this->db->trans_begin();
            $this->db
                ->set('onoff', 1)
                ->where('link_idx', $groupNum)
                ->update('service_link');
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }


        }






        public function turnOnFaq(int $groupNum): bool
        {
            $this->db->trans_begin();
            $this->db
                ->set('onoff', 1)
                ->where('link_idx', $groupNum)
                ->update('faq_list');
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }


        }





        public function turnOffFaq(int $groupNum): bool
        {
            $this->db->trans_begin();
            $this->db
                ->set('onoff', 0)
                ->where('link_idx', $groupNum)
                ->update('faq_list');
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }


        }






        public function turnOnBoard(int $groupNum): bool
        {
            $this->db->trans_begin();
            $this->db
                ->set('onoff', 1)
                ->where('link_idx', $groupNum)
                ->update('boards');
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }


        }





        public function turnOffBoard(int $groupNum): bool
        {
            $this->db->trans_begin();
            $this->db
                ->set('onoff', 0)
                ->where('link_idx', $groupNum)
                ->update('boards');
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }


        }







        public function turnOffKeywordGroup(int $groupNum): bool
        {
            $this->db->trans_begin();
            $this->db
                ->set('onoff', 0)
                ->where('db_index', $groupNum)
                ->update('db_get');
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }


        }



        public function turnOnKeywordGroup(int $groupNum): bool
        {
            $this->db->trans_begin();
            $this->db
                ->set('onoff', 1)
                ->where('db_index', $groupNum)
                ->update('db_get');
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }


        }




        public function updateButton(string $link_address,  int $link_index): bool
        {


            $this->db->trans_begin();
            $this->db
                ->set('link_address', $link_address)
                ->where('link_idx', $link_index)
                ->update('service_link');
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }



                            
        }


        public function updateFaq(string $question,  int $idx, string $answer): bool
        {


            $this->db->trans_begin();
            $this->db
                ->set('button_name', $question)
                ->set('link_address', $answer)
                ->where('link_idx', $idx)
                ->update('faq_list');
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }



                            
        }





        public function updateBoard(string $question,  int $idx, string $answer): bool
        {


            $this->db->trans_begin();
            $this->db
                ->set('button_name', $question)
                ->set('link_address', $answer)
                ->where('link_idx', $idx)
                ->update('boards');
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }



                            
        }





        public function updateKeywordGroup(string $groupname, string $keywords, int $keywordscount, int $dbindex): bool
        {


            $this->db->trans_begin();
            $this->db
                ->set('keyword_count', $keywordscount)
                ->set('db_history', $keywords)
                ->where('db_index', $dbindex)
                ->update('db_get');
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }




            $this->db->trans_begin();
            $this->db->insert('db_get', [
                'keyword_group_name' => $groupname,
                'db_history' => $keywords,
                'keyword_count' => $keywordscount,
            ]);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }

                            
        }








        public function insertMentoring(string $var1, string $var2, string $var3, string $var4, string $var5, string $var6, string $var7): bool
        {




            $this->db->trans_begin();
            $this->db->insert('apply_list', [
                'apply_where' => "온라인멘토링",
                'var_1' => $var1,
                'var_2' => $var2,
                'var_3' => $var3,
                'var_4' => $var4,
                'var_5' => $var5,
                'var_6' => $var6,
                'var_7' => $var7,
            ]);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }

                            
        }

        public function insertInterview(string $var1, string $var2, string $var3, string $var4, string $var5, string $var6): bool
        {




            $this->db->trans_begin();
            $this->db->insert('apply_list', [
                'apply_where' => "온라인모의면접",
                'var_1' => $var1,
                'var_2' => $var2,
                'var_3' => $var3,
                'var_4' => $var4,
                'var_5' => $var5,
                'var_6' => $var6,
            ]);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }

                            
        }




        public function getApplies(string $apply_where): array
        {
            return $this->db->select('apply_list.apply_index AS `apply_index`')
                            ->select('apply_list.apply_when AS `apply_when`')
                            ->select('apply_list.var_1 AS `var_1`')
                            ->select('apply_list.var_2 AS `var_2`')
                            ->select('apply_list.var_3 AS `var_3`')
                            ->select('apply_list.var_4 AS `var_4`')
                            ->select('apply_list.var_5 AS `var_5`')
                            ->select('apply_list.var_6 AS `var_6`')
                            ->select('apply_list.var_7 AS `var_7`')
                            ->select('apply_list.var_8 AS `var_8`')
                            ->select('apply_list.var_9 AS `var_9`')
                            ->select('apply_list.var_10 AS `var_10`')
                            ->where('apply_where', $apply_where)
                            ->get('apply_list')
                            ->result_array();
        }





        public function getSpecificSeminarApplier(string $apply_number): array
        {



            $query = $this->db->query("SELECT replace(var_2,'-','') as `var_2`  FROM `apply_list` where apply_where='세미나' and replace(var_2,'-','') ='".$apply_number."'")->result_array();

            return $query;

        }





        public function getSpecificSeminar(string $var5, string $var6): array
        {
            return $this->db->select('apply_list.apply_index AS `apply_index`')
                            ->where('apply_where', "온라인모의면접")
                            ->where('var_5', $var5)
                            ->where('var_6', $var6)
                            ->get('apply_list')
                            ->result_array();
        }


        public function getSpecificMentoring(string $var4, string $var6, string $var7): array
        {
            return $this->db->select('apply_list.apply_index AS `apply_index`')
                            ->where('apply_where', "온라인멘토링")
                            ->where('var_4', $var4)
                            ->where('var_6', $var6)
                            ->where('var_7', $var7)
                            ->get('apply_list')
                            ->result_array();
        }



        public function getChat(string $chatIdx): array
        {
            // return $this->db->select('seminar_chat.chat_idx AS `chat_idx`')
            //                 ->select('seminar_chat.chat_message AS `chat_message`')
            //                 ->where('chat_idx>', $chatIdx)
            //                 ->order_by("seminar_chat.chat_idx", "desc")
            //                 ->limit(3)
            //                 ->get('seminar_chat')


            $query = $this->db->query("select * from (select * FROM `seminar_chat` where chat_idx>" . $chatIdx . " order by chat_idx desc limit 3) as A order by chat_idx asc")->result_array();;

            return $query;
        }
        


        

        public function getEmailMentoring(string $var3, string $var4): array
        {
            return $this->db->select('apply_list.apply_index AS `apply_index`')
                            ->where('apply_where', "온라인멘토링")
                            ->where('var_4', $var4)
                            ->where('var_3', $var3)
                            ->get('apply_list')
                            ->result_array();
        }


        public function getPhoneNumberMentoring(string $var2, string $var4): array
        {
            return $this->db->select('apply_list.apply_index AS `apply_index`')
                            ->where('apply_where', "온라인멘토링")
                            ->where('var_4', $var4)
                            ->where('var_2', $var2)
                            ->get('apply_list')
                            ->result_array();
        }






        public function getSpecificInterview(string $var5, string $var6): array
        {
            return $this->db->select('apply_list.apply_index AS `apply_index`')
                            ->where('apply_where', "온라인모의면접")
                            ->where('var_5', $var5)
                            ->where('var_6', $var6)
                            ->get('apply_list')
                            ->result_array();
        }




        public function getPhoneNumberInterview(string $var2): array
        {
            return $this->db->select('apply_list.apply_index AS `apply_index`')
                            ->where('apply_where', "온라인모의면접")
                            ->where('var_2', $var2)
                            ->get('apply_list')
                            ->result_array();
        }




        public function getEmailInterview(string $var3): array
        {
            return $this->db->select('apply_list.apply_index AS `apply_index`')
                            ->where('apply_where', "온라인모의면접")
                            ->where('var_3', $var3)
                            ->get('apply_list')
                            ->result_array();
        }





        public function insertSeminar(string $var1, string $var2, string $var3, string $var4, string $var5): bool
        {




            $this->db->trans_begin();
            $this->db->insert('apply_list', [
                'apply_where' => "세미나",
                'var_1' => $var1,
                'var_2' => $var2,
                'var_3' => $var3,
                'var_4' => $var4,
                'var_5' => $var5,
            ]);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }

                            
        }




        public function insertChat(string $message): bool
        {




            $this->db->trans_begin();
            $this->db->insert('seminar_chat', [
                'chat_message' => $message,
                'chat_ip' => $_SERVER["REMOTE_ADDR"],
            ]);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }

                            
        }





        public function createNewKeywordGroup(string $groupname, string $keywords, int $keywordscount): bool
        {


            $this->db->trans_begin();
            $this->db->insert('db_get', [
                'keyword_group_name' => $groupname,
                'db_history' => $keywords,
                'keyword_count' => $keywordscount,
            ]);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }

                            
        }





        public function createNewHireInfo(string $company_name, string $location, string $logo_image, string $company_category, string $company_tags, string $slide_images, string $company_info, string $recruit_info, string $faqs): bool
        {


            $this->db->trans_begin();
            $this->db->insert('reqruit_info', [
                'company_name' => $company_name,
                'location' => $location,
                'logo_image' => $logo_image,
                'company_category' => $company_category,
                'company_tags' => $company_tags,
                'slide_images' => $slide_images,
                'company_info' => $company_info,
                'recruit_info' => $recruit_info,
                'faqs' => $faqs,
            ]);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }

                            
        }






        public function createNewEventInfo(string $company_name, string $location, string $slide_images, string $company_info): bool
        {


            $this->db->trans_begin();
            $this->db->insert('event_info', [
                'company_name' => $company_name,
                'location' => $location,
                'slide_images' => $slide_images,
                'company_info' => $company_info,
            ]);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }

                            
        }





        public function createNewEventResultInfo(string $company_name, string $location, string $slide_images, string $company_info): bool
        {


            $this->db->trans_begin();
            $this->db->insert('event_result_info', [
                'company_name' => $company_name,
                'location' => $location,
                'slide_images' => $slide_images,
                'company_info' => $company_info,
            ]);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }

                            
        }





        public function updateHireInfo(string $company_name, string $location, string $logo_image, string $company_category, string $company_tags, string $slide_images, string $company_info, string $recruit_info, string $faqs, string $company_index): bool
        {
            $this->db->trans_begin();
            $this->db
                ->set('company_name', $company_name)
                ->set('location', $location)
                ->set('logo_image', $logo_image)
                ->set('company_category', $company_category)
                ->set('company_tags', $company_tags)
                ->set('slide_images', $slide_images)
                ->set('company_info', $company_info)
                ->set('recruit_info', $recruit_info)
                ->set('faqs', $faqs)
                ->where('company_index', $company_index)
                ->update('reqruit_info');
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }


        }







        public function updateEventInfo(string $company_name, string $location, string $slide_images, string $company_info, string $company_index): bool
        {
            $this->db->trans_begin();
            $this->db
                ->set('company_name', $company_name)
                ->set('location', $location)
                ->set('slide_images', $slide_images)
                ->set('company_info', $company_info)
                ->where('company_index', $company_index)
                ->update('event_info');
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }


        }





        public function updateEventResultInfo(string $company_name, string $location, string $slide_images, string $company_info, string $company_index): bool
        {
            $this->db->trans_begin();
            $this->db
                ->set('company_name', $company_name)
                ->set('location', $location)
                ->set('slide_images', $slide_images)
                ->set('company_info', $company_info)
                ->where('company_index', $company_index)
                ->update('event_result_info');
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }


        }





        public function deleteEventInfo(string $company_index): bool
        {

            $query = $this->db->query("DELETE FROM `event_info` where company_index='" . $company_index . "'");

            return true;


        }




        public function deleteEventResultInfo(string $company_index): bool
        {

            $query = $this->db->query("DELETE FROM `event_result_info` where company_index='" . $company_index . "'");

            return true;


        }









        public function createNewFaq(string $question, string $answer): bool
        {


            $this->db->trans_begin();
            $this->db->insert('faq_list', [
                'button_name' => $question,
                'link_address' => $answer,
            ]);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }

                            
        }








        public function createNewBoard(string $question, string $answer, string $whereto): bool
        {


            $this->db->trans_begin();
            $this->db->insert('boards', [
                'button_name' => $question,
                'link_address' => $answer,
                'page_name' => $whereto,
            ]);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }

                            
        }




        public function getServerNum(): array
        {
            return $this->db->select('server_condition.server_num AS `serverNum`')
                            ->order_by("server_condition.servernum_created", "desc")
                            ->limit(1)
                            ->get('server_condition')
                            ->result_array();
        }



    }
}
