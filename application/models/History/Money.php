<?php

if (! class_exists('Money')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Money extends CI_Model
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->database();
        }


        // public function editAdmin() : void {
        //     $data['status'] = true;
        //     $this->Money->update_admin($this->input->get('id'), $this->input->get('check'));
        // 	header('Content-type: application/json');
        //     echo json_encode($data);
        // }
        // public function editCredit() : void {
        //     $data['status'] = true;
        //     $this->Money->update_credit($this->input->get('id'), $this->input->get('check'));
        // 	header('Content-type: application/json');
        //     echo json_encode($data);
        // }
        /*
         $this->db->trans_begin();
            $this->db
                ->set('condition', $condition)
                ->where('company_id', '1')
                ->update('gifticon_condition');
                if($this->db->trans_status()) {
                    $this->db->trans_commit();
                    return true;
                } else {
                    $this->db->trans_rollback();
                    return false;
                }
        */

        public function update_admin(string $id, string $check): bool
        {
            $this->db->trans_begin();
            $this->db
                ->set('is_admin', $check)
                ->where('idx', $id)
                ->update('members');
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }
        }
        public function update_credit(string $id, string $check): bool
        {
            $this->db->trans_begin();
            $this->db
                ->set('credit_pay', $check)
                ->where('idx', $id)
                ->update('members');
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }
        }



        public function getTransactionHistory(): array
        {
            /**
             * 메소드 설명을
             * 여기다 입력
             *
             * @param   $reqData->userIdx => 조회할 유저 인덱스,
             * @param   $reqData->reqStartDate => 조회할 시작기간,
             * @param   $reqData->reqEndDate => 조회할 끝기간,
             * @param   $reqData->pagingStart => 페이징,
             * @param   $reqData->targetOpt => 페이징,
             *
             * @throws  throwClass throw 타입 설명
             * @used    \사용된\클래스\메소드
             * @author  저자 이름
             * @version 버전명
             * @return type 변수이름 리턴 설명
             */

            $this->db->query("set @NUM = 0.0000;");

            $sql = "select * from transaction_000001 where to_idx=" . $this->session->userIdx . " order by transaction_timestamp desc";

            return $this->db->query($sql)
                            ->result_array();
        }


        public function member_modify(string $indexnumber, string $member_id, string $member_name, string $member_mail): bool
        {
            $sql = "update members set  member_mail='" . $member_mail . "', member_name='" . $member_name . "' where idx=" . $indexnumber;
            // echo $sql;
            $this->db->query($sql);
            return true;
        }


        public function getAllOwnInfoHistory(): array
        {
            /**
             * 메소드 설명을
             * 여기다 입력
             *
             * @param   $reqData->userIdx => 조회할 유저 인덱스,
             * @param   $reqData->reqStartDate => 조회할 시작기간,
             * @param   $reqData->reqEndDate => 조회할 끝기간,
             * @param   $reqData->pagingStart => 페이징,
             * @param   $reqData->targetOpt => 페이징,
             *
             * @throws  throwClass throw 타입 설명
             * @used    \사용된\클래스\메소드
             * @author  저자 이름
             * @version 버전명
             * @return type 변수이름 리턴 설명
             */


            $sql = "select * from token_own_info order by own_last desc";

            return $this->db->query($sql)
                            ->result_array();
        }
        public function getAllCustomer(): array
        {
            /**
             * 메소드 설명을
             * 여기다 입력
             *

             */


            // $sql = "select * from members order by  idx desc";
            // $sql = `
            // SELECT idx, member_id, member_email, member_phone, is_admin, member_jointime, credit_pay,
            // (SELECT COUNT(1) FROM orderlist WHERE userid LIKE CONCAT('%',m.member_id,'%')) AS order_count,
            // (SELECT COUNT(1) FROM gifticon_list WHERE member_id = m.member_id) AS g_count,
            // (SELECT COUNT(1) FROM charge_history WHERE state='message' and member_id = m.member_id) AS m_count
            // FROM members m
            // ORDER BY idx DESC
            // `;
            // ->where([
            //     'member_id' => $userId
            // ])

            return $this->db->select('idx')
                            ->select('member_id')
                            ->select('member_email')
                            ->select('member_phone')
                            ->select('is_admin')
                            ->select('member_jointime')
                            ->select('credit_pay')
                            ->select('(SELECT COUNT(1) FROM orderlist WHERE userid LIKE CONCAT(\'%\',m.member_id,\'%\')) AS order_count')
                            ->select('(SELECT COUNT(1) FROM gifticon_list WHERE member_id = m.member_id) AS g_count')
                            ->select('(SELECT COUNT(1) FROM charge_history WHERE state=\'message\' and member_id = m.member_id) AS m_count')
                            ->select('(IFNULL((SELECT SUM(mileage) FROM mileage WHERE member_id = m.member_id and used IS NULL),0) - IFNULL((SELECT SUM(mileage) FROM mileage WHERE member_id = m.member_id and used = \'used\'),0)) as mileage')

                            ->get('members m')
                            // ->order_by("idx", "desc")
                            -> result_array();

            // return $this->db->query($sql)->result_array();
        }
        public function getTransactionHistoryByOwnIndex(int $own_idx): array
        {
            /**
             * 메소드 설명을
             * 여기다 입력
             *
             * @param   $reqData->userIdx => 조회할 유저 인덱스,
             * @param   $reqData->reqStartDate => 조회할 시작기간,
             * @param   $reqData->reqEndDate => 조회할 끝기간,
             * @param   $reqData->pagingStart => 페이징,
             * @param   $reqData->targetOpt => 페이징,
             *
             * @throws  throwClass throw 타입 설명
             * @used    \사용된\클래스\메소드
             * @author  저자 이름
             * @version 버전명
             * @return type 변수이름 리턴 설명
             */

            $this->db->query("set @NUM = 0.0000;");

            $sql = "select * from transaction_000001 where own_index=" . (string)$own_idx;

            return $this->db->query($sql)
                            ->result_array();
        }
        public function getTransactionHistoryByToIdx(int $to_idx): array
        {
            /**
             * 메소드 설명을
             * 여기다 입력
             *
             * @param   $reqData->userIdx => 조회할 유저 인덱스,
             * @param   $reqData->reqStartDate => 조회할 시작기간,
             * @param   $reqData->reqEndDate => 조회할 끝기간,
             * @param   $reqData->pagingStart => 페이징,
             * @param   $reqData->targetOpt => 페이징,
             *
             * @throws  throwClass throw 타입 설명
             * @used    \사용된\클래스\메소드
             * @author  저자 이름
             * @version 버전명
             * @return type 변수이름 리턴 설명
             */

            $this->db->query("set @NUM = 0.0000;");

            $sql = "select * from transaction_000001 where from_idx=" . (string)$to_idx;

            //echo  $sql;

            return $this->db->query($sql)
                            ->result_array();
        }
        public function getMemberByMemberIdx(int $member_idx): array
        {
            /**
             * 메소드 설명을
             * 여기다 입력
             *
             * @param   $reqData->userIdx => 조회할 유저 인덱스,
             * @param   $reqData->reqStartDate => 조회할 시작기간,
             * @param   $reqData->reqEndDate => 조회할 끝기간,
             * @param   $reqData->pagingStart => 페이징,
             * @param   $reqData->targetOpt => 페이징,
             *
             * @throws  throwClass throw 타입 설명
             * @used    \사용된\클래스\메소드
             * @author  저자 이름
             * @version 버전명
             * @return type 변수이름 리턴 설명
             */

            $this->db->query("set @NUM = 0.0000;");

            $sql = "select * from members where idx=" . (string)$member_idx;

            //echo  $sql;

            return $this->db->query($sql)
                            ->result_array();
        }
    }
}
