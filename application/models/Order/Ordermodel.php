<?php

if (! class_exists('Ordermodel')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Ordermodel extends CI_Model
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->database();
        }


        //$this->Ordermodel->set_rider_info($checkdb['orderNum'],$detail_result['message'][5]->state,$detail_result['message'][9]->start_lat/360000,$detail_result['message'][9]->start_lon/360000,$detail_result['message'][9]->dest_lat/360000,$detail_result['message'][9]->dest_lon/360000,$detail_result['message'][9]->rider_lat/360000,$detail_result['message'][9]->rider_lon/360000);
        public function set_rider_info(string $orderNum, string $state, $start_lat, $start_lon, $dest_lat, $dest_lon, $rider_lat, $rider_lon): bool
        {
            $this->db->trans_begin();
            $this->db->insert('rider_info', [
                'orderNum' => $orderNum,
                'state' => $state,
                'start_lat' => $start_lat,
                'start_lon' => $start_lon,
                'dest_lat' => $dest_lat,
                'dest_lon' => $dest_lon,
                'rider_lat' => $rider_lat,
                'rider_lon' => $rider_lon
            ]);


            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }
        }


        public function update_order_cancel(string $userid, string $serial)
        {
            $this->db->trans_begin();
            $this->db
                ->set('state', '취소')
                ->where('orderNum', $serial)
                ->where('userId', $userid)
                ->update('orderlist');
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }
        }

        // coupon_id
        // item_id
        // buy_date,
        // expire_date,
        // is_present
        // receiver
        // sender
        // coupon_status

        /*
        SELECT
    (SELECT is_condition FROM message_condition WHERE company_id = 1 AND message_id = 9) AS s9,
    (SELECT is_condition FROM message_condition WHERE company_id = 1 AND message_id = 12) AS s12,
    (SELECT is_condition FROM message_condition WHERE company_id = 1 AND message_id = 15) AS s15,
    (SELECT is_condition FROM message_condition WHERE company_id = 1 AND message_id = 18) AS s18,
    (SELECT is_condition FROM message_condition WHERE company_id = 1 AND message_id = 21) AS s21
FROM dual;
        */


        public function get_message_condition(): array
        {
            $sql = "
            SELECT 
	(SELECT is_condition FROM message_condition WHERE company_id = 1 AND message_id = 9) AS s9,
	(SELECT is_condition FROM message_condition WHERE company_id = 1 AND message_id = 12) AS s12,
	(SELECT is_condition FROM message_condition WHERE company_id = 1 AND message_id = 15) AS s15,
	(SELECT is_condition FROM message_condition WHERE company_id = 1 AND message_id = 18) AS s18,
	(SELECT is_condition FROM message_condition WHERE company_id = 1 AND message_id = 21) AS s21
FROM dual
            ";

            // return $this->db->select('(SELECT is_condition FROM message_condition WHERE company_id = 1 AND message_id = 9) AS s9')
            // ->select('(SELECT is_condition FROM message_condition WHERE company_id = 1 AND message_id = 12) AS s12')
            // ->select('(SELECT is_condition FROM message_condition WHERE company_id = 1 AND message_id = 15) AS s15')
            // ->select('(SELECT is_condition FROM message_condition WHERE company_id = 1 AND message_id = 18) AS s18')
            // ->select('(SELECT is_condition FROM message_condition WHERE company_id = 1 AND message_id = 21) AS s21')
            // // ->select('credit_pay')
            // // ->select('(SELECT COUNT(1) FROM orderlist WHERE userid LIKE CONCAT(\'%\',m.member_id,\'%\')) AS order_count')
            // // ->select('(SELECT COUNT(1) FROM gifticon_list WHERE member_id = m.member_id) AS g_count')
            // // ->select('(SELECT COUNT(1) FROM charge_history WHERE state=\'message\' and member_id = m.member_id) AS m_count')
            // ->get('dual')
            // // ->order_by("idx", "desc")
            // -> result_array();

            return $this->db->query($sql)
                            ->result_array();
        }



        public function update_message_condition($s9, $s12, $s15, $s18, $s21): bool
        {
            $this->db->trans_begin();
            $this->db
                ->set('is_condition', $s9)
                ->where('company_id', '1')
                ->where('message_id', '9')
                ->update('message_condition');

            $this->db
                ->set('is_condition', $s12)
                ->where('company_id', '1')
                ->where('message_id', '12')
                ->update('message_condition');

            $this->db
                ->set('is_condition', $s15)
                ->where('company_id', '1')
                ->where('message_id', '15')
                ->update('message_condition');

            $this->db
                ->set('is_condition', $s18)
                ->where('company_id', '1')
                ->where('message_id', '18')
                ->update('message_condition');

            $this->db
                ->set('is_condition', $s21)
                ->where('company_id', '1')
                ->where('message_id', '21')
                ->update('message_condition');

            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }
        }
        public function update_gifticon_condition(string $condition): bool
        {
            $this->db->trans_begin();
            $this->db
                ->set('condition', $condition)
                ->where('company_id', '1')
                ->update('gifticon_condition');
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }
        }
        public function send_gifticon(string $member_id, string $coupon_id, string $item_id, string $buy_date, string $expire_date, string $is_present, string $receiver, string $sender, string $coupon_status): bool
        {
            $this->db->trans_begin();
            $this->db->insert('order_message_list', [
                'member_id' => $member_id,
                'coupon_id' => $coupon_id,
                'buy_date' => $buy_date,
                'expire_date' => $expire_date,
                'is_present' => $is_present,
                'receiver' => $receiver,
                'sender' => $sender,
                'coupon_status' => $coupon_status
            ]);

            $this->db->insert('charge_history', [
                'state' => 'gifticon',
                'member_id' => $member_id,
                'item_id' => $coupon_id
            ]);



            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }
        }




        public function check_gifticon_send(): array
        {
            $sql = "SELECT * FROM gifticon_condition WHERE company_id = 1";

            return $this->db->query($sql)
                            ->result_array();
        }




        // 보내전에 값이 있는지 없는지 체크해서 전송하기
        // member_id $checkdb['StartName'],
        // orderNum 194658008   $checkdb['orderNum']
        // message_type = user_template_no
        // is_send = 1

        //if($this->Ordermodel->check_order_message_send( $checkdb['StartName'], $checkdb['orderNum'], $user_template_no, 1 ) == 0){

        public function check_order_message_send(string $member_id, string $orderNum, string $message_type, string $is_send): bool
        {
            /*

            */

            $query = $this->db->query("SELECT COUNT(1) AS total FROM order_message_list WHERE member_id = '" . $member_id . "' AND orderNum = '" . $orderNum . "' AND message_type = '" . $message_type . "' AND is_send = '" . $is_send . "';");
            $cnt = $query->row()->count;
            if ($cnt == 0) {
                $this->db->trans_begin();
                $this->db->insert('order_message_list', [
                    'member_id' => $member_id,
                    'orderNum' => $orderNum,
                    'message_type' => $message_type,
                    'is_send' => $is_send
                ]);

                $this->db->insert('charge_history', [
                    'state' => 'message',
                    'member_id' => $member_id
                ]);


                if ($this->db->trans_status()) {
                    $this->db->trans_commit();
                    return true;
                } else {
                    $this->db->trans_rollback();
                    return false;
                }
            } else {
                return false;
            }
        }


        public function get_order_count(string $memberid): array
        {
            $sql = "SELECT COUNT(1) AS total FROM orderlist WHERE orderMobile = '" . $memberid . "';";

            return $this->db->query($sql)
                            ->result_array();
        }


        public function getLiveOrder(string $start = null, string $end = null): array
        {
            /**
             * 메소드 설명을
             * 여기다 입력
             *

             */


            $base = $this->db->where('state !=', '취소')
                             ->where('state !=', '왼료')
                             ->order_by('orderIndex', 'desc');

            if ($start && $end) {
                $base = $base->where(sprintf("order_datetime BETWEEN '%s' AND '%s'", $start, $end));
            }

            return $this->db->get('orderlist')->result_array();
            // $sql = "select * from orderlist where state!='취소' and state!='완료' order by  orderIndex desc";

            // return $this->db->query($sql)
            //                 ->result_array();
        }


        public function getAllOrder(): array
        {
            /**
             * 메소드 설명을
             * 여기다 입력
             *

             */


            $sql = "select * from orderlist order by  orderIndex desc";

            return $this->db->query($sql)
                            ->result_array();
        }

        /**
         * 취소 완료
         */
        public function getOrderFinish(string $start = null, string $end = null): array
        {
            $base = $this->db->where_in('state', [
                '취소','완료'
            ])->order_by('orderIndex', 'desc');
            if ($start && $end) {
                $base = $base->where(sprintf("order_datetime BETWEEN '%s' AND '%s'", $start, $end));
            }
            return $base->get('orderlist')
                        ->result_array();
        }

        public function get_usercheck(string $phonenumber): array
        {
            $query = $this->db->query("SELECT * FROM `insung_members` where phone_number='" . $phonenumber . "'");

            return $query->result_array();
        }


        public function order_rider_update($serial, $rider_code_no, $rider_name, $rider_tel_number): bool
        {
            $this->db->trans_begin();
            $this->db->where('orderNum', $serial)
                     ->update('orderlist', [
                       'checked' => 0,
                       'rider_code_no' => $rider_code_no,
                       'rider_name' => $rider_name,
                       'rider_tel_number' => $rider_tel_number,
                    ]);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }
        }





        public function order_state_update($serial, $state): bool
        {
            var_dump($serial);
            echo "<hr>";
            echo "<hr>";
            echo "<hr>";
            var_dump($state);

            $this->db->trans_begin();
            $this->db->where('orderNum', $serial)
                     ->update('orderlist', [
                       'checked' => 0,
                       'state' => $state,
                    ]);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }
        }


        public function get_order(string $serial): array
        {
            $query = $this->db->query("SELECT * FROM `orderlist` where orderNum='" . $serial . "'");

            return $query->result_array();
        }


        public function get_order_ing(): array
        {
            $query = $this->db->query("SELECT * FROM `orderlist` where state!='취소' and state !='완료'");

            return $query->result_array();
        }








        public function add_user_register(array $userData): bool
        {
            $this->db->trans_begin();
            $this->db->insert('insung_members', [


                'user_password' => $userData['password'],
                'user_name' => $userData['customerName'],

                'user_dong' => $userData['dongName'],
                'phone_number' => $userData['telNo'],

                'user_id' => $userData['user_id'],


            ]);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }
        }












        public function add_order(array $userData): bool
        {
            $this->db->trans_begin();
            $this->db->insert('orderlist', [
                'userId' => $userData['userId'],

                'message_receiver_info' => $userData['notiReceiver'],

                // ALTER TABLE `orderlist` ADD `message_receiver_info` VARCHAR(255) NULL COMMENT '알림톡 받을 핸드폰 번호' AFTER `payType`;




                'orderName' => $userData['orderName'],
                'orderMobile' => $userData['orderMobile'],

                'startLocal' => $userData['startLocal'],
                'startTelNo' => $userData['startTelNo'],
                'startSido' => $userData['startSido'],
                'startGugun' => $userData['startGugun'],
                'startDong' => $userData['startDong'],
                'startName' => $userData['startName'],

                'destLocal' => $userData['destLocal'],
                'destTelNo' => $userData['destTelNo'],
                'destSido' => $userData['destSido'],
                'destGugun' => $userData['destGugun'],
                'destDong' => $userData['destDong'],
                'destName' => $userData['destName'],


                'deliverType' => $userData['deliverType'],
                'payType' => $userData['payType'],
                'doc' => $userData['doc'],
                'sfast' => $userData['sfast'],



                'price' => $userData['price'],
                'start_lon' => $userData['start_lon'],
                'start_lat' => $userData['start_lat'],
                'dest_lon' => $userData['dest_lon'],
                'dest_lat' => $userData['dest_lat'],


                'memo' => $userData['memo'],

                'orderNum' => $userData['orderNum'],

                'card_number' => $userData['card_number'],
                'card_year' => $userData['card_year'],
                'card_month' => $userData['card_month'],
                'card_email' => $userData['card_email'],





            ]);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }
        }
    }
}
