<?php

if (! class_exists('Ordermodel')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Ordermodel extends CI_Model
    {

        /**
         * Ordermodel.php를 쓰는것으로 확인
         *
         */
        public function __construct()
        {
            parent::__construct();
            $this->load->database();
        }



        public function get_usercheck(string $phonenumber): array
        {
            $query = $this->db->query("SELECT * FROM `insunb_members` where phone_number='" . $phonenumber . "'");

            return $query->result_array();
        }



        public function getAllOrder(): array
        {
            /**
             * 메소드 설명을
             * 여기다 입력
             *

             */


            $sql = "select * from orderlist order by  orderIndex desc";

            return $this->db->query($sql)
                            ->result_array();
        }


        /**
         * 취소 완료
         */
        public function getOrderFinish(): array
        {
            $sql = "select * from orderlist order by  orderIndex desc";

            return $this->db->query($sql)
                            ->result_array();
        }

        public function add_order(array $userData): bool
        {
            $this->db->trans_begin();
            $this->db->insert('orderlist', [
                'userId' => $userData['userId'],





                'orderName' => $userData['orderName'],
                'orderMobile' => $userData['orderMobile'],

                'startLocal' => $userData['startLocal'],
                'startTelNo' => $userData['startTelNo'],
                'startSido' => $userData['startSido'],
                'startGugun' => $userData['startGugun'],
                'startDong' => $userData['startDong'],
                'startName' => $userData['startName'],

                'destLocal' => $userData['destLocal'],
                'destTelNo' => $userData['destTelNo'],
                'destSido' => $userData['destSido'],
                'destGugun' => $userData['destGugun'],
                'destDong' => $userData['destDong'],
                'destName' => $userData['destName'],


                'deliverType' => $userData['deliverType'],
                'payType' => $userData['payType'],
                'doc' => $userData['doc'],
                'sfast' => $userData['sfast'],



                'price' => $userData['price'],
                'start_lon' => $userData['start_lon'],
                'start_lat' => $userData['start_lat'],
                'dest_lon' => $userData['dest_lon'],
                'dest_lat' => $userData['dest_lat'],


                'memo' => $userData['memo'],

                'orderNum' => $userData['orderNum'],

                'card_number' => $userData['card_number'],
                'card_month' => $userData['card_month'],
                'card_year' => $userData['card_year'],
                'card_email' => $userData['card_email'],





            ]);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }
        }
    }
}
