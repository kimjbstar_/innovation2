<?php

if (! class_exists('Mileage')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Mileage extends CI_Model
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->database();
        }
        public function get_current_mileage($id): array
        {
            $sql = "SELECT IFNULL((SELECT SUM(mileage) FROM mileage WHERE member_id = ? and used IS NULL),0) AS mileage, 
            IFNULL((SELECT SUM(mileage) FROM mileage WHERE member_id = ? and used = 'used'),0) AS used_mileage, 
            IFNULL((SELECT SUM(mileage) FROM mileage WHERE member_id = ? and used IS NULL),0) - IFNULL((SELECT SUM(mileage) FROM mileage WHERE member_id = ? and used = 'used'),0) as sum_mileage FROM DUAL; ";
            $result = $this->db->query($sql, [$id, $id, $id, $id]);
            return $result->result();
        }

        public function set_mileage($id, $num, $price, $mileage, $used): bool
        {
            $this->db->trans_begin();
            $this->db->insert('mileage', [
                'member_id' => $id,
                'orderNum' => $num,
                'price' => $price,
                'mileage' => $mileage,
                'used'=> $used

            ]);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }
        }

        public function get_mileage_history(): array
        {
            $query = "SELECT id, member_id, orderNum, price, mileage, used, update_date FROM mileage where member_id is not null;";
            $result = $this->db->query($query);
            return $result->result_array();
        }
    }
}
