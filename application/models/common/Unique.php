<?php

if (! class_exists('Unique')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Unique extends CI_Model
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->database();
        }

        public function isUnique(array $data): bool
        {
            return $this->db->select($data['colName'])
                            ->where([
                                $data['colName'] => $data['reqData']
                            ])
                            ->get($data['tableName'])
                            ->num_rows() === 0;
        }
    }
}
