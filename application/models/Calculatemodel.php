<?php

if (! class_exists('Calculatemodel')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class Calculatemodel extends CI_Model
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->database();
        }
        public function get_calculate(): array
        {
            $query = $this->db->query("SELECT * FROM orderlist WHERE  state = '완료'");
            return $query->result_array();
        }
    }
}
