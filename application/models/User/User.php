<?php

if (! class_exists('User')) {
    defined('BASEPATH') or exit('No direct script access allowed');
    class User extends CI_Model
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->database();
        }


        public function update_gifticon_status(string $coupon_id)
        {
            $this->db->trans_begin();
            $this->db->where('coupon_id', $coupon_id)
                     ->update('coupon_status', 'used');
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }
        }
        //$this->User->get_gifticon_list($this->session->userId)
        public function get_gifticon_list(string $userId): array
        {
            $query = $this->db->query("SELECT * FROM gifticon_list where member_id = " . $userId);

            return $query->result_array();
        }

        public function add_weather_grid(string $full_addr, string $addr_1, string $addr_2, string $addr_3, string $gpsx, string $gpsy, string $nx, string $ny): bool
        {
            $this->db->trans_begin();
            $this->db->insert('weather_grid', [
                'full_addr' => $full_addr,
                'addr_1' => $addr_1,
                'addr_2' => $addr_2,
                'addr_3' => $addr_3,
                'gpsx' => $gpsx,
                'gpsy' => $gpsy,
                'nx' => $nx,
                'ny' => $ny,
            ]);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }
        }


        public function add(array $userData): bool
        {
            $this->db->trans_begin();
            $this->db->insert('members', [
                'member_id' => $userData['username'],
                // 'member_email' => $userData['email'],
                // 'member_phone' => $userData['phone'],
                'member_password' => $userData['password'],
            ]);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }
        }




        public function get_order(string $phonenumber): array
        {
            $query = $this->db->query("SELECT orderNum FROM `orderlist` where orderMobile='" . $phonenumber . "' order by order_datetime desc limit 30");

            return $query->result_array();
        }


        public function get_nxy(string $gps_x, string $gps_y): array
        {
            $query = $this->db->query("SELECT full_addr,nx,ny,abs(gpsx-" . $gps_x . ") + abs(gpsy-" . $gps_y . ") as 'distance' FROM `weather_grid` ORDER BY `distance` ASC limit 1");

            return $query->result_array();
        }


        public function getInfoByUserId(string $userId): ?object
        {
            return $this->db->select('member_email AS `userEmail`')
                            ->select('member_phone AS `userPhone`')
                            ->select('idx AS `userIdx`')
                            ->where([
                                'member_id' => $userId
                            ])
                            ->get('members')
                            ->result()[0] ?? null;
        }

        public function attempt(array $userInfo): array
        {
            $retArr = [
                'stat' => false,
                'message' => 'misMatch',
                'data' => null,
            ];
            $data = $this->db->select('member_password AS `userPw`')
                            ->select('member_email AS `userEmail`,idx AS `userIdx`, member_id AS `userId`, member_phone AS `userPhone`, is_admin AS `isAdmin`, credit_pay AS `creditPay`')
                            ->where([
                                'member_id' => $userInfo['username']
                            ])
                            ->get('members');
            if ($this->checkPassword($data, $userInfo['password'])) {
                $data = $data->row();
                $retArr = [
                    'stat' => true,
                    'data' => [
                        'userIdx' => $data->userIdx,
                        'userId' => $data->userId,
                        'userPhone' => $data->userPhone,
                        'userEmail' => $data->userEmail,
                        'isAdmin' => $data->isAdmin === '1',
                        'isLogin' => true,
                        'creditPay' => $data->creditPay,
                    ]
                ];
            }
            return $retArr;
        }
        public function update(array $reqData, string $userIdx): bool
        {
            $this->db->trans_begin();
            $this->db->where('idx', $userIdx)
                     ->update('members', $reqData);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }
        }
        private function checkPassword(CI_DB_mysqli_result $result, string $userPw): bool
        {
            return password_verify($userPw, $result->row()->userPw ?? '');
        }
    }
}
