<!-- <div id="load" class="loading-overlay display-flex flex-column justify-content-center align-items-center">
    <div class="primary-color font-28 fas fa-spinner fa-spin"></div>
</div> -->
<!--Loading Container End-->

<div class="row h-100">
    <div class="col-xs-12 col-sm-12">

        <!--Page Title & Icons Start-->
        <div class="header-icons-container text-center shadow-sm">
            <a href="<?=$url['back']?>">
                <span class="float-left">
                    <img src="<?=$img['back']?>" alt="Back Icon">
                </span>
            </a>
            <span>인증번호 입력</span>
        </div>
        <!--Page Title & Icons End-->

        <div class="rest-container">

            <!--Verification Information Container Start-->
            <div class="text-center header-icon-logo-margin">
                <img src="https://via.placeholder.com/124x144" alt="Main Logo">
                <div class="margin-top-25">
                    <div class="verify-text">
                        <span class="font-20">인증번호</span>
                    </div>
                    <span class="font-weight-light font-roboto font-15">
                        SMS로 문자메세지를 발송했습니다. <br/>
                        핸드폰을 확인 바랍니다.
                        <br/>
                        <?=$data['userId']?>
                    </span>
                </div>
            </div>
            <!--Verification Information Container End-->

            <!--Verification Code Container Start-->
            <div class="sign-up-form-container text-center">
                <form class="width-100" method="post" action="<?=$url['verifyCode']?>">
                    <input type="hidden" value="<?=$data['csrf']['hash']?>" name="<?=$data['csrf']['name']?>">
                    <div class="form-group">
                        <div class="input-group border-bottom">
                            <input class="form-control verify-sms" type="tel" name="authCode" placeholder="----" maxlength="4">
                        </div>
                    </div>
                    <div>
                        <span>인증번호가 안왔나요?</span> <button type="button" id="reSendSmsBtn" style="background: transparent;border: 0;" class="href-decoration-none primary-color">인증문자 재발송</button>
                    </div>
                    <div class="form-submit-button">
                        <button style="width: 100%;" type="submit" class="btn btn-primary text-uppercase verify-btn">Verify</button>
                    </div>
                </form>
            </div>
            <!--Verification Code Container Start-->

        </div>
    </div>

    <!--Terms And Conditions Agreement Container Start-->
    <div class="col-xs-12 col-sm-12 text-center sms-rate-text font-roboto flex-end margin-bottom-30">
        <div class="container-sms-rate-text width-100 font-11">
            <span class="light-gray font-weight-light">By signing up you have agreed to our </span>
            <br/>
            <a href="#" class="dark-link">
                <span class="font-weight-light">Terms of Use & Privacy Policy</span>
            </a>
        </div>
    </div>
    <!--Terms And Conditions Agreement Container End-->

</div>