<!-- <div id="load" class="loading-overlay display-flex flex-column justify-content-center align-items-center">
    <div class="primary-color font-28 fas fa-spinner fa-spin"></div>
</div> -->
<!--Loading Container End-->

<div class="row h-100">
    <div class="col-xs-12 col-sm-12">
        <!--Page Title & Icons Start-->
        <div class="header-icons-container text-center shadow-sm">
            <a href="sign-up.html">
                <span class="float-left">
                    <img src="<?=$img['back']?>" alt="Back Icon">
                </span>
            </a>
            <span>인증성공</span>
        </div>
        <!--Page Title & Icons End-->

        <div class="rest-container">

            <!--Successful Verification Information Start-->
            <div class="text-center header-icon-logo-margin header-icon-logo-margin-extra ">
                <img src="<?=$img['verified']?>" alt="Verification Successful">
            </div>
            <div class="verification-text">
                인증되셨습니다.
            </div>
            <div class="all-container text-center font-weight-light">
                지금부터 서비스를 이용하실 수 있습니다.
            </div>
            <!--Successful Verification Information End-->

            <!--Login Button Start-->
            <div class="sign-up-form-container text-center">
                <form class="width-100">
                    <div class="form-submit-button">
                        <a href="<?=$url['main']?>" class="btn btn-primary font-weight-light text-uppercase">퀵서비스는 퀵카</a>
                    </div>
                </form>
            </div>
            <!--Login Button End-->

        </div>
    </div>

    <!--Terms And Conditions Agreement Container Start-->
    <div class="col-xs-12 col-sm-12 text-center sms-rate-text font-roboto flex-end margin-bottom-30">
        <div class="container-sms-rate-text width-100 font-11">
            <span class="light-gray font-weight-light">By signing up you have agreed to our </span>
            <br/>
            <a href="#" class="dark-link">
                <span class="font-weight-light">Terms of Use & Privacy Policy</span>
            </a>
        </div>
    </div>
    <!--Terms And Conditions Agreement Container End-->

</div>