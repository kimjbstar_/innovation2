<body>

    <!--Loading Container Start-->
    <!-- <div id="load" class="loading-overlay display-flex flex-column justify-content-center align-items-center">
        <div class="primary-color font-28 fas fa-spinner fa-spin"></div>
    </div> -->
    <!--Loading Container End-->

    <div class="row h-100">
        <div class="col-xs-12 col-sm-12">

            <!--Page Title & Icons Start-->
            <div class="header-icons-container text-center shadow-sm">
                <a href="sign-in.html">
                    <span class="float-left">
                        <img src="<?=$img['back']?>" alt="Back Icon">
                    </span>
                </a>
                <span>회원가입</span>
            </div>
            <!--Page Title & Icons End-->

            <div class="rest-container">
                <div class="text-center header-icon-logo-margin header-icon-logo-margin-extra">
                    <img src="https://via.placeholder.com/124x144" alt="Main Logo">
                </div>

                <!--Phone Verification Container Start-->
                <div class="sign-up-form-container text-center">
                    <form id="sendMessageForm" class="width-100" method="post" action="<?=$url['sendMessage']?>">

                        <!--Phone Number Container Start-->
                        <div class="form-group">
                            <div class="input-group">
                                <input id="csrf" type="hidden" value="<?=$data['csrf']['hash']?>" name="<?=$data['csrf']['name']?>">
                                <input class="form-control h-100" value="<?=$data['userPhone']?>" id="phone-input" type="tel" name="phone"
                                    autocomplete="off" data-intl-tel-input-id="82" placeholder="(201) 555-0123">
                            </div>
                        </div>
                        <!--Phone Number Container Start-->

                        <div class="form-submit-button small-padding">
                            <button type="submit" id="sendMessage" style="width: 100%;" href="" class="btn btn-primary text-uppercase">인증문자 발송</button>
                        </div>
                    </form>

                    <!--Note Text Start-->
                    <div class="text-center sms-rate-text">
                        <span>You should receive an SMS for verification. Message and data rates may apply</span>
                    </div>
                    <!--Note Text End-->

                </div>
                <!--Phone Verification Container End-->

            </div>
        </div>

        <!--Terms And Conditions Agreement Container Start-->
        <div class="col-xs-12 col-sm-12 text-center sms-rate-text font-roboto flex-end margin-bottom-30">
            <div class="container-sms-rate-text width-100 font-11">
                <span class="light-gray font-weight-light">By signing up you have agreed to our </span>
                <br />
                <a href="#" class="dark-link">
                    <span class="font-weight-light">Terms of Use & Privacy Policy</span>
                </a>
            </div>
        </div>
        <!--Terms And Conditions Agreement Container End-->

    </div>