<body>


<?php

// var_dump($data);

?>

<link rel="stylesheet" href="https://quickcar.co.kr/css/common.css?d=<?=md5(date("Y-m-d H:i:s"))?>">
<link rel="stylesheet" href="https://quickcar.co.kr/css/header.css?d=<?=md5(date("Y-m-d H:i:s"))?>">
<link rel="stylesheet" href="https://quickcar.co.kr/css/menu.css?ver=0.0.1">
<link rel="stylesheet" href="https://quickcar.co.kr/css/content.css?d=<?=md5(date("Y-m-d H:i:s"))?>">

<!--Loading Container Start-->
<!-- <div id="load" class="loading-overlay display-flex flex-column justify-content-center align-items-center">
    <div class="primary-color font-28 fas fa-spinner fa-spin"></div>
</div> -->
<!--Loading Container End-->

<div class="row h-100">
    <div class="col-xs-12 col-sm-12">

        <!--Page Title & Icons Start-->
        <div class="header-icons-container text-center shadow-sm">
            <a href="<?=$url['back']?>">
                <span class="float-left">
                    <img src="<?=$img['back']?>" alt="Back Icon">
                </span>
            </a>
            <span class="title">퀵서비스 주문</span>
            <a href="#">
            <span class="float-right menu-open closed">
                        <!-- <img src="../icons/menu.svg" alt="Menu Hamburger Icon"> -->
                        <button class="btn btn-outline-primary top-button">메뉴</button>
                    </span>
            </a>
        </div>
        <!--Page Title & Icons End-->


            <div class="content_box margin-rel-b-20" style="margin-top:75px;">
                <p class="content_title margin-rel-b-26"><strong>퀵카를 이용해주셔서 감사합니다.<br> 어플을 받으시고, 가입하시면 이용하시면서 <br>아메리카노 기프티콘을 보내드립니다.</strong></p>
                <div class="f f-col f-al-center w-100 which_car">
                    <div class="data-kind-2 content_gray_inner_box hide" data-motorcycle_weight="350kg">
                        <p class="content_gray_title margin-rel-b-8">설치 순서 - 1</p>
                        <!-- <p class="margin-rel-b-8">가로 160cm 폭 110cm 높이 110cm 물품이 많으면 상하차 비용 별도 부과 될 수 있습니다</p> -->
                        <!-- <p class="margin-rel-b-8">계단 유무 등 상하차 관련 추가요금 발생할 수 있습니다.</p> -->
                        <!-- <p class="margin-rel-b-8"><img src="/img/damas.jpg" style="width:100%"></p> -->
                        <p class="margin-rel-b-8">아래의 "어플 다운로드" 버튼을 누르셔서 설치 파일을 받아주세요.</p>
                    </div>
                </div>

            </div>


            <div class="content_box margin-rel-b-20" style="margin-top:50px;">
                
                <div class="f f-col f-al-center w-100 which_car">
                    <div class="data-kind-2 content_gray_inner_box hide" data-motorcycle_weight="350kg">
                        <p class="content_gray_title margin-rel-b-8">설치 순서 - 2</p>
                        <!-- <p class="margin-rel-b-8">가로 160cm 폭 110cm 높이 110cm 물품이 많으면 상하차 비용 별도 부과 될 수 있습니다</p> -->
                        <!-- <p class="margin-rel-b-8">계단 유무 등 상하차 관련 추가요금 발생할 수 있습니다.</p> -->
                        <p class="margin-rel-b-8"><img src="/img/install_01.jpg" style="width:100%"></p>
                        <p class="margin-rel-b-8">다운로드 받으신 파일을 실행해주세요.</p>
                    </div>
                </div>
            </div>


            <div class="content_box margin-rel-b-20" style="margin-top:50px;">
                
                <div class="f f-col f-al-center w-100 which_car">
                    <div class="data-kind-2 content_gray_inner_box hide" data-motorcycle_weight="350kg">
                        <p class="content_gray_title margin-rel-b-8">설치 순서 - 3</p>
                        <!-- <p class="margin-rel-b-8">가로 160cm 폭 110cm 높이 110cm 물품이 많으면 상하차 비용 별도 부과 될 수 있습니다</p> -->
                        <!-- <p class="margin-rel-b-8">계단 유무 등 상하차 관련 추가요금 발생할 수 있습니다.</p> -->
                        <p class="margin-rel-b-8"><img src="/img/install_02.jpg" style="width:100%"></p>
                        <p class="margin-rel-b-8">설치 버튼을 눌러주세요.</p>
                    </div>
                </div>
            </div>


            <div class="content_box margin-rel-b-20" style="margin-top:50px;margin-bottom:200px !important;">
                
                <div class="f f-col f-al-center w-100 which_car">
                    <div class="data-kind-2 content_gray_inner_box hide" data-motorcycle_weight="350kg">
                        <p class="content_gray_title margin-rel-b-8">설치 순서 - 4</p>
                        <!-- <p class="margin-rel-b-8">가로 160cm 폭 110cm 높이 110cm 물품이 많으면 상하차 비용 별도 부과 될 수 있습니다</p> -->
                        <!-- <p class="margin-rel-b-8">계단 유무 등 상하차 관련 추가요금 발생할 수 있습니다.</p> -->
                        <p class="margin-rel-b-8"><img src="/img/install_03.jpg" style="width:100%"></p>
                        <p class="margin-rel-b-8">설치가 완료되었습니다. 감사합니다!</p>
                    </div>
                </div>
            </div>


            <div class="bottom_bar" style="position:relative;">
                <div class="calc-progress" style="position: absolute; width: calc(100% - 2.24rem); height: 55px; line-height: 55px; background: rgb(3, 207, 93); border-radius: 10px; color: rgb(255, 255, 255); text-align: center; vertical-align: middle; z-index: 100; font-size: x-large; bottom: 10px; display: none;">운임료 계산중입니다...</div>
                <div id="block-price" class="f f-col">

                    <div class="block-price-itm f f-ju-between f-al-center margin-rel-b-8" id="addprice" style="width:100%; display:none;">
                        <div class="f">
                            <img src="/svg/ic-receipt.svg" class="margin-rel-r-9" alt="receipt">
                            <p class="text-title">할증 요금</p>
                        </div>
                        <p id="block-pay_price" class="font-bold font-size-16 font-color-accent">
                            <span class="text-pay_price-detail" style="margin-right: 5px;font-size: 0.89rem;"></span>
                            <span class="text-pay_price" style="font-weight: bold;">0원</span>
                        </p>
                    </div>                    
                </div>
                <button onclick="do_download()" id="btn-Confirm" class="btn green w-100"><p>어플 다운로드</p></button>
            </div>


        </div>
    </div>

    
    <?php $this->load->view('common/slideMenu', renderSlideMenu());?>


</div>



<script src="https://quickcar.co.kr/js/lib/jquery.js?lastModify=38b5fe31dc483f36ca2e7aa78545b53f"></script>


<script>
    function do_download(){
        location.replace("https://quickcar.co.kr/install.apk");
    }
</script>
