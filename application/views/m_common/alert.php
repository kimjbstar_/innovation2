<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        window.addEventListener('load', function(e){
            <?php if ($url):?>
            Swal.fire('<?=$message?>').then((v) => {
                location.href = '<?=$url?>';
            });
            <?php else: ?>
                Swal.fire('<?=$message?>');
            <?php endif;?>
        });
    </script>
</body>
</html>