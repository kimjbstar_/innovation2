<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!DOCTYPE html>


<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>관리자</title>



    <?php foreach ($static['assets'] as $val): ?>
    <?php $tmp = explode('.', $val); $last = explode('?', $tmp[count($tmp) - 1])[0];?>
    <?php if ($last === 'css'):?>
    <link rel="stylesheet" href="<?=$val?>">
    <?php endif;?>
    <?php if ($last === 'js'):?>
    <script src="<?=$val?>"></script>
    <?php endif;?>
    <?php endforeach;?>


    <link rel="shortcut icon" href="<?=$static['assets']['images/favicon.png']?>" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-167386364-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-167386364-3');
    </script>


    <!-- kokokokokokokokokoko -->


</head>



<body>


    <?php
        // if ($this->session->isAdmin<1) {
        //     goBackAlert('You are not admin', $this->config->site_url('index'));
        //     exit();
        // } else {
        // }
    ?>

    <div class="main-wrapper">

        <!-- partial:partials/_sidebar.html -->
        <nav class="sidebar">
            <div class="sidebar-header">

                <div class="sidebar-toggler not-active">
                    <span></span>
                    <span></span>
                    <span></span>

                </div>
                <!-- <h5 class="">Quickcar 관리자</h5> -->
            </div>
            <div class="sidebar-body">
                <ul class="nav">
                    <!-- <li class="nav-item nav-category">키워드 관리</li> -->









            
            


                    <li class="nav-item nav-category">링크 관리</li>            
                        <li class="nav-item">
                            <a href="<?=$static['url']['admin_links_buttons']?>" class="nav-link">
                                <!-- <i class="link-icon" data-feather="box"></i> -->
                                <span class="link-title">페이지별 링크 관리</span>
                            </a>
                        </li>
                    </li>


                    <li class="nav-item nav-category">참가 신청서 관리</li>
                        <li class="nav-item">
                            <a href="<?=$static['url']['admin_apply_mentoring']?>" class="nav-link">
                                <!-- <i class="link-icon" data-feather="box"></i> -->
                                <span class="link-title">온라인 멘토링</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$static['url']['admin_apply_mock_interview']?>" class="nav-link">
                                <!-- <i class="link-icon" data-feather="box"></i> -->
                                <span class="link-title">온라인 모의 면접</span>
                            </a>
                        </li>
                        <!-- <li class="nav-item">
                            <a href="<?=$static['url']['admin_apply_consulting']?>" class="nav-link">
                                <i class="link-icon" data-feather="box"></i>
                                <span class="link-title">주제관 상담하기</span>
                            </a>
                        </li> -->
                        <li class="nav-item">
                            <a href="<?=$static['url']['admin_apply_seminar']?>" class="nav-link">
                                <!-- <i class="link-icon" data-feather="box"></i> -->
                                <span class="link-title">세미나</span>
                            </a>
                        </li>
                    </li>

                    <li class="nav-item nav-category">컨텐츠 관리</li>
                        <li class="nav-item">
                            <a href="<?=$static['url']['admin_contents_hire_infos']?>" class="nav-link">
                                <!-- <i class="link-icon" data-feather="box"></i> -->
                                <span class="link-title">채용 정보 관리</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$static['url']['admin_contents_events']?>" class="nav-link">
                                <!-- <i class="link-icon" data-feather="box"></i> -->
                                <span class="link-title">이벤트 관리</span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?=$static['url']['admin_contents_eventresults']?>" class="nav-link">
                                <!-- <i class="link-icon" data-feather="box"></i> -->
                                <span class="link-title">이벤트 결과 관리</span>
                            </a>
                        </li>

                        

                        <li class="nav-item">
                            <a href="<?=$static['url']['admin_contents_faqs']?>" class="nav-link">
                                <!-- <i class="link-icon" data-feather="box"></i> -->
                                <span class="link-title">FAQ 관리</span>
                            </a>
                        </li>
                    </li>



                    <li class="nav-item nav-category">게시판 관리</li>
                        <li class="nav-item">
                            <a href="<?=$static['url']['admin_boards_notices']?>" class="nav-link">
                                <!-- <i class="link-icon" data-feather="box"></i> -->
                                <span class="link-title">공지, 안내</span>
                            </a>
                        </li>
                        <!-- <li class="nav-item">
                            <a href="<?=$static['url']['admin_boards_informations']?>" class="nav-link">
                                <i class="link-icon" data-feather="box"></i>
                                <span class="link-title">안내</span>
                            </a>
                        </li> -->
                    </li>


                    <li class="nav-item nav-category">로그아웃</li>
                    <li class="nav-item">
                        <a href="<?=$static['url']
                        ['logout']?>" class="nav-link">
                            <!-- <i class="link-icon" data-feather="box"></i> -->
                            <span class="link-title">로그아웃</span>
                        </a>
                    </li>



                </ul>
            </div>
        </nav>
        <!-- partial -->

        <div class="page-wrapper">

            <!-- partial:partials/_navbar.html -->
            <nav class="navbar">
                <a href="#" class="sidebar-toggler">
                    <i data-feather="menu"></i>
                </a>
                <div class="navbar-content">
                    <!-- <form class="search-form"> -->
                    <!-- </form> -->
                </div>
            </nav>
            <!-- partial -->

            <div class="page-content">