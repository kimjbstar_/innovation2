<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!DOCTYPE html>


<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>관리자</title>



    <?php foreach ($static['assets'] as $val): ?>
    <?php $tmp = explode('.', $val); $last = explode('?', $tmp[count($tmp) - 1])[0];?>
    <?php if ($last === 'css'):?>
    <link rel="stylesheet" href="<?=$val?>">
    <?php endif;?>
    <?php if ($last === 'js'):?>
    <script src="<?=$val?>"></script>
    <?php endif;?>
    <?php endforeach;?>


    <link rel="shortcut icon" href="<?=$static['assets']['images/favicon.png']?>" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-167386364-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-167386364-3');
    </script>


    <!-- kokokokokokokokokoko -->


</head>



<body>


    <?php
        // if ($this->session->isAdmin<1) {
        //     goBackAlert('You are not admin', $this->config->site_url('index'));
        //     exit();
        // } else {
        // }
    ?>

    <div class="main-wrapper">


        <div class="page-wrapper" style="margin-left:0; width:100%;">


            <div class="page-content">