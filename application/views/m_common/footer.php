<div class="example">
    <!-- Button trigger modal -->
    <button type="button" id="domodal" style="display:none;" class="btn btn-primary" data-toggle="modal"
        data-target="#exampleModalLongScollable">
        Launch demo modal
    </button>
    <!-- Modal -->
    <div class="modal fade" id="exampleModalLongScollable" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalScrollableTitle" style="display: none; padding-right: 17px;" aria-modal="true">
        <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
            <div class="modal-content" style="overflow:scroll;">
            </div>
        </div>
    </div>
</div>

<script>
    async function getDBPopup(dbindex) {
        var dbinfo = await fetch('useDB/getSpecificDB?dbidx='+dbindex);
        console.log(dbinfo);
        dbinfo.text().then(getdb => {
            console.log(getdb);
            $('#exampleModalLongScollable > div > div ').html(getdb);
        })

        $('#domodal').click();
    }



    async function getHireInfoPopup(dbindex) {
        var dbinfo = await fetch('useDB/getHireInfoPopup?dbidx='+dbindex);
        console.log(dbinfo);
        dbinfo.text().then(getdb => {
            console.log(getdb);
            $('#exampleModalLongScollable > div > div ').html(getdb);
        })

        $('#domodal').click();
    }


    async function getNewHireInfoPopup(dbindex) {
        var dbinfo = await fetch('useDB/getNewHireInfo?dbidx='+dbindex);
        console.log(dbinfo);
        dbinfo.text().then(getdb => {
            console.log(getdb);
            $('#exampleModalLongScollable > div > div ').html(getdb);
        })

        $('#domodal').click();
    }








    async function getEventInfoPopup(dbindex) {
        var dbinfo = await fetch('useDB/getEventInfoPopup?dbidx='+dbindex);
        console.log(dbinfo);
        dbinfo.text().then(getdb => {
            console.log(getdb);
            $('#exampleModalLongScollable > div > div ').html(getdb);
        })

        $('#domodal').click();
    }


    async function getNewEventInfoPopup(dbindex) {
        var dbinfo = await fetch('useDB/getNewEventInfo?dbidx='+dbindex);
        console.log(dbinfo);
        dbinfo.text().then(getdb => {
            console.log(getdb);
            $('#exampleModalLongScollable > div > div ').html(getdb);
        })

        $('#domodal').click();
    }











    async function getEventResultInfoPopup(dbindex) {
        var dbinfo = await fetch('useDB/getEventResultInfoPopup?dbidx='+dbindex);
        console.log(dbinfo);
        dbinfo.text().then(getdb => {
            console.log(getdb);
            $('#exampleModalLongScollable > div > div ').html(getdb);
        })

        $('#domodal').click();
    }


    async function getNewEventResultInfoPopup(dbindex) {
        var dbinfo = await fetch('useDB/getNewEventResultInfo?dbidx='+dbindex);
        console.log(dbinfo);
        dbinfo.text().then(getdb => {
            console.log(getdb);
            $('#exampleModalLongScollable > div > div ').html(getdb);
        })

        $('#domodal').click();
    }












    async function getNewDBPopup() {
        var dbinfo = await fetch('useDB/getNewDB?dbidx=');
        console.log(dbinfo);
        dbinfo.text().then(getdb => {
            console.log(getdb);
            $('#exampleModalLongScollable > div > div ').html(getdb);
        })

        $('#domodal').click();
    }



    async function getNewNoticePopup() {
        var dbinfo = await fetch('useDB/getNewNotice?dbidx=');
        console.log(dbinfo);
        dbinfo.text().then(getdb => {
            console.log(getdb);
            $('#exampleModalLongScollable > div > div ').html(getdb);
        })

        $('#domodal').click();
    }


    async function getNewInformationPopup() {
        var dbinfo = await fetch('useDB/getNewInformation?dbidx=');
        console.log(dbinfo);
        dbinfo.text().then(getdb => {
            console.log(getdb);
            $('#exampleModalLongScollable > div > div ').html(getdb);
        })

        $('#domodal').click();
    }



    $('#exampleModalLongScollable').modal({
        keyboard: true,
        focus: true,
        show: false
    });






</script>




</body>

</html>