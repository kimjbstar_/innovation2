<nav class="page-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Manage</a></li>
						<li class="breadcrumb-item active" aria-current="page">Manage Sales</li>
					</ol>
				</nav>

				<div class="row">
					<div class="col-md-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <h6 class="card-title">Manage Sales</h6>
                <div class="table-responsive">
                  <table id="dataTableExampleis" class="table">
                    <thead>
                      <tr>
                        <th>셋팅 값 이름</th>
                        <th>현재 값</th>
                        <th>값 수정</th>
                      </tr>
                    </thead>
                    <tbody>
<?php


foreach ($data as $value) {
    // var_dump($value); ?>                                        

                      <tr>
                        <td><?=$value['value_description']; ?></td>
                        <td><?=$value['setting_value']; ?></td>
                        
                        <td><button type="button" class="btn btn-primary" data-toggle="modal" onclick="get_setting_value('<?=$value['setting_index']?>');" data-target="#exampleModal">View</button></td>
                      </tr>    
<?php
} ?>        

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
					</div>
				</div>

			</div>
            <!-- Modal -->
            <div class="modal fade bd-example-modal-xl" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog  modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">셋팅값 수정</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        
                        <form action="<?=$action['updateSettingValue']?>" method="post">
                    <input type="hidden"  value="<?=$this->security->get_csrf_hash();?>" name="<?=$this->security->get_csrf_token_name()?>">	
                            <div class="modal-body">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="dataTableExample" class="table">
                                                <thead>
                                                    <tr>
                                                        <th>셋팅 값 이름</th>
                                                        <th>값</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                

                                                    <tr>
                                                        <td>v9을 졸업한 후에 붙을 인덱스 설정</td>
                                                        <td><input type="number" step="0.0001" name="setting_value" value=""></td>
                                                        <input type="hidden"  name="setting_index" value="">
                                                    </tr>    


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-warning">저장</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
	
		</div>
	</div>


<script>
    function get_setting_value(get_index){
        fetchlink=`<?=$action['get_setting_value'];?>?indexnumber=${get_index}`;
        console.log(fetchlink);

        fetch(fetchlink)
        .then(response => response.json())
        .then(json => {
            console.log(json.answer);
            $('#dataTableExample > tbody').html(json.answer)            
        })
        .catch(err => console.log(err));

        // return fetch(fetchlink)
        // .then(response => {
        //     console.log();
        //     if (!response.ok) {
        //         throw new Error(response)
        //     }
        //     console.log(response);
        //     console.log(response.json());
        // }).then(json=>{
        //     console.log(json);
        // })
        // .catch(error => {
        //     Swal.showValidationMessage(
        //     `Request failed: ${error}`
        //     )
        // })
    }
</script>


