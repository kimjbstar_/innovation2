<script>
    var table;
    $(function () {
        table = $('#dataTableExampleis').DataTable({
            "aLengthMenu": [
                [10, 30, 50, -1],
                [10, 30, 50, "All"]
            ],
            "iDisplayLength": 10,
            "language": {
                search: ""
            }
        });
        $('#dataTableExampleis').each(function () {
            var datatable = $(this);
            // SEARCH - Add the placeholder for Search and Turn this into in-line form control
            var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
            search_input.addClass('search-input');
            search_input.attr('placeholder', '검색');
            search_input.removeClass('form-control-sm');
            // LENGTH - Inline-Form control
            var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
            length_sel.removeClass('form-control-sm');
        });
    });


    function fnExcelReport(action) {
        var now = new Date();
        var year = now.getFullYear();
        var month = now.getMonth()+1;
        var title = ""
        //기프티콘_2020년6월.xls
        if(action === '알림톡'){
            $('.search-input').val('알림톡');
            title = action+'_'+year+'년'+month+'월.xls'
        }
        else if(action === '기프티콘'){
            $('.search-input').val('기프티콘');
            title = action+'_'+year+'년'+month+'월.xls'
        }else{
            title = '전체_'+year+'년'+month+'월.xls'
        }
        $('#dataTableExampleis').DataTable().search( $('.search-input').val(), false, true ).draw(); 
        // return false;

		var tab_text = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
		tab_text = tab_text
				+ '<head><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8">';
		tab_text = tab_text
				+ '<xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>'
		tab_text = tab_text + '<x:Name>Test Sheet</x:Name>';
		tab_text = tab_text
				+ '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
		tab_text = tab_text
				+ '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';
		tab_text = tab_text + "<table border='1px'>";
		var exportTable = $('#dataTableExampleis').clone();
		exportTable.find('input').each(function(index, elem) {
			$(elem).remove();
		});
		tab_text = tab_text + exportTable.html();
		tab_text = tab_text + '</table></body></html>';
		var data_type = 'data:application/vnd.ms-excel';
		var ua = window.navigator.userAgent;
		var msie = ua.indexOf("MSIE ");
		var fileName = title;
		//Explorer 환경에서 다운로드
		if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
			if (window.navigator.msSaveBlob) {
				var blob = new Blob([ tab_text ], {
					type : "application/csv;charset=utf-8;"
				});
				navigator.msSaveBlob(blob, fileName);
			}
		} else {
			var blob2 = new Blob([ tab_text ], {
				type : "application/csv;charset=utf-8;"
			});
			var filename = fileName;
			var elem = window.document.createElement('a');
			elem.href = window.URL.createObjectURL(blob2);
			elem.download = filename;
			document.body.appendChild(elem);
			elem.click();
			document.body.removeChild(elem);
        }
        $('.search-input').val('');
        $('#dataTableExampleis').DataTable().search( $('.search-input').val(), false, true ).draw(); 
	}
</script>



<nav class="page-breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">관리</a></li>
        <li class="breadcrumb-item active" aria-current="page">마일리지 관리</li>
    </ol>
</nav>

<div class="row">

    

    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h6 class="card-title">마일리지 사용 내역</h6>
                <button class="btn btn-primary float-right mb-3 mr-1" onclick="fnExcelReport('')">전체 다운로드</button>
                <!-- <button class="btn btn-primary float-right mb-3 mr-1" onclick="fnExcelReport('받은 마일리지')">받은 마일리지 내역 다운로드</button> -->
                <!-- <button class="btn btn-primary float-right mb-3 mr-1" onclick="fnExcelReport('사용 마일리지')">사용 마일리지 내역 다운로드</button> -->
                

                <div class="table-responsive">
                    <table id="dataTableExampleis" class="table">
                        <thead>
                            <tr>
                                <th>
                                    아이디
                                </th>
                                <th>
                                    구분
                                </th>
                                <th>
                                    마일리지
                                </th>
                                <th>
                                    날짜
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

foreach ($data["mileage"] as $value) {
    ?>

                            <tr>
                                <td>
                                  <?= $value["member_id"]?>
                                </td>
                                <td>
                                <?php 
                                
                                if($value["used"] == "used"){
                                    echo "사용마일리지";
                                }else{
                                    echo "받은마일리지";
                                }
                                
                                
                                ?>
                                </td>
                                <td>
                                <?= $value["mileage"]?>
                                </td>
                                <td>
                                <?= $value["update_date"]?>
                                </td>


                            </tr>
                            <?php
} ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    

    $(function(){
        // setTimeout("history.go(0);", 30000); // 1초는 1000 입니다.
        
    })
</script>
