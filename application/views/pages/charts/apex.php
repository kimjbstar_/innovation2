				<div class="row">
					<div class="col-md-12 grid-margin">
						<div class="card">
							<div class="card-body">
								<h6 class="card-title">Apex Charts</h6>
								<p>Modern & Interactive Open-source Charts. Read the <a href="https://apexcharts.com/" target="_blank"> Official ApexChart Documentation</a> for a full list of instructions and other options.</p>
							</div>
						</div>
					</div>
				</div>

        <div class="row">
					<div class="col-xl-6 grid-margin stretch-card">
						<div class="card">
							<div class="card-body">
								<h6 class="card-title">Line chart</h6>
                <div id="apexLine"></div>
							</div>
						</div>
					</div>
					<div class="col-xl-6 grid-margin stretch-card">
						<div class="card">
							<div class="card-body">
								<h6 class="card-title">Bar chart</h6>
                <div id="apexBar"></div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xl-6 grid-margin stretch-card">
						<div class="card">
							<div class="card-body">
								<h6 class="card-title">Area chart</h6>
                <div id="apexArea"></div>
							</div>
						</div>
					</div>
					<div class="col-xl-6 grid-margin stretch-card">
						<div class="card">
							<div class="card-body">
								<h6 class="card-title">Mixed chart</h6>
                <div id="apexMixed"></div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xl-6 grid-margin stretch-card">
						<div class="card">
							<div class="card-body">
								<h6 class="card-title">Donut chart</h6>
                <div id="apexDonut"></div>
							</div>
						</div>
					</div>
					<div class="col-xl-6 grid-margin stretch-card">
						<div class="card">
							<div class="card-body">
								<h6 class="card-title">Pie chart</h6>
                <div id="apexPie"></div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xl-6 grid-margin stretch-card">
						<div class="card">
							<div class="card-body">
								<h6 class="card-title">HeatMap chart</h6>
                <div id="apexHeatMap"></div>
							</div>
						</div>
					</div>
					<div class="col-xl-6 grid-margin stretch-card">
						<div class="card">
							<div class="card-body">
								<h6 class="card-title">Radar chart</h6>
                <div id="apexRadar"></div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xl-6 grid-margin grid-margin-xl-0 stretch-card">
						<div class="card">
							<div class="card-body">
								<h6 class="card-title">Scatter chart</h6>
                <div id="apexScatter"></div>
							</div>
						</div>
					</div>
					<div class="col-xl-6 stretch-card">
						<div class="card">
							<div class="card-body">
								<h6 class="card-title">RadialBar chart</h6>
                <div id="apexRadialBar"></div>
							</div>
						</div>
					</div>
        </div>

			</div>

			<!-- partial:../../partials/_footer.html -->
			<footer class="footer d-flex flex-column flex-md-row align-items-center justify-content-between">
				<p class="text-muted text-center text-md-left">Copyright © 2019 <a href="https://www.nobleui.com" target="_blank">NobleUI</a>. All rights reserved</p>
				<p class="text-muted text-center text-md-left mb-0 d-none d-md-block">Handcrafted With <i class="mb-1 text-primary ml-1 icon-small" data-feather="heart"></i></p>
			</footer>
			<!-- partial -->
	
		</div>
	</div>


