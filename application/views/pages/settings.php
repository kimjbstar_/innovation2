<nav class="page-breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">관리</a></li>
        <li class="breadcrumb-item active" aria-current="page">설정</li>
    </ol>
</nav>

<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h6 class="card-title">기프티콘 지급 설정</h6>
                <form>
                    <div class="form-group">
                        <style>
                            #gifticon_count{
                                width: 80px;
                                margin-left: 2rem;
                            }
                        </style>
                        <!-- default value 10
                    1회누적, 아메리카노
                    3회누적, 아메리카노
                    150000원누적, 아메리카노    - 기능만 개발
                    
                    -->
                        <input type="number" class="form-control d-inline text-right" id="gifticon_count" value="10">
                        <span class="d-inline">회 당 기프티콘 발송</span>
                    </div>
                </form>
                    <hr class="my-5">

                <h6 class="card-title">알림톡 발송 설정</h6>
                <form>
                    <div class="form-group">
                        <!-- <div class="form-check">
                            <label class="form-check-label">
                                <input type="checkbox" checked="" class="form-check-input">
                                Checked
                                <i class="input-frame"></i></label>
                        </div> -->
                        <div class="form-check">
                            <label class="form-check-label">
                                <label class="sr-only">9번 배차처리 주문자에게</label>
                                <input type="checkbox" class="form-check-input">배차처리 주문자에게<i
                                    class="input-frame"></i></label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <label class="sr-only">12번 배차처리 상대방에게</label>
                                <input type="checkbox" class="form-check-input">배차처리 상대방에게<i
                                    class="input-frame"></i></label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <label class="sr-only">15번 배송완료 기존고객</label>
                                <input type="checkbox" class="form-check-input">배송완료 기존고객<i
                                    class="input-frame"></i></label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <label class="sr-only">18번 선물 보내줄 때</label>
                                <input type="checkbox" class="form-check-input">선물 보내줄 때<i
                                    class="input-frame"></i></label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <label class="sr-only">21번 배송완료 미고객</label>
                                <input type="checkbox" class="form-check-input">배송완료 미고객<i
                                    class="input-frame"></i></label>
                        </div>
                        <div class="form-group text-right">
                            <button type="button" class="btn btn-primary">적용</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>