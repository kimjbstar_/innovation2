
<nav class="page-breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">관리</a></li>
        <li class="breadcrumb-item"><a href="#">설정</a></li>
        <li class="breadcrumb-item active" aria-current="page">기프티콘 지급 설정</li>
    </ol>
</nav>

<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">


                <h6 class="card-title">기프티콘 지급 설정</h6>
                <div class="table-responsive">

                    <?php
                    //var_dump($data['check']);
                    ?>
                    <div id='TextBoxesGroup'>

                        <?php

                     $condition = $data['check'][0]['condition'];
                        // $condition = null;
                     $condition_explode = explode('/', $condition);
                    $count = 1;
                     foreach ($condition_explode as $value) {
                         ?>


                        <div id="TextBoxDiv<?=$count?>" class="row my-2">
                            <!-- <label class="col-2">설정 : </label> -->
                            <input type='textbox' id='textbox<?=$count?>' value="<?=$value?>" class="form-control col-2 text-right"><span class='p-2 mr-5'>회</span>
                            <input type='button' value='삭제' seq="<?=$count?>" class="removeButton btn btn-danger mx-2">
                        </div>
                        <?php
                        $count ++;
                     }
                        ?>
                        <!-- 
                        <div id="TextBoxDiv2" class="row">
                            <label class="col-2">설정 2 : </label><input type='textbox' id='textbox1' value="1" class="form-control col-2">
                            <input type='button' value='삭제' id='removeButton' class="btn btn-danger m-2">
                        </div> -->

                        <!-- <div id="TextBoxDiv1">
                <label>Textbox #1 : </label><input type='textbox' id='textbox1' value="1">
            </div> -->
                    </div>
                    <input type='button' value='추가' id='addButton' class="btn btn-primary">
                    <!-- <input type='button' value='Remove Button' id='removeButton'> -->
                        <br>

                    <input type='button' value='적용' id='getButtonValue' class="btn btn-primary my-5 float-right">

                    
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        var counter = <?= $count ?>;
        $("#addButton").click(function () {
            if (counter > 20) {
                alert("20개 이상 생성할 수 없습니다.");
                return false;
            }
            var newTextBoxDiv = $(document.createElement('div'))
                .attr("id", 'TextBoxDiv' + counter).attr("class", 'row my-2');
            newTextBoxDiv.after().html(
                '<input type="text" name="textbox' + counter +
                '" id="textbox' + counter +
                '" value="" class=\'form-control text-right col-2\'><span class=\'p-2 mr-5\'>회</span> <input type=\'button\' value=\'삭제\' seq=\''+ counter +'\' class=\'removeButton btn btn-danger mx-2\'>'
                );
            newTextBoxDiv.appendTo("#TextBoxesGroup");
            counter++;
        });

        $(".removeButton").on('click', function (e) {
            console.log("click", e);
            if (counter == 1) {
                alert("삭제할 설정이 없습니다.");
                return false;
            }
            seq = $(e.target).attr("seq");
            console.log($(e.target));
            //counter--;
            $("#TextBoxDiv" + seq).remove();
        });
        $("#getButtonValue").click(function () {
            var msg = '';
            for (i = 1; i < counter; i++) {
                if(typeof $('#textbox' + i).val() === 'undefined'){
                    continue;
                }
                if($('#textbox' + i).val() === ''){
                    continue;
                }
                msg += '/' + $('#textbox' + i).val();
            }
            console.log(msg.substring(1));
            msg = msg.substring(1);

            //   var str = "Hello world!";
            //   var res = str.substring(1)
            //   ello world!
            // fetch('https://quickcar.co.kr/Order/getprice?kind='+vehicle+'&doc=1&start='+start_adr+'&end='+dest_adr+'&count_p='+count_p+'&etc_p='+etc_p+'&memo='+memo+'&product='+product)
            // .then(function(response) {
            //     return response.json();
            // })
            // .then(function(myJson) {
            //     console.log(myJson['price']);
            //     console.log(myJson['add_price']);

            // return false; 



            fetchlink = `/pages/gifticon_setting/save?c=${msg}`;
            console.log(fetchlink);

            fetch(fetchlink)
            .then(response => response.json())
            .then(json => {
                console.log(json.answer);
                alert("적용되었습니다.");
            })
            .catch(err => console.log(err));
            // ajax 통신으로 설정 적용
        });
    });
</script>
