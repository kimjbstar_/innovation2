<script>
    var table;
$.fn.datepicker.dates['en'] = {
    "days": ["일", "월", "화", "수", "목", "금", "토"],
    "daysShort": ["일", "월", "화", "수", "목", "금", "토"],
    "daysMin": ["일", "월", "화", "수", "목", "금", "토"],
    "months": ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
    "monthsShort": ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
    "today": "오는",
    "clear": "닫기",
    "titleFormat": "yyyy-mm"
}
    $(function () {
        table = $('#dataTableExampleis1').DataTable({
            "aLengthMenu": [
                [10, 30, 50, -1],
                [10, 30, 50, "All"]
            ],
            "iDisplayLength": 10,
            "language": {
                search: ""
            }
        });
        const $endVal = $('#startDate');
        const $startVal = $('#endDate');


        
            $startVal.datepicker({
                changeMonth: true,
                changeYear: true,
                autoclose: true,
            }).on('hide', function(hideData) {
                console.log('asdf');
                $('#searchByDate').click();
            });
            
            $endVal.datepicker({
                changeMonth: true,
                changeYear: true,
                autoclose: true,
            });

        $('#dataTableExampleis1').each(function () {
            var datatable = $(this);
            // SEARCH - Add the placeholder for Search and Turn this into in-line form control
            var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
            search_input.addClass('search-input');
            search_input.attr('placeholder', '검색');
            search_input.removeClass('form-control-sm');
            // LENGTH - Inline-Form control
            var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
            length_sel.removeClass('form-control-sm');
        });


    });

    function searchByDate() {
        const $endVal = $('#startDate');
        const $startVal = $('#endDate');

        console.log(


            $startVal.val(),
            $endVal.val(),
        );
        if ($startVal.val() && $endVal.val()) {
            
            location.replace(`${location.origin}/Pages/charges?start=${$('#startDate').val()}&end=${$('#endDate').val()}`);
        }
    }


    function fnExcelReport(action) {
        var now = new Date();
        var year = now.getFullYear();
        var month = now.getMonth()+1;
        var title = ""
        //기프티콘_2020년6월.xls
        if(action === '알림톡'){
            $('.search-input').val('알림톡');
            title = action+'_'+year+'년'+month+'월.xls'
        }
        else if(action === '기프티콘'){
            $('.search-input').val('기프티콘');
            title = action+'_'+year+'년'+month+'월.xls'
        }else{
            title = '전체_'+year+'년'+month+'월.xls'
        }
        $('#dataTableExampleis1').DataTable().search( $('.search-input').val(), false, true ).draw(); 
        // return false;

		var tab_text = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
		tab_text = tab_text
				+ '<head><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8">';
		tab_text = tab_text
				+ '<xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>'
		tab_text = tab_text + '<x:Name>Test Sheet</x:Name>';
		tab_text = tab_text
				+ '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
		tab_text = tab_text
				+ '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';
		tab_text = tab_text + "<table border='1px'>";
		var exportTable = $('#dataTableExampleis1').clone();
		exportTable.find('input').each(function(index, elem) {
			$(elem).remove();
		});
		tab_text = tab_text + exportTable.html();
		tab_text = tab_text + '</table></body></html>';
		var data_type = 'data:application/vnd.ms-excel';
		var ua = window.navigator.userAgent;
		var msie = ua.indexOf("MSIE ");
		var fileName = title;
		//Explorer 환경에서 다운로드
		if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
			if (window.navigator.msSaveBlob) {
				var blob = new Blob([ tab_text ], {
					type : "application/csv;charset=utf-8;"
				});
				navigator.msSaveBlob(blob, fileName);
			}
		} else {
			var blob2 = new Blob([ tab_text ], {
				type : "application/csv;charset=utf-8;"
			});
			var filename = fileName;
			var elem = window.document.createElement('a');
			elem.href = window.URL.createObjectURL(blob2);
			elem.download = filename;
			document.body.appendChild(elem);
			elem.click();
			document.body.removeChild(elem);
        }
        $('.search-input').val('');
        $('#dataTableExampleis1').DataTable().search( $('.search-input').val(), false, true ).draw(); 
	}
</script>



<nav class="page-breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">관리</a></li>
        <li class="breadcrumb-item active" aria-current="page">충전금 관리</li>
    </ol>
</nav>

<div class="row">

    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h6 class="card-title">충전금 현황
                    <span class="float-right">
                        잔액 : 
                        <span id="balance"></span>
                        <!-- //$data["charge"][0]["charge"] -->
                    </span>

                </h6>
                <style>
                    .progress{
                        background-color: #a6a6a6;
                    }
                </style>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25"
                        aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h6 class="card-title">충전금 사용 내역</h6>
                <button class="btn btn-primary float-right mb-3 mr-1" onclick="fnExcelReport('')">전체 다운로드</button>
                <button class="btn btn-primary float-right mb-3 mr-1" onclick="fnExcelReport('기프티콘')">기프티콘 내역 다운로드</button>
                <button class="btn btn-primary float-right mb-3 mr-1" onclick="fnExcelReport('알림톡')">알림톡 내역 다운로드</button>
                <button id="searchByDate" class="btn btn-primary float-right mb-3 mr-1" onclick="searchByDate()">기간별 검색</button>
                <input id="endDate" class="float-right mb-3 mr-1">
                <input id="startDate" class="float-right mb-3 mr-1">

                

                <div class="table-responsive">
                    <table id="dataTableExampleis1" class="table">
                        <thead>
                            <tr>
                                <th>
                                    발송 시점
                                </th>
                                <th>
                                    발송 품목

                                </th>
                                <th>
                                    회원 아이디
                                </th>
                                <th>
                                    사용 금액
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $sum = 0; foreach ($data["all"] as $value) { ?>

                            <tr>
                                <td>
                                    <?= $value["update_date"]?>
                                </td>
                                <td>
                                    <?php if ($value["state"] == "gifticon") {
                                            echo "기프티콘";
                                        //기프티콘 발급 내역 (이디야 아메리카노 3200원 *1 - 부가세 포함)
                                        } else {
                                            echo "알림톡";
                                            //알림톡 발송 내역 (발송 건당 7.5원 * 1.1 - 부가세 별도)
                                        } ?>
                                </td>
                                
                                <td>
                                <?= $value["member_id"]?>
                                </td>
                                <td>
                                    <?php
                                      if ($value["state"] == "gifticon") {
                                          //echo "기프티콘";
                                          //기프티콘 발급 내역 (이디야 아메리카노 3200원 *1 - 부가세 포함)
                                          echo(3200 * 1.1) . "원";
                                          $sum += (3200 * 1.1);
                                      } else {
                                          //echo "알림톡";
                                          //알림톡 발송 내역 (발송 건당 7.5원 * 1.1 - 부가세 별도)
                                          echo(7.5) . "원";
                                          $sum += (7.5);
                                      } ?>
                                </td>


                            </tr>
                            <?php
} ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function AddComma(num)
    {
        var regexp = /\B(?=(\d{3})+(?!\d))/g;
        return num.toString().replace(regexp, ',');
    }

    $(function(){

        
        setTimeout("history.go(0);", 30000); // 1초는 1000 입니다.


        var charge =  <?= $data["charge"][0]["charge"]?>;
        
        var sum = <?= $sum ?>;
        var balance = charge - sum;
        var ratio =  ( balance / charge ) * 100;
        ratio = ratio.toFixed(2);
        
        console.log(charge, sum, ratio);
        $("#balance").html(AddComma(balance) + "원");
        $(".progress").html(`
             <div class="progress-bar" role="progressbar" style="width: ${ratio}%;" aria-valuenow="${ratio}"
            aria-valuemin="0" aria-valuemax="100">${AddComma(balance)}원 (${ratio}%)</div>
        `);
    })
</script>
<!-- <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25"
                        aria-valuemin="0" aria-valuemax="100">250,000</div>
                </div> -->