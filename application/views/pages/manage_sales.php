<nav class="page-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Manage</a></li>
						<li class="breadcrumb-item active" aria-current="page">Manage Sales</li>
					</ol>
				</nav>

				<div class="row">
					<div class="col-md-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <h6 class="card-title">Manage Sales</h6>
                <div class="table-responsive">
                  <table id="dataTableExampleis" class="table">
                    <thead>
                      <tr>
                        <th>Sales indexnumber</th>
                        <th>User ID</th>
                        <th>VIA</th>
                        <th>How Much</th>
                        <th>Sales date</th>
                        <th>View transactions</th>
                      </tr>
                    </thead>
                    <tbody>
<?php


foreach ($data as $value) {
    // var_dump($value); ?>                                        

                      <tr>
                        <td><?=$value['own_index']; ?></td>
                        <td><?=getIdByUserIdx($value['own_last_owner'])->raw; ?></td>
                        <td><?php if ($value['sale_index']=="1") {
        echo 'ETH';
    } ?></td>
                        <td><?=$value['own_last_amount']; ?></td>
                        <td><?=$value['own_last']; ?></td>
                        <td><button type="button" class="btn btn-primary" data-toggle="modal" onclick="get_transactions('<?=$value['own_index']?>');" data-target="#exampleModal">View</button></td>
                      </tr>    
<?php
} ?>        

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
					</div>
				</div>

			</div>
            <!-- Modal -->
            <div class="modal fade bd-example-modal-xl" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog  modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Transactions in sale</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="dataTableExample" class="table">
                                            <thead>
                                                <tr>
                                                    <th>TRANSACTION</th>
                                                    <th>TO USER</th>
                                                    <th>HOW MUCH</th>
                                                    <th>AS WHAt</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                            

                                                <tr>
                                                    <td>0X0IJEFI89039923J8J390089JWDDHCH</td>
                                                    <td>admin@admin.com</td>
                                                    <td>0.0002</td>
                                                    <td>gas</td>
                                                </tr>    


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>












                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
	
		</div>
	</div>


<script>
    function get_transactions(get_index){
        fetchlink=`<?=$action['get_transaction'];?>?indexnumber=${get_index}`;
        console.log(fetchlink);

        fetch(fetchlink)
        .then(response => response.json())
        .then(json => {
            console.log(json.answer);
            $('#dataTableExample > tbody').html(json.answer)            
        })
        .catch(err => console.log(err));

        // return fetch(fetchlink)
        // .then(response => {
        //     console.log();
        //     if (!response.ok) {
        //         throw new Error(response)
        //     }
        //     console.log(response);
        //     console.log(response.json());
        // }).then(json=>{
        //     console.log(json);
        // })
        // .catch(error => {
        //     Swal.showValidationMessage(
        //     `Request failed: ${error}`
        //     )
        // })
    }
</script>


