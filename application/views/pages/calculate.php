<script>
    $(function () {
        $('#dataTableExampleis').DataTable({
            "aLengthMenu": [
                [10, 30, 50, -1],
                [10, 30, 50, "All"]
            ],
            "iDisplayLength": 10,
            "language": {
                search: ""
            }
        });
        $('#dataTableExampleis').each(function () {
            var datatable = $(this);
            // SEARCH - Add the placeholder for Search and Turn this into in-line form control
            var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
            search_input.attr('placeholder', '검색');
            search_input.removeClass('form-control-sm');
            // LENGTH - Inline-Form control
            var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
            length_sel.removeClass('form-control-sm');
        });


    });

    function excel_download() {
        // 엑셀 다운로드 소스
        var htmltable = document.getElementById('dataTableExampleis');
        var html = htmltable.outerHTML;
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));

        // var oWin = window.open("data:application/vnd.ms-excel", "_blank");
 
        // oWin.document.write(cssText);
        // oWin.document.write(html);
        // oWin.document.close();
        // success = true, false
        // var success = oWin.document.execCommand('SaveAs', false, 'title.xls');
        // oWin.close();


        // $(".custom-select").change(function(){
        //     var value =  $(this).val();
        // })
    }




//[1:43 PM] 기프티콘 내역리스트 , 알림톡 내역리스트를 계정별로 별도 다운로드   기프티콘_2020년6월.xls (edited)
    function fnExcelReport() {
		var tab_text = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
		tab_text = tab_text
				+ '<head><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8">';
		tab_text = tab_text
				+ '<xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>'
		tab_text = tab_text + '<x:Name>Test Sheet</x:Name>';
		tab_text = tab_text
				+ '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
		tab_text = tab_text
				+ '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';
		tab_text = tab_text + "<table border='1px'>";
		var exportTable = $('#dataTableExampleis').clone();
		exportTable.find('input').each(function(index, elem) {
			$(elem).remove();
		});
		tab_text = tab_text + exportTable.html();
		tab_text = tab_text + '</table></body></html>';
		var data_type = 'data:application/vnd.ms-excel';
		var ua = window.navigator.userAgent;
		var msie = ua.indexOf("MSIE ");
		var fileName = '퀵정산관리.xls';
		//Explorer 환경에서 다운로드
		if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
			if (window.navigator.msSaveBlob) {
				var blob = new Blob([ tab_text ], {
					type : "application/csv;charset=utf-8;"
				});
				navigator.msSaveBlob(blob, fileName);
			}
		} else {
			var blob2 = new Blob([ tab_text ], {
				type : "application/csv;charset=utf-8;"
			});
			var filename = fileName;
			var elem = window.document.createElement('a');
			elem.href = window.URL.createObjectURL(blob2);
			elem.download = filename;
			document.body.appendChild(elem);
			elem.click();
			document.body.removeChild(elem);
		}
	}
</script>

<nav class="page-breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">관리</a></li>
        <li class="breadcrumb-item active" aria-current="page">퀵 정산 관리</li>
    </ol>
</nav>
<!-- 
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h6 class="card-title">검색</h6>
                <form>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Test</label>
                                <input type="text" class="form-control col-md-8">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Test</label>
                                <input type="text" class="form-control col-md-8">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Test</label>
                                <input type="text" class="form-control col-md-8">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Test</label>
                                <input type="text" class="form-control col-md-8">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Test</label>
                                <input type="text" class="form-control col-md-8">
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div> -->


<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h6 class="card-title">퀵 정산 관리</h6>
                <button class="btn btn-primary float-right mb-3" onclick="fnExcelReport()">검색 결과 엑셀 다운로드</button>
                <div class="table-responsive">
                    <table id="dataTableExampleis" class="table">
                        <thead>
                            <tr>
                                <th>주문번호</th>
                                <th>상태</th>
                                <th>배송수단</th>
                                <th>지급방법</th>
                                <th>배송방법</th>
                                <th>배송선택</th>
                                <th>요청 시간</th>
                                <th>주문자 연락처</th>
                                <th>카드정보</th>
                                <th>출발지</th>
                                <th>출발지 성함</th>
                                <th>출발지 연락처</th>
                                <th>도착지</th>
                                <th>도착지 성함</th>
                                <th>도착지 연락처</th>
                                <th>메모</th>
                                <th>금액</th>
                                <th>담당기사</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php


foreach ($data['calculate'] as $value) {
    // var_dump($value);
    $state_class = 'text-secondary';
    $deliverType = "";
    $payType = "";
    $doc = "";
    $sfast = "";
    //         'deliverType' => $this->session->kind,     //  kind	 배송수단(1:오토, 2:다마스, 5:라보, 4:밴, 6:지하철, 3:트럭)	 필수항목
    switch ($value['deliverType']) {
        case "1":
            $deliverType = '오토바이';
        break;
        case "2":
            $deliverType = '다마스';
        break;
        case "3":
            $deliverType = '트럭';
        break;
        case "4":
            $deliverType = '밴';
        break;
        case "5":
            $deliverType = '라보';
        break;
        case "6":
            $deliverType = '지하철';
        break;
    }
    //         'payType' => trim($this->input->get('payType', true)),      //지급방법(1:선불, 2:착불, 3:신용, 4:송금, 5:수금)	 필수항목
    switch ($value['payType']) {
        case "1":
            $payType = '선불';
        break;
        case "2":
            $payType = '착불';
        break;
        case "3":
            $payType = '신용';
        break;
        case "4":
            $payType = '카드';
        break;
        case "5":
            $payType = '수금';
        break;
    }
    // 왕복 , 경유, 긴급 3
    //         'doc' => trim($this->input->get('doc', true)),              //  doc	 배송방법(1:편도, 3:왕복, 5:경유)	 필수항목
    switch ($value['doc']) {
        case "1":
            $doc = '편도';
        break;
        case "3":
            $doc = '왕복';
        break;
        case "5":
            $doc = '경우';
        break;
    }
    //         'sfast' => trim($this->input->get('sfast', true)),          //  sfast	 배송선택(1:일반, 3:급송, 5:조조, 7:야간)	 필수항목
    switch ($value['sfast']) {
        case "1":
            $sfast = '일반';
        break;
        case "3":
            $sfast = '급송';
        break;
        case "5":
            $sfast = '조조';
        break;
        case "7":
            $sfast = '야간';
        break;
    } ?>

                            <tr>
                                <td><?=$value['orderNum']; ?></td>
                                <td>
                                    <span class="<?=$state_class?>"><?=$value['state']; ?> </span>
                                </td>
                                <td><?=$deliverType?></td>
                                <td><?=$payType?></td>
                                <td><?=$doc?></td>
                                <td><?=$sfast?></td>
                                <td><?=$value['order_datetime']; ?></td>
                                <td><?=$value['orderMobile']; ?></td>
                                <td><?=$value['card_number']; ?></td>
                                <td><?=$value['startLocal']; ?></td>
                                <td><?=$value['StartName']; ?></td>
                                <td><?=$value['startTelNo']; ?></td>
                                <td><?=$value['destLocal']; ?></td>
                                <td><?=$value['destName']; ?></td>
                                <td><?=$value['destTelNo']; ?></td>

                                <td><?=$value['memo']; ?></td>
                                <td><?=$value['price']; ?></td>
                                <td><?=$value['rider_name']; ?> ( <?=$value['rider_tel_number']; ?> )</td>
                            </tr>
                            <?php
} ?>

                        </tbody>
                    </table>
                </div>
                <!-- 


            완료 인 경우만 골라서 보여주기 
            <i data-feather="alert-octagon"></i> 
                준비중입니다. -->
                <!-- 월별 정산
            회원별 정산 
            결제방식에 따른 정산 - 선불, 착불, 카드 


            기존 정산 다운로드 한 적 있음/없음/전체


            탭 형식 ?  버튼으로 일단 구현? 
            
            
            
            
            
            
            
            
            
            
            
            
            -->
            </div>
        </div>
    </div>
</div>