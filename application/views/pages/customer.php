<!-- 필수, SheetJS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.14.3/xlsx.full.min.js"></script>
<!--필수, FileSaver savaAs 이용 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.8/FileSaver.min.js"></script>
<script>
    var table;
    $(function () {
        table = $('#dataTableExampleis').DataTable({
            "aLengthMenu": [
                [10, 30, 50, -1],
                [10, 30, 50, "All"]
            ],
            "iDisplayLength": 10,
            "language": {
                search: ""
            }
        });
        $('#dataTableExampleis').each(function () {
            var datatable = $(this);
            // SEARCH - Add the placeholder for Search and Turn this into in-line form control
            var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
            search_input.attr('placeholder', '검색');
            search_input.removeClass('form-control-sm');
            // LENGTH - Inline-Form control
            var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
            length_sel.removeClass('form-control-sm');
        });
       

        // 엑셀 다운로드 소스
    //     var htmltable= document.getElementById('dataTableExampleis');
    //    var html = htmltable.outerHTML;
    //    window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
     // $(".custom-select").change(function(){
        //     var value =  $(this).val();
        // })
    });
</script>





<nav class="page-breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">관리</a></li>
        <li class="breadcrumb-item active" aria-current="page">회원관리</li>
    </ol>
</nav>

<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h6 class="card-title">회원관리</h6>
                <div class="table-responsive">
                    <table id="dataTableExampleis" class="table">

                        <!-- - 
1 아이디,
2- 가입 시점,
3- 주소,
4- 관리자 부여/회수 버튼,
5- 신용 부여 버튼(members 테이블의 credit_pay가 1 이면 신용 발송 가능)
6- "완료" state의 발송 횟수 ("발송 완료")
7- 기프티콘 지급 횟수
8- 알림톡 발송 횟수
 -->

                        <thead>
                            <tr>

                                <th>
                                    아이디
                                </th>
                                <th>
                                    마일리지
                                </th>
                                <th>
                                    가입시점

                                </th>
                                <th>
                                    주소

                                </th>
                                <th>
                                    관리자 부여/회수

                                </th>
                                <th>
                                    신용 부여

                                </th>
                                <th>
                                    발송 횟수

                                </th>
                                <!-- <th>
                                    기프티콘 지급 횟수

                                </th> -->
                                <th>
                                    알림톡 발송 횟수

                                </th>

                                <!-- <th>User Index</th>
                                <th>User ID</th>
                                <th>가입일</th>
                                <th>주문내역</th>
                                <th>Detail</th>
                                <th>비밀번호 초기화</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php


foreach ($data as $value) {
    // var_dump($value); ?>

                            <tr>
                                <!-- <td><?=$value['idx']; ?></td> -->
                                <td>
                                    <!-- <label class="sr-only">아이디</label> -->
                                    <?=$value['member_id']; ?>
                                </td>
                                <td>
                                    <!-- <label class="sr-only">아이디</label> -->
                                    <?=$value['mileage']; ?>
                                </td>
                                <td>
                                    <!-- <label class="sr-only">가입시점</label> -->
                                    <?=$value['member_jointime']; ?>
                                </td>
                                <td>
                                <!-- <label class="sr-only">주소</label> -->
                                </td>
                                <td>
                                <!-- <label class="sr-only">관리자 부여/회수</label> -->
                                <div class="btn-group btn-group-sm" role="group">
                                    <?php
                                    $is_admin1 = 'btn-secondary';
    $is_admin2 = 'btn-secondary';
    if ($value['is_admin'] == 1) {
        $is_admin1 = 'btn-primary active';
    } else {
        $is_admin2 = 'btn-primary active';
    } ?>
                                    <!-- active 를 primary 로-->
                                    <button type="button" class="btn <?=$is_admin1?>" onclick="changeAdmin('<?=$value['idx']; ?>', '1')">관리자</button>
                                    <button type="button" class="btn <?=$is_admin2?>" onclick="changeAdmin('<?=$value['idx']; ?>', '0')">회원</button>
                                    </div>
                                </td>
                                <td>
                                <?php
                                    $credit_pay1 = 'btn-secondary';
    $credit_pay2 = 'btn-secondary';
    if ($value['credit_pay'] == 1) {
        $credit_pay1 = 'btn-primary active';
    } else {
        $credit_pay2 = 'btn-primary active';
    } ?>
                                <!-- <label class="sr-only">신용부여</label> -->
                                    <div class="btn-group btn-group-sm" role="group">
                                    <!-- active 를 primary 로-->
                                    <button type="button" class="btn <?=$credit_pay1?>" onclick="changeCredit('<?=$value['idx']; ?>', '1')">신용</button>
                                    <button type="button" class="btn <?=$credit_pay2?>" onclick="changeCredit('<?=$value['idx']; ?>', '0')">미신용</button>
                                    </div>
                                </td>
                                <td>
                                <!-- <label class="sr-only">발송횟수</label> -->
                                <?= $value['order_count']?>
                                </td>
                                <td>
                                <!-- <label class="sr-only">알림톡 발송횟수</label> -->
                                <?= $value['m_count']?>
                                </td>

                                <!-- <td><button type="button" class="btn btn-primary" data-toggle="modal"
                                        onclick="get_transactions('<?=$value['idx']?>');"
                                        data-target="#exampleModal">View</button></td>
                                <td><button type="button" class="btn btn-primary" data-toggle="modal"
                                        onclick="get_detail('<?=$value['idx']?>');"
                                        data-target="#exampleModal1">Detail</button></td>

                                <td>
                                    <form action="<?=$action['setRandomPassword']?>" method="post">
                                        <input type="hidden" value="<?=$this->security->get_csrf_hash(); ?>"
                                            name="<?=$this->security->get_csrf_token_name()?>">

                                        <input type="hidden" name='idx' value="<?=$value['idx']?>">
                                        <button type="submit" class="btn btn-primary">비밀번호초기화(클릭하지마세요)</button>
                                    </form>
                                </td> -->
                            </tr>
                            <?php
} ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
<!-- Modal -->
<div class="modal fade bd-example-modal-xl" id="exampleModal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Transactions in sale</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="dataTableExample" class="table">
                                <thead>
                                    <tr>
                                        <th>TRANSACTION</th>
                                        <th>TO USER</th>
                                        <th>HOW MUCH</th>
                                        <th>AS WHAt</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>0X0IJEFI89039923J8J390089JWDDHCH</td>
                                        <td>admin@admin.com</td>
                                        <td>0.0002</td>
                                        <td>gas</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade bd-example-modal-xl" id="exampleModal1" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Member Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <form method="post" name="listForm" action="">
                                <table id="dataTableExample1" class="table">

                                    <tbody>

                                        <tr>
                                            <th>Name</th>
                                            <td><input type=text name='member_id' value=""></td>
                                        </tr>
                                        <tr>
                                            <th>e-mail</th>
                                            <td><input type=text name='member_mail' value=""></td>
                                        </tr>


                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" onclick="member_save();">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>




<!-- Modal -->
<div class="modal fade bd-example-modal-xl" id="exampleModal2" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">레벨 지정</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?=$action['setStartLevel']?>" method="post">
                <input type="hidden" value="<?=$this->security->get_csrf_hash();?>"
                    name="<?=$this->security->get_csrf_token_name()?>">
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="dataTableExample2" class="table">

                                    <tbody>

                                        <tr>
                                            <th>레벨</th>
                                            <td>V<input type=text name='start_level' value=""></td>
                                        </tr>
                                        <tr>
                                            <th>진입매출값</th>
                                            <td><input type=text name='start_sales' value=""> ETH</td>
                                        </tr>
                                        <input type="hidden" name='idx' id='idx' value="">


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-secondary">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>









</div>
</div>


<script>
    function member_save() {

        fetchlink =
            `<?=$action['member_modify_detail'];?>?indexnumber=${$('#set_detail_idx').val()}&member_id=${$('#set_detail_member_id').val()}&member_id=${$('#set_detail_member_id').val()}&member_mail=${$('#set_detail_member_mail').val()}`;
        console.log(fetchlink);
        location.href = fetchlink;



    }

    function get_transactions(get_index) {
        fetchlink = `<?=$action['get_transaction'];?>?indexnumber=${get_index}`;
        console.log(fetchlink);

        fetch(fetchlink)
            .then(response => response.json())
            .then(json => {
                console.log(json.answer);
                $('#dataTableExample > tbody').html(json.answer)
            })
            .catch(err => console.log(err));
    }

    function get_detail(get_index) {
        fetchlink = `<?=$action['get_detail'];?>?indexnumber=${get_index}`;
        console.log(fetchlink);

        fetch(fetchlink)
            .then(response => response.json())
            .then(json => {
                console.log(json.answer);
                $('#dataTableExample1 > tbody').html(json.answer)
            })
            .catch(err => console.log(err));
    }



    function changeAdmin(id, check){
            fetchlink = `/Pages/customer/editAdmin?id=${id}&check=${check}`;
            console.log(fetchlink);

            fetch(fetchlink)
            .then(response => response.json())
            .then(json => {
                console.log(json.answer);
                alert("적용되었습니다.");
                
            })
            .catch(err => console.log(err));
            // location.reload();
            location.replace('/Pages/customer');
    }
    function changeCredit(id, check){
            fetchlink = `/Pages/customer/editCredit?id=${id}&check=${check}`;
            console.log(fetchlink);

            fetch(fetchlink)
            .then(response => response.json())
            .then(json => {
                console.log(json.answer);
                alert("적용되었습니다.");
            })
            .catch(err => console.log(err));
            location.replace('/Pages/customer');
    }

</script>


