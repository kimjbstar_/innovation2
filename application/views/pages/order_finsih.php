<nav class="page-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Manage</a></li>
						<li class="breadcrumb-item active" aria-current="page">완결 주문내역</li>
					</ol>
				</nav>

				<div class="row">
					<div class="col-md-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <h6 class="card-title">Customer</h6>
                <div class="table-responsive">
                  <table id="dataTableExampleis" class="table">
                    <thead>
                      <tr>
                        <th>User Index</th>
                        <th>User ID</th>
                        <th>가입일</th>     
						<th>주문내역</th>
						<th>Detail</th>
						<th>비밀번호 초기화</th>
                      </tr>
                    </thead>
                    <tbody>
<?php


foreach ($data as $value) {
    // var_dump($value); ?>                                        

                      <tr>
                        <td><?=$value['idx']; ?></td>
                        <td><?=$value['member_id']; ?></td>
						<td><?=$value['member_jointime']; ?></td>
						<td><button type="button" class="btn btn-primary" data-toggle="modal" onclick="get_transactions('<?=$value['idx']?>');" data-target="#exampleModal">View</button></td>
						<td><button type="button" class="btn btn-primary" data-toggle="modal" onclick="get_detail('<?=$value['idx']?>');" data-target="#exampleModal1">Detail</button></td>
                        
						<td>
                            <form action="<?=$action['setRandomPassword']?>" method="post">
                                <input type="hidden"  value="<?=$this->security->get_csrf_hash(); ?>" name="<?=$this->security->get_csrf_token_name()?>">	
                                
                                <input type="hidden"  name='idx' value="<?=$value['idx']?>"> 
                                <button type="submit" class="btn btn-primary">비밀번호초기화(클릭하지마세요)</button>
                            </form>
                        </td>
                      </tr>    
<?php
} ?>        

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
					</div>
				</div>

			</div>
            <!-- Modal -->
            <div class="modal fade bd-example-modal-xl" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog  modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Transactions in sale</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="dataTableExample" class="table">
                                            <thead>
                                                <tr>
                                                    <th>TRANSACTION</th>
                                                    <th>TO USER</th>
                                                    <th>HOW MUCH</th>
                                                    <th>AS WHAt</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>0X0IJEFI89039923J8J390089JWDDHCH</td>
                                                    <td>admin@admin.com</td>
                                                    <td>0.0002</td>
                                                    <td>gas</td>
                                                </tr>    
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
			
			<!-- Modal -->
            <div class="modal fade bd-example-modal-xl" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog  modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Member Detail</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
									<form method="post" name="listForm" action="" >
                                        <table id="dataTableExample1" class="table">
                                            
                                            <tbody>
                           
                                                <tr>
													<th>Name</th>
                                                    <td><input type=text  name='member_id' value=""></td>                               
												</tr>
												<tr>
													<th>e-mail</th>
                                                    <td><input type=text  name='member_mail' value=""></td>       
												</tr>


                                            </tbody>
                                        </table>
										</form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" onclick="member_save();">Save</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>




			<!-- Modal -->
            <div class="modal fade bd-example-modal-xl" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog  modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">레벨 지정</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="<?=$action['setStartLevel']?>" method="post">
                    <input type="hidden"  value="<?=$this->security->get_csrf_hash();?>" name="<?=$this->security->get_csrf_token_name()?>">	
                            <div class="modal-body">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="dataTableExample2" class="table">
                                                
                                                <tbody>
                            
                                                    <tr>
                                                        <th>레벨</th>
                                                        <td>V<input type=text  name='start_level' value=""></td>                               
                                                    </tr>
                                                    <tr>
                                                        <th>진입매출값</th>
                                                        <td><input type=text  name='start_sales' value=""> ETH</td>       
                                                    </tr>
                                                    <input type="hidden"  name='idx' id='idx' value=""> 


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-secondary" >Save</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>









		</div>
	</div>


<script>
	function member_save(){
        // alert(++);
		
        fetchlink=`<?=$action['member_modify_detail'];?>?indexnumber=${$('#set_detail_idx').val()}&member_id=${$('#set_detail_member_id').val()}&member_id=${$('#set_detail_member_id').val()}&member_mail=${$('#set_detail_member_mail').val()}`;
        console.log(fetchlink);
        location.href=fetchlink;



    }

    function get_transactions(get_index){
        fetchlink=`<?=$action['get_transaction'];?>?indexnumber=${get_index}`;
        console.log(fetchlink);

        fetch(fetchlink)
        .then(response => response.json())
        .then(json => {
            console.log(json.answer);
            $('#dataTableExample > tbody').html(json.answer)            
        })
        .catch(err => console.log(err));
    }

	function get_detail(get_index){
        fetchlink=`<?=$action['get_detail'];?>?indexnumber=${get_index}`;
        console.log(fetchlink);

        fetch(fetchlink)
        .then(response => response.json())
        .then(json => {
            console.log(json.answer);
            $('#dataTableExample1 > tbody').html(json.answer)            
        })
        .catch(err => console.log(err));
    }
</script>


