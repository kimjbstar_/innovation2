<nav class="page-breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">관리</a></li>
        <li class="breadcrumb-item"><a href="#">설정</a></li>
        <li class="breadcrumb-item active" aria-current="page">알림톡 발송 설정</li>
    </ol>
</nav>

<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">

                <h6 class="card-title">알림톡 발송 설정</h6>
                <form>
                    <div class="form-group">
                        <!-- <div class="form-check">
                            <label class="form-check-label">
                                <input type="checkbox" checked="" class="form-check-input">
                                Checked
                                <i class="input-frame"></i></label>
                        </div> -->
                        <div class="form-check">
                            <label class="form-check-label">
                                <label class="sr-only">9번 배차처리 주문자에게</label>
                                <input type="checkbox" class="form-check-input" id="no9">배차처리 주문자에게<i
                                    class="input-frame"></i></label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <label class="sr-only">12번 배차처리 상대방에게</label>
                                <input type="checkbox" class="form-check-input" id="no12">배차처리 상대방에게<i
                                    class="input-frame"></i></label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <label class="sr-only">15번 배송완료 기존고객</label>
                                <input type="checkbox" class="form-check-input" id="no15">배송완료 기존고객<i
                                    class="input-frame"></i></label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <label class="sr-only">18번 선물 보내줄 때</label>
                                <input type="checkbox" class="form-check-input" id="no18">선물 보내줄 때<i
                                    class="input-frame"></i></label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <label class="sr-only">21번 배송완료 미고객</label>
                                <input type="checkbox" class="form-check-input" id="no21">배송완료 미고객<i
                                    class="input-frame"></i></label>
                        </div>
                        <div class="form-group text-right">
                            <button type="button" class="btn btn-primary btn-submit">적용</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>

<script>
    $(function(){
        var os9 = '<?= $data['condition'][0]['s9']?>';
        var os12 = '<?= $data['condition'][0]['s12']?>';
        var os15 = '<?= $data['condition'][0]['s15']?>';
        var os18 = '<?= $data['condition'][0]['s18']?>';
        var os21 = '<?= $data['condition'][0]['s21']?>';


        $('input:checkbox[id="no9"]').attr("checked", os9 === 'Y' ? true : false);
        $('input:checkbox[id="no12"]').attr("checked",  os12 === 'Y' ? true : false);
        $('input:checkbox[id="no15"]').attr("checked",  os15 === 'Y' ? true : false);
        $('input:checkbox[id="no18"]').attr("checked",  os18 === 'Y' ? true : false);
        $('input:checkbox[id="no21"]').attr("checked",  os21 === 'Y' ? true : false);

        $('.btn-submit').on('click', function(){
            //9, 12 15 18 21
            var s9 = $('input:checkbox[id="no9"]').is(":checked") ? 'Y' : 'N';
            var s12 = $('input:checkbox[id="no12"]').is(":checked") ? 'Y' : 'N';
            var s15 = $('input:checkbox[id="no15"]').is(":checked") ? 'Y' : 'N';
            var s18 = $('input:checkbox[id="no18"]').is(":checked") ? 'Y' : 'N';
            var s21 = $('input:checkbox[id="no21"]').is(":checked") ? 'Y' : 'N';
            console.log(s9, s12, s15, s18, s21);
            //return false;
            fetchlink = `/Pages/message_setting/save?s9=${s9}&s12=${s12}&s15=${s15}&s18=${s18}&s21=${s21}`;
            console.log(fetchlink);

            fetch(fetchlink)
            .then(response => response.json())
            .then(json => {
                console.log(json.answer);
            })
            .catch(err => console.log(err));
            alert("적용되었습니다.");
        });
    });
</script>
