<script>
$.fn.datepicker.dates['en'] = {
    "days": ["일", "월", "화", "수", "목", "금", "토"],
    "daysShort": ["일", "월", "화", "수", "목", "금", "토"],
    "daysMin": ["일", "월", "화", "수", "목", "금", "토"],
    "months": ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
    "monthsShort": ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
    "today": "오는",
    "clear": "닫기",
    "titleFormat": "yyyy-mm"
}
$(function () {

    const $table = $('#dataTableExampleis').DataTable({
        "aLengthMenu": [
            [10, 30, 50, -1],
            [10, 30, 50, "All"]
        ],
        "iDisplayLength": 10,
        "language": {
            search: ""
        }
    });

    $('#dataTableExampleis').each(function () {
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', '검색');
        search_input.removeClass('form-control-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.removeClass('form-control-sm');
    });

    const $endVal = $('#max-date');
    const $startVal = $('#min-date');


    $startVal.datepicker({
        changeMonth: true,
        changeYear: true,
        autoclose: true,
    });
    
    $endVal.datepicker({
        changeMonth: true,
        changeYear: true,
        autoclose: true,
    }).on('hide', function(hideData){
        $('#searchByDate').click();
    });
    
    $('#searchByDate').on('click', function(e){
        if (!($startVal.val() && $endVal.val())) {
            location.replace(`${location.origin}/Pages/order_finish`);
            return;
        }
        location.replace(`${location.origin}/Pages/order_finish?start=${$startVal.val()}&end=${$endVal.val()}`);
    });


});
</script>

<nav class="page-breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Manage</a></li>
        <li class="breadcrumb-item active" aria-current="page">완결 주문내역</li>
    </ol>
</nav>

<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <h6 class="card-title">완결 주문내역</h6>

                <div class="table-responsive">
                    <input type="text" id="min-date" class="form-control" placeholder="시작일" style="max-width:150px" value="<?=$query['start']?>">
                    <input type="text" id="max-date" class="form-control" placeholder="종료일" style="max-width:150px" value="<?=$query['end']?>"  >
                    <button type="button" class="btn" id="searchByDate">날짜별 검색</button>
                  <table id="dataTableExampleis" class="table">
                    <thead>
                      <tr>
                        <th>주문번호</th>
                        <th>상태</th>
						<th>요청 시간</th>
                        <th>주문자 연락처</th>
                        <th>카드정보</th>
                        <th>출발지</th>
                        <th>출발지 성함</th>
                        <th>출발지 연락처</th>
                        <th>도착지</th>     
                        <th>도착지 성함</th>   
                        <th>도착지 연락처</th>   
						
						<th>메모</th>
						<th>금액</th>
						<th>담당기사</th>
						<th>취소</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($data as $value) { ?>                                        

                      <tr>
                        <td><?=$value['orderNum']; ?></td>
                        <td>
                            <?php
                                $state_class = 'text-secondary';
                                if ($value['state'] == '취소') {
                                    $state_class = 'text-danger';
                                } elseif ($value['state'] == '완료') {
                                }
                            ?>
                            <span class="<?=$state_class?>"><?=$value['state']; ?> </span>
                            
                        </td>
                        <td><?=$value['order_datetime']; ?></td>
                        <td><?=$value['orderMobile']; ?></td>
                        <td>
                            <?=$value['card_number']; ?>
                            
                            <?php
                                if ($value['card_number'] !== "") {
                                    echo '(' . $value['card_month'] . '/' . $value['card_year'] . ')';
                                    echo '<br>';
                                    echo $value['card_email'];
                                } ?>
                        </td>
						<td><?=$value['startLocal']; ?></td>
						<td><?=$value['StartName']; ?></td>
						<td><?=$value['startTelNo']; ?></td>
						<td><?=$value['destLocal']; ?></td>
						<td><?=$value['destName']; ?></td>
						<td><?=$value['destTelNo']; ?></td>
						
						<td><?=$value['memo']; ?></td>
						<td><?=$value['price']; ?></td>
						<td><?=$value['rider_name']; ?> ( <?=$value['rider_tel_number']; ?> )</td>
						<td><button type="button" class="btn btn-primary" data-toggle="modal" onclick="get_detail('<?=$value['orderNum']?>');" data-target="#exampleModal1">취소</button></td>
                        
                      </tr>    
                <?php } ?>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
					</div>
				</div>

			</div>
            <!-- Modal -->
            <div class="modal fade bd-example-modal-xl" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog  modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Transactions in sale</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="dataTableExample" class="table">
                                            <thead>
                                                <tr>
                                                    <th>TRANSACTION</th>
                                                    <th>TO USER</th>
                                                    <th>HOW MUCH</th>
                                                    <th>AS WHAt</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>0X0IJEFI89039923J8J390089JWDDHCH</td>
                                                    <td>admin@admin.com</td>
                                                    <td>0.0002</td>
                                                    <td>gas</td>
                                                </tr>    
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
			
			<!-- Modal -->
            <div class="modal fade bd-example-modal-xl" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog  modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Member Detail</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
									<form method="post" name="listForm" action="" >
                                        <table id="dataTableExample1" class="table">
                                            
                                            <tbody>
                           
                                                <tr>
													<th>Name</th>
                                                    <td><input type=text  name='member_id' value=""></td>                               
												</tr>
												<tr>
													<th>e-mail</th>
                                                    <td><input type=text  name='member_mail' value=""></td>       
												</tr>


                                            </tbody>
                                        </table>
										</form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" onclick="member_save();">Save</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>




			<!-- Modal -->
            <div class="modal fade bd-example-modal-xl" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog  modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">레벨 지정</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="<?=$action['setStartLevel']?>" method="post">
                    <input type="hidden"  value="<?=$this->security->get_csrf_hash();?>" name="<?=$this->security->get_csrf_token_name()?>">	
                            <div class="modal-body">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="dataTableExample2" class="table">
                                                
                                                <tbody>
                            
                                                    <tr>
                                                        <th>레벨</th>
                                                        <td>V<input type=text  name='start_level' value=""></td>                               
                                                    </tr>
                                                    <tr>
                                                        <th>진입매출값</th>
                                                        <td><input type=text  name='start_sales' value=""> ETH</td>       
                                                    </tr>
                                                    <input type="hidden"  name='idx' id='idx' value=""> 


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-secondary" >Save</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>









		</div>
	</div>


<script>
	function member_save(){
        // alert(++);
		
        fetchlink=`<?=$action['member_modify_detail'];?>?indexnumber=${$('#set_detail_idx').val()}&member_id=${$('#set_detail_member_id').val()}&member_id=${$('#set_detail_member_id').val()}&member_mail=${$('#set_detail_member_mail').val()}`;
        console.log(fetchlink);
        location.href=fetchlink;



    }

    function get_transactions(get_index){
        fetchlink=`<?=$action['get_transaction'];?>?indexnumber=${get_index}`;
        console.log(fetchlink);

        fetch(fetchlink)
        .then(response => response.json())
        .then(json => {
            console.log(json.answer);
            $('#dataTableExample > tbody').html(json.answer)            
        })
        .catch(err => console.log(err));
    }

	function get_detail(get_index){
        fetchlink=`<?=$action['get_detail'];?>?indexnumber=${get_index}`;
        console.log(fetchlink);

        fetch(fetchlink)
        .then(response => response.json())
        .then(json => {
            console.log(json.answer);
            $('#dataTableExample1 > tbody').html(json.answer)            
        })
        .catch(err => console.log(err));
    }
</script>


