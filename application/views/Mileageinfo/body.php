<body>
    <link rel="stylesheet" href="https://quickcar.co.kr/css/common.css?d=<?=md5(date("Y-m-d H:i:s"))?>">
    <link rel="stylesheet" href="https://quickcar.co.kr/css/header.css?d=<?=md5(date("Y-m-d H:i:s"))?>">
    <link rel="stylesheet" href="https://quickcar.co.kr/css/menu.css?ver=0.0.1">
    <link rel="stylesheet" href="https://quickcar.co.kr/css/content.css?d=<?=md5(date("Y-m-d H:i:s"))?>">

    <!--Loading Container Start-->
    <div id="load" class="loading-overlay display-flex flex-column justify-content-center align-items-center">
        <div class="primary-color font-28 fas fa-spinner fa-spin"></div>
    </div>
    <!--Loading Container End-->

    <div class="row h-100">
        <div class="col-xs-12 col-sm-12">

            <!--Page Title & Icons Start-->
            <div class="header-icons-container text-center shadow-sm">
                <a href="<?=$url['back']?>">
                    <span class="float-left">
                        <img src="<?=$img['back']?>" alt="Back Icon">
                    </span>
                </a>
                <span class="title">마일리지 확인</span>
                <a href="#">
                <span class="float-right menu-open closed">
                        <!-- <img src="../icons/menu.svg" alt="Menu Hamburger Icon"> -->
                        <button class="btn btn-outline-primary top-button">메뉴</button>
                    </span>
                </a>
            </div>

            <div class="rest-container p-3 text-center" style="top:125px;">

                <span class="d-block h6 pb-5">
                    마일리지는 10,000 이상부터 <br>
                    1,000 단위로 사용 가능합니다.
                </span>

                <span class="d-block h5 mt-5 font-weight-bold text-danger">
                    <label class="h5 font-weight-bold">현재 마일리지 : </label>
                    <?=(int)$data['mileage'][0]->sum_mileage?>
                </span>

                <span class="d-block h5 font-weight-bold text-primary">
                    <label class="h5 font-weight-bold">사용가능한 마일리지 : </label>
                    <?=$data['mileage'][0]->useAbleMileage?>
                </span>
            </div>
        </div>


        <?php $this->load->view('common/slideMenu', renderSlideMenu());?>


    </div>



    <script src="https://quickcar.co.kr/js/lib/jquery.js?lastModify=38b5fe31dc483f36ca2e7aa78545b53f"></script>