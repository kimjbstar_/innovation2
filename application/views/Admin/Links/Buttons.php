<nav class="page-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#"><?=$text['menu_1'];?></a></li>
                    <li class="breadcrumb-item active" aria-current="page"><?=$text['menu_2'];?></li>
                </ol>
            </nav>

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-title"><?=$text['menu_2'];?></h6>
                            <!-- <div style="text-align:right">
                                <p>서버 <?=number_format($serverNum);?>개 가동중 / 가동중 키워드 <?=number_format($validKeywordsCount);?>개 / 가동 가능 키워드 <?=number_format($serverNum*15);?>개</p>
                                <button onclick="getNewDBPopup();" type="button" class="btn btn-warning">신규 키워드 그룹 생성</button>
                                <br>
                            </div> -->
                            <div class="table-responsive">
                                <table id="dataTableExample" class="table">
                                    <thead>
                                        <tr>
                                            <th>페이지명</th>
                                            <th>링크명</th>
                                            <th>On/Off</th>
                                            <th>링크 주소</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                    foreach ($db as $datum) {
                                        // var_dump($datum);
                                        ?>
                                        <tr ondblclick="getDBPopup(<?=$datum['linkIdx'];?>);" >
                                            <td><?=$datum['pageName'];?></td>
                                            <td><?=$datum['buttonName'];?></td>
                                            <td>
                                                <div class="btn-group btn-group-toggle is_admin" data-toggle="buttons">
                                                    <label onclick="location.replace('Keywordcontrol/turnOnLink?groupnum=<?=$datum['linkIdx'];?>');" class="btn btn-primary <?php
                                                            if ($datum['onOff']==1){
                                                                echo 'active';
                                                            }
                                                        ?>">
                                                        <input type="radio" name="is_admin" autocomplete="off" value="1" 
                                                        <?php
                                                            if ($datum['onOff']==1){
                                                                echo 'checked=""';
                                                            }
                                                        ?>> On
                                                    </label>
                                                    <label onclick="location.replace('Keywordcontrol/turnOffLink?groupnum=<?=$datum['linkIdx'];?>');" class="btn btn-primary <?php
                                                            if ($datum['onOff']==0){
                                                                echo 'active';
                                                            }
                                                        ?>">
                                                        <input type="radio" name="is_admin" autocomplete="off" value="0"
                                                        <?php
                                                            if ($datum['onOff']==0){
                                                                echo 'checked=""';
                                                            }
                                                        ?>> Off
                                                    </label>
                                                </div>
                                            </td>
                                            <td><?=$datum['linkAddress'];?></td>
                                            
                                        </tr>
                                        <?php
                                    }

                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>