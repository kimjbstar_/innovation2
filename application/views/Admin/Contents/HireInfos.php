<nav class="page-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#"><?=$text['menu_1'];?></a></li>
                    <li class="breadcrumb-item active" aria-current="page"><?=$text['menu_2'];?></li>
                </ol>
            </nav>

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-title"><?=$text['menu_2'];?></h6>
                            <div style="text-align:right">
                                <button onclick="getNewHireInfoPopup();" type="button" class="btn btn-warning">신규 회사 생성</button>
                                <br>
                            </div>
                            <div class="table-responsive">
                                <table id="dataTableExample" class="table">
                                    <thead>
                                        <tr>
                                            <th>회사명</th>
                                            <th>지역</th>
                                            <th>회사 카테고리</th>
                                            <th>태그</th>
                                            <th>조회수</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                    foreach ($db as $datum) {
                                        // var_dump($datum);
                                        ?>
                                        <tr ondblclick="getHireInfoPopup(<?=$datum['company_index'];?>);" >
                                            <td><?=$datum['company_name'];?></td>
                                            <td><?=$datum['location'];?></td>
                                            <td><?=$datum['company_category'];?></td>
                                            <td><?=$datum['company_tags'];?></td>
                                            <td><?=$datum['hit'];?></td>
                                            
                                        </tr>
                                        <?php
                                    }

                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>