<nav class="page-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#"><?=$text['menu_1'];?></a></li>
                    <li class="breadcrumb-item active" aria-current="page"><?=$text['menu_2'];?></li>
                </ol>
            </nav>

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-title"><?=$text['menu_2'];?></h6>
                            <div class="table-responsive">
                                <table id="dataTableExample" class="table">
                                    <thead>
                                        <tr>
                                            <th>신청일</th>
                                            <th>신청자명</th>
                                            <th>휴대폰번호</th>
                                            <th>이메일</th>
                                            <th>구분</th>
                                            <th>소속</th>
                                            <th>신청일자</th>
                                            <th>신청시간</th>
                                            <th>삭제</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                    foreach ($db as $datum) {
                                        // var_dump($datum);
                                        ?>
                                        <tr ondblclick="getDBPopup(<?=$datum['apply_index'];?>);" >
                                            <td><?=$datum['apply_when'];?></td>
                                            <td><?=$datum['var_1'];?></td>
                                            <td><?=$datum['var_2'];?></td>
                                            <td><?=$datum['var_3'];?></td>
                                            <td><?=$datum['var_4'];?></td>
                                            <td><?=$datum['var_5'];?></td>
                                            <td><?=$datum['var_6'];?></td>
                                            <td><?=$datum['var_7'];?></td>
                                            <td>
                                                <div class="btn-group btn-group-toggle is_admin" data-toggle="buttons">
                                                    <label onclick="location.replace('UseDB/deleteDB?apply_index=<?=$datum['apply_index'];?>');" class="btn btn-primary ">
                                                        <input type="radio" name="is_admin" autocomplete="off" value="1" 
                                                        > 삭제
                                                    </label>
                                                </div>
                                            </td>
                                            
                                        </tr>
                                        <?php
                                    }

                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>