<body>


<link rel="stylesheet" href="https://quickcar.co.kr/css/common.css?d=<?=md5(date("Y-m-d H:i:s"))?>">
<link rel="stylesheet" href="https://quickcar.co.kr/css/header.css?d=<?=md5(date("Y-m-d H:i:s"))?>">
<link rel="stylesheet" href="https://quickcar.co.kr/css/menu.css?ver=0.0.1">
<link rel="stylesheet" href="https://quickcar.co.kr/css/content.css?d=<?=md5(date("Y-m-d H:i:s"))?>">



<div class="row h-100">
    <div class="col-xs-12 col-sm-12">

        <!--Page Title & Icons Start-->
        <div class="header-icons-container text-center shadow-sm">
            <a href="<?=$url['back']?>">
                <span class="float-left">
                    <img src="<?=$img['back']?>" alt="Back Icon">
                </span>
            </a>
            <span class="title">퀵서비스 주문</span>
            <a href="#">
            <span class="float-right menu-open closed">
                        <button class="btn btn-outline-primary top-button">메뉴</button>
                    </span>
            </a>

         

        </div>







            <div class="content_box margin-rel-b-20" style="margin-top:95px;">
               <img src="img/quickcar.jpeg" style="width:100%;">

            </div>

            <div class="content_box margin-rel-b-20" style="margin-top:25px;">
                <div class="f f-col f-al-center w-100 which_car">
                    

                    <button onclick="do_go()" id="btn-Confirm" class="btn green w-50" style="background: rgb(30,166,159);"><p>앱에서 부르기</p></button><br>
                    <button onclick="do_tel()" id="btn-Confirm" class="btn green w-50" style="background: rgb(7,65,149);"><p>전화로 부르기</p></button><br>
                    <button onclick="do_go_home()" id="btn-Confirm" class="btn green w-50" style="background: rgb(21,16,118);"><p>홈페이지 바로가기</p></button><br>
                    
                </div>

            </div>

            <div class="content_box margin-rel-b-20" style="margin-top:25px;">
                <div class="f f-col f-al-center w-100 which_car">
                    <div class="data-kind-2 content_gray_inner_box hide" data-motorcycle_weight="350kg">
                        <p class="margin-rel-b-8">상호 : 주식회사 퀵카</p>
                        <p class="margin-rel-b-8">대표번호 : 1661-2482(24시간82빨리)</p>
                        <p class="margin-rel-b-8">사업자 번호 : 451-88-01938</p>
                        <p class="margin-rel-b-8">화물주선업 면허:제250114호</p>
                        <p class="margin-rel-b-8">통신판매업:제2016-서울동대문-0728호</p>
                        <p class="margin-rel-b-8">주소 : 서울시 동대문구 신설동18 진성빌딩</p>
                    </div>
                </div>

            </div>



        </div>
    </div>

    
    <?php $this->load->view('common/slideMenu', renderSlideMenu());?>


</div>



<script src="https://quickcar.co.kr/js/lib/jquery.js?lastModify=38b5fe31dc483f36ca2e7aa78545b53f"></script>


<script>
    function do_go(){
        location.replace("https://quickcar.co.kr");
    }
    function do_tel(){
        location.replace("tel:1661-2482");
    }
    function do_go_home(){
        location.replace("http://quickcar.modoo.at");
    }
</script>
