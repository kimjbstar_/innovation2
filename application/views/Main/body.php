
<script>
    var thisuser = 0;
    // var pop_open = false;
    var order_first = '<?= $this->input->get('order_first') ?>';
    <?php //$this->session->unset_userdata('order_first') ?>
    // var order_first = '1';


    // var rx = /INPUT|SELECT|TEXTAREA/i;

    // $(document).bind("keydown keypress", function(e){
    //     if( e.which == 8 ){ // 8 == backspace
    //         if(!pop_open){
    //             if(!rx.test(e.target.tagName) || e.target.disabled || e.target.readOnly ){
    //                 //location.href='<?=$url['back']?>';
    //                 location.replace("/finish");
    //                 e.preventDefault();
                
    //             }
    //         }else{
    //             return false;
    //         }
    //     }
    // });
</script>

<body>

    <!--Loading Container Start-->
    <!-- <div id="load" class="loading-overlay display-flex flex-column justify-content-center align-items-center">
    <div class="primary-color font-28 fas fa-spinner fa-spin"></div>
</div> -->
    <!--Loading Container End-->
    <!-- <div id="postCodeLayer"
        style="display:none;position:fixed;overflow:hidden;z-index:1;-webkit-overflow-scrolling:touch;width: 95%;height: 80%;border: 5px solid;left: 3%;top: 10%;">

    </div> -->

    <style>

        #origin-input-container{
            position:absolute;
            z-index:2;
        }
        .addr-input-box {
            display: none;
            background-color: white;
            position: absolute;
            overflow: hidden;
            z-index: 110;
            -webkit-overflow-scrolling: touch;
            width: 95%;
            height: 80%;
            border: 5px solid;
            left: 3%;
            /* top: 12%; */
            top: 80px;

        }

        @media (max-width: 992px) {


           

            .addr-input-box {
                min-height: calc(100vh - 350px);
                overflow-y: scroll;
            }
            body{
                min-height: calc(100vh - 350px);
            }

        }


        .addr-input-box .box-1,
        .addr-input-box .box-2,
        .addr-input-box .box-3,
        .addr-input-box .box-4,
        .addr-input-box .address-submit {
            display: none;
        }

        #addr-input-box-wrap {
            margin: 0 .3rem;
            border: 1px solid #aaa;
        }
    </style>
    <script>
        function foldDaumPostcode() {
            $(".addr-input-box #wrap").css("display", 'none');
        }

        function sample3_execDaumPostcode() {

            $(".focus_input").focus();


            const elem = document.getElementById("addr-input-box-wrap");
            var currentScroll = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
            new daum.Postcode({
                oncomplete: function (data) {
                    var addr = ''; // 주소 변수
                    var extraAddr = ''; // 참고항목 변수
                    if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                        addr = data.roadAddress;
                    } else { // 사용자가 지번 주소를 선택했을 경우(J)
                        addr = data.jibunAddress;
                    }
                    if (data.userSelectedType === 'R') {
                        if (data.bname !== '' && /[동|로|가]$/g.test(data.bname)) {
                            extraAddr += data.bname;
                        }
                        if (data.buildingName !== '' && data.apartment === 'Y') {
                            extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                        }
                        if (extraAddr !== '') {
                            extraAddr = ' (' + extraAddr + ')';
                        }
                        document.getElementById("extraAddress").value = extraAddr;

                    } else {
                        document.getElementById("extraAddress").value = '';
                    }
                    document.getElementById('postcode').value = data.zonecode;
                    document.getElementById("address").value = addr;
                    document.getElementById("detailAddress").focus();
                    elem.style.display = 'none';
                    document.body.scrollTop = currentScroll;
                    $(".addr-input-box .box-1").show();
                    $(".addr-input-box .box-2").show();
                    $(".addr-input-box .box-3").show();
                    $(".addr-input-box .box-4").show();
                    $(".addr-input-box .address-submit").show();
                    $(".addr-input-box .zipcode-search").hide();
                    console.log(isSelect);
                    $("#" + isSelect).val(data.address);
                    if (isSelect === 'origin-input') {

                        $("#origin-building-name").val(data.buildingName);
                        $("#origin-search-address").val(data.jibunAddress);
                        localStorage.setItem("originData.searchAddress", data.jibunAddress);
                    } else if (isSelect === 'destination-input') {
                        $("#destination-building-name").val(data.buildingName);
                        $("#destination-search-address").val(data.jibunAddress);
                        localStorage.setItem("destinationData.searchAddress", data.jibunAddress);
                    }

                    // if ($('#origin-input').val() != "" && $('#destination-input').val() != "") {
                    //     location.replace("/order/index?departure=" + $('#origin-input').val() +
                    //         "&destination=" + $('#destination-input').val() + "&thisuser=" + thisuser);
                    // }

                    console.log("success");
                },
                onresize: function (size) {
                    elem.style.height = size.height + 'px';
                },
                width: '100%',
                height: '100%',
                focusInput : true,
                focusContent : true
            }).embed(elem);
            elem.style.display = 'block';
            // $(".focus_input").focus();
            // $("document iframe document iframe .form_search .post_search").focus();

            // document.getElementsByTagName("iframe")[0].contentWindow.document.getElementsByTagName("iframe")[0].contentWindow.document.getElementsByClassName('post_search').focus();
            // console.log('level1');
            // console.log('level1', document.getElementsByTagName("iframe")[0]);
            // console.log('level2');
            // console.log('level2', document.getElementsByTagName("iframe")[0].contentWindow.document.getElementsByTagName("IFRAME")[0]);
            // document.iframe.document.iframe.OgnizSmllCDForm.ofcCd.focus(); 
            
        }

        function addressBoxClose() {
            if (isSelect === 'origin-input') {
                $("#origin-detail").val($('#detailAddress').val());
                originData = {
                    'postcode': $('#postcode').val(),
                    'address': $('#address').val(),
                    'detailAddress': $('#detailAddress').val(),
                    'extraAddress': $('#extraAddress').val()
                };
                localStorage.setItem("originData.postcode", originData.postcode);
                localStorage.setItem("originData.address", originData.address);
                localStorage.setItem("originData.detailAddress", originData.detailAddress);
                localStorage.setItem("originData.extraAddress", originData.extraAddress);

            } else if (isSelect === 'destination-input') {
                $("#destination-detail").val($('#detailAddress').val());
                destinationData = {
                    'postcode': $('#postcode').val(),
                    'address': $('#address').val(),
                    'detailAddress': $('#detailAddress').val(),
                    'extraAddress': $('#extraAddress').val()
                };
                localStorage.setItem("destinationData.postcode", destinationData.postcode);
                localStorage.setItem("destinationData.address", destinationData.address);
                localStorage.setItem("destinationData.detailAddress", destinationData.detailAddress);
                localStorage.setItem("destinationData.extraAddress", destinationData.extraAddress);
            }


            $(".addr-input-box").css("display", 'none');
            // pop_open = false;
            $(".addr-input-box .box-1").hide();
            $(".addr-input-box .box-2").hide();
            $(".addr-input-box .box-3").hide();
            $(".addr-input-box .box-4").hide();
            $(".addr-input-box .address-submit").hide();
            $(".addr-input-box .zipcode-search").show();
            $('#postcode').val("");
            $('#address').val("");
            $('#detailAddress').val("");
            $('#extraAddress').val("");


            // if (isSelect === 'origin-input') {

            // $("#origin-building-name").val(data.buildingName);
            // $("#origin-search-address").val(data.jibunAddress);
            // } else if (isSelect === 'destination-input') {
            // $("#destination-building-name").val(data.buildingName);
            // $("#destination-search-address").val(data.jibunAddress);
            // }


            var address = "departure=" + $('#origin-input').val() + "&destination=" + $('#destination-input').val();
            var detailAddress = "odetail=" + $('#origin-detail').val() + "&ddetail=" + $('#destination-detail').val();
            var searchAddress = "osearch=" + $('#origin-search-address').val() + "&dsearch=" + $(
                '#destination-search-address').val();
            if ($('#origin-input').val() != "" && $('#destination-input').val() != "") {
                location.replace("/order/index?" + address + "&" + detailAddress + "&" + searchAddress + "&thisuser=" +
                    thisuser);
            }
        }
    </script>

    <div class="addr-input-box py-3 px-1">
        <input type="text" class="focus_input" style="width:0;height:0;position:fixed;top:0;left:0;">
        <form>
            <div class="form-group zipcode-search text-center font-weight-bold">
                <!-- <input type="button" class="btn col-12 btn-primary btn-lg"
                    value="주소검색"> -->
                    <label class="text-primary">아래 검색창을 클릭하여 주소를 입력해주세요.</label>
            </div>
            <div class="form-group box-1">
                <label><strong>우편번호</strong></label>
                <input type="text" id="postcode" class="form-control" placeholder="우편번호" readonly>
            </div>
            <div class="form-group box-2">
                <label><strong>주소</strong></label>
                <input type="text" id="address" class="form-control" placeholder="주소" readonly>
            </div>
            <div class="form-group box-3">
                <label><strong>상세주소</strong></label>
                <input type="text" id="detailAddress" class="form-control" placeholder="상세주소">
            </div>
            <div class="form-group box-4">
                <label><strong>참고항목</strong></label>
                <input type="text" id="extraAddress" class="form-control" placeholder="참고항목" readonly>
            </div>
            <div class="form-group address-submit my-3">
                <input type="button" class="btn col-12 btn-primary btn-lg" onclick="addressBoxClose()" value="확인">
            </div>
        </form>

        <!--display:none;border:1px solid;width:500px;height:300px;margin:5px 0;position:relative -->
        <div id="addr-input-box-wrap">
            <!-- <img src="" id="btnFoldWrap"
                style="cursor:pointer;position:absolute;right:0px;top:-1px;z-index:1" onclick="foldDaumPostcode()"
                alt="접기 버튼"> -->
        </div>

    </div>




    <?php
    if (!is_null($this->session->userId)) {
        ?>
    <div id="guide_pop"
        style="position: fixed;top: 38px;right: 28px;width: 227px;height: 187px;background-image: url('https://quickcar.co.kr/img/1487432_4.png');background-size: cover;z-index: 99999;transform: rotate(-180deg);padding-top: 50px;">

    </div>
    <?php
    }
?>



    <div class="row h-100">
        <div class="col-xs-12 col-sm-12 remaining-height">

            <!--Page Title & Icons Start-->
            <div class="header-icons-container position-relative text-center shadow-sm">
                <span class="float-left back-to-map hidden">
                    <img src="<?=$img['back']?>" alt="Back Icon">
                </span>
                <span class="title">퀵카 Quickcar</span>
                <a href="#">
                    <span class="float-right menu-open closed">
                        <!-- <img src="../icons/menu.svg" alt="Menu Hamburger Icon"> -->
                        <button class="btn btn-outline-primary top-button">메뉴</button>
                    </span>
                </a>
            </div>

            <a href="tel:1661-2482">
            <div class=" position-relative text-center"
                style="background-color: #13245E !important; font-size: 0.8rem !important; z-index:10;">
                <img src="img/order_via_call.png" class="img-fluid" style=" max-height: 100px;">
            </div>
            </a>


            <!--Page Title & Icons End-->

            <!-- test tag -->
            <div>
                <div class="margin-top-10 height-auto " id="origin-input-container">
                    <div class="w-100 map-input-container map-input-container-top shadow">
                        <span class="fas fa-location-arrow location-icon-rotate map-input-icon"></span>
                        <div class="map-input test display-flex">

                            <!--  id="departure-input" -->
                            <input id="origin-input" class="controls flex-1" type="text" placeholder="출발지를 입력하세요.">
                            <input id="origin-detail" type="hidden">
                            <input id="origin-search-address" type="hidden">
                            <input id="origin-building-name" type="hidden">
                            <!-- <span class="mic"><img src="../icons/microphone.svg" alt="Microphone Icon"></span> -->
                            <span class="remove"> <img src="../icons/cross-light.svg" alt="Remove Icon Light Color">
                            </span>
                        </div>
                    </div>
                    <div class="w-100 map-input-container map-input-container-bottom shadow">
                        <span class="map-input-icon"><img src="../icons/circle.svg" alt="Current Location Icon"></span>
                        <div class="map-input test display-flex">
                            <input data-search="<?=$url['search']?>" id="destination-input" class="controls flex-1"
                                type="text" placeholder="도착지를 입력하세요.">
                            <input id="destination-detail" type="hidden">
                            <input id="destination-search-address" type="hidden">
                            <input id="destination-building-name" type="hidden">
                            <!-- <span class="mic"><img src="../icons/microphone.svg" alt="Microphone Icon"></span> -->
                            <span class="remove">
                                <img src="../icons/cross-light.svg" alt="Remove Icon Light Color">
                            </span>
                        </div>
                        <span class="dotted-line-index"></span>
                    </div>
                </div>
            </div>
            <!-- test tag -->


            <!-- <div class="hidden">
                <div class="margin-top-10 height-auto" id="origin-input-container">
                    <div class="w-100 map-input-container map-input-container-top">
                        <span class="fas fa-location-arrow location-icon-rotate map-input-icon"></span>
                        <div class="map-input display-flex">

                             id="departure-input"
                            <input id="origin-input" class="controls flex-1" type="text" placeholder="출발지를 입력하세요.">
                            <input id="origin-building-name" type="hidden">
                            <span class="mic"><img src="../icons/microphone.svg" alt="Microphone Icon"></span>
                            <span class="remove"> <img src="../icons/cross-light.svg" alt="Remove Icon Light Color">
                            </span>
                        </div>
                    </div>
                    <div class="w-100 map-input-container map-input-container-bottom">
                        <span class="map-input-icon"><img src="../icons/circle.svg" alt="Current Location Icon"></span>
                        <div class="map-input display-flex">
                            <input data-search="<?=$url['search']?>" id="destination-input" class="controls flex-1"
                                type="text" placeholder="도착지를 입력하세요.">
                            <input id="destination-building-name" type="hidden">
                            <span class="mic"><img src="../icons/microphone.svg" alt="Microphone Icon"></span>
                            <span class="remove">
                                <img src="../icons/cross-light.svg" alt="Remove Icon Light Color">
                            </span>
                        </div>
                        <span class="dotted-line-index"></span>
                    </div>
                </div>
            </div> -->

            <!--Google Maps Start-->
            <style>
                .map-class{
                    z-index:1;
                    margin-top: -124px;
                }
            </style>
            <div id="map" class="h-100 map-class"></div>
            <!--Google Maps End-->

            <div class="not-map">

                <!--Request Ride Button Container Start-->
                <div class="request-ride-btn hidden">
                    <button type="button" class="btn btn-primary">Request a Ride</button>
                </div>
                <!--Request Ride Button Container End-->

                <!--Choose a Car Container Start-->
                <div class="ride-options-bottom hidden">
                    <div class="cars-options">
                        <div class="car-option" data-about-target-id="about-section-1">
                            <img src="https://via.placeholder.com/80x42" alt="Car Option 1">
                        </div>
                        <div class="car-option active" data-about-target-id="about-section-2">
                            <img src="https://via.placeholder.com/80x42" alt="Car Option 2">
                        </div>
                        <div class="car-option" data-about-target-id="about-section-3">
                            <img src="https://via.placeholder.com/80x42" alt="Car Option 3">
                        </div>
                    </div>
                    <div class="about-section">
                        <div class="about-section-1 hidden">
                            <div class="about-section-title">
                                Economy Ride: <strong>$30.00</strong> / 6.85 km /
                                <span class="black-text">3 Minutes</span>
                            </div>
                            <div class="about-section-more">
                                NOTE: This is an approximate estimate. Actual cost may vary according
                                to the traffic.
                            </div>
                        </div>
                        <div class="about-section-2">
                            <div class="about-section-title">
                                <div class="about-section-title">
                                    Limo Ride: <strong>$50.00</strong> / 6.85 km /
                                    <span class="black-text">7 Minutes</span>
                                </div>
                            </div>
                            <div class="about-section-more">
                                NOTE: This is an approximate estimate. Actual cost may vary according
                                to the traffic.
                            </div>
                        </div>
                        <div class="about-section-3 hidden">
                            <div class="about-section-title">
                                <div class="about-section-title">
                                    Super Ride: <strong>$100.00</strong> / 6.85 km /
                                    <span class="black-text">10 Minutes</span>
                                </div>
                            </div>
                            <div class="about-section-more">
                                NOTE: This is an approximate estimate. Actual cost may vary according
                                to the traffic.
                            </div>
                        </div>
                    </div>
                    <div class="payment-options">
                        <div class="send-wishes-container">
                            <div class="float-left wishes">
                                <img src="../icons/wishes.svg" class="h-100" alt="Wishes Icon">
                            </div>
                            <div class="payment-option-content">
                                Wishes
                                <span class="payment-options-more">Write your wishes to driver</span>
                            </div>
                        </div>
                        <div class="default-payment-method-container display-flex">
                            <div class="float-left wishes">
                                <img src="../icons/cash.svg" alt="Cash Icon" class="h-100">
                            </div>
                            <div class="payment-option-content">
                                Cash
                                <span class="payment-options-more">Default Payment Method</span>
                            </div>
                        </div>
                    </div>
                    <div class="confirm-ride-btn">
                        <button type="button" class="btn btn-primary">Confirm</button>
                    </div>
                </div>
                <!--Choose a Car Container End-->

                <!--Wish Container Start-->
                <div class="wish-container hidden">
                    <div class="close-wish">
                        <span class="float-right">
                            <img src="../icons/close.svg" alt="Close Icon">
                        </span>
                        <span class="clearfix"></span>
                    </div>
                    <div class="wish-content-intro-container">
                        <span class="write-wishes-text">Write Wishes To Driver</span>
                        <span class="float-right"><img src="../icons/angle-right.svg" alt="Angle Right Icon"></span>
                    </div>
                    <div class="font-weight-light wishes-main-text">
                        <p>
                            By Clicking on the link above, you can write your comments or wishes to the driver.
                        </p>
                        <p>
                            Please write a polite message and kindly state your wishes.
                            Make sure that your wishes will not make the driver uncomfortable.
                            Enjoy your ride!
                        </p>
                    </div>
                    <div class="enter-wish hidden">
                        <textarea class="width-100 height-100 font-weight-light"
                            placeholder="Please enter your wishes here..."></textarea>
                    </div>
                    <div class="send-wish-btn hidden">
                        <button type="button" class="btn btn-primary">Send</button>
                    </div>
                </div>
                <!--Wish Container End-->

                <!--Payment Options Container Start-->
                <div class="payment-options-container hidden">
                    <div class="payment-ready-btn">
                        <span class="text-uppercase float-right ready">
                            Ready
                            <img class="hidden" src="../icons/close.svg" alt="Close Icon"></span>
                    </div>
                    <div class="payment-options-intro-container">
                        <span class="payment-options-text"><span class="fas fa-dollar-sign"></span> Payment By
                            Cash</span>
                        <span class="fas fa-chevron-down float-right"></span>
                    </div>
                    <div>
                        <ul class="payment-option-list hidden">
                            <li>
                                <span class="fas fa-dollar-sign"></span> Payment By Cash
                                <span class="float-right check">
                                    <img src="../icons/check.svg" alt="Check Icon">
                                </span>
                            </li>
                            <li>
                                <span class="fab fa-cc-visa"></span> Credit Card
                                <span class="float-right hidden check">
                                    <img src="../icons/check.svg" alt="Check Icon">
                                </span>
                            </li>
                            <li>
                                <span class="paypal-icon"><img src="../icons/paypal.svg" alt="Paypal Icon"></span>
                                PayPal
                                <span class="float-right hidden check">
                                    <img src="../icons/check.svg" alt="Check Icon">
                                </span>
                            </li>
                        </ul>
                    </div>
                    <div class="add-new-card-container">
                        <button type="button" class="btn btn-dark">Add New Credit Card <img class="plus-icon"
                                src="../icons/plus.svg" alt="Add Icon"></button>
                    </div>
                    <div class="payment-options-main-text">
                        Paying by card is fast and convenient option. Click the button to
                        add a new card. The amount will be charged after the trip.
                    </div>
                </div>
                <!--Payment Options Container End-->

                <!--Tap on Car Red Text Start-->
                <div class="tap-on-car hidden">
                    <span>Tap on the car of your choice</span>
                </div>
                <!--Tap on Car Red Text End-->

                <!--Tapped Car Information Container Start-->
                <div class="tapped-car-info hidden disabled">
                    <div class="tapped-car-container">

                        <!--Chosen Car Information Container Start-->
                        <div class="tapped-car-info-container">
                            <div class="tapped-car-info-header-icons text-center">
                                <span class="slide-up">
                                    <img src="../icons/up-arrow.svg" alt="Up Arrow Icon">
                                </span>
                                <span class="float-right close">
                                    <img src="../icons/close.svg" alt="Close Icon">
                                </span>
                            </div>
                            <div class="rider-info">
                                <div class="float-left">
                                    <span>Giordano Bruno</span>
                                    <div class="font-14 rating">
                                        <span class="fas fa-star text-white font-weight-light"></span>
                                        <span class="fas fa-star text-white font-weight-light"></span>
                                        <span class="fas fa-star text-white font-weight-light"></span>
                                        <span class="fas fa-star text-white font-weight-light"></span>
                                        <span class="fas fa-star text-white font-weight-light"></span>
                                    </div>
                                </div>
                                <div class="float-right profile-picture">
                                    <img src="https://via.placeholder.com/76x76" alt="Profile Picture">
                                </div>
                            </div>
                            <div class="margin-top-25">
                                <img src="https://via.placeholder.com/178x73" alt="Mercedes Car" class="img-responsive">
                            </div>
                            <div class="requested-car-info">
                                <div class="float-left font-weight-light">
                                    <div>Mercedes-Benz</div>
                                    <div>GL-278-LG</div>
                                </div>
                                <div class="float-right request-btn">
                                    <button type="button" class="btn btn-transparent">Request</button>
                                </div>
                                <div class="float-right ride-status hidden">
                                    <span class="text-uppercase pending-request float-right font-13"><span
                                            class="pending-spinner fas fa-spinner fa-spin primary-color font-13"></span>
                                        Pending</span>
                                    <span
                                        class="text-uppercase just-approved float-right font-13 hidden">Approved</span>
                                    <span class="miles-till-arrival hidden">2.850</span>
                                    <span class="text-uppercase on-the-way float-right font-13 hidden">on the way</span>
                                    <div>
                                        <a href="chat.html" class="btn pending-icon message"><span
                                                class="fas fa-comments"></span></a>
                                        <a class="btn pending-icon call"><span class="fas fa-phone"></span></a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <!--Chosen Car Information Container End-->

                        <!--More Information About Chosen Car Container Start-->
                        <div class="tapped-car-more-info">
                            <button type="button" class="cancel-trip btn btn-transparent text-uppercase">Cancel
                                Trip</button>
                            <div class="more-info-container">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Vestibulum rhoncus est pellentesque
                                    elit.
                                </p>
                                <p>
                                    Nisi scelerisque eu ultrices vitae auctor eu. Felis donec et odio pellentesque diam
                                    volutpat commodo sed egestas. Sit amet nisl suscipit adipiscing bibendum.
                                </p>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua.
                                </p>
                            </div>
                        </div>
                        <!--More Information About Chosen Car Container End-->

                    </div>
                </div>
                <!--Tapped Car Information Container End-->

                <!--Submit Review Container Start-->
                <div class="submit-review hidden">
                    <div class="text-center driver-info">
                        <img class="review-profile" src="https://via.placeholder.com/76x76" alt="Driver Picture">
                        <div class="float-right close-review">
                            <img src="../icons/close.svg" alt="Close Icon">
                        </div>
                        <div class="driver-name">Giordano Bruno</div>
                        <div class="driver-stars rating">
                            <span class="fas fa-star text-white font-weight-light"></span>
                            <span class="fas fa-star text-white font-weight-light"></span>
                            <span class="fas fa-star text-white font-weight-light"></span>
                            <span class="fas fa-star text-white font-weight-light"></span>
                            <span class="fas fa-star text-white font-weight-light"></span>
                        </div>
                    </div>
                    <div class="text-center trip-desc-info">
                        <div class="trip-desc-question">How Was Your Trip ?</div>
                        <div class="please-rate">Please rate, your feedback will help improve driving experience.</div>
                    </div>
                    <div class="comment-info">
                        <span class="comment-label"> * </span> Comments
                        <textarea class="w-100"></textarea>
                        <button class="btn btn-primary text-uppercase">Submit Review</button>
                    </div>
                </div>
                <!--Submit Review Container End-->
            </div>


            <!-- <a href="tel:1661-2482"> -->
                <!-- <div class=" text-center shadow"
                    style="position:fixed; bottom: 0; left: 0; width:100%; background-color: #13245E !important;font-size: 0.8rem !important;vertical-align: top; z-index:10;">
                    <img src="img/order_bonus.png" class="img-fluid" style="max-height: 100px;">
                </div> -->
            <!-- </a> -->

            <a href="tel:1661-2482">
            <div class=" position-relative text-center"
                style="position:fixed; bottom: 0; left: 0; background-color: #13245E !important; font-size: 0.8rem !important; z-index:10;">
                <img src="img/order_via_call.png" class="img-fluid" style=" max-height: 100px;">
            </div>
            </a>


        </div>

        <!--Main Menu Start-->
        <?php $this->load->view('common/slideMenu', renderSlideMenu()); ?>

        <!--Main Menu End-->

        <!-- Modal Start-->
        <div class="modal fade" id="searchingModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content search">
                    <div class="modal-body">
                        <div class="text-center">
                            <span class="fas fa-spinner fa-spin"></span>
                            <div class="label-title">Searching Driver...</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Modal End-->

    </div>

    <style>
        div#postCodeLayer {
            z-index: 9999 !important;
        }
    </style>

    <script>
        $(document).ready(function () {
            $('#origin_input').text(var_origin);
            $('#destination_input').text(var_destination);
            // 10초 뒤 가이드 숨기기
            setTimeout(function () {
                $('#guide_pop').css('display', 'none');
            }, 10000);
        });
        $('#guide_pop').click(function () {
            $('#guide_pop').css('display', 'none');
        });
    </script>