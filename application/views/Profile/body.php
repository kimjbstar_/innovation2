<!-- <div id="load" class="loading-overlay display-flex flex-column justify-content-center align-items-center">
    <div class="primary-color font-28 fas fa-spinner fa-spin"></div>
</div> -->
<!--Loading Container End-->

<div class="row h-100">
    <div class="col-xs-12 col-sm-12">

        <!--Page Title & Icons Start-->
        <div class="header-icons-container text-center shadow-sm">
            <a href="<?=$url['back']?>">
                <span class="float-left">
                    <img src="<?=$img['back']?>" alt="Back Icon">
                </span>
            </a>
            <span>최근 발송내역</span>
            <a href="#">
                <span class="float-right menu-open closed">
                    <img src="<?=$img['menu']?>" alt="Menu Hamburger Icon">
                </span>
            </a>
        </div>
        <!--Page Title & Icons Eㄴnd-->

        <div class="rest-container">

            <!--Profile Information Container Start-->
            <div class="text-center header-icon-logo-margin">
                <!-- <div class="profile-picture-container">
                    <img src="../images/avatar.svg" alt="Profile Picture">
                    <span class="fas fa-camera">
                         <input class="file-prompt" type="file" accept="image/*">
                    </span>
                </div> -->
                <div class="display-flex flex-column">
                    <span class="profile-name"><?=$data['userId']?></span>
                    <span class="profile-email font-weight-light"><?=$data['userEmail']?></span>
                </div>
            </div>
            <!--Profile Information Container End-->

            <!--Profile Information Fields Container Start-->
            <div class="sign-up-form-container text-center">
                <form class="width-100" method="post" action="<?=$url['save']?>">
                    <input type="hidden" value="<?=$data['csrf']['hash']?>" name="<?=$data['csrf']['name']?>">
                    <!--Profile Field Container Start-->
                    <!-- <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span><img src="<?=$img['phone']?>" alt="Phone Number"></span>
                            </div>
                            <input class="form-control" type="tel" value="<?=$data['userPhone']?>" name="phone" placeholder="핸드폰 번호(숫자만 입력 바랍니다.)">
                        </div>
                    </div> -->
                    <!--Profile Field Container End-->

                    <!--Profile Field Container Start-->
                    <!-- <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span>
                                    <img src="../icons/avatar-light.svg" alt="Avatar Icon">
                                </span>
                            </div>
                            <input class="form-control" type="text" autocomplete="off" name="full-name" placeholder="Full Name">
                        </div>
                    </div> -->
                    <!--Profile Field Container End-->

                    <!--Profile Field Container Start-->
                    <!-- <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span>
                                    <img src="<?=$img['envelope']?>" alt="Envelope Icon">
                                </span>
                            </div>
                            <input class="form-control" value="<?=$data['userEmail']?>" type="email" name="email" placeholder="이메일">
                        </div>
                    </div> -->
                    <!--Profile Field Container End-->

                    <!--Profile Field Container Start-->
                    <!-- <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span>
                                    <img src="../icons/marker.svg"  alt="Marker Icon">
                                </span>
                            </div>
                            <input class="form-control" type="text" autocomplete="off" name="country" placeholder="Country">
                        </div>
                    </div> -->
                    <!--Profile Field Container End-->

                    <!--Profile Field Container Start-->
                    <!-- <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span>
                                    <img src="../icons/marker.svg"  alt="Marker Icon">
                                </span>
                            </div>
                            <input class="form-control" type="text" autocomplete="off" name="city" placeholder="City">
                        </div>
                    </div> -->
                    <!--Profile Field Container End-->

                    <!--Profile Field Container Start-->
                    <!-- <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span>
                                    <img src="../icons/marker.svg"  alt="Marker Icon">
                                </span>
                            </div>
                            <input class="form-control" type="text" autocomplete="off" name="address" placeholder="Address">
                        </div>
                    </div> -->
                    <!--Profile Field Container End-->

                    <!--Profile Field Container Start-->
                    <!-- <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                               <span>
                                    <img src="../icons/marker.svg"  alt="Marker Icon">
                                </span>
                            </div>
                            <input class="form-control" type="text" autocomplete="off" name="zip" placeholder="Zip Code">
                        </div>
                    </div> -->
                    <!--Profile Field Container End-->

                    <!--Profile Field Container Start-->
                    <!-- <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span>
                                    <img src="<?=$img['lock']?>" alt="Lock Icon">
                                </span>
                            </div>
                            <input class="form-control" type="password" name="password" placeholder="비밀번호">
                            <div class="input-group-append password-visibility">
                                <span>
                                    <img src="<?=$img['eye']?>" alt="Password Visibility Icon">
                                </span>
                            </div>
                        </div>
                    </div> -->
                    <!--Profile Field Container End-->

                    <!--Profile Field Container Start-->
                    <!-- <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span>
                                    <img src="<?=$img['lock']?>" alt="Lock Icon">
                                </span>
                            </div>
                            <input class="form-control" type="password" name="passwordConfirm" placeholder="비밀번호 재입력">
                            <div class="input-group-append password-visibility">
                                <span>
                                    <img src="<?=$img['eye']?>" alt="Password Visibility Icon">
                                </span>
                            </div>
                        </div>
                    </div> -->
                    <!--Profile Field Container End-->

                    <!-- <div class="form-submit-button">
                        <button href="index.html" class="btn btn-dark text-uppercase ">저장<span class="far fa-save margin-left-10"></span></button>
                    </div> -->
                </form>
            </div>
            <!--Profile Information Fields Container End-->

        </div>
    </div>

    <!--Terms And Conditions Agreement Container Start-->
    <div class="col-xs-12 col-sm-12 text-center sms-rate-text font-roboto flex-end margin-bottom-30">
        <div class="container-sms-rate-text width-100 font-11">
            <span class="light-gray font-weight-light">By signing up you have agreed to our </span>
            <br/>
            <a href="#" class="dark-link">
                <span class="font-weight-light">Terms of Use & Privacy Policy</span>
            </a>
        </div>
    </div>
    <!--Terms And Conditions Agreement Container End-->

    <?php $this->load->view('common/slideMenu', renderSlideMenu()); ?>
</div>