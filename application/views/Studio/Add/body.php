<body>

<!--Loading Container Start-->
<!-- <div id="load" class="loading-overlay display-flex flex-column justify-content-center align-items-center">
    <div class="primary-color font-28 fas fa-spinner fa-spin"></div>
</div> -->
<!--Loading Container End-->

<div class="row h-100">
    <div class="col-xs-12 col-sm-12">

        <!--Page Title & Icons Start-->
        <div class="header-icons-container text-center shadow-sm">
            <a href="<?=$url['back']?>">
                <span class="float-left">
                    <img src="<?=$img['back']?>" alt="Back Icon">
                </span>
            </a>
            <span class="title">스튜디오 추가</span>
            <a href="#">
            <span class="float-right menu-open closed">
                        <!-- <img src="../icons/menu.svg" alt="Menu Hamburger Icon"> -->
                        <button class="btn btn-outline-primary top-button">메뉴</button>
                    </span>
            </a>
        </div>
        <!--Page Title & Icons End-->

        <div class="rest-container">

            <!--Profile Information Container Start-->
            <div class="text-center header-icon-logo-margin">

                <!-- <div class="profile-picture-container">
                    <img src="../images/avatar.svg" alt="Profile Picture">
                    <span class="fas fa-camera">
                         <input class="file-prompt" type="file" accept="image/*">
                    </span>
                </div> -->

                <div class="display-flex flex-column">
                    <span class="profile-name">스튜디오 추가</span>
                    <!-- <span class="profile-email font-weight-light">nata_vacheishvili@gurucar.com</span> -->
                </div>
            </div>
            <!--Profile Information Container End-->

            <!--Profile Information Fields Container Start-->
            <div class="sign-up-form-container text-center">
                <form method="post" id="saveForm" name="saveStudio" action="<?=$url['save']?>" class="width-100">

                    <!--Profile Field Container Start-->
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span><img src="<?=$img['register']?>" alt="Register"></span>
                            </div>
                            <input class="form-control" type="text" name="studioName" placeholder="스튜디오 이름">
                        </div>
                    </div>
                    <!--Profile Field Container End-->

                    <!--Profile Field Container Start-->
                    <!--Profile Field Container End-->

                    <!--Profile Field Container Start-->
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span> <img src="<?=$img['phone']?>" alt="Phone"> </span>
                            </div>
                            <!-- <textarea class="form-control"  name="" id="" cols="30" rows="10"></textarea> -->

                            <input class="form-control" type="text" autocomplete="off" name="chargePhone" placeholder="스튜디오 대표 전화번호">
                        </div>
                    </div>
                    <!--Profile Field Container End-->
                    
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span> <img src="<?=$img['address']?>" alt="Phone"> </span>
                            </div>

                            <div id="postCodeLayer" style="display:none;position:fixed;overflow:hidden;z-index:1;-webkit-overflow-scrolling:touch;"></div>
                            <input id="addressInput" class="form-control" type="text" autocomplete="off" name="address" placeholder="스튜디오 주소를 입력 해주세요!">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span> <img src="<?=$img['register']?>" alt="Phone"> </span>
                            </div>
                            <input id="explain" class="form-control" type="text" autocomplete="off" name="explain" placeholder="스튜디오를 설명해주세요!">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span> <img src="<?=$img['avater']?>" alt="Phone"> </span>
                            </div>
                            <input class="form-control" type="text" autocomplete="off" name="enterpriseCode" placeholder="스튜디오 사업자 번호">
                        </div>
                    </div>

                    <!-- <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span> <img src="<?=$img['avater']?>" alt="Phone"> </span>
                            </div>
                            <input value="감성,인스타,고급진" id="hashTag" class="form-control" type="text" autocomplete="off" name="hashTag" data-role="tagsinput" placeholder="해시태그">
                        </div>
                    </div> -->
                    
                    <input type="hidden" value="<?=$data['csrf']['hash']?>" name="<?=$data['csrf']['name']?>">
                    <div class="form-submit-button">
                        <button href="index.html" class="btn btn-dark text-uppercase ">Save <span class="far fa-save margin-left-10"></span></button>
                    </div>
                </form>
            </div>
            <!--Profile Information Fields Container End-->

        </div>
    </div>

    <!--Terms And Conditions Agreement Container Start-->
    <div class="col-xs-12 col-sm-12 text-center sms-rate-text font-roboto flex-end margin-bottom-30">
        <div class="container-sms-rate-text width-100 font-11">
            <span class="light-gray font-weight-light">By signing up you have agreed to our </span>
            <br/>
            <a href="#" class="dark-link">
                <span class="font-weight-light">Terms of Use & Privacy Policy</span>
            </a>
        </div>
    </div>
    
    <?php $this->load->view('common/slideMenu', renderSlideMenu());?>


</div>