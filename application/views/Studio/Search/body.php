<body>
    
    <!--Loading Container Start-->
    <!-- <div id="load" class="loading-overlay display-flex flex-column justify-content-center align-items-center">
        <div class="primary-color font-28 fas fa-spinner fa-spin"></div>
    </div> -->
    <!--Loading Container End-->

    <div class="row h-100">
        <div class="col-xs-12 col-sm-12 remaining-height">

            <!--Page Title & Icons Start-->
            <div class="header-icons-container text-center position-relative shadow-sm">
                <a href="<?=$url['back']?>">
                    <span class="float-left">
                        <img src="../icons/back.svg" alt="Back Icon">
                    </span>
                </a>
                <span class="title">Notifications</span>
                <a href="<?=$url['back']?>">
                <span class="float-right menu-open closed">
                        <!-- <img src="../icons/menu.svg" alt="Menu Hamburger Icon"> -->
                        <button class="btn btn-outline-primary top-button">메뉴</button>
                    </span>
                </a>
            </div>
            <!--Page Title & Icons End-->

            <!--Notification/Support Navigation Container Start-->
            <div class="navigation-container">
                <ul class="nav nav-tabs" id="navigation-tabs" role="tablist">
                    <li class="nav-item" style="width: 100%;">
                        <a class="nav-link active" data-toggle="tab" role="tab" id="profile-tab" href="#notifications">
                            검색결과 (<span class="pink"><?=$data['searchCount']?></span>)
                        </a>
                    </li>
                </ul>
            </div>
            <!--Notification/Support Navigation Container End-->

            <div class="tab-content h-100 overflow-scroll font-roboto" id="navigation-tabs-content">
                <!--Notification Container Start-->
                <div class="tab-pane fade show active all-support-team-messages" id="notifications" role="tabpanel">
                    <!--Notification Message Container Start-->
                    <?php foreach ($data['searchRlts'] as $result): ?>
                    <a href="<?=$result['reserveUrl']?>">
                        <div class="support-team-message-container display-flex">
                            <div class="message-information display-flex">
                                <div class="float-left message-content">
                                    <div class="name"><?=$result['studioName']?></div>
                                    <div class="message-description cut font-roboto-condensed font-italic">
                                        <span><?=$result['comment']?></span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    
                    </a>
                    
                    <?php endforeach;?>
                    <!--Notification Message Container End-->
                </div>

            </div>

        </div>

    </div>