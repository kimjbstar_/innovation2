<!DOCTYPE html>
<html lang="ko">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>미래인사혁신관</title>
    <meta name="description" content="미래인사혁신관" />
    <meta name="keywords" content="미래인사혁신관" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="" />
    <meta property="og:title" content="미래인사혁신관" />
    <meta property="og:description" content="미래인사혁신관" />
    <link rel="stylesheet" href="/static/css/style.css">
    <!--<link rel="stylesheet" href="/static/css/mobile.css">-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <script type="text/javascript" src="https://unpkg.com/swiper@5.4.5/js/swiper.min.js"></script>
</head>

<body>
    <div class="container">
        <div id="header">
            <div class="top">
                <div class="inner">
                    <a href="#" class="upper"><span class="blind">인사혁신처</span></a>
                    <a href="#" class="btn_login">회원가입ㆍ로그인</a>
                </div>
            </div>
            <div class="bottom">
                <div class="inner">
                    <a href="#" class="logo"><span class="blind">2020 온라인 공직박람회</span></a>
                    <ul>
                        <li>
                            <a href="#">개막관</a>
                        </li>
                        <li>
                            <a href="#">주제관</a>
                        </li>
                        <li>
                            <a href="#">채용정보관</a>
                        </li>
                        <li>
                            <a href="#">참여 프로그램</a>
                        </li>
                        <li>
                            <a href="#">소통ㆍ이벤트</a>
                        </li>
                        <li>
                            <a href="#">인재혁신세미나</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="innovation_wrap">
                <div class="section" style="background-image: url(/static/img/innovation/1-bg01.png);">
                    <h2>미래인사혁신관</h2>
                    <div class="top_bg">
                        <a href="#section1">미래인재</a>
                        <a href="#section2">인재개발</a>
                        <a href="#section3">국가인재 DB</a>
                    </div>
                </div>
                <div class="section" id="section1">
                    <h2 class="mint"><span>미래인재</span></h2>
                    <span class="tit_ubar mint"></span>
                    <div class="cont_wrap">
                        <div class="box_tab mint" id="tab_05">
                            <ul>
                                <li>
                                    <button class="selected" data-tab="tab_05_tab1">새롭게 직렬, 직류 개편!</button>
                                </li>
                                <li>
                                    <button data-tab="tab_05_tab2">신설 직렬, 직류 자세히 알아보기</button>
                                </li>
                            </ul>
                            <div class="tab_cont">
                                <div class="clear">
                                    <div class="open" id="tab_05_tab1">
                                        <div class="video_clip large">
                                            <div class="video"></div>
                                        </div>
                                    </div>
                                    <div id="tab_05_tab2">
                                        <div class="inner_tab" id="tab_06">
                                            <ul>
                                                <li>
                                                    <button class="selected" data-tab="tab_06_tab1">데이터 직류</button>
                                                </li>
                                                <li>
                                                    <button data-tab="tab_06_tab2">방재안전연구 직렬</button>
                                                </li>
                                            </ul>
                                            <div class="inner_tab_cont">
                                                <div>
                                                    <div class="open" id="tab_06_tab1">
                                                        <div class="inner_slider slider06">
                                                            <div class="nav">
                                                                <button class="prev"><span class="blind">이전</span></button>
                                                                <button class="next"><span class="blind">다음</span></button>
                                                            </div>
                                                            <div class="swiper-wrapper">
                                                                <div class="swiper-slide">
                                                                    <img src="/static/img/innovation/1-cont02.png" alt="" />
                                                                </div>
                                                                <div class="swiper-slide">
                                                                    <img src="/static/img/innovation/1-cont03.png" alt="" />
                                                                </div>
                                                                <div class="swiper-slide">
                                                                    <img src="/static/img/innovation/1-cont04.png" alt="" />
                                                                </div>
                                                                <div class="swiper-slide">
                                                                    <img src="/static/img/innovation/1-cont05.png" alt="" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="tab_06_tab2">
                                                        <div class="inner_slider slider07">
                                                            <div class="nav">
                                                                <button class="prev"><span class="blind">이전</span></button>
                                                                <button class="next"><span class="blind">다음</span></button>
                                                            </div>
                                                            <div class="swiper-wrapper">
                                                                <div class="swiper-slide">
                                                                    <img src="/static/img/innovation/1-cont06.png" alt="" />
                                                                </div>
                                                                <div class="swiper-slide">
                                                                    <img src="/static/img/innovation/1-cont07.png" alt="" />
                                                                </div>
                                                                <div class="swiper-slide">
                                                                    <img src="/static/img/innovation/1-cont08.png" alt="" />
                                                                </div>
                                                                <div class="swiper-slide">
                                                                    <img src="/static/img/innovation/1-cont09.png" alt="" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section" id="section2">
                    <h2 class="mint"><span>인재개발</span></h2>
                    <span class="tit_ubar mint"></span>
                    <div class="cont_wrap">
                        <div class="box_tab mint" id="tab_01">
                            <ul>
                                <li>
                                    <button class="selected" data-tab="tab_01_tab1">인재개발플랫폼</button>
                                </li>
                                <li>
                                    <button data-tab="tab_01_tab2">국가공무원인재개발원</button>
                                </li>
                            </ul>
                            <div class="tab_cont_mid" style="background-color: #57B6CE; padding: 20px;">
                            </div>
                            <div class="tab_cont">
                                <div class="clear">
                                    <div class="open" id="tab_01_tab1">
                                        <div class="video_clip large" style="margin-top: 0px;">
                                            <div class="video"></div>
                                        </div>
                                        <div class="inner_slider slider01">
                                            <div class="nav">
                                                <button class="prev"><span class="blind">이전</span></button>
                                                <button class="next"><span class="blind">다음</span></button>
                                            </div>
                                            <div class="swiper-wrapper">
                                                <div class="swiper-slide">
                                                    <img src="/static/img/innovation/2-cont02.png" alt="" />
                                                </div>
                                                <div class="swiper-slide">
                                                    <img src="/static/img/innovation/2-cont03.png" alt="" />
                                                </div>
                                                <div class="swiper-slide">
                                                    <img src="/static/img/innovation/2-cont04.png" alt="" />
                                                </div>
                                                <div class="swiper-slide">
                                                    <img src="/static/img/innovation/2-cont05.png" alt="" />
                                                </div>
                                                <div class="swiper-slide">
                                                    <img src="/static/img/innovation/2-cont06.png" alt="" />
                                                </div>
                                                <div class="swiper-slide">
                                                    <img src="/static/img/innovation/2-cont07.png" alt="" />
                                                </div>
                                                <div class="swiper-slide">
                                                    <img src="/static/img/innovation/2-cont08.png" alt="" />
                                                </div>
                                                <div class="swiper-slide">
                                                    <img src="/static/img/innovation/2-cont09.png" alt="" />
                                                </div>

                                            </div>
                                        </div>
                                        <div class="inner_tab" id="tab_02">
                                            <ul>
                                                <li>
                                                    <button class="selected" data-tab="tab_02_tab1">비대면 교육</button>
                                                </li>
                                                <li>
                                                    <button data-tab="tab_02_tab2">맞춤형 밀레니얼 콘텐츠 체험하기</button>
                                                </li>
                                            </ul>
                                            <div class="inner_tab_cont">
                                                <div>
                                                    <div class="open" id="tab_02_tab1">
                                                        <div class="video_clip large" style="margin-bottom: 20px;">
                                                            <div class="video"></div>
                                                        </div>
                                                    </div>
                                                    <div id="tab_02_tab2">
                                                        <div class="video_clip large" style="margin-bottom: 20px;">
                                                            <div class="video"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="inner_tab" id="tab_03">
                                            <ul class="button">
                                                <li>
                                                    <button class="selected" data-tab="tab_03_tab1">비대면 온라인 강의는<br />어떻게
                                                        진행될까요?</button>
                                                </li>
                                                <li>
                                                    <button data-tab="tab_03_tab2">비대면 강의를 받아본<br />소감은 어떨까요?</button>
                                                </li>
                                            </ul>
                                            <div class="inner_tab_cont">
                                                <div>
                                                    <div class="open" id="tab_03_tab1">
                                                        <div class="video_clip large" style="margin-bottom: 20px;">
                                                            <div class="video"></div>
                                                        </div>
                                                    </div>
                                                    <div id="tab_03_tab2">
                                                        <div class="video_clip large" style="margin-bottom: 20px;">
                                                            <div class="video"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab_01_tab2">
                                        <div class="video_clip large" style="margin-top: 0px;">
                                            <div class="video"></div>
                                        </div>
                                        <div class="inner_slider slider02">
                                            <div class="nav">
                                                <button class="prev"><span class="blind">이전</span></button>
                                                <button class="next"><span class="blind">다음</span></button>
                                            </div>
                                            <div class="swiper-wrapper">
                                                <div class="swiper-slide">
                                                    <img src="/static/img/innovation/2-cont02.png" alt="" />
                                                </div>
                                                <div class="swiper-slide">
                                                    <img src="/static/img/innovation/2-cont02.png" alt="" />
                                                </div>
                                                <div class="swiper-slide">
                                                    <img src="/static/img/innovation/2-cont02.png" alt="" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section" id="section3">
                    <h2 class="purple"><span>국가인재 DB</span></h2>
                    <span class="tit_ubar purple"></span>
                    <div class="cont_wrap">
                        <div class="box_tab purple col3" id="tab_04">
                            <ul>
                                <li>
                                    <button class="selected" data-tab="tab_04_tab1">국가인재DB란?</button>
                                </li>
                                <li>
                                    <button data-tab="tab_04_tab2">국가인재DB 국민추천제란?</button>
                                </li>
                                <li>
                                    <button data-tab="tab_04_tab3">정부 헤드헌팅이란?</button>
                                </li>
                            </ul>
                            <div class="tab_cont">
                                <div class="clear">
                                    <div class="open" id="tab_04_tab1">
                                        <div class="inner_slider slider03">
                                            <div class="nav">
                                                <button class="prev"><span class="blind">이전</span></button>
                                                <button class="next"><span class="blind">다음</span></button>
                                            </div>
                                            <div class="swiper-wrapper">
                                                <div class="swiper-slide">
                                                    <img src="/static/img/innovation/slide01-8.png" alt="" />
                                                </div>
                                                <div class="swiper-slide">
                                                    <img src="/static/img/innovation/slide01-7.png" alt="" />
                                                </div>
                                                <div class="swiper-slide">
                                                    <img src="/static/img/innovation/slide01-1.png" alt="" />
                                                </div>
                                                <div class="swiper-slide">
                                                    <img src="/static/img/innovation/slide01-2.png" alt="" />
                                                </div>
                                                <div class="swiper-slide">
                                                    <img src="/static/img/innovation/slide01-3.png" alt="" />
                                                </div>
                                                <div class="swiper-slide">
                                                    <img src="/static/img/innovation/slide01-4.png" alt="" />
                                                </div>
                                                <div class="swiper-slide">
                                                    <img src="/static/img/innovation/slide01-5.png" alt="" />
                                                </div>
                                                <div class="swiper-slide">
                                                    <img src="/static/img/innovation/slide01-6.png" alt="" />
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab_04_tab2">
                                        <div class="video_clip large" style="margin-top: 0px;">
                                            <div class="video"></div>
                                        </div>
                                        <div class="inner_tab col3" id="tab_08">
                                            <ul>
                                                <li>
                                                    <button class="selected" data-tab="tab_08_tab1">지연수<p>국립고궁박물관 전시홍보과장</p></button>
                                                </li>
                                                <li>
                                                    <button data-tab="tab_08_tab2">김희경<p>우정공무원교육원장</p></button>
                                                </li>
                                                <li>
                                                    <button data-tab="tab_08_tab3">오창수/이상욱<p>우정사업본부/관세청</p></button>
                                                </li>
                                            </ul>
                                            <div class="inner_tab_cont">
                                                <div>
                                                    <div class="open" id="tab_08_tab1">
                                                        <div class="video_clip large" style="margin-top: 0px;">
                                                            <div class="video"></div>
                                                        </div>
                                                    </div>
                                                    <div id="tab_08_tab2">
                                                        <div class="video_clip large" style="margin-top: 0px;">
                                                            <div class="video"></div>
                                                        </div>
                                                    </div>
                                                    <div id="tab_08_tab3">
                                                        <div class="video_clip large" style="margin-top: 0px;">
                                                            <div class="video"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab_04_tab3">
                                        <div class="inner_slider slider05">
                                            <div class="nav">
                                                <button class="prev"><span class="blind">이전</span></button>
                                                <button class="next"><span class="blind">다음</span></button>
                                            </div>
                                            <div class="swiper-wrapper">
                                                <div class="swiper-slide">
                                                    <img src="/static/img/innovation/slide-2-1.png" alt="" />
                                                </div>
                                                <div class="swiper-slide">
                                                    <img src="/static/img/innovation/slide-2-2.png" alt="" />
                                                </div>
                                                <div class="swiper-slide">
                                                    <img src="/static/img/innovation/slide-2-3.png" alt="" />
                                                </div>
                                                <div class="swiper-slide">
                                                    <img src="/static/img/innovation/slide-2-4.png" alt="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="inner_tab col5" id="tab_09">
                                            <ul>
                                                <li>
                                                    <button class="selected" data-tab="tab_09_tab1">윤지숙<p>통계정 마이크로데이터과장</p></button>
                                                </li>
                                                <li>
                                                    <button data-tab="tab_09_tab2">유선희<p>부산광역시 인재개발원장</p></button>
                                                </li>
                                                <li>
                                                    <button data-tab="tab_09_tab3">김두환<p>건강보험공단 빅데이터센터장</p></button>
                                                </li>
                                                <li>
                                                    <button data-tab="tab_09_tab4">오창수<p>우정사업본부 예금위험관리과장</p></button>
                                                </li>
                                                <li>
                                                    <button data-tab="tab_09_tab5">홍주의<p>한국보훈복지의료공단 홍보실장</p></button>
                                                </li>
                                            </ul>
                                            <div class="inner_tab_cont">
                                                <div>
                                                    <div class="open" id="tab_09_tab1">
                                                        <img src="/static/img/innovation/slide3-1.png" alt="">
                                                    </div>
                                                    <div id="tab_09_tab2">
                                                        <img src="/static/img/innovation/slide3-2.png" alt="">
                                                    </div>
                                                    <div id="tab_09_tab3">
                                                        <img src="/static/img/innovation/slide3-3.png" alt="">
                                                    </div>
                                                    <div id="tab_09_tab4">
                                                        <img src="/static/img/innovation/slide3-4.png" alt="">
                                                    </div>
                                                    <div id="tab_09_tab5">
                                                        <img src="/static/img/innovation/slide3-5.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                

                <div class="section" style="background-image: url(/static/img/innovation/5-bg01.png);">
                    <div class="cont_wrap">
                        <div class="horz_wrap gray">
                            <img src="/static/img/innovation/4-cont01.png" alt="" />
                            <img src="/static/img/innovation/4-cont02.png" alt="" />
                        </div>
                        <div class="quiz gray">
                            <a href="https://docs.google.com/forms/d/e/1FAIpQLSeuB-vJP2P4Kkf2N6Zj_J_wG-FTwKIt_DS4GjxntqWSusokxg/viewform"
                                target="_blank" class="btn_quiz">퀴즈 풀기</a>
                        </div>
                        <img src="/static/img/innovation/4-cont03.png" alt="" class="pc" />
                        <img src="/static/img/mobile/innovation/4-cont03.png" alt="" class="mobile" />
                    </div>
                </div>
            </div>
        </div>
        <div id="footer">
            <div class="inner">
                <div class="top">
                    <a href="#" class="footer_logo"><span class="blind">인사혁신처</span></a>
                    <ul>
                        <li class="fb">
                            <a href="#"><span class="blind">facebook</span></a>
                        </li>
                        <li class="tw">
                            <a href="#"><span class="blind">twitter</span></a>
                        </li>
                        <li class="ig">
                            <a href="#"><span class="blind">instagram</span></a>
                        </li>
                        <li class="yt">
                            <a href="#"><span class="blind">youtube</span></a>
                        </li>
                    </ul>
                </div>
                <div class="bottom">
                    <ul>
                        <li><a href="#">개인정보 처리방침</a></li>
                        <li><a href="#">영상정보처리기 운영ㆍ관리방침</a></li>
                        <li><a href="#">저작권보호 및 이용정책</a></li>
                    </ul>
                    <p>30102 세종특별자치시 한누리대로 499(어진동) 세종포스트빌딩 6층~12층</p>
                    <p>copyright© Ministry of Personnel Management. All Rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
    <script src="/static/js/common.js"></script>
    <script>
        var innerSlider01 = new Swiper('.slider01', {
            loop: false,
            slideActiveClass: 'active',
            navigation: {
                nextEl: '.slider01 .next',
                prevEl: '.slider01 .prev',
            },
            observer: true,
            observeParents: true,
        })

        var innerSlider02 = new Swiper('.slider02', {
            loop: false,
            slideActiveClass: 'active',
            navigation: {
                nextEl: '.slider02 .next',
                prevEl: '.slider02 .prev',
            },
            observer: true,
            observeParents: true,
        })

        var innerSlider03 = new Swiper('.slider03', {
            loop: false,
            slideActiveClass: 'active',
            navigation: {
                nextEl: '.slider03 .next',
                prevEl: '.slider03 .prev',
            },
            observer: true,
            observeParents: true,
        })

        var innerSlider04 = new Swiper('.slider04', {
            loop: false,
            slideActiveClass: 'active',
            navigation: {
                nextEl: '.slider04 .next',
                prevEl: '.slider04 .prev',
            },
            observer: true,
            observeParents: true,
        })

        var innerSlider05 = new Swiper('.slider05', {
            loop: false,
            slideActiveClass: 'active',
            navigation: {
                nextEl: '.slider05 .next',
                prevEl: '.slider05 .prev',
            },
            observer: true,
            observeParents: true,
        })

        var innerSlider06 = new Swiper('.slider06', {
            loop: false,
            slideActiveClass: 'active',
            navigation: {
                nextEl: '.slider06 .next',
                prevEl: '.slider06 .prev',
            },
            observer: true,
            observeParents: true,
        })

        var innerSlider07 = new Swiper('.slider07', {
            loop: false,
            slideActiveClass: 'active',
            navigation: {
                nextEl: '.slider07 .next',
                prevEl: '.slider07 .prev',
            },
            observer: true,
            observeParents: true,
        })

        $('.box_tab ul li button,.inner_tab ul li button').click(function () {
            const sibling = $(this).parent().siblings()
            const target = $(this).attr('data-tab')
            sibling.each(function () {
                $(this).children().removeClass('selected')
            })
            $(this).addClass('selected')
            const wrapper = $(this).closest('div').children('.tab_cont,.inner_tab_cont').children().children()
            wrapper.each(function () {
                $(this).removeClass('open')
            })
            $('#' + target).addClass('open')
        })

        $(document).ready(function () {
            $('a[href^="#"]').on('click', function (e) {
                e.preventDefault();

                var target = this.hash;
                $target = $(target);

                $('html, body').stop().animate({
                    'scrollTop': $target.offset().top
                }, 900, 'swing', function () {
                    window.location.hash = target;
                });
            });
        });










        function playthis(playurl){
                    var play_video = '<iframe width="778" height="447.6" src="https://www.youtube.com/embed/'+playurl+'?autoplay=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                        $($('.video')[2]).html(play_video)
                    // location.href="#playhere";
                }

        document.addEventListener('DOMContentLoaded', function(){

            var play_video = '<iframe width="1280" height="700" src="https://www.youtube.com/embed/Li8xyXT8ZPQ?autoplay=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                $($('.video')[0]).html(play_video) //ws.메인


            var play_video = '<iframe width="1280" height="720" src="https://www.youtube.com/embed/vFyTaAbOFO4?autoplay=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                $($('.video')[1]).html(play_video) //ws.인재개발플랫폼
            var play_video = '<iframe width="1280" height="720" src="https://www.youtube.com/embed/JBovvH_VUEk?autoplay=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                $($('.video')[2]).html(play_video) 




            var play_video = '<iframe width="1040" height="585" src="https://www.youtube.com/embed/Zorn5SoapYI?autoplay=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'; 
            $($('.video')[2]).html(play_video) //ws.비대면교육
            var play_video = '<iframe width="1040" height="585" src="https://www.youtube.com/embed/aqwadBTjPuk?autoplay=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
            $($('.video')[3]).html(play_video) //ws.맞춤형밀레니얼
            var play_video = '<iframe width="1040" height="585" src="https://www.youtube.com/embed/G72TvcbZO1A?autoplay=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
            $($('.video')[4]).html(play_video) //ws.비대면온라인강의는 어떻게 진행될까요?

            var play_video = '<iframe width="1040" height="585" src="https://www.youtube.com/embed/YGur6Bux-HY?autoplay=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'; 
            $($('.video')[5]).html(play_video) //ws.비대면강의를 받아본 소감을 어떨까요?
            var play_video = '<iframe width="1040" height="585" src="https://www.youtube.com/embed/-uNhFC-qaBc?autoplay=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
            $($('.video')[6]).html(play_video)


        });














    </script>
</body>

</html>










