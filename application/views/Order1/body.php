<body>


<?php

// var_dump($data);

?>

<link rel="stylesheet" href="https://quickcar.co.kr/css/common.css?d=<?=md5(date("Y-m-d H:i:s"))?>">
<link rel="stylesheet" href="https://quickcar.co.kr/css/header.css?d=<?=md5(date("Y-m-d H:i:s"))?>">
<link rel="stylesheet" href="https://quickcar.co.kr/css/menu.css?ver=0.0.1">
<link rel="stylesheet" href="https://quickcar.co.kr/css/content.css?d=<?=md5(date("Y-m-d H:i:s"))?>">

<!--Loading Container Start-->
<!-- <div id="load" class="loading-overlay display-flex flex-column justify-content-center align-items-center">
    <div class="primary-color font-28 fas fa-spinner fa-spin"></div>
</div> -->
<!--Loading Container End-->

<div class="row h-100">
    <div class="col-xs-12 col-sm-12">

        <!--Page Title & Icons Start-->
        <div class="header-icons-container text-center shadow-sm">
            <a href="<?=$url['back']?>">
                <span class="float-left">
                    <img src="<?=$img['back']?>" alt="Back Icon">
                </span>
            </a>
            <span class="title">퀵서비스 주문</span>
            <a href="#">
            <span class="float-right menu-open closed">
                        <!-- <img src="../icons/menu.svg" alt="Menu Hamburger Icon"> -->
                        <button class="btn btn-outline-primary top-button">메뉴</button>
                    </span>
            </a>
        </div>
        <!--Page Title & Icons End-->

        <div class="rest-container">
            <div class="content_box margin-rel-b-20" >
                <div class="f">
                    <div class="dest_icon_container">
                        <object data="https://quickcar.co.kr/svg/ic-from-destination.svg" alt="from"></object>
                        <object data="https://quickcar.co.kr/svg/dash.svg" alt="dash"></object>
                        <object data="https://quickcar.co.kr/svg/ic-to-destination.svg" alt="to" class="long_dash"></object>
                    </div>
                    <div class="f f-col w-100">
                        <div class="from_container">
                            <div class="f f-al-center" style="justify-content: start !important;">
                                <p class="font-size-12">출발</p>
                            </div>

                            <?php
                                $osearch = "";
                                $dsearch = "";
                                if ($this->input->get("osearch") == "") {
                                    $osearch = $data['departure'];
                                } else {
                                    $osearch = $this->input->get("osearch");
                                }
                                if ($this->input->get("dsearch") == "") {
                                    $dsearch = $data['destination'];
                                } else {
                                    $dsearch = $this->input->get("dsearch");
                                }

                            ?>
                            <input wrap="hard" cols="20" rows="2" class="margin-rel-b-8 font-bold font-size-16" id="address_start" name="address_start" placeholder="출발지를 입력하세요." value="<?=$data['departure'];?>" readonly>
                            <input wrap="hard" cols="20" rows="2" class="margin-rel-b-8 font-bold font-size-16" id="address_start_detail" name="address_start_detail" placeholder="상세주소를 입력하실 수 있습니다." value="<?=$this->input->get("odetail")?>" >
                            <input type="hidden" id="start_search_address"  value="<?=$osearch?>">
                            <input type="hidden" id="dest_search_address"  value="<?=$dsearch?>">
                            <!-- <input wrap="hard" cols="20" rows="2" class="margin-rel-b-8 font-bold font-size-16" id="address_start_detail" name="address_start_detail" placeholder="상세주소를 입력하실 수 있습니다." value="" > -->
                            <input type="text" id="start_name"  name="start_name" placeholder="출발지 성함을 입력해주세요." value="<?php echo (!isset($data['dep_name']) ? $data['dep_name'] : "앱사용자"); ?>" ><br>
                            <input type="number" id="start_telno"  name="start_telno" placeholder="출발지 연락처를 입력해주세요." value="<?=$data['dep_number'];?>" >
                        </div>
                        <div class="to_container">
                            <div class="f f-al-center" style="flex-wrap:wrap;">
                                <p class="f font-size-12">도착</p>
                            </div>
                            <input wrap="hard" cols="20" rows="2" class="padding-rel-t-12 margin-rel-b-8 font-bold font-size-16" id="address_dest" name="address_dest" placeholder="도착지를 입력하세요." value="<?=$data['destination'];?>"  readonly>
                            <input wrap="hard" cols="20" rows="2" class="margin-rel-b-8 font-bold font-size-16" id="address_dest_detail" name="address_dest_detail" placeholder="상세주소를 입력하실 수 있습니다." value="<?=$this->input->get("ddetail")?>" >
                            <!-- <input wrap="hard" cols="20" rows="2" class="margin-rel-b-8 font-bold font-size-16" id="address_dest_detail" name="address_dest_detail" placeholder="상세주소를 입력하실 수 있습니다." value="" > -->
                            <input type="text" id="dest_name"  name="dest_name" placeholder="도착지 성함을 입력해주세요." value="<?=$data['dest_name'];?>"><br>
                            <input type="number" id="dest_telno"  name="dest_telno" placeholder="도착지 연락처를 입력해주세요." value="<?=$data['dest_number'];?>">
                        </div>
                    </div>
                </div>
            </div>


            <div class="content_box margin-rel-b-20">
                <p class="content_title margin-rel-b-26" id="title-vehicle"><strong>어떤 차가 필요하신가요?</strong></p>
                <div class="f f-col f-al-center w-100 which_car">
                    <div class="type_select_menu t_transport margin-rel-b-17">
                        <div class="SYS-kind type_select" data-kind="1">
                            <img src="/img/icon/ic-category-motorcycle@2x.png" alt="motorcycle">
                            <p>오토바이</p>
                        </div>
                        <div class="SYS-kind type_select" data-kind="2">
                            <img src="/img/icon/ic-category-damas@2x.png" alt="damas">
                            <p>다마스</p>
                        </div>
                        <div class="SYS-kind type_select selected" data-kind="5">
                            <img src="/img/icon/ic-category-labo@2x.png" alt="labo">
                            <p>라보</p>
                        </div>
                        <!-- <div class="SYS-kind type_select" data-kind="4">
                            <img src="/img/icon/ic-category-van@2x.png" alt="van">
                            <p>밴</p>
                        </div> -->
                        <div class="SYS-kind type_select" data-kind="3">
                            <img src="/img/icon/ic-category-truck@2x.png" alt="truck">
                            <p>1톤</p>
                        </div>
                        <div class="SYS-kind type_select" data-kind="3">
                            <img src="/img/icon/ic-category-truck@2x.png" alt="truck">
                            <p>화물</p>
                        </div>
                    </div>
                    <div class="data-kind-1 content_gray_inner_box hide">
                        <p class="content_gray_title margin-rel-b-8">30kg 이하 가능</p>
                        <p class="margin-rel-b-8">오토바이의 경우 30Kg 초과 시 추가 요금이 부과될 수 있습니다.</p>
                    </div>
                    <div class="data-kind-2 content_gray_inner_box hide" data-motorcycle_weight="350kg">
                        <p class="content_gray_title margin-rel-b-8">350kg 이하 가능</p>
                        <!-- <p class="margin-rel-b-8">가로 160cm 폭 110cm 높이 110cm 물품이 많으면 상하차 비용 별도 부과 될 수 있습니다</p> -->
                        <!-- <p class="margin-rel-b-8">계단 유무 등 상하차 관련 추가요금 발생할 수 있습니다.</p> -->
                        <p class="margin-rel-b-8"><img src="/img/damas.jpg" style="width:100%"></p>
                        <p class="margin-rel-b-8">물품이 많거나 계단유무 등에 따라 상하차관련 추가요금 발생할 수 있습니다.</p>
                    </div>
                    <div class="data-kind-5 content_gray_inner_box" data-motorcycle_weight="400kg">
                        <p class="content_gray_title margin-rel-b-8">400kg 이하 가능</p>
                        <!-- <p class="margin-rel-b-8">가로 210cm 폭140cm 물품이 많으면 상하차 비용 별도 부과 될 수 있습니다.</p> -->
                        <!-- <p class="margin-rel-b-8">계단 유무 등 상하차 관련 추가요금 발생할 수 있습니다.</p> -->
                        <p class="margin-rel-b-8"><img src="/img/labo.jpg" style="width:100%"></p>
                        <p class="margin-rel-b-8">물품이 많거나 계단유무 등에 따라 상하차관련 추가요금 발생할 수 있습니다.</p>
                    </div>
                    <!-- <div class="data-kind-4 content_gray_inner_box hide" data-motorcycle_weight="400kg">
                        <p class="content_gray_title margin-rel-b-8">400kg 이하 가능</p>
                    </div> -->
                    <div class="data-kind-3 content_gray_inner_box hide" data-motorcycle_weight="1.4t">
                        <p class="content_gray_title margin-rel-b-8">1t 이하 가능</p>
                        <!-- <p class="margin-rel-b-8">가로 280cm 폭 160cm 상하차 도움비 별도 부과 될 수 있습니다.</p> -->
                        <!-- <p class="margin-rel-b-8">계단 유무 등 상하차 관련 추가요금 발생할 수 있습니다.</p> -->
                        <p class="margin-rel-b-8"><img src="/img/1t.jpg" style="width:100%"></p>
                        <p class="margin-rel-b-8">물품이 많거나 계단유무 등에 따라 상하차관련 추가요금 발생할 수 있습니다.</p>
                    </div>
                    <div class="data-kind-3 content_gray_inner_box hide" data-motorcycle_weight="1.4t">
                    <a href="tel:1661-2482"><p class="content_gray_title margin-rel-b-8">1661-2482 전화문의 바랍니다.</p><p>(터치하여 전화연결)</p></a>
                    </div>
                </div>
            </div>



            <div class="content_box margin-rel-b-20">
                <p class="content_title margin-rel-b-26" id="title-product"><strong>보내시는 물건은 무엇인가요?</strong></p>
                <div class="f f-col f-al-center w-100 which_product">
                    <div class="type_select_menu t_product">
                        <div class="SYS-item_type type_select" data-item_type="1">
                            <img src="/img/icon/ic-item-post@2x.png" alt="post">
                            <p>서류</p>
                        </div>
                        <div class="SYS-item_type type_select" data-item_type="2">
                            <img src="/img/icon/ic-item-shopback@2x.png" alt="small">
                            <p>쇼핑백</p>
                        </div>
                        <div class="SYS-item_type type_select" data-item_type="2">
                            <img src="/img/icon/ic-item-box-small@2x.png" alt="small">
                            <p>소박스</p>
                        </div>
                        <div class="SYS-item_type type_select" data-item_type="4">
                            <img src="/img/icon/ic-item-box-large@2x.png" alt="large">
                            <p>대박스</p>
                        </div>
                        <div class="SYS-item_type type_select" data-item_type="4" data-itemtype="other">
                            <img src="/img/icon/ic-item-others@2x.png" alt="other">
                            <p>기타</p>
                        </div>
                    </div>


                    <div class="data-kind-1 content_gray_inner_box hide">
                        <p class="margin-rel-b-8">개수를 선택해 주세요.</p>
                        <ul class="text_select">
                            <li class="btn-motorcycle_weight" data-motorcycle_weight="1"><p>1개</p></li>
                            <li class="btn-motorcycle_weight" data-motorcycle_weight="2"><p>2개</p></li>
                            <li class="btn-motorcycle_weight" data-motorcycle_weight="3"><p>3개</p></li>
                        </ul>
                    </div>

                    <div class="data-kind-1 content_gray_inner_box hide">
                        <p class="margin-rel-b-8">개수를 선택해 주세요.</p>
                        <ul class="text_select">
                            <li class="btn-motorcycle_weight" data-motorcycle_weight="1"><p>1개</p></li>
                            <li class="btn-motorcycle_weight" data-motorcycle_weight="2"><p>2개</p></li>
                            <li class="btn-motorcycle_weight" data-motorcycle_weight="3"><p>3개</p></li>
                        </ul>
                    </div>

                    <div class="data-kind-1 content_gray_inner_box hide">
                        <p class="margin-rel-b-8">개수를 선택해 주세요.</p>
                        <ul class="text_select">
                            <li class="btn-motorcycle_weight" data-motorcycle_weight="1"><p>1개</p></li>
                            <li class="btn-motorcycle_weight" data-motorcycle_weight="2"><p>2개</p></li>
                        </ul>
                    </div>

                    <div class="data-kind-1 content_gray_inner_box hide">
                        <p class="margin-rel-b-8">어떤 물건인지 알려주세요.</p>
                        <ul class="text_select">
                            <input type="text" id="product"  name="start_telno" placeholder="보내시는 물건이 무엇인가요?" value=""  style="height: 50px;padding-left: 10px;border: 1px solid green;">
                        </ul>
                    </div>


                    <div class="data-kind-1 content_gray_inner_box hide">
                        <p class="margin-rel-b-8">개수를 작성해 주세요.</p>
                        <ul class="text_select">
                            <input type="number" id="product_count"  name="start_telno" placeholder="개수를 작성해 주세요?" value=""  style="width:calc(100% - 0.05rem);  height: 50px;padding-left: 10px;border: 1px solid green;">
                        </ul>
                    </div>
                </div>
            </div>







            <div class="content_box margin-rel-b-20" style="margin-bottom: 50px !important;">
                <p class="content_title margin-rel-b-12"><span style="color: #d00000;"><strong>기타요청사항을 입력해주세요.</strong></span></p>
                <textarea id="memo_add" name="memo_add" rows="4" class="border_txtarea_big" style="height: 4.29rem;" placeholder="예약시간 또는 짐의 종류나 기타 요청 사항을 적어주세요."></textarea>
            </div>


            <div class="bottom_bar" style="position:relative !important;">
                <div class="calc-progress" style="position: absolute; width: calc(100% - 2.24rem); height: 55px; line-height: 55px; background: rgb(3, 207, 93); border-radius: 10px; color: rgb(255, 255, 255); text-align: center; vertical-align: middle; z-index: 100; font-size: x-large; bottom: 10px; display: none;">운임료 계산중입니다...</div>
                <div id="block-price" class="f f-col">
                    <div class="block-price-itm f f-ju-between f-al-center margin-rel-b-8" style="width:100%;">
                        <div class="f">
                            <img src="/svg/ic-receipt.svg" class="margin-rel-r-9" alt="receipt">
                            <p class="text-title">기본 운임</p>
                        </div>
                        <p id="block-pay_price" class="font-bold font-size-16 font-color-accent">
                            <span class="text-pay_price-detail" style="margin-right: 5px;font-size: 0.89rem;"></span>
                            <span class="text-pay_price" style="font-weight: bold;">0원</span>
                        </p>
                    </div>
                    <div class="block-price-itm f f-ju-between f-al-center margin-rel-b-8" id="addprice" style="width:100%; display:none;">
                        <div class="f">
                            <img src="/svg/ic-receipt.svg" class="margin-rel-r-9" alt="receipt">
                            <p class="text-title">할증 요금</p>
                        </div>
                        <p id="block-pay_price" class="font-bold font-size-16 font-color-accent">
                            <span class="text-pay_price-detail" style="margin-right: 5px;font-size: 0.89rem;"></span>
                            <span class="text-pay_price" style="font-weight: bold;">0원</span>
                        </p>
                    </div>                    
                </div>
                <button onclick="do_order()" id="btn-Confirm" class="btn green w-100"><p>계속하기</p></button>
            </div>


        </div>
    </div>

    
    <?php $this->load->view('common/slideMenu', renderSlideMenu());?>


</div>



<script src="https://quickcar.co.kr/js/lib/jquery.js?lastModify=38b5fe31dc483f36ca2e7aa78545b53f"></script>

<script>

    var vehicle=0;
    var product='';
    var weight='';
    var count_p=0;
    var insert_count_p_num=[];
    var etc_p ="";
    var price=0;
    var add_price=0;
    var distance=0;
    var start_adr="";
    var dest_adr="";


    function timeout(ms, promise) {
        return new Promise(function(resolve, reject) {
            setTimeout(function() {
                reject(new Error("timeout"));
            }, ms)
            promise.then(resolve, reject)
        })
    }

    function get_fee() {
        
        if (vehicle!=0 && vehicle!=-1 &&  product != '' ){

            start_adr=$('#address_start').val();
            dest_adr=$('#address_dest').val();
            start_detail=$('#address_start_detail').val();
            dest_detail=$('#address_dest_detail').val();
            var etc_p = $('#product').val();
            var memo =$('#memo_add').val();



            osearch = $('#start_search_address').val(); 
            dsearch = $('#dest_search_address').val();

            // if(osearch == ""){
            //     osearch = start_adr;
            // } else if (dsearch == ""){
            //     dsearch = dest_adr;
            // }
            

            console.log("osearch = ", osearch);
            console.log("dsearch = ", dsearch);
            $('.calc-progress').css('display','block');

            //console.log('https://quickcar.co.kr/Order/getprice?kind='+vehicle+'&doc=1&start='+start_adr+'&end='+dest_adr+'&count_p='+count_p+'&etc_p='+etc_p+'&memo='+memo+'&product='+product+'&start_detail='+start_detail+'&dest_detail='+dest_detail);

            //fetch('https://quickcar.co.kr/Order/getprice?kind='+vehicle+'&doc=1&start='+start_adr+'&end='+dest_adr+'&count_p='+count_p+'&etc_p='+etc_p+'&memo='+memo+'&product='+product+'&start_detail='+start_detail+'&dest_detail='+dest_detail)
            //fetch('/Order/getprice?kind='+vehicle+'&doc=1&start='+start_adr+'&end='+dest_adr+'&count_p='+count_p+'&etc_p='+etc_p+'&memo='+memo+'&product='+product+'&start_detail='+start_detail+'&dest_detail='+dest_detail)
/*

            timeout(1000, fetch('/hello')).then(function(response) {
                // process response
            }).catch(function(error) {
                // might be a timeout error
            })
*/


            timeout(600000, fetch('/Order/getprice?kind='+vehicle+'&doc=1&start='+osearch+'&end='+dsearch+'&count_p='+count_p+'&etc_p='+etc_p+'&memo='+memo+'&product='+product+'&start_detail='+start_detail+'&dest_detail='+dest_detail)) 
            .then(function(response) {
                return response.json();
            })
            .then(function(myJson) {
                console.log(myJson['price']);
                console.log(myJson['add_price']);
                console.log(JSON.stringify(myJson));
                $($('.text-pay_price')[0]).text((formatNumber(myJson['price']))+'원')
                $($('.text-pay_price')[1]).text((formatNumber(myJson['add_price']))+'원')
                if (myJson['add_price']!=0){
                    $('#addprice').css('display','flex');
                }
                else{
                    $('#addprice').css('display','none');
                }

                price=myJson['price'];
                add_price=myJson['add_price'];
                if (distance==0){
                    UTIL.alert("거리 : "+myJson['distance']+ " Km")
                }

                distance=myJson['distance'];
                $('.calc-progress').css('display','none');
            }).catch(function(error) {
                // might be a timeout error
                Swal.fire({
                    title: '네트워크에 일시적으로 오류가 있습니다. 다시 시도 부탁드립니다.',
                    // text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '주문하기'
                    }).then((result) => {
                    if (result.value) {
                       location.replace('/Main');
                    }
                    })
            })
                       
            /*
            fetch('/Order/getprice?kind='+vehicle+'&doc=1&start='+osearch+'&end='+dsearch+'&count_p='+count_p+'&etc_p='+etc_p+'&memo='+memo+'&product='+product+'&start_detail='+start_detail+'&dest_detail='+dest_detail)
            .then(function(response) {
                return response.json();
            })
            .then(function(myJson) {
                console.log(myJson['price']);
                console.log(myJson['add_price']);
                console.log(JSON.stringify(myJson));
                $($('.text-pay_price')[0]).text((formatNumber(myJson['price']))+'원')
                $($('.text-pay_price')[1]).text((formatNumber(myJson['add_price']))+'원')
                if (myJson['add_price']!=0){
                    $('#addprice').css('display','flex');
                }
                else{
                    $('#addprice').css('display','none');
                }

                price=myJson['price'];
                add_price=myJson['add_price'];
                if (distance==0){
                    UTIL.alert("거리 : "+myJson['distance']+ " Km")
                }

                distance=myJson['distance'];
                $('.calc-progress').css('display','none');
            })
            */


        }
        else{

            $($('.text-pay_price')[0]).text('0원')
            $($('.text-pay_price')[1]).text('0원')
            $('#addprice').css('display','none');
        }

    }





    $(document).ready(function(){


        $('#addprice').css('display','none');
        $('.t_transport .type_select').removeClass( 'selected' );
        $('.content_gray_inner_box').css('display','none');

        $('.t_product .type_select').click(function(){
            console.log($($(this).children('p')[0]).text());
            $('.t_product .type_select').removeClass( 'selected' );
            $(this).addClass('selected');
            product=$($(this).children('p')[0]).text();


            $('.which_product .content_gray_inner_box').css('display','none');
            $($('.which_product .content_gray_inner_box')[$(this).index()-1]).css('display','block');


            // dvmoomoodv test
            // var vehicletext=$($(this).children('p')[0]).text();
            console.log("선택된 차량 = ", vehicle);
            if(vehicle == 3){
                $('.which_product .content_gray_inner_box').css('display','none');
                $($('.which_product .content_gray_inner_box')[4]).css('display','block');
            }
            


            get_fee();
        });



        $('.t_transport .type_select').click(function(){
            console.log($($(this).children('p')[0]).text());
            $('.t_transport .type_select').removeClass( 'selected' );
            $(this).addClass('selected');
            $('.which_car .content_gray_inner_box').css('display','none');
            $($('.which_car .content_gray_inner_box')[$(this).index()]).css('display','block');

            var vehicletext=$($(this).children('p')[0]).text();

            // *     'kind'  => '배송수단', (1(오토) / 2(다마스) / 5(라보) / 4(밴) / 6(지하철) / 3(트럭)),
            if (vehicletext=='오토바이'){
                vehicle=1;
                insert_count_p_num=[3,3,2];
            }
            else if (vehicletext=='다마스'){
                vehicle=2;
                insert_count_p_num=[0,20,10];
            }
            else if (vehicletext=='라보'){
                vehicle=5;
                insert_count_p_num=[0,25,20];
            }
            else if (vehicletext=='1톤'){
                insert_count_p_num=[0,0,0];
                vehicle=3;
            }
            else if (vehicletext=='화물'){
                insert_count_p_num=[0,0,0];
                vehicle=-1;
            }


            var addstring="";
            for (var i=0;i<insert_count_p_num[0];i++)
                {
                    addstring+=`<li class="btn-motorcycle_weight" data-motorcycle_weight="`+(i+1)+`"><p>`+(i+1)+`개</p></li>`;
                }
            $($($('.which_product .content_gray_inner_box')[0]).find('.text_select')).html(addstring);


            var addstring="";
            for (var i=0;i<insert_count_p_num[1];i++)
                {
                    addstring+=`<li class="btn-motorcycle_weight" data-motorcycle_weight="`+(i+1)+`"><p>`+(i+1)+`개</p></li>`;
                }
            $($($('.which_product .content_gray_inner_box')[1]).find('.text_select')).html(addstring);


            var addstring="";
            for (var i=0;i<insert_count_p_num[2];i++)
                {
                    addstring+=`<li class="btn-motorcycle_weight" data-motorcycle_weight="`+(i+1)+`"><p>`+(i+1)+`개</p></li>`;
                }
            $($($('.which_product .content_gray_inner_box')[2]).find('.text_select')).html(addstring);


            // test
            // $('.which_product  .content_gray_inner_box')[3].css('display', 'block');



            $('.btn-motorcycle_weight').removeClass( 'selected' );


            $('.which_product .btn-motorcycle_weight').click(function(){
                console.log($(this).data('motorcycle_weight'));
                $('.btn-motorcycle_weight').removeClass( 'selected' );
                $(this).addClass('selected');
                count_p = $(this).data('motorcycle_weight');
                get_fee();
            });

            count_p=0;


            get_fee();

        });



        $('.which_car .btn-motorcycle_weight').click(function(){
            console.log($(this).data('motorcycle_weight'));
            $('.btn-motorcycle_weight').removeClass( 'selected' );
            $(this).addClass('selected');

            weight=$(this).data('motorcycle_weight');


            get_fee();
        });



        $('.which_product .btn-motorcycle_weight').click(function(){
            console.log($(this).data('motorcycle_weight'));
            $('.btn-motorcycle_weight').removeClass( 'selected' );
            $(this).addClass('selected');

            count_p = $(this).data('motorcycle_weight');


            get_fee();
        });




    });

    function formatNumber (num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }


    function do_order(){

            // *     'kind'  => '배송수단', (1(오토) / 2(다마스) / 5(라보) / 4(밴) / 6(지하철) / 3(트럭)),

        var start_adr=$('#address_start').val();
        var dest_adr=$('#address_dest').val();
        var etc_p = $('#product').val();
        // dvmoomoodv 1톤 트럭용 카운트 
        var etc_p_count = $('#product_count').val();
        var memo =$('#memo_add').val();


        var start_name = $('#start_name').val()
        var start_telno = $('#start_telno').val()
        var dest_telno = $('#dest_telno').val()
        var dest_name = $('#dest_name').val()



        if (start_telno.substr(0, 1) !== '0' || dest_telno.substr(0, 1) !== '0') {
            UTIL.alert('정확한 전화번호를 입력 해주세요.', () => {
                window.scrollTo(0,0);
                const $val = [$('#start_telno'), $('#dest_telno')];
                for (let v in $val) {
                    if ($val[v].val().substr(0, 1) !== '0') {
                        $val[v].focus();
                        break;
                    }
                }
            });


        }
        else if (price==0){
            UTIL.alert('요금 산정이 되지 않았습니다. 보내시는 물건을 다시 클릭해주세요.');
        }
        else if (vehicle==-1){
            UTIL.alert('화물 물건은 전화주문 부탁드립니다.');
        }
        else if (vehicle==0){
            location.href="#title-vehicle";
            UTIL.alert('차종을 선택해주세요.');
        }
        else if (product==''){
            location.href="#title-product";
            UTIL.alert('보내시는 물품을 선택해주세요.');
        }
        else if ($('#address_start').val()==""){
            $('#address_start').focus();
            UTIL.alert('출발지를 입력해주세요.');
        }
        else if ($('#address_dest').val()==""){
            $('#address_dest').focus();
            UTIL.alert('도착지를 입력해주세요.');
        }
        else if ($('#start_name').val()==""){
            $('#start_name').focus();
            UTIL.alert('출발지 성함을 입력해주세요.');
        }
        else if ($('#start_telno').val()==""){
            $('#start_telno').focus();
            UTIL.alert('출발지 연락처를 입력해주세요.');
        }
        else if ($('#dest_name').val()==""){
            $('#dest_name').focus();
            UTIL.alert('도착지 성함을 입력해주세요.');
        }
        else if ($('#dest_telno').val()==""){
            $('#dest_telno').focus();
            UTIL.alert('도착지 연락처를 입력해주세요.');
        }

        // else if (weight=='' && vehicle==1){
        //     UTIL.alert('물품의 개수를 선택해주세요.')
        // }


        else if (vehicle==1 && ( product=="쇼핑백" || product=="소박스" || product=="대박스" ) && count_p==0){
            UTIL.alert('물품의 개수를 선택해주세요.');
        }
        else if (vehicle==2 && ( product=="소박스" || product=="대박스" ) && count_p==0){
            UTIL.alert('물품의 개수를 선택해주세요.');
        }
        else if (vehicle==5 && ( product=="소박스" || product=="대박스" ) && count_p==0){
            UTIL.alert('물품의 개수를 선택해주세요.');
        }


        else if ( product=="기타" && etc_p==""){
            UTIL.alert('어떤 물품을 보내시나요?');
        }

        // dvmoomoodv 1톤 트럭 조건 추가 
        else if(vehicle == 3 && etc_p_count == ""){
            UTIL.alert('어떤 물품을 보내시나요?');
        }

        else{
            //dvmoomoodv 1톤 트럭용 변수 적용 
            // 화면 전환시 기존에 쓰이는 etc_p에 대입
            etc_p = etc_p_count;
            location.replace ('/order/order2?kind='+vehicle+'&doc=1&start='+start_adr+'&end='+dest_adr+'&count_p='+count_p+'&etc_p='+etc_p+'&memo='+memo+'&price='+price+'&add_price='+add_price+'&start_name='+start_name+'&start_telno='+start_telno+'&dest_telno='+dest_telno+'&dest_name='+dest_name+'&product='+product);
            //location.replace ('https://quickcar.co.kr/order/order2?kind='+vehicle+'&doc=1&start='+start_adr+'&end='+dest_adr+'&count_p='+count_p+'&etc_p='+etc_p+'&memo='+memo+'&price='+price+'&add_price='+add_price+'&start_name='+start_name+'&start_telno='+start_telno+'&dest_telno='+dest_telno+'&dest_name='+dest_name+'&product='+product);
        }

                // 'user_id'=&c_name=&c_mobile=&s_start=&start_telno=&start_sido=&start_gugun=&start_dong=&s_dest=&dest_telno=&dest_sido=&dest_gugun=&dest_dong=&kind=&pay_gbn=&doc' => $info['doc'],
                // 'sfast' => $info['sfast'],




    }



</script>