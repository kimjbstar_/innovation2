<body>

    <style>
        .content_box {
            left: 0% !important;
        }
    </style>


    <?php

// var_dump($data);

?>

    <link rel="stylesheet" href="https://quickcar.co.kr/css/common.css?d=<?=md5(date("Y-m-d H:i:s"))?>">
    <link rel="stylesheet" href="https://quickcar.co.kr/css/header.css?d=<?=md5(date("Y-m-d H:i:s"))?>">
    <link rel="stylesheet" href="https://quickcar.co.kr/css/menu.css?ver=0.0.1">
    <link rel="stylesheet" href="https://quickcar.co.kr/css/content.css?d=<?=md5(date("Y-m-d H:i:s"))?>">

    <!--Loading Container Start-->
    <div id="load" class="loading-overlay display-flex flex-column justify-content-center align-items-center">
        <div class="primary-color font-28 fas fa-spinner fa-spin"></div>
    </div>
    <!--Loading Container End-->

    <div class="row h-100">
        <div class="col-xs-12 col-sm-12">

            <!--Page Title & Icons Start-->
            <div class="header-icons-container text-center shadow-sm">
                <a href="<?=$url['back']?>">
                    <span class="float-left">
                        <img src="<?=$img['back']?>" alt="Back Icon">
                    </span>
                </a>
                <span class="title">기프티콘 내역</span>
                <a href="#">
                <span class="float-right menu-open closed">
                        <!-- <img src="../icons/menu.svg" alt="Menu Hamburger Icon"> -->
                        <button class="btn btn-outline-primary top-button">메뉴</button>
                    </span>
                </a>
            </div>

            <div class="rest-container">


<style>
    .list-group-item:hover{
        background-color:#e5e5e5;
        box-shadow: 0 .5rem 1rem rgba(0,0,0,.15)!important;
        cursor: pointer;
    }
</style>
                <ul class="list-group">

                <?php foreach ($data['list'] as $value) {?>


                    <?php
                    if ($value['coupon_status'] == "notUsed") {
                        echo "<a href='/Gifticonitem?c=" . $value['coupon_id'] . "&e=" . $value['expire_date'] . "'>";
                    } else {
                        //echo "<a href='/Gifticonitem?c=".$value['coupon_id']."&e=".$value['expire_date']."'>";
                        ?>
                        <a  href='#' onclick="alert('이미 사용되었습니다.')">
                        <?php
                    }
                    ?>


                    
                        <li class="list-group-item my-3 mx-5">
                            <div class="row">
                                <div class="col-1">
                                    <img class="img-fluid rounded-circle shadow"
                                        src="https://tara2.tarapay.net/TaraPay/assets/images/brand/item/0000108380.png?lastModify=aef82cf6988ae9c3691696a545b155fb">
                                </div>
                                <div class="col-11 py-4">
                                    <h5>이디야커피</h5>
                                    <h6>유효기간 : <?= $value['expire_date']?></h6>
                                    <h6>사용여부 : <?php

                                    if ($value['coupon_status'] == "notUsed") {
                                        echo "미사용";
                                    } else {
                                        echo "사용";
                                    }


                                    ?></h6>
                                </div>
                            </div>
                        </li>
                    </a>
                <?php } ?>
                 
                    
                </ul>



                <div class="bg_inner_type1 padding-rel-b-64">
                    <!-- <div class="inner_menu">
                    <ul>
                        <li class="selected"><a href="/sub/OrderHistory/index?cond=all">전체</a></li>
                        <li><a href="/sub/OrderHistory/index?cond=today">당일</a></li>
                        <li><a href="/sub/OrderHistory/index?cond=month">당월</a></li>
                    </ul>
                </div> -->
                    <div id="output-orderlist"
                        style="width:100%;display:flex;align-items:center;flex-direction:column;">
                    </div>
                </div>




            </div>
        </div>


        <?php $this->load->view('common/slideMenu', renderSlideMenu());?>


    </div>



    <script src="https://quickcar.co.kr/js/lib/jquery.js?lastModify=38b5fe31dc483f36ca2e7aa78545b53f"></script>