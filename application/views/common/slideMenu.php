    <!--Main Menu Start-->
    <div class="main-menu hidden-soft shadow-sm">
        <div class="mini-profile-info">
            <div class="menu-close">
                <span class="float-right">
                    <img src="<?=$img['close']?>" alt="Close Icon">
                </span>
            </div>
            <div class="profile-info" >
                <div class="profile-name text-center"><img src="https://quickcar.co.kr/quickcar_logo.png" style="width:80%;"></div>
                <div class="profile-name text-center"><?=$userId?></div>
                <div class="profile-email text-center"><?=$userMail?></div>
            </div>
        </div>
        <div class="menu-items">
            <div class="all-menu-items">
                <a class="menu-item" href="<?=$url['main']?>">
                    <div>
                        <span class="menu-item-icon menu-dark">
                            <img src="<?=$img['home']?>" alt="Home Icon">
                        </span>
                        <span class="menu-item-icon menu-light">
                            <img src="<?=$img['homeLight']?>" alt="Home Lighter Icon">
                        </span>
                        <span class="menu-item-title">홈으로</span>
                        <span class="menu-item-click fas fa-arrow-right"></span>
                    </div>
                </a>
                <a class="menu-item" href="<?=$url['orderlist']?>">
                    <span class="menu-item-icon menu-dark profile">
                        <img src="<?=$img['avaterDark']?>" alt="Avatar Darker Icon">
                    </span>
                    <span class="menu-item-icon menu-light profile">
                        <img src="<?=$img['avater']?>" alt="Avatar Darker Icon">
                    </span>
                    <span class="menu-item-title profile">최근 보낸 내역</span>
                    <span class="menu-item-click fas fa-arrow-right"></span>
                </a>
                <?php if (!is_null($this->session->userId)) { ?>
                <a class="menu-item" href="/Mileageinfo">
                    <span class="menu-item-icon menu-dark profile">
                        <img src="<?=$img['avaterDark']?>" alt="Avatar Darker Icon">
                    </span>
                    <span class="menu-item-icon menu-light profile">
                        <img src="<?=$img['avater']?>" alt="Avatar Darker Icon">
                    </span>
                    <span class="menu-item-title profile">마일리지 확인</span>
                    <span class="menu-item-click fas fa-arrow-right"></span>
                </a>
                <?php } ?>

                <?php if (is_null($this->session->userId)) { ?>
                
                    <a href="<?=$this->config->site_url('Check')?>" class="menu-item margin-top-auto">
                        <span class="menu-item-icon menu-dark logout">
                            <img src="<?=$img['logout']?>" alt="Logout Icon">
                        </span>
                        <span class="menu-item-icon menu-light logout">
                            <img src="<?=$img['logoutLight']?>" alt="Logout Icon">
                        </span>
                        <span class="menu-item-title logout">로그인</span>
                    </a>
                <?php } else { ?>

                    <a href="<?=$url['logout']?>" class="menu-item margin-top-auto">
                        <span class="menu-item-icon menu-dark logout">
                            <img src="<?=$img['logout']?>" alt="Logout Icon">
                        </span>
                        <span class="menu-item-icon menu-light logout">
                            <img src="<?=$img['logoutLight']?>" alt="Logout Icon">
                        </span>
                        <span class="menu-item-title logout">로그아웃</span>
                    </a>

                <?php } ?>
            </div>
        </div>
    </div>