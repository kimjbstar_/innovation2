<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=APP_NAME?></title>
</head>

<body>
    
    <script src="<?=$swal?>"></script>
    <script>
        //var order_first = '<?= $this->session->userdata('order_first') ?>';
        <?php //$this->session->unset_userdata('order_first') ?>
        //if(order_first == '1'){

            window.addEventListener('load', async function (e) {
                let { value } = await Swal.fire({
                    title: '<?=$title?>',
                    allowOutsideClick: false,
                    confirmButtonColor : '#ea5b11',
                    html: '<?=$html?>',
                    showCloseButton: true,
                    showCancelButton: true,
                    focusConfirm: false,
                    confirmButtonText: '계속주문하기',
                    cancelButtonText: '종료',
                }).then((result) => {
                    if(result.dismiss == 'cancel'){
                        location.replace("/finish");   
                    }
                    // order_first = 0;
                    // var len = history.length - 1;
                    // history.go(-len);
                    location.replace('/Main?order_first=1');
                });
                <?php if (isset($url)): ?>
                if (value) {
                    location.replace ('<?=$url?>');
                }
                <?php endif; ?>
            });
       // }
    </script>
</body>

</html>