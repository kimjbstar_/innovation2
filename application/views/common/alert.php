<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=APP_NAME?></title>
</head>

<body>
    <script src="<?=$swal?>"></script>
    <script>
        window.addEventListener('load', async function (e) {
            let { value } = await Swal.fire({
                title: '<?=$title?>',
                allowOutsideClick: false,
                confirmButtonColor : '#ea5b11',
            });
            <?php if (isset($url)): ?>
            if (value) {
                location.replace ('<?=$url?>');
            }
            <?php endif; ?>
        });
    </script>
</body>

</html>