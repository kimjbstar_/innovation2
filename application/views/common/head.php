<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
    <?php if (isset($css)):?>
        <?php foreach ($css as $file): ?>
        <link rel="stylesheet" href="<?=$file?>">
        <?php endforeach;?>
    <?php endif;?>
    <title><?=$title?></title>
</head>
<script src="//code.jquery.com/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8/dist/sweetalert2.min.js"></script>
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=fe72cff6c3223257f0c2a034f0748abe&libraries=services"></script>
<script>
    var geocoder = new kakao.maps.services.Geocoder();

    var_destination = '';
    var_origin='';
    var isSelect = '';
    var originData = {};
    var destinationData = {};
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-385ZXPBF37"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-385ZXPBF37');
</script>