

<div class="menuwrap">
    <nav id="menu">
        <div style="background-color:#27218E;width:100%;height:20vh;text-align:center;">
            <img id="hamburger2" src="http://publicservicefair.kr/static/cil_x.png" style="position:absolute;right:5%;top :2%;">
            <div style="color:white;font-size:1.5em;">
                <!-- <br/><br/>로그인해주세요. -->
            </div>
        </div>
        <div style="width:100%;height:80vh; overflow-y:scroll;padding-left:20px;padding-right:20px;padding-top:20px;">
            <a href="http://publicservicefair.kr/PracticeTest/OnlineInterview" style="text-decoration:none;">
                <div style="border-bottom:1px solid #DDDDDD;height:40px;font-weight:600;font-size:1em;padding-left:10px;text-decoration:none;color:black;">
                    모의면접 신청하기
                </div>
            </a>
            <a href="http://publicservicefair.kr/PracticeTest/OnlineMentoring" style="text-decoration:none;">
                <div style="margin-top:15px;border-bottom:1px solid #DDDDDD;height:40px;font-weight:600;font-size:1em;padding-left:10px;text-decoration:none;color:black;">
                    멘토링 신청하기
                </div>
            </a>
            <a href="http://publicservicefair.kr/Home" style="text-decoration:none;">
                <div style="margin-top:15px;border-bottom:1px solid #DDDDDD;height:40px;font-weight:600;font-size:1em;padding-left:10px;text-decoration:none;color:black;">
                    개막관
                </div>
            </a>
            <a href="http://publicservicefair.kr/ThemeHall" style="text-decoration:none;">
                <div style="margin-top:15px;border-bottom:1px solid #DDDDDD;height:40px;font-weight:600;font-size:1em;padding-left:10px;text-decoration:none;color:black;">
                    주제관
                </div>
            </a>
            <a href="http://publicservicefair.kr/RecruitInfo" style="text-decoration:none;">
                <div style="margin-top:15px;border-bottom:1px solid #DDDDDD;height:40px;font-weight:600;font-size:1em;padding-left:10px;text-decoration:none;color:black;">
                    채용정보관
                </div>
            </a>
            <a href="http://publicservicefair.kr/Participate" style="text-decoration:none;">
                <div style="margin-top:15px;border-bottom:1px solid #DDDDDD;height:40px;font-weight:600;font-size:1em;padding-left:10px;text-decoration:none;color:black;">
                    참여관
                </div>
            </a>
            <a href="http://publicservicefair.kr/Seminar" style="text-decoration:none;">
                <div style="margin-top:15px;border-bottom:1px solid #DDDDDD;height:40px;font-weight:600;font-size:1em;padding-left:10px;text-decoration:none;color:black;">
                    세미나관
                </div>
            </a>
            <a href="http://publicservicefair.kr/CommunicationEvent" style="text-decoration:none;">
                <div style="margin-top:15px;border-bottom:1px solid #DDDDDD;height:40px;font-weight:600;font-size:1em;padding-left:10px;text-decoration:none;color:black;">
                    소통·이벤트
                </div>
            </a>
        </div>
        <div style="width:100%; height:10vh;text-align:min-height:100px;center;margin:0 auto;">
            <img id="" src="http://publicservicefair.kr/static/menu_logo.png" style="">
        </div>        
    </nav>
</div>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" crossorigin="anonymous">   
<style>

body{
    background-color:#ffffff;
    
}


.nav{
    -webkit-user-select: none;
    user-select: none;
    background-color: #fff;
    position: fixed;
    z-index: 100000;
    top: 0;
    width: 100%;
    height: 71px;

}

.nav_top{
    position: relative;
    display: flex;
    align-items: center;
    border-bottom: 1px solid #d8d8d8;
    height: 70px;
}
.nav_top .logo {
    left: 5.19%;
}
.nav_top .hamburger {
    right: 7.19%;
}
.nav_top .hamburger, .nav_top .logo {
    display: inline-block;
    position: absolute;
}



.mobile-menu {
    display: block;
    position: fixed;
    top: 15px;
    left: 15px;
    z-index: 500;
    width: 45px;
    height: 45px;
    padding: 5px;
    background-color: #ffffff;
    border: 0;
}
.mobile-menu i {
    font-size: 2em;
}
.menuwrap {
    position: fixed;
    top: 0;
    left: -300px; /* 너비 300px 인 사이드바를 왼쪽으로 300px 이동시켜 화면에 보이지 않게 함 */
    z-index: 1000000;
    overflow: auto;
    width: 300px; /* 너비 */
    height: 100%;
    /* padding: 50px 20px; */
    box-sizing: border-box;
    transition: left .3s ease-in-out; /* 0.3초 동안 이동 애니메이션 */
    background-color: #ffffff;
}
.menuwrap.on {
    left: 0;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
document.addEventListener('DOMContentLoaded', function(){
    document.querySelector("#hamburger").addEventListener("click", function(e){
        if ( document.querySelector('.menuwrap').classList.contains('on') ){
            //메뉴닫힘
            document.querySelector('.menuwrap').classList.remove('on');
        } else {
            //메뉴펼처짐
            document.querySelector('.menuwrap').classList.add('on');
        }
    });
    document.querySelector("#hamburger2").addEventListener("click", function(e){
        if ( document.querySelector('.menuwrap').classList.contains('on') ){
            //메뉴닫힘
            document.querySelector('.menuwrap').classList.remove('on');
        } else {
            //메뉴펼처짐
            document.querySelector('.menuwrap').classList.add('on');
        }
    });
    var imagemap = '<img src="/static/media/mobile_footer.158ff430.png" usemap="#image-map"><map name="image-map"><area target="" alt="Home" title="Home" href="http://publicservicefair.kr/Home" coords="129,87,26,45" shape="rect"><area target="" alt="개인정보처리방침" title="개인정보처리방침" href="http://www.mpm.go.kr/mpm/useinfo/privacy/" coords="83,111,24,92" shape="rect"><area target="" alt="영상정보처리기기운영관리방침" title="영상정보처리기기운영관리방침" href="http://www.mpm.go.kr/mpm/useinfo/media/" coords="192,109,89,93" shape="rect"><area target="" alt="저작권보호및이용정책" title="저작권보호및이용정책" href="http://www.mpm.go.kr/mpm/useinfo/copyrightPolicy/" coords="195,89,268,109" shape="rect"><area target="" alt="페이스북" title="페이스북" href="https://ko-kr.facebook.com/miraesaram/" coords="188,15,221,52" shape="rect"><area target="" alt="트위터" title="트위터" href="https://twitter.com/miraesaram" coords="229,18,261,50" shape="rect"><area target="" alt="인스타그램" title="인스타그램" href="https://www.instagram.com/mpm_kr" coords="268,12,303,54" shape="rect"><area target="" alt="유투브" title="유투브" href="https://www.youtube.com/channel/UCTlXbNVlKyqLmDTqmlrdEFA" coords="310,14,343,50" shape="rect"></map>';
    $(".fotter").html(imagemap);

    $(".logo").on("click", function(){ 
        location.href='http://publicservicefair.kr/Home';
    });
    $('form').attr('method','post');


});
</script>