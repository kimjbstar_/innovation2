<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=APP_NAME?></title>
</head>
<body>
    <script src="<?=$swal?>"></script>
    <?php if ($url): ?>
    <script>window.addEventListener('load', async function(e){
    let { value } = await Swal.fire({
        title: '<?=$title?>',
        allowOutsideClick: false,
        confirmButtonColor : '#ea5b11',
    });
    if (value) {
        this.location.href = '<?=$url?>'
    }
});
    </script>
    <?php else:?>
    <script>
window.addEventListener('load', async function(e){
    let { value } = await Swal.fire({
        title: '<?=$title?>',
        allowOutsideClick: false,
    });
});
    </script>
    <?php endif;?>
</body>
</html>