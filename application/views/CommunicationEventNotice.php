
<!DOCTYPE html>
<html lang="ko">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>이벤트·소통관</title>
    <meta name="description" content="이벤트·소통관" />
    <meta name="keywords" content="이벤트·소통관" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="" />
    <meta property="og:title" content="이벤트·소통관" />
    <meta property="og:description" content="이벤트·소통관" />
    <link rel="stylesheet" href="/static/css/style.css">
    <!--<link rel="stylesheet" href="/static/css/mobile.css">-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <script type="text/javascript" src="https://unpkg.com/swiper@5.4.5/js/swiper.min.js"></script>
</head>

<body>
    <div class="container">
        <div id="header">
            <div class="top">
                <div class="inner">
                    <a href="#" class="upper"><span class="blind">인사혁신처</span></a>
                    <a href="#" class="btn_login">회원가입ㆍ로그인</a>
                </div>
            </div>
            <div class="bottom">
                <div class="inner">
                    <a href="#" class="logo"><span class="blind">2020 온라인 공직박람회</span></a>
                    <ul>
                        <li>
                            <a href="#">개막관</a>
                        </li>
                        <li>
                            <a href="#">주제관</a>
                        </li>
                        <li>
                            <a href="#">채용정보관</a>
                        </li>
                        <li>
                            <a href="#">참여 프로그램</a>
                        </li>
                        <li>
                            <a href="#">소통ㆍ이벤트</a>
                        </li>
                        <li>
                            <a href="#">인재혁신세미나</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content" style="background-color:#0E0C34">
            <div class="innovation_wrap">
                <div class="section" style="background-image: url(/static/event_communication_bg.JPG);min-height: 205px;">
                    <h2>소통ㆍ이벤트</h2>

                    <div class="box_tab mint" id="tab_00" style="width: 70%;bottom: 0px;position: absolute;left: 15%;">
                        <ul>
                            <li style="width: 47.5%;">
                                <a href="/CommunicationEvent"><button class="selected" data-tab="tab_05_tab1">이벤트</button></a>
                            </li>
                            <li style="margin-left: 5%;width: 47.5%;">
                                <a href="/CommunicationEvent?selected=Communication"><button data-tab="tab_05_tab2">소통 게시판</button></a>
                            </li>
                        </ul>       
                    </div>             
                </div>
                <div class="section" id="section1" style="padding-top: 0;">
           
                    <div class="cont_wrap2">

                        <div class="content_title" style="text-align:left;padding-left:20px;margin-bottom:30px;">
                            소통 · 이벤트  >  이벤트
                        </div>                         
                        <div class="box_tab mint" id="tab_05">
                            <div class="tab_cont" style="background-color:white;border-radius:30px;">
                                <div class="clear">
                                    <div class="open" id="tab_05_tab2">
                                        <div style="width:100%">
                                            <?php
                                                // var_dump($Notice[0]);
                                            ?>

                                            <table style="width: 100%;">
                                                <thead>
                                                    <tr style="width:100%;color:white;">
                                                        <th  style="border-radius: 20px;"><?=$Notice[0]['buttonName'];?></th>                                         
                                                    </tr>
                                                </thead>
                                                <tbody>


                                                <tr>
                                                    <td><?=$Notice[0]['linkAddress'];?></td>
                                                </tr>             

                                                </tbody>
                                            </table>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div id="footer">
            <div class="inner">
                <div class="top">
                    <a href="#" class="footer_logo"><span class="blind">인사혁신처</span></a>
                    <ul>
                        <li class="fb">
                            <a href="#"><span class="blind">facebook</span></a>
                        </li>
                        <li class="tw">
                            <a href="#"><span class="blind">twitter</span></a>
                        </li>
                        <li class="ig">
                            <a href="#"><span class="blind">instagram</span></a>
                        </li>
                        <li class="yt">
                            <a href="#"><span class="blind">youtube</span></a>
                        </li>
                    </ul>
                </div>
                <div class="bottom">
                    <ul>
                        <li><a href="#">개인정보 처리방침</a></li>
                        <li><a href="#">영상정보처리기 운영ㆍ관리방침</a></li>
                        <li><a href="#">저작권보호 및 이용정책</a></li>
                    </ul>
                    <p>30102 세종특별자치시 한누리대로 499(어진동) 세종포스트빌딩 6층~12층</p>
                    <p>copyright© Ministry of Personnel Management. All Rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
    <script src="/static/js/common.js"></script>
    <script>

        var slider1 = new Swiper('.slide1', {
            loop: true,
            slideActiveClass: 'active',
            navigation: {
                nextEl: '#first_next',
                prevEl: '#first_prev',
            },
            autoHeight:true,
            spaceBetween:30,
            pagination: {
                // el: '.slide1 .dots',
                type: 'bullets',
            },
        })



        var slider2 = new Swiper('.slide2', {
            loop: true,
            slideActiveClass: 'active',
            navigation: {
                nextEl: '#second_next',
                prevEl: '#second_prev',
            },
            autoHeight:true,
            spaceBetween:30,
            pagination: {
                // el: '.slide1 .dots',
                type: 'bullets',
            },
        })

        // var innerSlider01 = new Swiper('.slider01', {
        //     loop: false,
        //     slideActiveClass: 'active',
        //     navigation: {
        //         nextEl: '.slider01 .next',
        //         prevEl: '.slider01 .prev',
        //     },
        //     observer: true,
        //     observeParents: true,
        // })


        // $('.box_tab ul li button,.inner_tab ul li button').click(function () {
        //     const sibling = $(this).parent().siblings()
        //     const target = $(this).attr('data-tab')
        //     sibling.each(function () {
        //         $(this).children().removeClass('selected')
        //     })
        //     $(this).addClass('selected')
        //     const wrapper = $(this).closest('div').children('.tab_cont,.inner_tab_cont').children().children()
        //     wrapper.each(function () {
        //         $(this).removeClass('open')
        //     })
        //     $('#' + target).addClass('open')
        // })
        // $('.box_tab ul li button,.inner_tab ul li button').click(function () {

        //     console.log($(this).text());

        //     if ($(this).text()=='이벤트'){
        //         $('.content_title').text('소통 · 이벤트  >  이벤트');
        //         $('.content_title').css('color','white');
        //         $('.content').css('background-color','#0E0C34');


        //         $($('#tab_00 button')[0]).css('background-color','#0E0C34');
        //         $($('#tab_00 button')[0]).css('color','#FFFFFF');

        //         $($('#tab_00 button')[1]).css('background-color','#ffffff');
        //         $($('#tab_00 button')[1]).css('color','#7E7E7E');

        //     }
        //     else if ($(this).text()=='소통 게시판'){
        //         $('.content_title').text('소통 · 이벤트  >  소통게시판');
        //         $('.content_title').css('color','#676767');
        //         $('.content').css('background-color','#ffffff');
        //         $($('#tab_00 button')[0]).css('background-color','#ACACAC');
        //         $($('#tab_00 button')[0]).css('color','#7E7E7E');

        //         $($('#tab_00 button')[1]).css('background-color','#ffffff');
        //         $($('#tab_00 button')[1]).css('color','#000000');
        //     }

        //     const sibling = $(this).parent().siblings()
        //     const target = $(this).attr('data-tab')
        //     sibling.each(function () {
        //         $(this).children().removeClass('selected')
        //     })
        //     $(this).addClass('selected')
            
        //     const wrapper = $('#tab_05').children('.tab_cont,.inner_tab_cont').children().children()
        //     wrapper.each(function () {
        //         $(this).removeClass('open')
        //     })
        //     $('#' + target).addClass('open')
        // })

        $(document).ready(function () {
            $('a[href^="#"]').on('click', function (e) {
                e.preventDefault();

                var target = this.hash;
                $target = $(target);

                $('html, body').stop().animate({
                    'scrollTop': $target.offset().top
                }, 900, 'swing', function () {
                    window.location.hash = target;
                });
            });
        });




        $('.clip_tab button').click(function () {
            $('.clip_tab li').removeClass('selected');
            $(this).parent().addClass('selected')
        })



        $('.qna_wrap ul li em').click(function () {
            if ($(this).parent().hasClass('selected')) {
                $('.qna_wrap ul li').removeClass('selected');
            } else {
                $('.qna_wrap ul li').removeClass('selected');
                $(this).parent().addClass('selected')
            }
        })








    </script>

        <style>


            table {
                width: 100%;
                border-collapse: collapse;
            }

            td {
                border-bottom: 1px solid #ACACAC;
                padding: 10px;
                text-align: center;
                background-color: #ffffff;
                height:40px;
                font-size:18px;
            }
            th {
                background-color: #0E0C34;
                color:white;
                padding: 10px;
                font-size:20px;
                height:45px;
            }





            .qna_wrap ul li::after {
                content: "∨";
                font-size: 20px;
                position: absolute;
                top: 25px;
                right: 20px;
                color: #838383;
                line-height: 40px;
            }

            .qna_wrap ul li em {
                padding: 20px;
            }
            .qna_wrap ul li em, .qna_wrap ul li a {
                color: #353535;
                font-size: 20px;
            }
            .prev{
                background-image: url(/static/Prev_Button.png);
                width: 55px;
                height: 55px;
                margin-left:45px;
                display: block;
                border: none;
                background-color: transparent;
                background-size: cover;
                margin-top:100.5px;
            }
            .next{
                background-image: url(/static/Next_Button.png);
                width: 55px;
                height: 55px;
                display: block;
                border: none;
                background-color: transparent;
                background-size: cover;
                margin-top:100.5px;
            }

            .cont_wrap2 {
                width: 100%;
                max-width: 1280px;
                margin: 0 auto;
                margin-top: 50px;
                position: relative;
                z-index: 1;
            }
            .tab_cont h2 {
                font-family: "GmarketSansTtf";
                font-size: 35px;
                text-align: center;
                line-height: 65px;
                font-weight:600;
                color: #000;
                word-break: keep-all;
                padding-bottom:30px;
            }


            .box_tab.mint > ul li button.selected {
                background-color: #0E0C34;
                color: #fff;
            }        
            .box_tab.mint > ul li button {
                border: 0px solid #0E0C34;
                border-bottom: none;
                box-sizing: border-box;
            }


            .content_title{
                font-weight:600;
                color:white;
                margin-top:20px;
                font-size:13px;
            }




            .apply-area .input-title {
                font-family: "Noto Sans CJK KR";
                font-size: 35px;
                font-weight: 600;
                margin-bottom: 35px;
                color: #000;
            }


            .apply-area .input-line {
                margin-top: 40px;
                display: block;
            }

            .apply-area .input-line .three_item {
                width: 31.3%;
            }
            .apply-area .input-line .input-item {
                display: inline;
                float: left;
            }

            .apply-area .input-sub-title {
                display: flex;
                font-family: "Noto Sans CJK KR";
                font-size: 20px;
                margin-bottom: 15px;
                font-weight: 600;
                color: #757575;
            }

            .apply-area input {
                padding-left: 20px;
                border-radius: .5em;
                border: 2px solid silver;
                height: 63px;
                font-size: 20px;
            }



            .apply-area input:focus {
            outline:none;
            border : 2px solid #27218F;
            border-radius:0.5em;

            
            }



            .apply-area textarea {
                padding-left: 20px;
                border-radius: .5em;
                border: 2px solid silver;
                height: 63px;
                font-size: 20px;
            }


            .apply-area select {
                padding-left: 20px;
                padding-right: 20px;
                border-radius: .5em;
                border: 2px solid silver;
                height: 63px;
                font-size: 20px;
                width:110%;
            }



            .apply-area select:focus {
            outline:none;
            border : 2px solid #27218F;
            border-radius:0.5em;

            
            }



            .input-line{
                width: 80%;
                margin-left: auto;
                margin-right: auto;
            }























            .contentbox{
        width:75%;
        margin: 0 auto;
    }

    .content_company_info{
        padding-bottom:200px;

    }
    

    .qna_wrap{
        margin-top:0px;
        border : 2px solid #CECECE;
    }



    .tag{
        border:1px solid #B7B7B7;
        width:120px;
        text-align:center;
        border-radius:15px;
        padding-top:5px;
        padding-bottom:5px;
        display:inline-block;
    }




    .qna_wrap ul{
        margin-top:0px;
        padding:0px;
    }

    .qna_wrap ul li{
        padding:0px ;
        color:#353535;
        border-bottom: 2px solid #CECECE;
    }


    .qna_wrap ul li em{
        padding : 20px;
    }

    .ptag{
        background-color:white;
        overflow-x:hidden;
    }


    .ptag img{
        max-width:100%;
    }


    .qna_wrap ul li .ptag{
        padding : 20px;
    }

    .qna_wrap ul li p{
        font-size:1em;
        margin-top:0;
    }


    .qna_wrap ul .selected{
        background-color:#0E0C34;
    }
    .qna_wrap ul .selected em{
        color:white;
    }

    .qna_wrap ul .selected p{
        /* background-color:white; */
    }


    .qna_wrap ul li em, .qna_wrap ul li a{
        color:#353535;
        font-size:20px;
    }



    .qna_wrap ul li::after {
        content: "∨";
        font-size: 20px;
        position: absolute;
        top: 25px;
        right: 20px;
        color: #838383;
        line-height: 40px;
    }

    .qna_wrap ul li.selected .ptag {
        display: block;
        font-size:20px;
        min-height:60px;
    }

    .qna_wrap ul li .ptag {
        display: none;
    }








        </style>

</body>

</html>










