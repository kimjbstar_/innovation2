<body>

<?php
    if (!is_null($this->session->userId)) {
        ?>
        <script>
            location.replace('Main');
        </script>

        <?php
    }
?>

<style>
    .passwordnumber{
        -webkit-text-security: disc;
    }
</style>


<!-- <body style="background-image: url(../icons/back.jpeg);background-size:cover;"> -->
<div style="
    background-color: white;
    width: 100%;
    position: fixed;
    z-index: -90;
    height: 100%;
    filter: opacity(0.4);
">
    </div>

    <!-- <div id="load" class="loading-overlay display-flex flex-column justify-content-center align-items-center">
        <div class="primary-color font-28 fas fa-spinner fa-spin"></div>
    </div> -->
    <div class="row h-100">
        <div class="col-xs-12 col-sm-12">
            <div class="text-center header-icon-logo-margin header-icon-logo-margin-extra">
                <img src="https://quickcar.co.kr/quickcar_logo.png" alt="Main Logo" style="width:200px;">
            </div>
            <div class="address-title text-center">
                <div class="display-flex justify-content-center">
                    <div class="float-left sign-in-item border-bottom-primary-100" data-class="login">로그인</div>
                    <div class="float-left sign-in-item border-bottom-light-grey" data-class="sign-up">회원가입</div>
                </div>
            </div>
            <div class="sign-up-form-container login text-center">
                <form class="width-100" method="post" action="<?=$url['login']?>">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span>
                                    <img src="<?=$img['avatarLight']?>" alt="Avatar Icon">
                                </span>
                            </div>
                            <input type="hidden" name="query" value="<?=$data['query']?>">
                            <input type="hidden" value="<?=$data['csrf']['hash']?>" name="<?=$data['csrf']['name']?>">
                            <input class="form-control" style="background-color:transparent" type="number" autocomplete="off" name="username" placeholder="아이디 (핸드폰번호)">
                        </div>
                    </div>
                    <!--Sign In Field End-->

                    <!--Sign In Field Start-->
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span>
                                    <img src="<?=$img['lock']?>" alt="Lock Icon">
                                </span>
                            </div>
                            <input class="form-control passwordnumber" style="background-color:transparent" type="number" maxlength="4"  oninput="maxLengthCheck(this)" pattern="[0-9]*" inputmode="numeric" name="password" placeholder="비밀번호 (네 자리 숫자)">
                            <div class="input-group-append password-visibility">
                                <span>
                                    <img src="<?=$img['eye']?>" alt="Password Visibility Icon">
                                </span>
                            </div>
                        </div>
                    </div>
                    <!--Sign In Field End-->

                    <div class="form-submit-button">
                        <button type="submit" class="btn btn-dark text-uppercase ">Login</button>
                    </div>
                </form>
            </div>
            <!--Sign In Container End-->

            <!--Sign Up Container Start-->
            <div id="sign-up" class="sign-up-form-container sign-up text-center hidden">
                <form class="width-100" method="post" action="<?=$url['signUp']?>">
                    <input type="hidden" value="<?=$data['csrf']['hash']?>" name="<?=$data['csrf']['name']?>">

                    <!--Sign Up Field Start-->
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span>
                                    <img src="<?=$img['avatarLight']?>" alt="Avatar Icon">
                                </span>
                            </div>
                            <input class="form-control" style="background-color:transparent"  type="number" autocomplete="off" name="username" placeholder="아이디 - (핸드폰 번호)">
                        </div>
                    </div>
                    <!--Sign Up Field End-->

                    <!--Sign Up Field Start-->
                    <!-- <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span>
                                    <img src="<?=$img['envelope']?>" alt="Envelope Icon">
                                </span>
                            </div>
                            <input class="form-control"  style="background-color:transparent" type="email" autocomplete="off" name="email" placeholder="이메일">
                        </div>
                    </div> -->
                    <!--Sign Up Field End-->

                    <!--Sign Up Field Start-->
                    <!-- <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span>
                                    <img src="<?=$img['phone']?>" alt="Phone Number">
                                </span>
                            </div>
                            <input class="form-control" style="background-color:transparent"  type="tel" name="phone" placeholder="핸드폰번호(숫자만 입력 바랍니다.)">
                        </div>
                    </div> -->
                    <!--Sign Up Field End-->

                    <!--Sign Up Field Start-->
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span>
                                    <img src="<?=$img['lock']?>" alt="Lock Icon">
                                </span>
                            </div>
                            <input class="form-control passwordnumber" style="background-color:transparent"  type="number" maxlength="4"  oninput="maxLengthCheck(this)" pattern="[0-9]*" inputmode="numeric"  name="password" placeholder="비밀번호 (네 자리 숫자)">
                            <div class="input-group-append password-visibility">
                                <span>
                                    <img src="<?=$img['eye']?>" alt="Password Visibility Icon">
                                </span>
                            </div>
                        </div>
                    </div>
                    <!--Sign Up Field End-->

                    <!--Sign Up Field Start-->
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span>
                                    <img src="<?=$img['lock']?>" alt="Lock Icon">
                                </span>
                            </div>
                            <input class="form-control passwordnumber"  style="background-color:transparent" type="number" maxlength="4"  oninput="maxLengthCheck(this)" pattern="[0-9]*" inputmode="numeric"  name="passwordConfirm" placeholder="비밀번호 재입력 (네 자리 숫자)">
                            <div class="input-group-append password-visibility">
                                <span>
                                    <img src="<?=$img['eye']?>" alt="Password Visibility Icon">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-submit-button">
                        <button href="sign-up.html" class="btn btn-dark text-uppercase ">회원가입</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 text-center sms-rate-text font-roboto flex-end margin-bottom-30">
            <div class="container-sms-rate-text width-100 font-11">
                <span class="light-gray font-weight-light">By signing up you have agreed to our </span>
                <br />
                <a href="#" class="dark-link">
                    <span class="font-weight-light">Terms of Use & Privacy Policy</span>
                </a>
            </div>
        </div>
    </div>

    <script>
        function maxLengthCheck(object){
            if (object.value.length > object.maxLength){
                object.value = object.value.slice(0, object.maxLength);
            }    
        }

    </script>