
<!DOCTYPE html>
<html lang="ko">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>영상관</title>
    <meta name="description" content="영상관" />
    <meta name="keywords" content="영상관" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="" />
    <meta property="og:title" content="영상관" />
    <meta property="og:description" content="영상관" />
    <link rel="stylesheet" href="/static/css/style.css">
    <link rel="stylesheet" href="/static/css/mobile.css">
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script> -->

    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="/slick/slick.min.js"></script>

    <link rel="stylesheet" type="text/css" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <script type="text/javascript" src="https://unpkg.com/swiper@5.4.5/js/swiper.min.js"></script>



    <script type="text/javascript" src="/slick/slick.min.js"></script>

    <link rel="stylesheet" type="text/css" href="/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="/slick/slick-theme.css"/>

</head>

<body>


    <div class="container">
        <div id="header">
            <div class="top">
                <div class="inner">
                    <a href="http://publicservicefair.kr/Home" class="upper"><span class="blind">인사혁신처</span></a>
                </div>
            </div>
            <div class="bottom">
                <div class="inner">
                    <a href="http://publicservicefair.kr/Home" class="logo"><span class="blind">2020 온라인 공직박람회</span></a>
                    <ul>
                        <li>
                            <a href="http://publicservicefair.kr/OpeningHall">영상관</a>
                        </li>
                        <li>
                            <a href="http://publicservicefair.kr/ThemeHall">주제관</a>
                        </li>
                        <li>
                            <a href="http://publicservicefair.kr/RecruitInfo">채용정보관</a>
                        </li>
                        <li>
                            <a href="http://publicservicefair.kr/Participate">참여관</a>
                        </li>
                        <li>
                            <a href="http://publicservicefair.kr/Seminar">세미나관</a>
                        </li>
                        <li>
                            <a href="javascript:alert('오픈 준비 중입니다.');">소통ㆍ이벤트</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="contentbox">
                <div class="content_header">
                    영상관
                    <div id="playhere" class="content_header_subject">
                        아래에서 보고싶은 영상을 선택 후 재생해주세요.
                    </div>
                </div>

                <div class="content_player">
                    <div id ="player"></div>
                </div>

                
            </div>
            <div class="contentbox playlist">
                <div class="content_company_info">


                    <div class="play_group1">



                        <div class="play_title first_title">
                            <div class="play_title_main">
                                개막영상
                            </div>  
                            <div class="play_title_sub">
                                공직자와 공직을 꿈꾸는 국민 여러분들과 함께 2020 공직박람회를 개막합니다.
                            </div>  
                        </div>
                        <div class="play_list">
                            <div class="play_item" onclick="playthis('3lbttZrzkfo');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/3lbttZrzkfo/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                    인사혁신처장님 개막 인사
                                </div>
                            </div>


                            <div class="play_item" onclick="playthis('ulXomcxd_1c');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/ulXomcxd_1c/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                    희망메시지
                                </div>
                            </div>

                            <div class="play_item" onclick="playthis('YSPs8UcaCs4');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/YSPs8UcaCs4/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                    2020 온라인 공직박람회 소개영상
                                </div>
                            </div>

                        </div>  



                    </div>






                    <div class="play_group2">



                        <div class="play_title second_title">
                            <div class="play_title_main">
                                토크콘서트
                            </div>  
                            <div class="play_title_sub">
                                공직을 희망하는 사람들은 꼭 한번 들어보세요. 공직 선배들의 생생한 경험담을 공유합니다.
                            </div> 
                            <div class="play_list">
                                


                            <div class="play_item" onclick="playthis('F_Eee2EzKkA');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/F_Eee2EzKkA/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                    토크콘서트 1부 - 상편
                                </div>
                            </div>





                            <div class="play_item" onclick="playthis('uf2EN1S8uX0');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/uf2EN1S8uX0/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                    토크콘서트 1부 - 하편
                                </div>
                            </div>



                            <div class="play_item" onclick="playthis('L2GhLoll6J8');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/L2GhLoll6J8/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                    토크콘서트 2부
                                </div>
                            </div>



                            <div class="play_item" onclick="playthis('vskkzC8WHYI');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/vskkzC8WHYI/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                    토크콘서트 3부
                                </div>
                            </div>

                            </div>  
                        </div>



                    </div>

                    



                    <div class="play_group2">



                        <div class="play_title third_title">
                            <div class="play_title_main">
                                공직자 인터뷰
                            </div>  
                            <div class="play_title_sub">
                                자랑스러운 공무원들의 우수 사례를 공유합니다.
                            </div>  

                        </div>                            
                        <div class="play_list">
                            



                            <div class="play_item" onclick="playthis('mtKE6FO4qdA');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/mtKE6FO4qdA/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                    국방부 육군 장교 중위
                                </div>
                            </div>



                            <div class="play_item" onclick="playthis('9n7fFzBD8hU');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/9n7fFzBD8hU/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                    7급 수습직원 선발시험
                                </div>
                            </div>




                            <div class="play_item" onclick="playthis('dSBGftTLRs0');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/dSBGftTLRs0/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                    보건복지부 5급
                                </div>
                            </div>




                            <div class="play_item" onclick="playthis('nnsxkO2E2MY');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/nnsxkO2E2MY/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                    순경 인터뷰
                                </div>
                            </div>




                            <div class="play_item" onclick="playthis('rTGhB-jONHA');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/rTGhB-jONHA/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                    9급 중증장애인
                                </div>
                            </div>




                            <div class="play_item" onclick="playthis('drHGheVNQ0I');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/drHGheVNQ0I/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                    9급 사서직
                                </div>
                            </div>






                        </div>  



                    </div>





                    <div class="play_group4">



                        <div class="play_title fourth_title">
                            <div class="play_title_main">
                                인사처TV 몰아보기
                            </div>  
                            <div class="play_title_sub">
                                <!--공직을 희망하는 사람들은 꼭 한번 들어보세요. 공직 선배들의 생생한 경험담을 공유합니다.-->
                            </div>  
                        </div>
                        <div class="play_list">





                            <div class="play_item" onclick="playthis('ZvpI64Z1jfM');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/ZvpI64Z1jfM/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                    "부장님이 어디 가셨지?" 신입사원 강어벙의 위기! (feat. 신입사원의 슬기로운 직장생활)
                                </div>
                            </div>




                            <div class="play_item" onclick="playthis('xo1XoXHJS4U');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/xo1XoXHJS4U/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                    "자네 지금 뭐 하는 건가?" 부장님이 뿔났다! 신입사원 강어벙의 실수는 무엇? (feat. 신입사원의 슬기로운 점심시간)
                                </div>
                            </div>






                            <div class="play_item" onclick="playthis('bSnxph0Ro-o');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/bSnxph0Ro-o/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                    "엄마 나 상받았어ㅠ 학생 때 이후로 처음이야" (feat. #이광수 #모기송 #패러디) 인사처 적극행정 우수공무원 수상이야기
                                </div>
                            </div>



                            <div class="play_item" onclick="playthis('h8naj_ZP5WM');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/h8naj_ZP5WM/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                    조셉이 양배추에서 인간이 된 사연은? 필경사 공무원
                                </div>
                            </div>




                            <div class="play_item" onclick="playthis('JnV4bO1Nl8s');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/JnV4bO1Nl8s/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                    미안하다 이거 보여주려고 패러디했다 (feat. 코로나19 대응의 압도적 스케일 실화냐?ㄷㄷ)
                                </div>
                            </div>






                            <div class="play_item" onclick="playthis('9x2N7tFXR5I');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/9x2N7tFXR5I/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                    2020 사랑방 손님과 마스크 5부제
                                </div>
                            </div>






                            <div class="play_item" onclick="playthis('qbFI3rdEuqM');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/qbFI3rdEuqM/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                    #지켜줘서 #건강해서 #고맙습니다 - 우한 교민 퇴소
                                </div>
                            </div>





                            










                            <div class="play_item" onclick="playthis('bBMS7SVZq8w');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/bBMS7SVZq8w/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                    [인사처 5주년 (for.you튜브)]인사 자~알 해랏~! 어?
                                </div>
                            </div>




                            <div class="play_item" onclick="playthis('BlSp_H-lxq8');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/BlSp_H-lxq8/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                    퇴근 전 😈앙마가 너의 이름을 부를 때......💬
                                </div>
                            </div>



                            <div class="play_item" onclick="playthis('G7XbmFY6V_I');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/G7XbmFY6V_I/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                인사혁신처를 아십니까? ('道'를 아십니까(?) 아님)
                                </div>
                            </div>



                            <div class="play_item" onclick="playthis('0LR38M6iUCw');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/0LR38M6iUCw/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                여러분의 손으로 국가인재데이터베이스를 완성해주세요! 국민과 함께 만드는 '국가인재DB 국민추천제' 김희경 우정공무원교육원장
                                </div>
                            </div>



                            <div class="play_item" onclick="playthis('Oaquw49XOxg');">
                                <div class="play_item_thumbnail">
                                    <img src = "https://img.youtube.com/vi/Oaquw49XOxg/hqdefault.jpg">
                                </div>
                                <div class="play_item_subject">
                                여러분의 손으로 국가인재데이터베이스를 완성해주세요! 국민과 함께 만드는 '국가인재DB 국민추천제' 오창수 우정사업본부 예금위험관리과장 이상욱 관세청 규제개혁법무담당관
                                </div>
                            </div>



                        </div>  



                    </div>








                    


                </div>
            </div>
        </div>
        <div id="footer">
            <div class="inner">
                <div class="top">
                    <a href="http://publicservicefair.kr/Home" class="footer_logo"><span class="blind">인사혁신처</span></a>
                    <ul>
                        <li class="fb">
                            <a href="https://ko-kr.facebook.com/miraesaram/"><span class="blind">facebook</span></a>
                        </li>
                        <li class="tw">
                            <a href="https://twitter.com/miraesaram"><span class="blind">twitter</span></a>
                        </li>
                        <li class="ig">
                            <a href="https://www.instagram.com/mpm_kr"><span class="blind">instagram</span></a>
                        </li>
                        <li class="yt">
                            <a href="https://www.youtube.com/channel/UCTlXbNVlKyqLmDTqmlrdEFA"><span class="blind">youtube</span></a>
                        </li>
                    </ul>
                </div>
                <div class="bottom">
                    <ul>
                        <li><a href="http://www.mpm.go.kr/mpm/useinfo/privacy/">개인정보 처리방침</a></li>
                        <li><a href="http://www.mpm.go.kr/mpm/useinfo/media/">영상정보처리기 운영ㆍ관리방침</a></li>
                        <li><a href="http://www.mpm.go.kr/mpm/useinfo/copyrightPolicy/">저작권보호 및 이용정책</a></li>
                    </ul>
                    <p>30102 세종특별자치시 한누리대로 499(어진동) 세종포스트빌딩 6층~12층</p>
                    <p>copyright© Ministry of Personnel Management. All Rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
    <script src="/static/js/common.js"></script>
</body>
<style>

    .play_group0{
        height:310px;
    }
    .play_group1{
        height:510px;
    }
    .play_group2{
        height:850px;
    }
    .play_group3{
        height:1330px;
    }

    .play_group4{
        height:1740px;
    }

    .play_title_modify{
        width:95%;
        margin-bottom:80px;
        border-radius:10px;   
        margin-left:auto;
        margin-right:auto;
        height:110px;
        color:white;
        padding-left:20px;
    }

    .play_title{
        width:95%;
        margin-bottom:80px;
        border-radius:10px;   
        margin-left:auto;
        margin-right:auto;
        height:110px;
        color:white;
        padding-left:20px;
    }
    .first_title{
        background-color:#0E0C34;
        color:white;
    }
    .second_title{
        background-color:#8A0505;
        color:white;
    }
    .third_title{
        background-color:#8A5505;
        color:white;
    }
    .fourth_title{
        background-color:#27218F;
        color:white;
        height:70px;
        margin-top: 15px;
    }
    .play_title_main{
        
        color:white;
        font-size:35px;
        padding-top:15px;
    }
    .play_title_sub{
        
        color:white;
        line-height:2;
        font-size:18px;
        font-weight:200;
        margin-top:15px;
    }


    .playlist{
        height:3950px;
    }
    .play_list{
        margin-top:50px;
        width: 100%;
        margin-left: 2%;
    }

    .play_item{
        width:29.5%;
        margin-left:1.3%;
        margin-right:1.3%;
        display:inline-block;
        float:left;
        margin-bottom:20px;
        height:300px;
    }

    .play_item img{
        max-width:100%;
    }


    .play_item_subject{
        margin-top:10px;
        font-weight:600;
        font-size:15px;
    }


    .contentbox{
        width:75%;
        margin: 0 auto;
    }

    .content_company_info{
        padding-bottom:200px;
        margin-top:150px;
        padding-top:50px;

    }
    

    .qna_wrap{

        border : 2px solid #CECECE;
    }



    .content_header{
        margin-top:50px;
        width: 100%;
        height:90px;
        color:white;
        border-radius:10px;
        padding-top:50px;
        font-size:50px;
        font-weight:600;
        vertical-align:middle;
        text-align:center;
    }
    .content_header_subject{
        color:white;
        padding-top:50px;
        font-weight:200;
        font-size:20px;
        text-align:center;

    }


    .content_title{
        font-weight:600;
        color:black;
        margin-top:20px;
        font-size:13px;
    }
    .content_pictures{
        height: 250px;
        margin-top: 20px;
        margin-bottom: 20px;
        width: 133%;
        margin-left:-16.5%;
    }

    .content_company{

    }
    .content_company img{
        
        border-radius: 70%;
        border:1px solid #B7B7B7;
        overflow: hidden;
        width: 90px;
        object-fit: cover;
        height: 90px;
        display:inline-block;
    }
    .content_company_title{
        display:inline-block;
        padding-left:20px;
        padding-bottom:50px;
    }

    .content_company_title1{
        font-size:30px;
        font-weight:600;
        line-height:2em;

    }
    .content_company_title2{
        font-size:15px;
        line-height:1em;
        font-weight:300;
        
    }



    .content_tags{
        margin-bottom:30px;
    }

    .tag{
        border:1px solid #B7B7B7;
        width:120px;
        text-align:center;
        border-radius:15px;
        padding-top:5px;
        padding-bottom:5px;
        display:inline-block;
    }



    hr{
        background-color:#CDCDCD;
    }


    .qna_wrap ul{
        margin-top:0px;
        padding:0px;
    }

    .qna_wrap ul li{
        padding:0px ;
        color:#353535;
        border-bottom: 2px solid #CECECE;
    }


    .qna_wrap ul li em{
        padding : 20px;
    }

    .qna_wrap ul li .ptag{
        padding : 20px;
    }

    .qna_wrap ul li p{
        font-size:1em;
        margin-top:0;
    }


    .qna_wrap ul .selected{
        background-color:#EBEBEB;
    }

    .qna_wrap ul .selected p{
        background-color:white;
    }


    .qna_wrap ul li em, .qna_wrap ul li a{
        color:#353535;
        font-size:20px;
    }



    .qna_wrap ul li::after {
        content: "∨";
        font-size: 20px;
        position: absolute;
        top: 25px;
        right: 20px;
        color: #838383;
        line-height: 40px;
    }

    .slick_list{
        height:238px;
        overflow-y:hidden;
        /* width:133%;
        margin-left:-20%; */
    }
    .slick_list::-webkit-scrollbar{
        display: none; /* Chrome, Safari, Opera*/
    }

    .slick_list .slick_card{
    
        border: 1px solid #868686;
        border-radius: 10px;
        margin-left: 15px;
        object-fit: cover;
        height: 236px;
        width: 325px !important;

    }
    #header {
        width: 100%;
        position: fixed;
        top: 0;
        background-color: white;
        z-index: 9099999;
        left: 0;
    }

    .content{
        background-image:url('http://publicservicefair.kr/static/opening_hall_bg.png');
        background-size: cover;
        background-color:#0E0C34;
        margin-top: 172px;
    }

 
    .playlist{
        background-color:#E2E2E2;
        width:60%;
    }

    .content_player{
        margin-top:150px;
        margin-left:auto;
        margin-right:auto;
        width:60vw;
        height:30vw;
        background-color:black;
    }



</style>

<script>
    $('.clip_tab button').click(function () {
        $('.clip_tab li').removeClass('selected');
        $(this).parent().addClass('selected')
    })

    $('.qna_wrap ul li em').click(function () {
        if ($(this).parent().hasClass('selected')) {
            $('.qna_wrap ul li').removeClass('selected');
        } else {
            $('.qna_wrap ul li').removeClass('selected');
            $(this).parent().addClass('selected')
        }
    })


    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);



    function playthis(playurl){
        // var play_video = '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/'+playurl+'?autoplay=1&rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
        // $('.content_player').html(play_video)
        location.href="#playhere";

        $('.content_player').html('<div id ="player"></div>');

        var player = new YT.Player('player', {
                height: '100%',
                width: '100%',
                videoId: playurl,
                autoplay:1,
                rel : 0, //0으로 해놓아야 재생 후 관련 영상이 안뜸
                events: {
                    'onReady': onPlayerReady, 
                    'onStateChange': onPlayerStateChange
                }
            });


        // function onYouTubeIframeAPIReady(fileName) {

        // }

    }

    function onPlayerReady(event) { 
      event.target.playVideo(); 
     } 

    function onPlayerStateChange(event) {
    if(event.data === 0) {
        $('.content_player').html('<div id ="player"></div>');
    }
}

    

</script>



</html>