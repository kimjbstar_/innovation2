<body>

<style>
.content_box{
    left : 0% !important;
}
</style>


<link rel="stylesheet" href="https://quickcar.co.kr/css/common.css?d=<?=md5(date("Y-m-d H:i:s"))?>">
<link rel="stylesheet" href="https://quickcar.co.kr/css/header.css?d=<?=md5(date("Y-m-d H:i:s"))?>">
<link rel="stylesheet" href="https://quickcar.co.kr/css/menu.css?ver=0.0.1">
<link rel="stylesheet" href="https://quickcar.co.kr/css/content.css?d=<?=md5(date("Y-m-d H:i:s"))?>">


<script>

$(function(){
    /*
     * this swallows backspace keys on any non-input element.
     * stops backspace -> back
     */
    var rx = /INPUT|SELECT|TEXTAREA/i;

    $(document).bind("keydown keypress", function(e){
        if( e.which == 8 ){ // 8 == backspace
            if(!rx.test(e.target.tagName) || e.target.disabled || e.target.readOnly ){
                location.href='<?=$url['back']?>';
                e.preventDefault();
                
            }
        }
    });

    $(window).bind("pageshow", function(event) {
    if (event.originalEvent.persisted) {
        location.href='<?=$url['back']?>';
    }
});

});
</script>


<div class="row h-100">
    <div class="col-xs-12 col-sm-12">

        <!--Page Title & Icons Start-->
        <div class="header-icons-container text-center shadow-sm">
            <a href="<?=$url['back']?>">
                <span class="float-left">
                    <img src="<?=$img['back']?>" alt="Back Icon">
                </span>
            </a>
            <span class="title">주문 내역</span>
            <a href="#">
            <span class="float-right menu-open closed">
                        <button class="btn btn-outline-primary top-button">메뉴</button>
                    </span>
            </a>
        </div>
        <!--Page Title & Icons End-->

        <div class="rest-container">

            <div class="bg_inner_type1 padding-rel-b-64">
                <div id="output-orderlist" style="width:100%;display:flex;align-items:center;flex-direction:column;">
                    <script type="text/javascript">
                        $(function () {
                            $('.SYS-address').bind('click', function (e) {
                                var serial_number = $(this).data('serial_number');
                                servRequest('Req_mapShipTrack', 'html', {
                                    'serial_number': serial_number
                                }, function (res, e) {
                                    var ss = $('<div></div>', {
                                        id: 'popWin'
                                    }); //.css('display','none') ;
                                    if ($('#popWin').length) $('#popWin').remove();
                                    $(document.body).append(ss);
                                    ss.append(res);
                                    $('#btn-mapShipTrack-close').unbind('click').bind('click', function (
                                        e) {
                                        $('#layer_pop').empty();
                                        $('#layer_pop, #layer_back').hide();
                                    });
                                    $('#popWin').appendTo('#layer_pop')
                                    //$('#layer_pop').attr('data-target',thisTarget).show();
                                    $('#layer_pop').show();
                                    $('#layer_back').show();
                                });
                            });
                        });
                    </script>




                    <?php if (count($data['orderedlist'])==0) { ?>


                    <div class="content_box margin-rel-b-16">
                        <div class="f f-ju-between padding-rel-b-16 margin-rel-b-16 border-bottom">
                            <div class="f f-col">
                                <p class="font-bold font-size-16">주문한 내역이 없습니다.</p>
                            </div>
                        </div>
                    </div>



                    <?php } else {
                        foreach ($data['orderedlist'] as $vv) { ?>
<!-- 
주문
배차
배송
운행
완료
취소
대기 -->



                    <?php
                        // var_dump($vv);
                        if ($vv[4]->departure_tel_number==$vv[1]->customer_tel_number) {
                            $thisisuser=1;
                        } else {
                            $thisisuser=2;
                        } ?>


                    <div class="content_box margin-rel-b-16" >
                        <div class="f f-ju-between padding-rel-b-16 margin-rel-b-16 border-bottom">
                            <div class="f f-col">
                                <p class="font-bold font-size-13">#<?=$vv[3]->serial_number; ?></p>
                                <p class="margin-rel-t-8 font-bold">
                                    | <span class="font-color-accent"><?=$vv[5]->total_cost; ?></span></p>
                            </div>
                            <div class="f f-col f-al-end">
                                <p>
                                    <span class="badge yellow margin-rel-t-8" style="display:inline-block;width:6rem" ><?=$vv[5]->state; ?></span>
                                    <?php  if ($vv[5]->state=="대기" || $vv[5]->state=="취소" || $vv[5]->state=="완료") { ?>
                                        <span class="badge green margin-rel-t-8" style="display:inline-block;width:6rem"  onclick="location.replace('order/index?departure=<?=$vv[4]->departure_address;?>&destination=<?=$vv[4]->destination_address;?>&thisuser=<?=$thisisuser;?>&dep_name=<?=$vv[4]->departure_company_name;?>&dep_number=<?=preg_replace('/[^0-9]*/s', '', $vv[4]->departure_tel_number);?>&dest_name=<?=$vv[4]->destination_company_name;?>&dest_number=<?=preg_replace('/[^0-9]*/s', '', $vv[4]->destination_tel_number);?>');">재발송</span>
                                    <?php } else { ?>
                                        <!-- <span class="badge green margin-rel-t-8" style="display:inline-block;width:6rem"  onclick="location.replace('order/index?departure=<?=$vv[4]->departure_address;?>&destination=<?=$vv[4]->destination_address;?>&thisuser=<?=$thisisuser;?>&dep_name=<?=$vv[4]->departure_company_name;?>&dep_number=<?=preg_replace('/[^0-9]*/s', '', $vv[4]->departure_tel_number);?>&dest_name=<?=$vv[4]->destination_company_name;?>&dest_number=<?=preg_replace('/[^0-9]*/s', '', $vv[4]->destination_tel_number);?>');">재발송</span> -->
                                        <span class="badge green margin-rel-t-8" style="display:inline-block;width:6rem"  onclick="location.replace('orderdetail?v=<?=$vv[3]->serial_number;?>');">기사 정보 확인</span>
                                    <?php } ?>
                                </p>
                                <p><?=$vv[3]->order_time; ?></p>
                            </div>
                        </div>
                        <div>
                            <div class="" style="">

                                <object data="/svg/ic-from-destination.svg" alt="from" style=""></object>
                                <p class="font-bold margin-rel-b-24" style="display: inline-block;vertical-align: top;"><?=$vv[4]->departure_address; ?></p>

                            </div>
                            <div class="">
                                <object data="/svg/ic-to-destination.svg" alt="to" class="long_dash"></object>
                                <p class="font-bold" style="display: inline-block;vertical-align: top;"><?=$vv[4]->destination_address; ?></p>
                            </div>

                            <!-- <?php  if ($vv[5]->state=="대기" || $vv[5]->state=="주문" || $vv[5]->state=="배차" || $vv[5]->state=="접수") { ?>
                                <span class="badge red margin-rel-t-8" style="display:inline-block;width:100%;border: 2px solid blue;color: blue;"  onclick="location.replace('order/cancel_order?serial=<?=$vv[3]->serial_number;?>&userid=jackkor2');">주문 취소하기</span>
                            <?php } ?> -->
                        </div>
                    </div>


                    <?php
                            }
                        }
                    ?>



                </div>
            </div>




        </div>
    </div>

    
    <?php $this->load->view('common/slideMenu', renderSlideMenu());?>


</div>



<script src="https://quickcar.co.kr/js/lib/jquery.js?lastModify=38b5fe31dc483f36ca2e7aa78545b53f"></script>

<script>

    var vehicle=0;
    var product='';
    var weight='';
    var count_p=0;
    var insert_count_p_num=[];
    var etc_p ="";
    var price=0;
    var add_price=0;
    var distance=0;
    var start_adr="";
    var dest_adr="";


    function get_fee() {
        
        if (vehicle!=0 && vehicle!=-1 &&  product != '' ){

            start_adr=$('#address_start').val();
            dest_adr=$('#address_dest').val();
            var etc_p = $('#product').val();
            var memo =$('#memo_add').val();
            
            $('.calc-progress').css('display','block');

            console.log('https://quickcar.co.kr/Order/getprice?kind='+vehicle+'&doc=1&start='+start_adr+'&end='+dest_adr+'&count_p='+count_p+'&etc_p='+etc_p+'&memo='+memo+'&product='+product);

            fetch('https://quickcar.co.kr/Order/getprice?kind='+vehicle+'&doc=1&start='+start_adr+'&end='+dest_adr+'&count_p='+count_p+'&etc_p='+etc_p+'&memo='+memo+'&product='+product)
            .then(function(response) {
                return response.json();
            })
            .then(function(myJson) {
                console.log(myJson['price']);
                console.log(myJson['add_price']);
                console.log(JSON.stringify(myJson));
                $($('.text-pay_price')[0]).text((myJson['price'])+'원')
                $($('.text-pay_price')[1]).text((myJson['add_price'])+'원')
                if (myJson['add_price']!=0){
                    $('#addprice').css('display','flex');
                }
                else{
                    $('#addprice').css('display','none');
                }

                price=myJson['price'];
                add_price=myJson['add_price'];
                if (distance==0){
                    UTIL.alert("거리 : "+myJson['distance']+ " Km")
                }

                distance=myJson['distance'];
                $('.calc-progress').css('display','none');
            });            
        }
        else{

            $($('.text-pay_price')[0]).text('0원')
            $($('.text-pay_price')[1]).text('0원')
            $('#addprice').css('display','none');
        }

    }





    $(document).ready(function(){


        $('#addprice').css('display','none');
        $('.t_transport .type_select').removeClass( 'selected' );
        $('.content_gray_inner_box').css('display','none');

        $('.t_product .type_select').click(function(){
            console.log($($(this).children('p')[0]).text());
            $('.t_product .type_select').removeClass( 'selected' );
            $(this).addClass('selected');
            product=$($(this).children('p')[0]).text();


            $('.which_product .content_gray_inner_box').css('display','none');
            $($('.which_product .content_gray_inner_box')[$(this).index()-1]).css('display','block');


            get_fee();
        });



        $('.t_transport .type_select').click(function(){
            console.log($($(this).children('p')[0]).text());
            $('.t_transport .type_select').removeClass( 'selected' );
            $(this).addClass('selected');
            $('.which_car .content_gray_inner_box').css('display','none');
            $($('.which_car .content_gray_inner_box')[$(this).index()]).css('display','block');

            var vehicletext=$($(this).children('p')[0]).text();

            // *     'kind'  => '배송수단', (1(오토) / 2(다마스) / 5(라보) / 4(밴) / 6(지하철) / 3(트럭)),
            if (vehicletext=='오토바이'){
                vehicle=1;
                insert_count_p_num=[3,3,2];
            }
            else if (vehicletext=='다마스'){
                vehicle=2;
                insert_count_p_num=[0,20,10];
            }
            else if (vehicletext=='라보'){
                vehicle=5;
                insert_count_p_num=[0,25,20];
            }
            else if (vehicletext=='1톤'){
                insert_count_p_num=[0,0,0];
                vehicle=3;
            }
            else if (vehicletext=='화물'){
                insert_count_p_num=[0,0,0];
                vehicle=-1;
            }


            var addstring="";
            for (var i=0;i<insert_count_p_num[0];i++)
                {
                    addstring+=`<li class="btn-motorcycle_weight" data-motorcycle_weight="`+(i+1)+`"><p>`+(i+1)+`개</p></li>`;
                }
            $($($('.which_product .content_gray_inner_box')[0]).find('.text_select')).html(addstring);


            var addstring="";
            for (var i=0;i<insert_count_p_num[1];i++)
                {
                    addstring+=`<li class="btn-motorcycle_weight" data-motorcycle_weight="`+(i+1)+`"><p>`+(i+1)+`개</p></li>`;
                }
            $($($('.which_product .content_gray_inner_box')[1]).find('.text_select')).html(addstring);


            var addstring="";
            for (var i=0;i<insert_count_p_num[2];i++)
                {
                    addstring+=`<li class="btn-motorcycle_weight" data-motorcycle_weight="`+(i+1)+`"><p>`+(i+1)+`개</p></li>`;
                }
            $($($('.which_product .content_gray_inner_box')[2]).find('.text_select')).html(addstring);




            $('.btn-motorcycle_weight').removeClass( 'selected' );


            $('.which_product .btn-motorcycle_weight').click(function(){
                console.log($(this).data('motorcycle_weight'));
                $('.btn-motorcycle_weight').removeClass( 'selected' );
                $(this).addClass('selected');
                count_p = $(this).data('motorcycle_weight');
                get_fee();
            });

            count_p=0;


            get_fee();

        });



        $('.which_car .btn-motorcycle_weight').click(function(){
            console.log($(this).data('motorcycle_weight'));
            $('.btn-motorcycle_weight').removeClass( 'selected' );
            $(this).addClass('selected');

            weight=$(this).data('motorcycle_weight');


            get_fee();
        });



        $('.which_product .btn-motorcycle_weight').click(function(){
            console.log($(this).data('motorcycle_weight'));
            $('.btn-motorcycle_weight').removeClass( 'selected' );
            $(this).addClass('selected');

            count_p = $(this).data('motorcycle_weight');


            get_fee();
        });




    });


    function do_order(){

            // *     'kind'  => '배송수단', (1(오토) / 2(다마스) / 5(라보) / 4(밴) / 6(지하철) / 3(트럭)),

        var start_adr=$('#address_start').val();
        var dest_adr=$('#address_dest').val();
        var etc_p = $('#product').val();
        var memo =$('#memo_add').val();


        var start_name = $('#start_name').val()
        var start_telno = $('#start_telno').val()
        var dest_telno = $('#dest_telno').val()
        var dest_name = $('#dest_name').val()



        if (price==0){
            UTIL.alert('요금 산정이 되지 않았습니다. 보내시는 물건을 다시 클릭해주세요.');
        }
        else if (vehicle==-1){
            UTIL.alert('화물 물건은 전화주문 부탁드립니다.');
        }
        else if (vehicle==0){
            UTIL.alert('차종을 선택해주세요.');
        }
        else if (product==''){
            UTIL.alert('보내시는 물품을 선택해주세요.');
        }
        else if ($('#address_start').val()==""){
            UTIL.alert('출발지를 입력해주세요.');
        }
        else if ($('#address_dest').val()==""){
            UTIL.alert('도착지를 입력해주세요.');
        }
        else if ($('#start_name').val()==""){
            UTIL.alert('출발지 성함을 입력해주세요.');
        }
        else if ($('#start_telno').val()==""){
            UTIL.alert('출발지 연락처를 입력해주세요.');
        }
        else if ($('#dest_name').val()==""){
            UTIL.alert('도착지 성함을 입력해주세요.');
        }
        else if ($('#dest_telno').val()==""){
            UTIL.alert('도착지 연락처를 입력해주세요.');
        }

        // else if (weight=='' && vehicle==1){
        //     UTIL.alert('물품의 개수를 선택해주세요.')
        // }


        else if (vehicle==1 && ( product=="쇼핑백" || product=="소박스" || product=="대박스" ) && count_p==0){
            UTIL.alert('물품의 개수를 선택해주세요.');
        }
        else if (vehicle==2 && ( product=="소박스" || product=="대박스" ) && count_p==0){
            UTIL.alert('물품의 개수를 선택해주세요.');
        }
        else if (vehicle==5 && ( product=="소박스" || product=="대박스" ) && count_p==0){
            UTIL.alert('물품의 개수를 선택해주세요.');
        }


        else if ( product=="기타" && etc_p==""){
            UTIL.alert('어떤 물품을 보내시나요?');
        }



        else{
            location.replace ('https://quickcar.co.kr/order/order2??kind='+vehicle+'&doc=1&start='+start_adr+'&end='+dest_adr+'&count_p='+count_p+'&etc_p='+etc_p+'&memo='+memo+'&price='+price+'&add_price='+add_price+'&start_name='+start_name+'&start_telno='+start_telno+'&dest_telno='+dest_telno+'&dest_name='+dest_name+'&product='+product);
        }

                // 'user_id'=&c_name=&c_mobile=&s_start=&start_telno=&start_sido=&start_gugun=&start_dong=&s_dest=&dest_telno=&dest_sido=&dest_gugun=&dest_dong=&kind=&pay_gbn=&doc' => $info['doc'],
                // 'sfast' => $info['sfast'],




    }



</script>
