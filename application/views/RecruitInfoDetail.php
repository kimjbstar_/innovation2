<!DOCTYPE html>
<html lang="ko">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>채용정보관</title>
    <meta name="description" content="채용정보관" />
    <meta name="keywords" content="채용정보관" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="" />
    <meta property="og:title" content="채용정보관" />
    <meta property="og:description" content="채용정보관" />
    <link rel="stylesheet" href="/static/css/style.css">
    <link rel="stylesheet" href="/static/css/mobile.css">
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script> -->

    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="/slick/slick.min.js"></script>

    <link rel="stylesheet" type="text/css" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <script type="text/javascript" src="https://unpkg.com/swiper@5.4.5/js/swiper.min.js"></script>



    <script type="text/javascript" src="/slick/slick.min.js"></script>

    <link rel="stylesheet" type="text/css" href="/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="/slick/slick-theme.css"/>

</head>

<body>


    <div class="container">
        <div id="header">
            <div class="top">
                <div class="inner">
                    <a href="http://publicservicefair.kr/Home" class="upper"><span class="blind">인사혁신처</span></a>
                    <a href="http://publicservicefair.kr/Auth/Login" class="btn_login">관리자 로그인</a>
                </div>
            </div>
            <div class="bottom">
                <div class="inner">
                    <a href="http://publicservicefair.kr/Home" class="logo"><span class="blind">2020 온라인 공직박람회</span></a>
                    <ul>
                        <li>
                            <a href="javascript:alert('오픈 준비 중입니다.');">영상관</a>
                        </li>
                        <li>
                            <a href="javascript:alert('오픈 준비 중입니다.');">주제관</a>
                        </li>
                        <li>
                            <a href="javascript:alert('오픈 준비 중입니다.');">채용정보관</a>
                        </li>
                        <li>
                            <a href="http://publicservicefair.kr/Participate">참여관</a>
                        </li>
                        <li>
                            <a href="http://publicservicefair.kr/Seminar">세미나관</a>
                        </li>
                        <li>
                            <a href="http://publicservicefair.kr/CommunicationEvent">소통ㆍ이벤트</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="contentbox">
                <div class="content_header">
                    나에게 꼭 맞는 채용정보
                </div>
                <div class="content_title">
                    채용정보관  >  채용정보상세
                </div>
                <div class="content_pictures">
                    <div class="slick_list">


                        <?php


                            foreach (explode("//",$data[0]['slide_images']) as $image){

                                ?>
                                    <img class="slick_card" src="/uploaded_images/<?=$image;?>" />

                                <?php
                            }

                            // if (count(explode("//",$data[0]['slide_images']))<4){
                            //     $i = 1; //i변수에 1을 대입합니다.

                            //     while($i<=(4-count(explode("//",$data[0]['slide_images'])))) //i가 10보다 작거나 같을 때 반복합니다
                            //     {
                            //         echo "<img class=\"slick_card\" src=\"/uploaded_images/slick_none_image.png\" />   "; 
                            //         $i++; //i를 1씩 증가합니다.(증감식)
                            //     }
                            // }


                        ?>


                    </div>
            


                </div>

                <div class="content_company">
                    <img src="/uploaded_images/<?=$data[0]['logo_image']?>">
                    <div class="content_company_title">
                        <div class="content_company_title1"><?=$data[0]['company_name']?></div>
                        <div class="content_company_title2"><?=$data[0]['company_category']?> | <?=$data[0]['location']?></div>
                    </div>
                </div>
                <div class="content_tags">

                    <?php

                        foreach (explode(",",$data[0]['company_tags']) as $tag){
                            ?>
                            <div class="tag">#<?=$tag;?></div>
                            <?php
                        }
                    ?>
                </div>
                
            </div>
            <hr></hr>
            <div class="contentbox">
                <div class="content_company_info">


                    <div class="frame">
                        <div class="qna_wrap">
                            <ul>
                                <li>
                                    <em>기관 정보</em>
                                    <div class="ptag">
                                        <?php
                                            echo html_entity_decode($data[0]['company_info']);

                                        ?>

                                    </div>
                                </li>
                                <li>
                                    <em>채용정보</em>
                                    <div class="ptag">
                                        <?php
                                            echo html_entity_decode($data[0]['recruit_info']);
                                        ?>
                                    </div>
                                </li>
                                <?php if ($data[0]['faqs']!=''){
                                    ?>


                                    <li>
                                        <em>FAQ</em>
                                        <div class="ptag">

                                        <?php


                                            foreach (explode("%%%",$data[0]['faqs']) as $faq){
                                                    $faq_q = explode("^Q^",explode("^A^",$faq)[0])[1];
                                                    $faq_a = explode("^A^",$faq)[1];

                                                ?>
                                                    <p style="font-weight:600;font-size:1.2em;"><?=$faq_q;?></p>
                                                    <p style="padding-left:15px;"><?=$faq_a;?></p>
                                                <?php
                                            }
                                        ?>


                                        </div>
                                    </li>

                                    <?php
                                }
                                ?>






                            </ul>
                        </div>
                    </div>



                </div>
            </div>
        </div>
        <div id="footer">
            <div class="inner">
                <div class="top">
                    <a href="http://publicservicefair.kr/Home" class="footer_logo"><span class="blind">인사혁신처</span></a>
                    <ul>
                        <li class="fb">
                            <a href="https://ko-kr.facebook.com/miraesaram/"><span class="blind">facebook</span></a>
                        </li>
                        <li class="tw">
                            <a href="https://twitter.com/miraesaram"><span class="blind">twitter</span></a>
                        </li>
                        <li class="ig">
                            <a href="https://www.instagram.com/mpm_kr"><span class="blind">instagram</span></a>
                        </li>
                        <li class="yt">
                            <a href="https://www.youtube.com/channel/UCTlXbNVlKyqLmDTqmlrdEFA"><span class="blind">youtube</span></a>
                        </li>
                    </ul>
                </div>
                <div class="bottom">
                    <ul>
                        <li><a href="http://www.mpm.go.kr/mpm/useinfo/privacy/">개인정보 처리방침</a></li>
                        <li><a href="http://www.mpm.go.kr/mpm/useinfo/media/">영상정보처리기 운영ㆍ관리방침</a></li>
                        <li><a href="http://www.mpm.go.kr/mpm/useinfo/copyrightPolicy/">저작권보호 및 이용정책</a></li>
                    </ul>
                    <p>30102 세종특별자치시 한누리대로 499(어진동) 세종포스트빌딩 6층~12층</p>
                    <p>copyright© Ministry of Personnel Management. All Rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
    <script src="/static/js/common.js"></script>
</body>
<style>
    .contentbox{
        width:75%;
        margin: 0 auto;
    }

    .content_company_info{
        padding-bottom:200px;

    }
    

    .qna_wrap{

        border : 2px solid #CECECE;
    }



    .content_header{
        margin-top:50px;
        background-color:#27218F;
        width: calc( 100% - 70px );
        height:90px;
        color:white;
        border-radius:10px;
        padding-left:70px;
        padding-top:50px;
        font-size:30px;
        vertical-align:middle;
    }

    .content_title{
        font-weight:600;
        color:black;
        margin-top:20px;
        font-size:13px;
    }
    .content_pictures{
        height: 250px;
        margin-top: 20px;
        margin-bottom: 20px;
        width: 133%;
        
    }

    .content_company{

    }
    .content_company img{
        
        border-radius: 70%;
        border:1px solid #B7B7B7;
        overflow: hidden;
        width: 90px;
        object-fit: contain;
        height: 90px;
        display:inline-block;
    }
    .content_company_title{
        display:inline-block;
        padding-left:20px;
        padding-bottom:50px;
    }

    .content_company_title1{
        font-size:30px;
        font-weight:600;
        line-height:2em;

    }
    .content_company_title2{
        font-size:15px;
        line-height:1em;
        font-weight:300;
        
    }



    .content_tags{
        margin-bottom:30px;
    }

    .tag{
        border:1px solid #B7B7B7;
        width:120px;
        text-align:center;
        border-radius:15px;
        padding-top:5px;
        padding-bottom:5px;
        display:inline-block;
    }



    hr{
        background-color:#CDCDCD;
    }


    .qna_wrap ul{
        margin-top:0px;
        padding:0px;
    }

    .qna_wrap ul li{
        padding:0px ;
        color:#353535;
        border-bottom: 2px solid #CECECE;
    }


    .qna_wrap ul li em{
        padding : 20px;
    }

    .ptag{
        background-color:white;
        overflow-x:scroll;
    }


    .ptag img{
        max-width:100%;
    }


    .qna_wrap ul li .ptag{
        padding : 20px;
    }

    .qna_wrap ul li p{
        font-size:1em;
        margin-top:0;
    }


    .qna_wrap ul .selected{
        background-color:#EBEBEB;
    }

    .qna_wrap ul .selected p{
        /* background-color:white; */
    }


    .qna_wrap ul li em, .qna_wrap ul li a{
        color:#353535;
        font-size:20px;
    }



    .qna_wrap ul li::after {
        content: "∨";
        font-size: 20px;
        position: absolute;
        top: 25px;
        right: 20px;
        color: #838383;
        line-height: 40px;
    }

    .slick_list{
        height:238px;
        overflow-y:hidden;
        /* width:133%;
        margin-left:-20%; */
    }
    .slick_list::-webkit-scrollbar{
        display: none; /* Chrome, Safari, Opera*/
    }

    .slick_list .slick_card{
    
        border: 1px solid #868686;
        border-radius: 10px;
        margin-right: 15px;
        object-fit: cover;
        height: 236px;
        width: 325px !important;

    }
    #header {
    width: 100%;
    position: fixed;
    top: 0;
    background-color: white;
    z-index: 9099999;
    left: 0;


    }


    .qna_wrap ul li.selected .ptag {
        display: block;
    }

    .qna_wrap ul li .ptag {
        display: none;
    }

</style>

<script>
    $('.clip_tab button').click(function () {
        $('.clip_tab li').removeClass('selected');
        $(this).parent().addClass('selected')
    })

    $('.qna_wrap ul li em').click(function () {
        if ($(this).parent().hasClass('selected')) {
            $('.qna_wrap ul li').removeClass('selected');
        } else {
            $('.qna_wrap ul li').removeClass('selected');
            $(this).parent().addClass('selected')
        }
    })

    var $jq = jQuery.noConflict();
    $jq(document).ready(function(){

      $jq('.slick_list').slick({

        lazyLoad: 'ondemand',
        slidesToShow: 4,
        slidesToScroll: 2,
        autoplay: true,
        infinite: true,
        speed: 500,
        // fade: true,


      });



    });
    

</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
document.addEventListener('DOMContentLoaded', function(){



    var imagemap = '                   <div class="inner">                <div class="top">                    <a  href="http://publicservicefair.kr/Home" class="footer_logo"><span class="blind">인사혁신처</span></a>                    <ul>                        <li class="fb">                            <a target="_blank" href="https://ko-kr.facebook.com/miraesaram/"><span class="blind">facebook</span></a>                        </li>                        <li class="tw">                            <a href="https://twitter.com/miraesaram"><span class="blind">twitter</span></a>                        </li>                        <li class="ig">                            <a target="_blank" href="https://www.instagram.com/mpm_kr"><span class="blind">instagram</span></a>                        </li>                        <li class="yt">                            <a target="_blank" href="https://www.youtube.com/channel/UCTlXbNVlKyqLmDTqmlrdEFA"><span class="blind">youtube</span></a>                        </li>                    </ul>                </div>                <div class="bottom">                    <ul>                        <li><a target="_blank" href="http://www.mpm.go.kr/mpm/useinfo/privacy/">개인정보 처리방침</a></li>                        <li><a target="_blank" href="http://www.mpm.go.kr/mpm/useinfo/media/">영상정보처리기 운영ㆍ관리방침</a></li>                        <li><a target="_blank" href="http://www.mpm.go.kr/mpm/useinfo/copyrightPolicy/">저작권보호 및 이용정책</a></li>                    </ul>                    <p>30102 세종특별자치시 한누리대로 499(어진동) 세종포스트빌딩 6층~12층</p>                    <p>copyright© Ministry of Personnel Management. All Rights reserved.</p>                </div>            </div>        ';



    $("#footer").html(imagemap);
    // $('form').attr('method','post');
    $($('.nav_bottom a')[4]).attr('href','http://publicservicefair.kr/Participate');
    // $($('.nav_bottom a')[2]).attr('href','http://publicservicefair.kr/ThemeHall');


    var pcheader = '        <div id="header">            <div class="top">                <div class="inner">                    <a target="_blank" href="http://publicservicefair.kr/Home" class="upper"><span class="blind">인사혁신처</span></a>                                 </div>            </div>            <div class="bottom">                <div class="inner">                    <a  href="http://publicservicefair.kr/Home" class="logo"><img src="/static/media/image5.d0ec5abb.png" style="margin-top:15px;"></a>                    <ul>                        <li>                            <a  href="http://publicservicefair.kr/OpeningHall">영상관</a>                        </li>                        <li>                            <a  href="http://publicservicefair.kr/ThemeHall">주제관</a>                        </li>                        <li>                            <a  href="http://publicservicefair.kr/RecruitInfo">채용정보관</a>                        </li>                        <li>                            <a href="http://publicservicefair.kr/Participate">참여관</a>                        </li>                        <li>                            <a href="http://publicservicefair.kr/Seminar">세미나관</a>                        </li>                        <li>                            <a href="http://publicservicefair.kr/CommunicationEvent">소통ㆍ이벤트</a>                        </li>                    </ul>                </div>            </div>        </div><link rel="stylesheet" href="/static/css/style.css">';
    $("#header").html(pcheader);

});
</script>

</html>