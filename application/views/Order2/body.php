<body>
    <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
    <link rel="stylesheet" href="https://quickcar.co.kr/css/common.css?d=<?=md5(date("Y-m-d H:i:s"))?>">
    <link rel="stylesheet" href="https://quickcar.co.kr/css/header.css?d=<?=md5(date("Y-m-d H:i:s"))?>">
    <link rel="stylesheet" href="https://quickcar.co.kr/css/menu.css?ver=0.0.1">
    <link rel="stylesheet" href="https://quickcar.co.kr/css/content.css?d=<?=md5(date("Y-m-d H:i:s"))?>">
    
    <div class="row h-100">
        <div class="col-xs-12 col-sm-12">
            <div class="header-icons-container text-center shadow-sm">
                <a href="<?=$url['back']?>">
                    <span class="float-left">
                        <img src="<?=$img['back']?>" alt="Back Icon">
                    </span>
                </a>
                <span class="title">퀵서비스 주문</span>
                <a href="#">
                <span class="float-right menu-open closed">
                        <button class="btn btn-outline-primary top-button">메뉴</button>
                    </span>
                </a>
            </div>

            <div class="rest-container">

                <div class="content_box margin-rel-b-20" style="margin-bottom:300px !important;">
                    <form id="frmOrder" name="frmOrder" method="post">
                        <input type="hidden" name="start_name" value="상호">
                        <input type="hidden" name="start_telno" value="010-2534-9557">
                        <input type="hidden" name="start_sido" value="서울">
                        <input type="hidden" name="start_gugun" value="금천구">
                        <input type="hidden" name="start_dong" value="독산동">
                        <input type="hidden" name="start_location" value="서울 금천구 시흥대로101길 38">
                        <input type="hidden" name="start_address" value="서울 금천구 시흥대로101길 38">
                        <input type="hidden" name="address_start" value="서울 금천구 시흥대로101길 38">
                        <input type="hidden" name="dest_name" value="테스트">
                        <input type="hidden" name="dest_telno" value="01037573400">
                        <input type="hidden" name="dest_sido" value="서울">
                        <input type="hidden" name="dest_gugun" value="강남구">
                        <input type="hidden" name="dest_dong" value="역삼동">
                        <input type="hidden" name="dest_location" value="서울 강남구 언주로85길 23-10">
                        <input type="hidden" name="dest_address" value="서울 강남구 언주로85길 23-10">
                        <input type="hidden" name="address_dest" value="서울 강남구 언주로85길 23-10">
                        <input type="hidden" name="item_type" value="1">
                        <input type="hidden" name="kind" value="1">
                        <input type="hidden" name="memo" value="">
                        <input type="hidden" name="memo_add" value="">
                        <input type="hidden" name="motorcycle_weight" value="9kg">
                        <input type="hidden" name="sfast" value="1">
                        <div class="f f-col">
                            <p class="content_title margin-rel-b-12"><strong>결제 방법을 선택해주세요.</strong></p>
                            <div class="f-al-center margin-rel-b-26 payType_class">
                                <div class="content_box"
                                    style="margin: 0px auto;margin-left: 0px;width:100%;padding:0.4rem 0px;box-shadow: none;">
                                    <label class="radio_container margin-rel-r-22">
                                        <input type="radio" name="pay_gbn" value="1" checked="">
                                        <span class="radio"></span>
                                        <p>선불</p>
                                    </label>
                                    <p class="content_title margin-rel-t-8" style="padding-left:30px;"><span
                                            style="color: #0066d0;">출발지에서 현금 또는 송금</span></p>
                                </div>
                                <div class="content_box"
                                    style="margin: 0px auto;margin-left: 0px;width:100%;padding:0.4rem 0px;box-shadow: none;">
                                    <label class="radio_container margin-rel-r-22">
                                        <input type="radio" name="pay_gbn" value="2">
                                        <span class="radio"></span>
                                        <p>착불</p>
                                    </label>
                                    <p class="content_title margin-rel-t-8" style="padding-left:30px;"><span
                                            style="color: #0066d0;">도착지에서 현금 또는 송금</span></p>
                                </div>
                                <?php
                                    if ($this->session->creditPay) {
                                        ?>                                
                                    <div class="content_box" style="margin: 0px auto;margin-left: 0px;width:100%;padding:0.4rem 0px;box-shadow: none;">
                                        <label class="radio_container margin-rel-r-22">
                                            <input type="radio" name="pay_gbn" value="3">
                                            <span class="radio"></span>
                                            <p>신용</p>
                                        </label>
                                        <p class="content_title margin-rel-t-8" style="padding-left:30px;"><span style="color: #0066d0;">기존 당사와 결제계약업체는 체크해 주세요.</span></p>
                                    </div>
                                <?php
                                    }
                                ?>
                                <div class="content_box"
                                    style="margin: 0px auto;margin-left: 0px;width:100%;padding:0.4rem 0px;box-shadow: none;">
                                    <label class="radio_container margin-rel-r-22">
                                        <input type="radio" name="pay_gbn" value="4">
                                        <span class="radio"></span>
                                        <p>카드</p>
                                    </label>
                                    <p class="content_title margin-rel-t-8" style="padding-left:30px;"><span
                                            style="color: #0066d0;"><span class="text-danger font-weight-bold">( 부가세 별도 )</span> 카드번호, 유효기간, 메일주소를 주시면 영수증 발급됨니다.</span>
                                    </p>

                                    <div id="card_info" class="data-kind-1 content_gray_inner_box hide">
                                        <p class="margin-rel-b-8"><strong>카드정보를 입력해주세요.</strong></p>
                                        <ul class="text_select">
                                            <input type="number" id="card_number"  name="start_telno" placeholder="카드번호" value=""  style="width:100%;height: 50px;padding-left: 10px;border: 1px solid green;">
                                        </ul>
                                        <ul class="text_select">
                                            <strong>유효기간&nbsp;&nbsp;:&nbsp;&nbsp;</strong><input type="number" id="card_year"  name="start_telno" placeholder="12" value=""  style="height: 30px;padding-left: 10px; width:20%;border: 1px solid green;"><strong>&nbsp;/&nbsp;</strong><input type="number" id="card_month"  name="start_telno" placeholder="24" value=""  style="height: 30px;width:20%;padding-left: 10px;border: 1px solid green;">
                                        </ul>
                                        <ul class="text_select">
                                            <input type="text" id="card_email"  name="start_telno" placeholder="이메일주소( 생략가능 - 영수증발송용 )" value=""  style="width:100%;height: 50px;padding-left: 10px;border: 1px solid green;">
                                        </ul>
                                    </div>

                                </div>

                            </div>
                            <p class="content_title margin-rel-b-12 "><strong>배송 종류를 선택해주세요.</strong></p>
                            <div class="f f-al-center margin-rel-b-26 doc_class">
                                <label class="radio_container margin-rel-r-22">
                                    <input type="radio" checked="checked" name="doc" value="1">
                                    <span class="radio"></span>
                                    <p>편도</p>
                                </label>
                                <label class="radio_container margin-rel-r-22">
                                    <input type="radio" name="doc" value="3">
                                    <span class="radio"></span>
                                    <p>왕복</p>
                                </label>
                                <label class="radio_container margin-rel-r-22">
                                    <input type="radio" name="doc" value="3">
                                    <span class="radio"></span>
                                    <p>경유</p>
                                </label>
                                <label class="radio_container margin-rel-r-22">
                                    <input type="radio" name="doc" value="3">
                                    <span class="radio"></span>
                                    <p>긴급</p>
                                </label>
                            </div>
                            <div id="kakaoNotiContainer" style="width:100%">
                                <p class="content_title margin-rel-b-12 ">
                                    <strong>현재 접수 고객님 휴대전화를 입력해주세요</strong>
                                </p>
                                <input type="tel" class="form-control input w-30"  name="" placeholder="알림톡 받을 핸드폰번호" style="width: 57%;" id="kakaoNotiPhoneNumber">
                                <div id="notiKakaoContainer">

                                    <div class="form-check" style="padding-bottom: 2%;margin-top:2%">
                                        <input class="form-check-input" type="radio" name="notiPhoneNumber" id="startLocatePhoneNumber" value="<?=$data['startTelNo']?>">
                                        <label class="form-check-label" for="startLocatePhoneNumber">
                                            출발지 연락처와 동일
                                        </label>
                                    </div>
                                    
                                    <div class="form-check" style="padding-bottom: 2%;">
                                        <input class="form-check-input" type="radio" name="notiPhoneNumber" id="endLocatePhoneNumber" value="<?=$data['endTelNo']?>">
                                        <label class="form-check-label" for="endLocatePhoneNumber">
                                            도착지 연락처와 동일
                                        </label>
                                    </div>
                                    
                                    <div class="form-check" style="padding-bottom: 2%;">
                                        <input class="form-check-input" type="radio" name="notiPhoneNumber" id="userType" value="option3">
                                        <label class="form-check-label" for="userType">
                                            직접입력
                                        </label>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="data-kind-3 content_gray_inner_box via_order hide" data-motorcycle_weight="1.4t">
                                <a href="tel:1661-2482"><p class="content_gray_title margin-rel-b-8">경유 주문은 1661-2482 전화문의 바랍니다.</p><p>(터치하여 전화연결)</p></a>
                            </div>
                            <div class="data-kind-3 content_gray_inner_box urgent_order hide" data-motorcycle_weight="1.4t">
                                <a href="tel:1661-2482"><p class="content_gray_title margin-rel-b-8">긴급 주문은 1661-2482 전화문의 바랍니다.</p><p>(터치하여 전화연결)</p></a>
                            </div>
                        </div>
                    </form>
                </div>




                <div class="bottom_bar" style="position:relative;">
                    <div class="calc-progress"
                        style="position: absolute; width: calc(100% - 2.24rem); height: 55px; line-height: 55px; background: rgb(3, 207, 93); border-radius: 10px; color: rgb(255, 255, 255); text-align: center; vertical-align: middle; z-index: 100; font-size: x-large; bottom: 10px; display: none;">
                        운임료 계산중입니다...</div>


                        

                    <div id="block-price" class="f f-col">
                        <div class="block-price-itm f f-ju-between f-al-center margin-rel-b-8" style="width:100%;">
                            <div class="f">
                                <p class="text-title">추천 요금</p>
                            </div>
                            <p id="block-pay_price" class="font-bold font-size-16 font-color-accent">
                                <span class="text-pay_price-detail"
                                    style="margin-right: 5px;font-size: 0.89rem;"></span>
                                <span class="text-pay_price" style="font-weight: bold;"><?=number_format($data['price']+$data['add_price']);?>  원</span>
                            </p>
                        </div>
                        <?php if( (!is_null($this->session->userId)) && $data['mileage'][0]->sum_mileage > 10000 ) {?>
                        <div class="block-price-itm f f-ju-between f-al-center" style="width:100%;">

                                <label class="radio_container margin-rel-r-22">
                                    <input type="radio" name="priceType" value="1" checked="checked">
                                    <span class="radio"></span>
                                    <p>금액 요청</p>
                                </label>

                                
                                <label class="radio_container margin-rel-r-22">
                                    <input type="radio" name="priceType" value="2">
                                    <span class="radio"></span>
                                    <p>마일리지 사용</p>
                                </label>
                                

                        </div>
                        <?php }?>
                        <hr style="margin:.7rem auto;width:70%;">
                                <script>
                                var priceType = '1';
                                var isMileage = false;
                                $(function(){
                                    $(".page-1").show();
                                    $(".page-2").hide();
                                    $('input[type=radio][name=priceType]').change(function() {
                                        if (this.value == '1') {
                                            $(".page-1").show();
                                            $(".page-2").hide();
                                            priceType = '1';
                                            isMileage = false;
                                        }
                                        else if (this.value == '2') {
                                            $(".page-1").hide();
                                            $(".page-2").show();
                                            priceType = '2';
                                            isMileage = true;
                                        }
                                    });
                                });
                                </script>
                        
                        

                        
                        <div class="block-price-itm f-ju-between f-al-center margin-rel-b-8 page-1" style="width:100%; text-align:center;margin-top:1rem;">
                            <div class="">
                                <p class="text-title"><strong style="font-size: 1.1rem;"> 
                                <ion-icon name="caret-forward-outline"></ion-icon>
                                 요청 금액 (수정가능)
                                 <ion-icon name="caret-back-outline"></ion-icon>
                                </strong></p>
                            </div>
                            <p id="block-pay_price" class="font-bold font-color-accent" style="font-size: 1.3rem;">
                                <span class="text-pay_price-detail"
                                    style="margin-right: 5px;"></span>
                                <span class="text-pay_price" style="font-weight: bold; font-size: 1.3rem;">

                                <input type="text" inputmode="numeric" pattern="[0-9]*" id="want_price" placeholder="금액 입력" value="<?=number_format($data['price']+$data['add_price']);?>"  style=" text-align:right;width: calc(100% - 50px); padding: .3rem .4rem;border: 3px solid #fb5756;">  
                                원</span>
                            </p>
                        </div>

                        <div class="block-price-itm f-ju-between f-al-center margin-rel-b-8 page-2" style="width:100%; text-align:center;margin-top:1rem;">
                            <div class="">
                                <p class="text-title"><strong style="font-size: 1.1rem;"> 
                                <ion-icon name="caret-forward-outline"></ion-icon>
                                 마일리지 사용
                                 <ion-icon name="caret-back-outline"></ion-icon>
                                </strong></p>
                            </div>
                            <p id="block-pay_price" class="font-bold font-color-accent" style="font-size: 1.3rem;">
                                <span class="text-pay_price-detail d-block" style="margin-right: 5px;">마일리지사용금액 (<?=$data['useAbleMileage']?>원)</span>
                                <span class="text-pay_price" style="font-weight: bold; font-size: 1.3rem;">
                                <!-- <button onclick="setMileage(0)" class="btn btn-secondary d-inline-block" style="height: 3rem; padding: 0rem 1rem; line-height:inherit;">감소</button> -->
                                <input type="number" data-origin="<?=$data['useAbleMileage']?>" id="mileage" class="d-inline-block" value="0" max="<?=$data['price'] + $data['add_price']?>" min="0" placeholder="금액 입력" style=" text-align:right;width: calc(100% - 180px); padding: .3rem .4rem;">  
                                <span >원</span>
                                <!-- <button onclick="setMileage(1)" class="btn btn-secondary d-inline-block" style="height: 3rem; padding: 0rem 1rem; line-height:inherit;">추가</button> -->
                                </span>
                            </p>

                                <hr>
                            <p id="block-pay_price" class="font-bold font-color-accent" style="font-size: 1.3rem;">
                                <span class="text-pay_price-detail"
                                    style="margin-right: 5px;">적용요금</span>
                                <span class="text-pay_price" style="font-weight: bold; font-size: 1.3rem;">

                                <input type="text" id="want_price2" placeholder="금액 입력" value="<?=number_format($data['price']+$data['add_price']);?>"  style=" text-align:right;width: calc(100% - 50px); padding: .3rem .4rem;border: 3px solid #fb5756;">  
                                원</span>
                            </p>

                        </div>



                    </div>
                    <button onclick="do_order()"  id="btn-Confirm" class="btn green w-100">
                        <p>주문확정</p>
                    </button>
                </div>


            </div>
        </div>


        <?php $this->load->view('common/slideMenu', renderSlideMenu());?>


    </div>



    <script src="https://quickcar.co.kr/js/lib/jquery.js?lastModify=38b5fe31dc483f36ca2e7aa78545b53f"></script>

    <script>
        var payType = 1;
        var doc = 1;
        var sfast = 1;
        var price = <?=$data['price']+$data['add_price'];?>;

        function getUrlQuery() {
            return decodeURI(location.search.substring(1)).split('&').map(v => v.split('=').map(i => i.trim())).reduce((prev, curr) => {
                                    prev[curr[0]] = curr[1];
                                    return prev;
                                }, {});
        }
        $('#card_info').css('display','none');

        $(document).ready(function () {

            $('.urgent_order').css('display','none');
            $('.via_order').css('display','none');
            const $kakaoNotiTo = $('#kakaoNotiPhoneNumber');
            $('#kakaoNotiContainer').on('change', function(e){ 
                const $target = $(e.target);
                if ($target[0].nodeName.toLowerCase() !== 'input') {
                    return;
                }
                
                if ($target.attr('id').toLowerCase() !== 'usertype') {
                    $kakaoNotiTo.val($target.val());
                    $kakaoNotiTo.attr('disabled', 'disabled');
                } else {
                    $kakaoNotiTo.val('');
                    $kakaoNotiTo.removeAttr('disabled');
                }
                
            });

            $('.payType_class label').click(function () {

                var payType_text = $($(this).find('p')).text();
                console.log(payType_text);
                if (payType_text == '선불') {
                    payType = 1;
                    $('#card_info').css('display','none');
                } else if (payType_text == '착불') {
                    payType = 2;
                    $('#card_info').css('display','none');
                } else if (payType_text == '신용') {
                    payType = 3;
                    $('#card_info').css('display','none');
                } else if (payType_text == '카드') {
                    payType = 6;
                    $('#card_info').css('display','block');
                }
            });
            
            $('.doc_class label').click(function () {

                var payType_text = $($(this).find('p')).text();
                console.log(payType_text);
                if (payType_text == '편도') {
                    doc = 1;
                    sfast = 1;
                    $($('.text-pay_price')[0]).text('<?=number_format($data['price']+$data['add_price']);?>  원')
                    $('#want_price').val('<?=$data['price']+$data['add_price'];?>')
                    price = <?=$data['price']+$data['add_price'];?>;
                    $('.via_order').css('display','none');
                    $('.urgent_order').css('display','none');
                } else if (payType_text == '왕복') {
                    doc = 3;
                    sfast = 1;
                    $($('.text-pay_price')[0]).text('<?=number_format(2*($data['price']+$data['add_price']));?>  원')
                    $('#want_price').val('<?=2*($data['price']+$data['add_price']);?>')
                    price = <?=2*($data['price']+$data['add_price']);?>;
                    $('.via_order').css('display','none');
                    $('.urgent_order').css('display','none');
                } else if (payType_text == '경유') {
                    doc = 5;
                    sfast = 1;
                    // UTIL.alert('경유 주문은 전화문의 바랍니다.');
                    $('.via_order').css('display','block');
                    $('.urgent_order').css('display','none');
                } else if (payType_text == '긴급') {
                    doc = 1;
                    sfast = 3;
                    // UTIL.alert('경유 주문은 전화문의 바랍니다.');
                    $('.urgent_order').css('display','block');
                    $('.via_order').css('display','none');
                }
            });

           
            var $input = $( "#want_price" );

            $input.on( "keyup", function( event ) {
            
                // 1.
                var selection = window.getSelection().toString();
                if ( selection !== '' ) {
                    return;
                }
            
                // 2.
                if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
                    return;
                }
                
                    // 3
                    var $this = $( this );
                    var input = $this.val();
            
                    // 4
                    var input = input.replace(/[\D\s\._\-]+/g, "");
            
                    // 5
                    input = input ? parseInt( input, 10 ) : 0;
            
                    // 6
                    $this.val( function() {
                        return ( input === 0 ) ? "" : input.toLocaleString( "en-US" );
                    });

            } );

});



        function do_order(){

                // *     'kind'  => '배송수단', (1(오토) / 2(다마스) / 5(라보) / 4(밴) / 6(지하철) / 3(트럭)),

            var start_adr=$('#address_start').text();
            var dest_adr=$('#address_dest').text();
            var etc_p = $('#product').val();
            var memo =$('#memo_add').val();
            var want_price = $('#want_price').val().replace(/,/g,"");
            var card_number = $('#card_number').val();
            var card_month =$('#card_month').val();
            var card_year = $('#card_year').val()
            var card_email = $('#card_email').val();
            const mileage = $("#mileage");

            const parsedQuery = getUrlQuery();

            // 할증요금이 있는경우
            if (Number(parsedQuery.add_price) > 1) {
                sfast = 8; // 할증 타입으로 전환
            }
            
            // 결제를 카드로 하는 경우
            if (Number(payType) === 6) {
                const requireValueSelector = [
                    '#card_number', '#card_year',
                    '#card_month'
                ];
                const isNotEnoughCardInfo = requireValueSelector.filter(selector => $(selector).val().trim() === '');
                // 정보가 없을 시
                if (isNotEnoughCardInfo.length) {
                    UTIL.alert(`카드 정보를 입력 바랍니다.`, () => {
                        $(isNotEnoughCardInfo[0]).focus()
                    });
                    return;
                }
            }
            
            if (UTIL.extractNumberFromString(mileage.attr('data-origin')) < Number(mileage.val())) {
                UTIL.alert(`최대 사용 마일리지는 ${mileage.attr('data-origin')}입니다.`);

            } else if (Number(mileage.val()) % 1000 !== 0) {
                UTIL.alert('마일리지는 1000원 단위로 사용 가능합니다.');

            } else if (doc==5){
                UTIL.alert('경유 주문은 전화문의 바랍니다.');
            }
            else if (want_price < price * 0.7){
                UTIL.alert('추천요금의 30%이내 할인요청만 가능합니다.');
            }
            else if (sfast == 3){
                UTIL.alert('긴급 물건은 전화문의 바랍니다.');
            }
            else if ($('#address_start').val()==""){
                UTIL.alert('출발지를 입력해주세요.');
            } else if ($('#kakaoNotiPhoneNumber').val() === "") {
                UTIL.alert('배송알림받을 핸드폰 번호를 입력하시기 바랍니다.');
                return;
            } else {



                // priceType : 금액요청, 마일리지사용  '1' '2'
                // isMileage : 마일리지 사용여부   true false

                //var want_price = $('#want_price').val().replace(/,/g,"");
                if(priceType == '1'){
                  console.log('priceType 1');
                }else if(priceType == '2'){
                    console.log('priceType 2');
                    want_price = $('#want_price2').val().replace(/,/g,"");
                }
                //isMileage

                var link = `/order/do_order?`;
                link += `payType=${payType}`;
                link += `&doc=${doc}`;
                link += `&sfast=${sfast}`;
                link += `&want_price=${want_price}`;
                link += `&card_number=${card_number}`;
                link += `&card_month=${card_month}`;
                link += `&card_year=${card_year}`;
                link += `&card_email=${card_email}`;
                link += `&is_mileage=${(isMileage) ? '1' : '0'}`;
                link += `&notiUserInfo=${$('#kakaoNotiPhoneNumber').val()}`;
                link += `&mileage=${$("#mileage").val()}`;

                location.replace(link);
            }
        }



        /**
            현재 마일리지 가지고 와야됨.
        */
        // 기본 요금
        var max = Number($("#want_price2").val().replace(/,/gi, ""));
        var mileageMax = Number(<?=$data['mileage'][0]->sum_mileage?>);
        var mileageCheck = 0;
        if( mileageMax < max ){
            mileageCheck = 1;
        }
        function setMileage(check){

            // 마일리지 사용
            var current = Number($("#mileage").val());

            if(check == 0){
                current -= 1000;
            }else if(check == 1){
                current += 1000;
                if(current > mileageMax){
                    current -= 1000;
                    return;
                }
            }

            if(current < 0 )
                current = 0;
            if(current > max){
                current = max;
            }

            $("#mileage").val(current);
            $("#want_price2").val(max - current);
        }

    </script>