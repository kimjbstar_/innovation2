<!DOCTYPE html>
<html lang="ko">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>공직역사가치관</title>
    <meta name="description" content="공직역사가치관" />
    <meta name="keywords" content="공직역사가치관" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="" />
    <meta property="og:title" content="공직역사가치관" />
    <meta property="og:description" content="공직역사가치관" />
    <link rel="stylesheet" href="/static/css/style.css">
    <!--<link rel="stylesheet" href="/static/css/mobile.css">-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <script type="text/javascript" src="https://unpkg.com/swiper@5.4.5/js/swiper.min.js"></script>
</head>

<body>
    <div class="container">
        <div id="header">
            <div class="top">
                <div class="inner">
                    <a href="#" class="upper"><span class="blind">인사혁신처</span></a>
                    <a href="#" class="btn_login">회원가입ㆍ로그인</a>
                </div>
            </div>
            <div class="bottom">
                <div class="inner">
                    <a href="#" class="logo"><span class="blind">2020 온라인 공직박람회</span></a>
                    <ul>
                        <li>
                            <a href="#">개막관</a>
                        </li>
                        <li>
                            <a href="#">주제관</a>
                        </li>
                        <li>
                            <a href="#">채용정보관</a>
                        </li>
                        <li>
                            <a href="#">참여 프로그램</a>
                        </li>
                        <li>
                            <a href="#">소통ㆍ이벤트</a>
                        </li>
                        <li>
                            <a href="#">인재혁신세미나</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="history_wrap">
                <div class="section history01">
                    <h2 class="red">공직역사가치관</h2>
                    <div class="video_clip large">
                        <div class="video"></div>
                    </div>
                </div>
                <div class="section history02">
                    <h2><span>공직이해</span></h2>
                    <p class="sub">버튼을 클릭해 세부사항을 확인해보세요!</p>
                    <div class="cont_wrap">
                        <div class="button_tab" id="tab_01">
                            <ul>
                                <li>
                                    <button class="selected" data-tab="tab_01_tab1">공무원의 종류</button>
                                </li>
                                <li>
                                    <button data-tab="tab_01_tab2">인사관리</button>
                                </li>
                                <li>
                                    <button data-tab="tab_01_tab5">복무관리</button>
                                </li>
                                <li>
                                    <button data-tab="tab_01_tab3">보수(급여)</button>
                                </li>
                                <li>
                                    <button data-tab="tab_01_tab4">인재개발</button>
                                </li>
                            </ul>
                            <div class="tab_cont">
                                <div>
                                    <div class="open" id="tab_01_tab1">
                                        <img src="/static/img/history/2-cont02.png" alt="" class="pc" />
                                    </div>
                                    <div id="tab_01_tab2">
                                        <img src="/static/img/history/2-cont03.png" alt="" class="pc" />
                                    </div>
                                    <div id="tab_01_tab5">
                                        <img src="/static/img/history/2-cont06.png" alt="" class="pc" />
                                    </div>
                                    <div id="tab_01_tab3">
                                        <img src="/static/img/history/2-cont04.png" alt="" class="pc" />
                                    </div>
                                    <div id="tab_01_tab4">
                                        <img src="/static/img/history/2-cont05.png" alt="" class="pc" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h2 style="margin-top: 160px;">공직 역사</h2>
                    <div class="cont_wrap">
                        <div class="button_tab" id="tab_02">
                            <ul>
                                <li>
                                    <button class="selected" data-tab="tab_02_tab1">인사제도의 변천</button>
                                </li>
                                <li>
                                    <button data-tab="tab_02_tab2">채용시험의 변천</button>
                                </li>
                                <li>
                                    <button data-tab="tab_02_tab3">보수(급여)의 변화</button>
                                </li>
                                <li>
                                    <button data-tab="tab_02_tab4">보수 수준의 변화</button>
                                </li>
                            </ul>
                            <div class="tab_cont">
                                <div>
                                    <div class="open" id="tab_02_tab1">
                                        <img src="/static/img/history/3-cont02.png" alt="" class="pc" />
                                    </div>
                                    <div id="tab_02_tab2">
                                        <img src="/static/img/history/3-cont03.png" alt="" class="pc" />
                                    </div>
                                    <div id="tab_02_tab3">
                                        <img src="/static/img/history/3-cont04.png" alt="" class="pc" />
                                    </div>
                                    <div id="tab_02_tab4">
                                        <img src="/static/img/history/3-cont05.png" alt="" class="pc" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cont_wrap">

                    </div>
                    <h2 style="margin-top: 160px;" class="slide_nav slide1"><button class="prev"><span class="blind">이전</span></button>공직가치<button class="next"><span class="blind">다음</span></button></h2>
                    <div class="cont_wrap">
                        <div class="slide_wrap slide1">
                            <ul class="swiper-wrapper">
                                <li class="swiper-slide">
                                    <img src="/static/img/history/4-cont_plus.png" alt="" class="pc" />
                                </li><!--추가-->
                                <li class="swiper-slide">
                                    <img src="/static/img/history/4-cont04.png" alt="" class="pc" />
                                </li>
                                <li class="swiper-slide">
                                    <img src="/static/img/history/4-cont05.png" alt="" class="pc" />
                                </li>
                                <li class="swiper-slide">
                                    <img src="/static/img/history/4-cont06.png" alt="" class="pc" />
                                </li>
                                <li class="swiper-slide">
                                    <img src="/static/img/history/4-cont07.png" alt="" class="pc" />
                                </li>
                                <li class="swiper-slide">
                                    <img src="/static/img/history/4-cont08.png" alt="" class="pc" />
                                </li>
                                <li class="swiper-slide">
                                    <img src="/static/img/history/4-cont09.png" alt="" class="pc" />
                                </li>
                            </ul>
                            <div class="dots"></div>
                        </div>
                    </div>
                </div>
                <div class="section" style="background-image: url(/static/img/history/5-bg01.png);">
                    <div class="bg_cont" style="background-image: url(/static/img/history/5-bg03.png);"></div>
                    <div class="cont_wrap">
                        <img src="/static/img/history/5-cont01.png" alt="" />
                        <div class="box_slider_wrap">
                            <ul class="swiper-wrapper">
                                <li class="swiper-slide">
                                    <div style="background-image: url(/static/img/history/5-cont05.png);"></div>
                                    <div>
                                        <h3>시권(試券, 퇴계 이황의 답안지, 1527년)</h3>
                                        <p>1527년(중종 22년) 경상도 향시(진사시)에 응시한 이황(1501~1570)의 답안지로, 인재를 어떻게 길러야 하는가를 묻는 첫 번째 문제에 공자의 예를 들며 “천하의 영재를 얻기는 어렵고 학자의 기질은 치우침이 있으니, 각각의 자질에 맞게 변화시켜 인재로 길러야 한다”고 답하였다. 이황은 당시 시험에서 1등으로 합격하였다.</p>
                                    </div>
                                </li>
                                <li class="swiper-slide">
                                    <div style="background-image: url(/static/img/history/5-cont06.png);"></div>
                                    <div>
                                        <h3>갑자사마방목(甲子司馬榜目, 합격명부, 1564년)</h3>
                                        <p>갑자년인1564년(명종 19년)에 실시된 사마시의 합격자 명부이다. 문과 예비 시험이라 할 수 있는 사마시는 식년시와 증광시로 구별되는데, 식년시는 3년마다 치러지는 정기 시험이고, 증광시는 국가에 경사가 있을 때 이를 기념하고자 치러지는 비정기 시험이다. 여기에는 이이(1536~1584)와 유성룡(1542~1607)이 급제자로 이름이 올라 있다. 이이는 “구도장원공(九度壯元公)”이라는 별명을 지녔는데, 이는 과거시험에서 아홉 번 장원 급제했다는 의미다. 한 번 급제하기도 쉽지 않은 과거시험에서 아홉 번이나 장원 급제했으니 조선 역사상 최고의 천재라 할 것이다.</p>
                                    </div>
                                </li>
                                <li class="swiper-slide">
                                    <div style="background-image: url(/static/img/history/5-cont07.png);"></div>
                                    <div>
                                        <h3>홍패(紅牌, 이순신의 무과합격증, 1576년)</h3>
                                        <p>1576년(선조 9년)에실시된 무과시험에서 이순신(1545~1598)이 병과 제4인으로 합격하여 받은 급제 증서로, 무과의 경우, 성적순대로 갑과 3명, 을과 5명, 병과 20명이었으니, 이순신은 전체로는 12등이었다.</p>
                                    </div>
                                </li>
                                <li class="swiper-slide">
                                    <div style="background-image: url(/static/img/history/5-cont08.png);"></div>
                                    <div>
                                        <h3>국조방목(國朝榜目, 조선 문과 급제자 명단, 1889년)</h3>
                                        <p>1392년(태조 1년)부터 1889년(고종 26년)까지의 문과 급제자를 수록한 명부로 '국조문과방목', 또는 '등과록'이라고도 불린다.</p>
                                    </div>
                                </li>
                                <li class="swiper-slide">
                                    <div style="background-image: url(/static/img/history/5-cont09.png);"></div>
                                    <div>
                                        <h3>동책정수(東策精粹, 과거시험의 모범답안 모음, 17세기 초)</h3>
                                        <p>조선 중종에서 명종 때 연간 과거에 급제한 사람들의 대책문 가운데 모범답안으로 평가된 것들을 수록한 책으로 상, 하 2권으로 구성되어 있다. 음주의 문제점, 스승의 참된 도리, 부유함과 교육, 출세와 은거 등의 주제에 대한 답안이 수록되어 있다.</p>
                                    </div>
                                </li>
                                <li class="swiper-slide">
                                    <div style="background-image: url(/static/img/history/5-cont10.png);"></div>
                                    <div>
                                        <h3>삼일유가(三日遊街, 작자미상)</h3>
                                        <p>과거에서 장원 급제한 주인공이 임금이 하사한 어사화(御賜花)를 머리에 꽂고 사흘 동안 시험관 및 선배 급제자, 친척들을 찾아다니며 축제 행사를 갖는 모습을 담은 풍속화다. 이 그림은 높은 관직을 지내고 오복을 누린 조선 시대 사대부의 일생을 그린 10폭 병풍으로 구성된 평생도 가운데 한 장면이다.</p>
                                    </div>
                                </li>
                            </ul>
                            <div class="nav">
                                <button class="prev"><span class="blind">이전</span></button>
                                <button class="next"><span class="blind">다음</span></button>
                            </div>
                            <div class="dots"></div>
                        </div>
                    </div>
                </div>
                <div class="section" style="background-image: url(/static/img/history/1-bg01.png);">
                    <div class="cont_wrap">
                        <div class="horz_wrap">
                            <img src="/static/img/history/6-cont01.png" alt="" />
                            <img src="/static/img/history/6-cont02.png" alt="" />
                        </div>
                        <div class="quiz red">
                            <a href="https://docs.google.com/forms/d/e/1FAIpQLSeuB-vJP2P4Kkf2N6Zj_J_wG-FTwKIt_DS4GjxntqWSusokxg/viewform" target="_blank" class="btn_quiz">퀴즈 풀기</a>
                        </div>
                        <img src="/static/img/history/6-cont03.png" alt="" class="pc" />
                    </div>
                </div>
            </div>
        </div>
        <div id="footer">
            <div class="inner">
                <div class="top">
                    <a href="#" class="footer_logo"><span class="blind">인사혁신처</span></a>
                    <ul>
                        <li class="fb">
                            <a href="#"><span class="blind">facebook</span></a>
                        </li>
                        <li class="tw">
                            <a href="#"><span class="blind">twitter</span></a>
                        </li>
                        <li class="ig">
                            <a href="#"><span class="blind">instagram</span></a>
                        </li>
                        <li class="yt">
                            <a href="#"><span class="blind">youtube</span></a>
                        </li>
                    </ul>
                </div>
                <div class="bottom">
                    <ul>
                        <li><a href="#">개인정보 처리방침</a></li>
                        <li><a href="#">영상정보처리기 운영ㆍ관리방침</a></li>
                        <li><a href="#">저작권보호 및 이용정책</a></li>
                    </ul>
                    <p>30102 세종특별자치시 한누리대로 499(어진동) 세종포스트빌딩 6층~12층</p>
                    <p>copyright© Ministry of Personnel Management. All Rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
    <script src="/static/js/common.js"></script>
    <script>
        var slider1 = new Swiper('.slide1', {
            loop: false,
            slideActiveClass: 'active',
            navigation: {
                nextEl: '.slide1 .next',
                prevEl: '.slide1 .prev',
            },
            autoHeight:true,
            spaceBetween:30,
            pagination: {
                el: '.slide1 .dots',
                type: 'bullets',
            },
        })

        var slider2 = new Swiper('.box_slider_wrap', {
            loop: false,
            slideActiveClass: 'active',
            navigation: {
                nextEl: '.box_slider_wrap .next',
                prevEl: '.box_slider_wrap .prev',
            },
            autoHeight:true,
            pagination: {
                el: '.box_slider_wrap .dots',
                type: 'bullets',
            },
        })


        $('.button_tab ul li button').click(function () {
            const sibling = $(this).parent().siblings()
            const target = $(this).attr('data-tab')
            sibling.each(function () {
                $(this).children().removeClass('selected')
            })
            $(this).addClass('selected')
            const wrapper = $(this).closest('div').children('.tab_cont').children().children()
            wrapper.each(function () {
                $(this).removeClass('open')
            })
            $('#' + target).addClass('open')
        })


        document.addEventListener('DOMContentLoaded', function(){

            var play_video = '<iframe width="1280" height="720" src="https://www.youtube.com/embed/iGzows62ZhQ?rel=0&amp; autoplay=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                $($('.video')[0]).html(play_video)



        });


    </script>
</body>

</html>