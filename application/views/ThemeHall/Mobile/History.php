<!DOCTYPE html>
<html lang="ko">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>공직역사가치관</title>
    <meta name="description" content="공직역사가치관" />
    <meta name="keywords" content="공직역사가치관" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="" />
    <meta property="og:title" content="공직역사가치관" />
    <meta property="og:description" content="공직역사가치관" />
    <link rel="stylesheet" href="/static/mobile/css/style.css">
    <!--<link rel="stylesheet" href="/static/mobile/css/mobile.css">-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <script type="text/javascript" src="https://unpkg.com/swiper@5.4.5/js/swiper.min.js"></script>
</head>

<body>
    <div class="container">
        <!-- 헤더 생략 -->
        <div class="content">
            <div class="section top bp12" style="background-image: url(/static/mobile/img/history/history_m_bg_01.png);">
                <div class="title">
                    <img src="/static/mobile/img/history/history_tit_btypehistory.png" alt="">
                </div>
                <div class="video_clip">
                    <div class="video">
                        <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                        <!-- ws. 201122 공직역사가치관 메인 -->
                        <iframe
                            src="https://www.youtube.com/embed/iGzows62ZhQ?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0"
                            frameborder="0"
                            allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                    </div>
                </div>
            </div>
            <div class="section bp12" style="background-image: url(/static/mobile/img/history/history_m_bg_02.png);">
                <div class="title">
                    <img src="/static/mobile/img/history/history_con1_tt_btype.png" alt="">
                </div>
                <div class="cont_wrap">
                    <div class="frame_cont">
                        <div class="block_cont">
                            <h3 class="red">공무원의 종류</h3>
                            <div class="block">
                                <img src="/static/mobile/img/history/history_con01_01.png" alt="">
                            </div>
                        </div>
                        <div class="block_cont">
                            <h3 class="red">인사관리</h3>
                            <div class="block">
                                <img src="/static/mobile/img/history/history_con01_02.png" alt="">
                            </div>
                        </div>
                        <div class="block_cont">
                            <h3 class="red">복무관리</h3>
                            <div class="block">
                                <img src="/static/mobile/img/history/history_con01_05.png" alt="">
                            </div>
                        </div>
                        <div class="block_cont">
                            <h3 class="red">보수(급여)</h3>
                            <div class="block">
                                <img src="/static/mobile/img/history/history_con01_03.png" alt="">
                            </div>
                        </div>
                        <div class="block_cont">
                            <h3 class="red">인재개발</h3>
                            <div class="block">
                                <img src="/static/mobile/img/history/history_con01_04.png" alt="">
                            </div>
                        </div>
                        <div class="block_cont">
                            <h3 class="red">인사제도의 변천</h3>
                            <div class="block">
                                <img src="/static/mobile/img/history/history_con02_01.png" alt="">
                                <img src="/static/mobile/img/history/history_con02_02.png" alt="">
                            </div>
                        </div>
                        <div class="block_cont">
                            <h3 class="red">채용시험의 변천</h3>
                            <div class="block">
                                <img src="/static/mobile/img/history/history_con03.png" alt="">
                            </div>
                        </div>
                        <div class="block_cont">
                            <h3 class="red">보수(급여)의 변화</h3>
                            <div class="block">
                                <img src="/static/mobile/img/history/history_con04_01.png" alt="">
                            </div>
                        </div>
                        <div class="block_cont">
                            <h3 class="red">보수 수준의 변화</h3>
                            <div class="block">
                                <img src="/static/mobile/img/history/history_con05_02.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="title">
                        <img src="/static/mobile/img/history/history_con06_tt.png" alt="">
                    </div>
                    <div class="frame_slider">
                        <div class="slide01">
                            <ul class="swiper-wrapper">
                                <li class="swiper-slide">
                                    <img src="/static/mobile/img/history/history_con06_01.png" alt="">
                                </li>
                                <li class="swiper-slide">
                                    <img src="/static/mobile/img/history/history_con06_02.png" alt="">
                                </li>
                                <li class="swiper-slide">
                                    <img src="/static/mobile/img/history/history_con06_03.png" alt="">
                                </li>
                                <li class="swiper-slide">
                                    <img src="/static/mobile/img/history/history_con06_04.png" alt="">
                                </li>
                                <li class="swiper-slide">
                                    <img src="/static/mobile/img/history/history_con06_05.png" alt="">
                                </li>
                                <li class="swiper-slide">
                                    <img src="/static/mobile/img/history/history_con06_06.png" alt="">
                                </li>
                            </ul>
                            <div class="dots"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section bp30" style="background-image: url(/static/mobile/img/history/history_m_bg_03.png);">
                <div class="title">
                    <img src="/static/mobile/img/history/history_con07_tt.png" alt="">
                </div>
                <div class="cont_wrap">
                    <div class="frame_slider history">
                        <div class="slide02">
                            <ul class="swiper-wrapper">
                                <li class="swiper-slide">
                                    <img src="/static/mobile/img/history/history_con07_01.png" alt="">
                                </li>
                                <li class="swiper-slide">
                                    <img src="/static/mobile/img/history/history_con07_02.png" alt="">
                                </li>
                                <li class="swiper-slide">
                                    <img src="/static/mobile/img/history/history_con07_03.png" alt="">
                                </li>
                                <li class="swiper-slide">
                                    <img src="/static/mobile/img/history/history_con07_04.png" alt="">
                                </li>
                                <li class="swiper-slide">
                                    <img src="/static/mobile/img/history/history_con07_05.png" alt="">
                                </li>
                            </ul>
                            <div class="dots"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section quiz_wrap" style="background-image: url(/static/mobile/img/history/history_m_bg_04.png);">
                <div class="quiz white">
                    <img src="/static/mobile/img/quiz/6-1.png" alt="">
                    <img src="/static/mobile/img/quiz//6-2.png" alt="">
                </div>
                <div class="quiz_btn red">
                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSeuB-vJP2P4Kkf2N6Zj_J_wG-FTwKIt_DS4GjxntqWSusokxg/viewform" target="_blank" class="btn_quiz">퀴즈 풀기</a>
                </div>
                <div class="cont_wrap">
                    <img src="/static/mobile/img/history/history_con08_03.png" alt="">
                </div>
            </div>
        </div>
        <div class="modal_wrap">
            <button class="close_btn">X</button>
            <div class="img_cont"></div>
        </div>
        <!-- 푸터생략 -->
    </div>
    <script src="/static/mobile/js/common.js"></script>
    <script>
        var slider1 = new Swiper('.slide01', {
            loop: false,
            slideActiveClass: 'active',
            // autoHeight: true,
            spaceBetween: 30,
            pagination: {
                el: '.slide01 .dots',
                type: 'bullets',
            },
        })

        var slider2 = new Swiper('.slide02', {
            loop: false,
            slideActiveClass: 'active',
            spaceBetween: 30,
            // autoHeight: true,
            pagination: {
                el: '.slide02 .dots',
                type: 'bullets',
            },
        })
    </script>
</body>

</html>