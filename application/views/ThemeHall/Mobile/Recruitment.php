<!DOCTYPE html>
<html lang="ko">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>공개경력채용관</title>
    <meta name="description" content="공개경력채용관" />
    <meta name="keywords" content="공개경력채용관" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="" />
    <meta property="og:title" content="공개경력채용관" />
    <meta property="og:description" content="공개경력채용관" />
    <link rel="stylesheet" href="/static/mobile/css/style.css">
    <!--<link rel="stylesheet" href="/static/mobile/css/mobile.css">-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <script type="text/javascript" src="https://unpkg.com/swiper@5.4.5/js/swiper.min.js"></script>
</head>

<body>
    <div class="container">
        <!-- 헤더 생략 -->
        <div class="content">
            <div class="section top bp12" style="background-image: url(/static/mobile/img/recruitment/1-bg01.png);">
                <div class="title">
                    <img src="/static/mobile/img/recruitment/1-cont01.png" alt="">
                </div>
                <div class="cont_wrap">
                    <img src="/static/mobile/img/recruitment/1-cont02.png" alt="">
                </div>
            </div>
            <div class="section bp12">
                <div class="cont_wrap">
                    <img src="/static/mobile/img/recruitment/2-cont01.png" alt="">
                </div>
            </div>
            <div class="section bp12" style="background-image: url(/static/mobile/img/recruitment/3-bg01.png); background-color: #5DB47A;">
                <div class="title">
                    <img src="/static/mobile/img/recruitment/3-cont01.png" alt="">
                </div>
                <div class="video_clip">
                    <div class="video">
                        <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                        <!-- ws. 201122 공개경력채용관 메인 -->
                        <iframe
                            src="https://www.youtube.com/embed/Kaxb3ta30L4?autopla4y=0&amp;mute=1&amp;loop=1&amp;controls=0"
                            frameborder="0"
                            allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                    </div>
                </div>
                <div class="cont_wrap mt10">
                    <div class="frame_cont">
                        <div class="block_cont">
                            <h3 class="yellow">국가공무원 5급<br>공개경쟁채용시험</h3>
                            <div class="block">
                                <img src="/static/mobile/img/recruitment/4-cont01.png" alt="">
                            </div>
                        </div>
                        <div class="block_cont">
                            <h3 class="yellow">국가공무원 7·9급<br>공개경쟁채용시험</h3>
                            <div class="block">
                                <img src="/static/mobile/img/recruitment/4-cont02.png" alt="">
                            </div>
                        </div>
                    </div>
                    <img src="/static/mobile/img/recruitment/5-cont01.png" alt="">
                </div>
            </div>
            <div class="section bp12" style="background-color: #FFE382;">
                <div class="title">
                    <img src="/static/mobile/img/recruitment/6-cont01.png" alt="">
                </div>
                <div class="video_clip">
                    <div class="video">
                        <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                        <!-- ws. 201122 공개경력채용관 메인 -->
                        <iframe
                            src="https://www.youtube.com/embed/G_gS4XcqZMk?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0"
                            frameborder="0"
                            allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                    </div>
                </div>
                <div class="title">
                    <img src="/static/mobile/img/recruitment/7-cont01.png" alt="">
                </div>
                <div class="cont_wrap">
                    <div class="content_slider">
                        <div class="slider01">
                            <ul class="swiper-wrapper">
                                <li class="swiper-slide">
                                    <em>민경채 도입 취지 및 소개</em>
                                    <img src="/static/mobile/img/recruitment/tab_cont04.png" alt="">
                                </li>
                                <li class="swiper-slide">
                                    <em>경채와 공채의 다른 점</em>
                                    <img src="/static/mobile/img/recruitment/tab_cont03.png" alt="">
                                </li>
                                <li class="swiper-slide">
                                    <em>합격자 통계</em>
                                    <img src="/static/mobile/img/recruitment/tab_cont01.png" alt="">
                                </li>
                                <li class="swiper-slide">
                                    <em>합격자 사례 소개</em>
                                    <img src="/static/mobile/img/recruitment/tab_cont02.png" alt="">
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section bp12" style="background-color: #54B27F;">
                <div class="title">
                    <img src="/static/mobile/img/recruitment/8-cont01.png" alt="">
                </div>
                <div class="video_clip mt10">
                    <h3 class="yellow">나는 네가 공고문을<br/>확인하지 않은 것을 알고 있다</h3>
                    <div class="video">
                        <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                        <!-- ws. 201122 공개경력채용관 탭 01 -->
                        <iframe src="https://www.youtube.com/embed/Hl_Wj_213Zc?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0" frameborder="0" allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                    </div>
                </div>
                <div class="video_clip mt10">
                    <h3 class="yellow">수험생 고민상담소<br/>파란라이트를 켜줘</h3>
                    <div class="video">
                        <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                        <!-- ws. 201122 공개경력채용관 탭 02 -->
                        <iframe src="https://www.youtube.com/embed/Y8N2XmQUh7Q?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0" frameborder="0" allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                    </div>
                </div>
                <div class="video_clip mt10">
                    <h3 class="yellow">부정행위,<br/>악몽의 시나리오</h3>
                    <div class="video">
                        <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                        <!-- ws. 201122 공개경력채용관 탭 03 -->
                        <iframe src="https://www.youtube.com/embed/tY1U3BKgpT0?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0" frameborder="0" allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                    </div>
                </div>
                <div class="cont_wrap mt5">
                    <img src="/static/mobile/img/recruitment/9-cont01.png" alt="">
                </div>
            </div>
            <div class="section quiz_wrap" style="background-image: url(/static/mobile/img/recruitment/10-bg01.png);">
                <div class="quiz green">
                    <img src="/static/mobile/img/quiz/4-1.png" alt="">
                    <img src="/static/mobile/img/quiz//4-2.png" alt="">
                </div>
                <div class="quiz_btn green">
                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSeuB-vJP2P4Kkf2N6Zj_J_wG-FTwKIt_DS4GjxntqWSusokxg/viewform" target="_blank" class="btn_quiz">퀴즈 풀기</a>
                </div>
                <div class="cont_wrap">
                    <img src="/static/mobile/img/recruitment/10-cont02-1.png" alt="">
                </div>
            </div>
        </div>
        <!-- 푸터 생략 -->
    </div>
    <script src="/static/mobile/js/common.js"></script>
    <script>
        var contentSlider01 = new Swiper('.slider01', {
            loop: false,
            slideActiveClass: 'active'
        })

        $('.box_tab ul li button').click(function () {
            const sibling = $(this).parent().siblings()
            const target = $(this).attr('data-tab')
            sibling.each(function () {
                $(this).children().removeClass('selected')
            })
            $(this).addClass('selected')
            const wrapper = $(this).closest('div').children('.tab_cont').children().children()
            wrapper.each(function () {
                $(this).removeClass('open')
            })
            $('#' + target).addClass('open')
        })
    </script>
</body>

</html>