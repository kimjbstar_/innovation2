<!DOCTYPE html>
<html lang="ko">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>K-시험방역관</title>
    <meta name="description" content="K-시험방역관" />
    <meta name="keywords" content="K-시험방역관" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="" />
    <meta property="og:title" content="K-시험방역관" />
    <meta property="og:description" content="K-시험방역관" />
    <link rel="stylesheet" href="/static/mobile/css/style.css">
    <!--<link rel="stylesheet" href="/static/mobile/css/mobile.css">-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <script type="text/javascript" src="https://unpkg.com/swiper@5.4.5/js/swiper.min.js"></script>
</head>

<body>
    <div class="container">
        <!-- 헤더 생략 -->
        <div class="content">
            <div class="section top bp12" style="background-image: url(/static/mobile/img/prevent/k-2-cont-tt.png);">
                <div class="title">
                    <img src="/static/mobile/img/prevent/k-1-cont-tt.png" alt="">
                </div>
                <div class="video_clip">
                    <div class="video">
                        <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                        <!-- ws. 201122 K-시험방역관 메인  -->
                        <iframe
                            src="https://www.youtube.com/embed/uxcLxzFIdd4?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0"
                            frameborder="0"
                            allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                    </div>
                </div>
            </div>
            <div class="section">
                <div class="cont_wrap" style="background-color: #008BA6;">
                    <img src="/static/mobile/img/prevent/Group 768.png" alt="">
                </div>
                <div class="video_clip mt10">
                    <h3 class="green">안전한 채용을 위한 인사혁신처의 노력</h3>
                    <div class="video">
                        <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                        <!-- ws. 201122 K-시험방역관 하단메인 01 방역 -->
                        <iframe
                            src="https://www.youtube.com/embed/FiCieG0nr5k?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0"
                            frameborder="0"
                            allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                    </div>
                </div>
                <div class="video_clip mt10">
                    <h3 class="green">공시생 3년차의 D-day</h3>
                    <div class="video">
                        <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                        <!-- ws. 201122 K-시험방역관 하단메인 01 방역 -->
                        <iframe
                            src="https://www.youtube.com/embed/Sg5AM3TAjEA?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0"
                            frameborder="0"
                            allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                    </div>
                </div>
                <div class="cont_wrap mt20">
                    <img src=".//img/prevent/k-2-cont01.png" alt="">
                    <img class="mt20" src="/static/mobile/img/prevent/k-2-cont02.png" alt="">
                    <img class="mt20" src="/static/mobile/img/prevent/k-2-cont03.png" alt="">
                    <div class="fake_btn">
                        <button class="popup_trigger" data-popup="/static/mobile/img/prevent/k-2-cont04-popup-01.png"
                            style="width: 96%; padding-top: 26%; bottom: 13.8%; left: 2%;"></button>
                            <button class="popup_trigger" data-popup="/static/mobile/img/prevent/k-2-cont04-popup-02.png"
                            style="width: 96%; padding-top: 26%; bottom: 10.2%; left: 2%;"></button>
                            <button class="popup_trigger" data-popup="/static/mobile/img/prevent/k-2-cont04-popup-03.png"
                            style="width: 96%; padding-top: 26%; bottom: 6.6%; left: 2%;"></button>
                            <button class="popup_trigger" data-popup="/static/mobile/img/prevent/k-2-cont04-popup-04.png"
                            style="width: 96%; padding-top: 26%; bottom: 2.4%; left: 2%;"></button>
                    </div>
                </div>
            </div>
            <div class="section quiz_wrap" style="background-color: #008BA6;">
                <div class="quiz white">
                    <img src="/static/mobile/img/quiz/7-1.png" alt="">
                    <img src="/static/mobile/img/quiz//7-2.png" alt="">
                </div>
                <div class="quiz_btn">
                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSeuB-vJP2P4Kkf2N6Zj_J_wG-FTwKIt_DS4GjxntqWSusokxg/viewform" target="_blank" class="btn_quiz">퀴즈 풀기</a>
                </div>
                <div class="cont_wrap">
                    <img src="/static/mobile/img/prevent/Group 770.png" alt="">
                </div>
            </div>
        </div>
        <div class="modal_wrap">
            <button class="close_btn">X</button>
            <div class="img_cont"></div>
        </div>
        <!-- 푸터 생략 -->
    </div>
    <script src="/static/mobile/js/common.js"></script>
</body>

</html>