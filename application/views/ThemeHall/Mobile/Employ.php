<!DOCTYPE html>
<html lang="ko">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>공정채용관</title>
    <meta name="description" content="공정채용관" />
    <meta name="keywords" content="공정채용관" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="" />
    <meta property="og:title" content="공정채용관" />
    <meta property="og:description" content="공정채용관" />
    <link rel="stylesheet" href="/static/mobile/css/style.css">
    <!--<link rel="stylesheet" href="/static/mobile/css/mobile.css">-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <script type="text/javascript" src="https://unpkg.com/swiper@5.4.5/js/swiper.min.js"></script>
</head>

<body>
    <div class="container">
        <!-- 헤더 생략 -->
        <div class="content">
            <div class="section top bp12" style="background-image: url(/static/mobile/img/employ/1-bg01.png);">
                <div class="title">
                    <img src="/static/mobile/img/employ/1-cont01.png" alt="">
                </div>
                <div class="video_clip">
                    <div class="video">
                        <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                        <!-- ws. 201122 공정채용관 메인 영상 -->
                        <iframe
                            src="https://www.youtube.com/embed/TPbsMZpoPeA?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0"
                            frameborder="0"
                            allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                    </div>
                </div>
            </div>
            <div class="section">
                <div class="cont_wrap">
                    <div class="fake_btn">
                        <button class="popup_trigger" data-popup="/static/mobile/img/employ/popup_1.png"
                            style="width: 80%; padding-top: 24%; top: 24%; left: 10%;"></button>
                        <button class="popup_trigger" data-popup="/static/mobile/img/employ/popup_2.png"
                            style="width: 80%; padding-top: 24%; top: 38%; left: 10%;"></button>
                        <button class="popup_trigger" data-popup="/static/mobile/img/employ/popup_3.png"
                            style="width: 80%; padding-top: 24%; top: 52%; left: 10%;"></button>
                        <button class="popup_trigger" data-popup="/static/mobile/img/employ/popup_4.png"
                            style="width: 80%; padding-top: 24%; top: 66%; left: 10%;"></button>
                        <button class="popup_trigger" data-popup="/static/mobile/img/employ/popup_5.png"
                            style="width: 80%; padding-top: 24%; top: 80%; left: 10%;"></button>
                    </div>
                    <img src="/static/mobile/img/employ/2-cont01.png" alt="">
                </div>
            </div>
            <div class="section bp12" style="background-image: url(/static/mobile/img/employ/3-bg01.png); background-color: #3866C9;">
                <div class="cont_wrap">
                    <img src="/static/mobile/img/employ/3-cont01.png" alt="">
                    <ul class="button_tab type1 col2">
                        <li><button class="popup_trigger" data-popup="/static/mobile/img/employ/fr-3-cont-popup-01.png"><img
                                    src="/static/mobile/img/employ/3-cont02.png" alt=""></button></li>
                        <li><button class="popup_trigger" data-popup="/static/mobile/img/employ/fr-3-cont-popup-04.png"><img
                                    src="/static/mobile/img/employ/3-cont03.png" alt=""></button></li>
                        <li><button class="popup_trigger" data-popup="/static/mobile/img/employ/fr-3-cont-popup-02.png"><img
                                    src="/static/mobile/img/employ/3-cont04.png" alt=""></button></li>
                        <li><button class="popup_trigger" data-popup="/static/mobile/img/employ/fr-3-cont-popup-05.png"><img
                                    src="/static/mobile/img/employ/3-cont05.png" alt=""></button></li>
                        <li><button class="popup_trigger" data-popup="/static/mobile/img/employ/fr-3-cont-popup-0.png"><img
                                    src="/static/mobile/img/employ/3-cont06.png" alt=""></button></li>
                        <li><button class="popup_trigger" data-popup="/static/mobile/img/employ/fr-3-cont-popup-05-1.png"><img
                                    src="/static/mobile/img/employ/3-cont07.png" alt=""></button></li>
                        <li><button class="popup_trigger" data-popup="/static/mobile/img/employ/fr-3-cont-popup-03.png"><img
                                    src="/static/mobile/img/employ/3-cont08.png" alt=""></button></li>
                    </ul>
                    <img src="/static/mobile/img/employ/4-cont01.png" alt="">
                    <div class="video_clip mt5">
                        <div class="video">
                            <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                            <!-- ws. 201122 공정채용의 수호자들 -->
                            <iframe
                                src="https://www.youtube.com/embed/81P5NkM6U6s?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0"
                                frameborder="0"
                                allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                        </div>
                    </div>
                    <img class="mt10" src="/static/mobile/img/employ/4-cont02.png" alt="">
                    <div class="qa_slider mt5">
                        <div class="slide01">
                            <ul class="swiper-wrapper">
                                <li class="swiper-slide">
                                    <em>시험문제 출제</em>
                                    <div>
                                        <p>Q. 시험문제 출제위원은 공정하게 선정되는 건가요?</p>
                                        <div>
                                            <span>최근 3년 간 학원강의 또는 학교 고시반을 담당하거나 수험서 발간경력이 있는 자는 시험위원에서 배제하고, 위촉된 시험위원은
                                                교육 및 보안서약서를 징구하는 등 위촉된 사실에 대해 반드시 비밀을 유지하도록 하고 있습니다.</span>
                                        </div>
                                    </div>
                                </li>
                                <li class="swiper-slide">
                                    <em>시험문제 선정</em>
                                    <div>
                                        <p>Q. 특정위원이 출제한 문제가 시험문제로 많이 선정되는 것은 아닌가요?</p>
                                        <div>
                                            <span>문제은행에 문제를 입고하는 ‘출제위원’과 최종적으로 문제를 선정하는 ‘선정위원’을 구분하여 특정위원이 출제한 문제가 최종
                                                시험문제에 다수 포함되는 상황을 사전에 방지하고 있습니다. 또한 선정위원 본인이 문제은행에 입고한 문제는 당해 시험에 실제 사용할
                                                문제로 선정할 수 없으며, 같은 과목 내에서 동일대학 출제위원의 문제가 일정 수를 넘어 선정되지 않도록 제한하고
                                                있습니다.</span>
                                        </div>
                                    </div>
                                </li>
                                <li class="swiper-slide">
                                    <em>시험문제 유출방지</em>
                                    <div>
                                        <p>Q. 출제 과정에서 시험문제가 외부인에 의해 유출될 가능성은 없나요?</p>
                                        <div>
                                            <span>문제선정 및 검토 등 모든 출제과정은 외부와의 접촉이 철저히 차단된 보안시설에서 이루어지고 있습니다. 해당 시설은 경비인력에
                                                의한 외부인 출입통제, CCTV 설치 등을 통해 엄격히 보안이 관리되고 있으며, 출제위원 및 관련 직원들은 통신장비를 보안시설
                                                입소시에 제출하고 시험 전까지 시설 내에서 합숙을 하고 있습니다. 보안시설 내에서도 시험관련 자료는 심사실 이외의 장소(숙소
                                                등)로의 무단 반출을 철저히 금지하고, 출제 기간 동안에는 어떠한 물건도 출제 보안시설 안에서 밖으로 나갈 수 없도록 철저하게
                                                관리하고 있습니다.</span>
                                        </div>
                                    </div>
                                </li>
                                <li class="swiper-slide">
                                    <em>시험문제 유출방지</em>
                                    <div>
                                        <p>Q. 시험문제 인쇄 과정에서의 시험문제 유출 가능성은 없나요?</p>
                                        <div>
                                            <span>시험문제 인쇄시 외부와의 접촉이 철저히 차단된 보안시설이 갖추어진 인쇄업체를 선정하고, 내외부 CCTV 점검 및 출입문·창문
                                                봉인, 인쇄 종사자 통신장비 수거 등 보안관련 사항을 철저히 점검하고 있습니다.</span>
                                        </div>
                                    </div>
                                </li>
                                <li class="swiper-slide">
                                    <em>시험문제 유출방지</em>
                                    <div>
                                        <p>Q. 인쇄된 문제는 시험장소(학교 등)로 안전하게 전달되는 건가요?</p>
                                        <div>
                                            <span>문제지는 인쇄 후부터 해당 시험실로 들어가기 직전까지 봉인되어 관리되며, 인사혁신처 담당 공무원 등의 감독 하에 보안경비업체의
                                                전문 경비인력이 보안차량을 이용하여 안전하게 시험장소로 문제지를 전달하고 있습니다.</span>
                                        </div>
                                    </div>
                                </li>
                                <li class="swiper-slide">
                                    <em>필기 시험 실시</em>
                                    <div>
                                        <p>Q. 특정 수험생에게 유리하게 시험장이 배정될 가능성은 없나요?</p>
                                        <div>
                                            <span>응시번호를 무작위로 부여하여 응시번호를 통해 나이·성별 등을 식별할 수 없도록 하고, 응시번호에 따라 시험장(학교 등) 및
                                                시험실을 배정하여 부정행위를 방지하고 있습니다.</span>
                                        </div>
                                    </div>
                                </li>
                                <li class="swiper-slide">
                                    <em>필기 시험 실시</em>
                                    <div>
                                        <p>Q. 시험장에서 시험시간 이전에 수험생에게 시험문제가 유출될 가능성은 없나요?</p>
                                        <div>
                                            <span>시험문제는 감독관에 의해 시험시작 약 5분전에 각 시험실로 전달되며, 시험문제가 시험실로 들어가게 된 이후에는 수험생의 출입을
                                                통제하여 시험문제가 사전에 유출될 가능성을 차단하고 있습니다. 또한 시험시작 전에는 문제지를 열람하는 행위를 금지하여 모든
                                                수험생이 동일한 시간 동안 문제를 풀 수 있도록 관리하고 있습니다.</span>
                                        </div>
                                    </div>
                                </li>
                                <li class="swiper-slide">
                                    <em>답안지 채점</em>
                                    <div>
                                        <p>Q. 제출한 답안지는 안전하게 채점하는 곳으로 전달되는 건가요?</p>
                                        <div>
                                            <span>답안지가 분실, 누락되지 않도록 각 시험장별로 8회 이상에 걸쳐 매수를 확인하고 있습니다. 또한 시험문제가 시험장소에 전달될
                                                때와 같이, 보안경비업체의 전문 경비인력, 인사혁신처 담당 직원 등이 보안차량을 이용하여 답안지를 안전하게 보안시설로 전달하고
                                                있습니다.</span>
                                        </div>
                                    </div>
                                </li>
                                <li class="swiper-slide">
                                    <em>답안지 채점</em>
                                    <div>
                                        <p>Q. 채점 과정에서 외부인 침입에 의하여 답안지가 변경·훼손될 우려는 없는 건가요?</p>
                                        <div>
                                            <span>경비인력에 의한 외부인 출입통제, CCTV 설치 등 외부인이 침입할 수 없도록 안전하게 관리되는 보안시설에서 답안지가 보관되며
                                                채점 작업이 이루어집니다.</span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <div class="dots"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section">
                <div class="title fixed">
                    <img src="/static/mobile/img/employ/5-cont03.png" alt="">
                </div>
                <div class="fixed_bg" style="background-image: url(/static/mobile/img/employ/5-bg01.png);">
                    <div class="video_slider">
                        <div class="slide01">
                            <ul class="swiper-wrapper">
                                <li class="swiper-slide">
                                    <div class="video_clip">
                                        <div class="video">
                                            <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                                            <!-- ws. 201122 차시별 탭 01 -->
                                            <iframe
                                                src="https://www.youtube.com/embed/0l8zMuOyOgQ?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0"
                                                frameborder="0"
                                                allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                                        </div>
                                    </div>
                                </li>
                                <li class="swiper-slide">
                                    <div class="video_clip">
                                        <div class="video">
                                            <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                                            <!-- ws. 201122 차시별 탭 02 -->
                                            <iframe
                                                src="https://www.youtube.com/embed/sALX1QlluHU?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0"
                                                frameborder="0"
                                                allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                                        </div>
                                    </div>
                                </li>
                                <li class="swiper-slide">
                                    <div class="video_clip">
                                        <div class="video">
                                            <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                                            <!-- ws. 201122 차시별 탭 03 -->
                                            <iframe
                                                src="https://www.youtube.com/embed/W5BqWPtVsN4?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0"
                                                frameborder="0"
                                                allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <div class="dots"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section quiz_wrap" style="background-image: url(/static/mobile/img/employ/6-bg01.png);">
                <div class="quiz white">
                    <img src="/static/mobile/img/employ/5-cont01.png" alt="">
                    <img src="/static/mobile/img/employ/5-cont02.png" alt="">
                </div>
                <div class="quiz_btn blue">
                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSeuB-vJP2P4Kkf2N6Zj_J_wG-FTwKIt_DS4GjxntqWSusokxg/viewform" target="_blank" class="btn_quiz">퀴즈 풀기</a>
                </div>
                <div class="cont_wrap">
                    <img src="/static/mobile/img/employ/6-cont03.png" alt="">
                </div>
            </div>
        </div>
        <div class="modal_wrap">
            <button class="close_btn">X</button>
            <div class="img_cont"></div>
        </div>
        <!-- 푸터 생략 -->
    </div>
    <script src="/static/mobile/js/common.js"></script>
    <script>
        var wideSlider01 = new Swiper('.slide01', {
            loop: false,
            slideActiveClass: 'active',
            pagination: {
                el: '.slide01 .dots',
                type: 'bullets',
            },
            spaceBetween: 20,
            // autoHeight: true
        })

        var videoSlider = new Swiper('.slide02', {
            loop: false,
            slideActiveClass: 'active',
            observer: true,
            observeParents: true,
            pagination: {
                el: '.slide02 .dots',
                type: 'bullets',
            },
            spaceBetween: 20,
            // autoHeight: true
        })
    </script>
</body>

</html>