<!DOCTYPE html>
<html lang="ko">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>미래인사혁신관</title>
    <meta name="description" content="미래인사혁신관" />
    <meta name="keywords" content="미래인사혁신관" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="" />
    <meta property="og:title" content="미래인사혁신관" />
    <meta property="og:description" content="미래인사혁신관" />
    <link rel="stylesheet" href="/static/mobile/css/style.css">
    <!--<link rel="stylesheet" href="/static/mobile/css/mobile.css">-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <script type="text/javascript" src="https://unpkg.com/swiper@5.4.5/js/swiper.min.js"></script>
</head>

<body>
    <div class="container">
        <!--헤더 생략  -->
        <div class="content">
            <div class="section top bp12"
                style="background-image: url(/static/mobile/img/innovation/fr_1-bg01.png); padding-top: 72%;">
                <div class="title fixed">
                    <img src="/static/mobile/img/innovation/fr-1-cont-tt.png" alt="">
                </div>
            </div>
            <div class="section bp12">
                <div class="title">
                    <img src="/static/mobile/img/innovation/fr-2-cont-tt.png" alt="">
                </div>
                <div class="parent_tab">
                    <ul id="tab01">
                        <li><button class="selected" data-tab="tab01-1">새롭게 직렬,<br>직류 개편!</button></li>
                        <li><button data-tab="tab01-2">신설 직렬, 직류<br>자세히 보기</button></li>
                    </ul>
                    <div class="tab_cont">
                        <div>
                            <div class="open" id="tab01-1">
                                <div class="video_clip">
                                    <div class="video">
                                        <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                                        <!-- ws. 201122 미래인사혁신관 메인 탭01 -->
                                        <iframe
                                            src="https://www.youtube.com/embed/QHRdASij2C4?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0"
                                            frameborder="0"
                                            allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                                    </div>
                                </div>
                            </div>
                            <div id="tab01-2" class="bg_white box_padding">
                                <div class="inner_tab">
                                    <ul id="tab02">
                                        <li><button class="selected" data-tab="tab02-1">데이터 직류</button></li>
                                        <li><button data-tab="tab02-2">방재 안전연구<br>직렬</button></li>
                                    </ul>
                                    <div class="tab_cont">
                                        <div>
                                            <div class="open" id="tab02-1">
                                                <div class="inner_slider">
                                                    <div class="slider01">
                                                        <ul class="swiper-wrapper">
                                                            <li class="swiper-slide">
                                                                <img src="/static/mobile/img/innovation/ft-2-btn01-cont01.png"
                                                                    alt="">
                                                            </li>
                                                            <li class="swiper-slide">
                                                                <img src="/static/mobile/img/innovation/ft-2-1bt-cont02.png" alt="">
                                                            </li>
                                                            <li class="swiper-slide">
                                                                <img src="/static/mobile/img/innovation/ft-2-btn01-cont03.png"
                                                                    alt="">
                                                            </li>
                                                            <li class="swiper-slide">
                                                                <img src="/static/mobile/img/innovation/ft-2-btn01-cont04.png"
                                                                    alt="">
                                                            </li>
                                                        </ul>
                                                        <div class="dots"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="tab02-2">
                                                <div class="inner_slider">
                                                    <div class="slider01">
                                                        <ul class="swiper-wrapper">
                                                            <li class="swiper-slide">
                                                                <img src="/static/mobile/img/innovation/ft-2-btn02-cont01.png"
                                                                    alt="">
                                                            </li>
                                                            <li class="swiper-slide">
                                                                <img src="/static/mobile/img/innovation/ft-2-btn02-cont02.png"
                                                                    alt="">
                                                            </li>
                                                            <li class="swiper-slide">
                                                                <img src="/static/mobile/img/innovation/ft-2-btn02-cont03.png"
                                                                    alt="">
                                                            </li>
                                                            <li class="swiper-slide">
                                                                <img src="/static/mobile/img/innovation/ft-2-btn02-cont04.png"
                                                                    alt="">
                                                            </li>
                                                        </ul>
                                                        <div class="dots"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section bp12">
                <div class="title">
                    <img src="/static/mobile/img/innovation/fr-3-cont-tt.png" alt="">
                </div>
                <div class="parent_tab">
                    <ul id="tab03">
                        <li><button class="selected" data-tab="tab03-1">인재개발 플랫폼</button></li>
                        <li><button data-tab="tab03-2">국가공무원<br>인재개발원</button></li>
                    </ul>
                    <div class="tab_cont">
                        <div>
                            <div class="open" id="tab03-1">
                                <div class="video_clip">
                                    <div class="video">
                                        <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                                        <!-- ws. 201122 미래인사혁신관 인재개발 탭01 -->
                                        <iframe
                                            src="https://www.youtube.com/embed/vFyTaAbOFO4?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0"
                                            frameborder="0"
                                            allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                                    </div>
                                </div>
                                <div class="inner_content">
                                    <div>
                                        <div class="cont_wrap">
                                            <img src="/static/mobile/img/innovation/ft-3-1bt-cont01.png" alt="">
                                        </div>
                                        <div class="more green">
                                            <button class="slider_trigger" data-popup="6">자세히보기</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="inner_content wide">
                                    <div>
                                        <div class="inner_title">
                                            <h3 class="green">비대면 교육</h3>
                                        </div>
                                        <div class="video_clip">
                                            <div class="video">
                                                <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                                                <!-- ws. 201122 공직역사가치관 인재개발 탭01-1 비대면교육 -->
                                                <iframe
                                                    src="https://www.youtube.com/embed/Zorn5SoapYI?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0"
                                                    frameborder="0"
                                                    allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                                            </div>
                                        </div>
                                        <div class="inner_tab sep_top">
                                            <ul id="tab04" class="s_t">
                                                <li><button class="selected" data-tab="tab04-1">비대면 온라인 강의는 어떻게
                                                        진행될까요?</button></li>
                                                <li><button data-tab="tab04-2">비대면 강의를 받아본 소감은 어떨까요?</button></li>
                                            </ul>
                                            <div class="tab_cont">
                                                <div>
                                                    <div class="open" id="tab04-1">
                                                        <div class="video_clip">
                                                            <div class="video">
                                                                <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                                                                <!-- ws. 201122 공직역사가치관 인재개발 탭01-1-1 어떻게 진행 -->
                                                                <iframe
                                                                    src="https://www.youtube.com/embed/G72TvcbZO1A?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0"
                                                                    frameborder="0"
                                                                    allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="tab04-2">
                                                        <div class="video_clip">
                                                            <div class="video">
                                                                <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                                                                <!-- ws. 201122 공직역사가치관 인재개발 탭01-1-2 강의 소감-->
                                                                <iframe
                                                                    src="https://www.youtube.com/embed/YGur6Bux-HY?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0"
                                                                    frameborder="0"
                                                                    allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="inner_content wide">
                                    <div>
                                        <div class="inner_title">
                                            <h3 class="green">맞춤형 밀레니얼<br />콘텐츠 체험하기</h3>
                                        </div>
                                        <div class="video_clip">
                                            <div class="video">
                                                <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                                                <!-- ws. 201122 미래인사혁신관 인재개발 탭02 밀레니엄-->
                                                <iframe
                                                    src="https://www.youtube.com/embed/aqwadBTjPuk?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0"
                                                    frameborder="0"
                                                    allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                                            </div>
                                        </div>
                                        <div class="inner_tab sep_top">
                                            <ul id="tab05" class="s_t">
                                                <li><button class="selected" data-tab="tab05-1">신임 과장을 위한<br>수업
                                                        맛보기</button></li>
                                                <li><button data-tab="tab05-2">Quiz 풀어보기</button></li>
                                            </ul>
                                            <div class="tab_cont">
                                                <div>
                                                    <div class="open" id="tab05-1">
                                                        <div class="cont_wrap">
                                                            <img src="/static/mobile/img/innovation/slider8-1.png" alt="">
                                                        </div>
                                                        <div class="more green">
                                                            <button class="slider_trigger" data-popup="2">자세히보기</button>
                                                        </div>
                                                    </div>
                                                    <div id="tab05-2">
                                                        <div class="cont_wrap">
                                                            <img src="/static/mobile/img/innovation/slider9-1.png" alt="">
                                                        </div>
                                                        <div class="more green">
                                                            <button class="slider_trigger" data-popup="3">자세히보기</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab03-2">
                                <div class="video_clip">
                                    <div class="video">
                                        <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                                        <!-- ws. 201122 미래인사혁신관 국가인재 탭02 국가공무원인재개발원-->
                                        <iframe
                                            src="https://youtube.be/embed/JBovvH_VUEk?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0"
                                            frameborder="0"
                                            allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                                    </div>
                                </div>
                                <div class="inner_content">
                                    <div>
                                        <div class="inner_tab under_bar">
                                            <ul id="tab06" class="s_t">
                                                <li><button class="selected" data-tab="tab06-1">비전 및 미션</button></li>
                                                <li><button data-tab="tab06-2">인재원 역사</button></li>
                                            </ul>
                                            <div class="tab_cont">
                                                <div>
                                                    <div class="open" id="tab06-1">
                                                        <div class="cont_wrap">
                                                            <img src="/static/mobile/img/innovation/3-cont01.png" alt="">
                                                        </div>
                                                    </div>
                                                    <div id="tab06-2">
                                                        <ul class="button_list">
                                                            <li>
                                                                <button class="popup_trigger"
                                                                    data-popup="http://publicservicefair.kr/static/img/innovation/tab13-1.jpg">공무원<br>교육훈련의
                                                                    태동<br>1949.3~1961.9</button>
                                                            </li>
                                                            <li>
                                                                <button class="popup_trigger"
                                                                    data-popup="http://publicservicefair.kr/static/img/innovation/tab13-2.jpg">중앙공무원<br>교육원
                                                                    시대개막<br>1960년대</button>
                                                            </li>
                                                            <li>
                                                                <button class="popup_trigger"
                                                                    data-popup="http://publicservicefair.kr/static/img/innovation/tab13-3.jpg"
                                                                    class="selected">새마을교육의<br>실시<br>1970년대</button>
                                                            </li>
                                                            <li>
                                                                <button class="popup_trigger"
                                                                    data-popup="http://publicservicefair.kr/static/img/innovation/tab13-4.jpg">교육훈련체계<br>개편<br>1980년대</button>
                                                            </li>
                                                            <li>
                                                                <button class="popup_trigger"
                                                                    data-popup="http://publicservicefair.kr/static/img/innovation/tab13-5.jpg">공무원교육의<br>세계화<br>1990년대</button>
                                                            </li>
                                                            <li>
                                                                <button class="popup_trigger"
                                                                    data-popup="http://publicservicefair.kr/static/img/innovation/tab13-6.jpg">공무원교육의<br>선진화<br>2000년대</button>
                                                            </li>
                                                            <li>
                                                                <button class="popup_trigger"
                                                                    data-popup="http://publicservicefair.kr/static/img/innovation/tab13-7.jpg">미래지향적<br>국가인재양성<br>2010년대~</button>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section bp12">
                <div class="title">
                    <img src="/static/mobile/img/innovation/fr-5-cont-tt.png" alt="">
                </div>
                <div class="title">
                    <img src="/static/mobile/img/innovation/ft-4-tt-02.png" alt="">
                </div>
                <div class="cont_wrap" style="padding: 5%;">
                    <img src="/static/mobile/img/innovation/ft-5-cont01.png" alt="">
                    <div class="more purple">
                        <button class="slider_trigger" data-popup="7">자세히보기</button>
                    </div>
                </div>
                <div class="parent_tab purple mt10">
                    <ul id="tab07">
                        <li><button class="selected" data-tab="tab07-1">국가인재DB<br>국민추천제란?</button></li>
                        <li><button data-tab="tab07-2">정부 헤드헌팅이란?</button></li>
                    </ul>
                    <div class="tab_cont">
                        <div>
                            <div class="open" id="tab07-1">
                                <div id="video01-wrap">
                                    <div class="video_clip" id="video01-1">
                                        <div class="video">
                                            <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                                            <!-- ws. 201122 미래인사혁신관 국가인재DB 국민추천제란-->
                                            <iframe
                                                src="https://www.youtube.com/embed/-uNhFC-qaBc?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0"
                                                frameborder="0"
                                                allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                                        </div>
                                    </div>
                                    <div class="video_clip hide" id="video01-2">
                                        <div class="video">
                                            <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                                            <!-- ws. 201122 미래인사혁신관 국가인재DB 국민추천제란-->
                                            <iframe
                                                src="https://www.youtube.com/embed/-uNhFC-qaBc?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0"
                                                frameborder="0"
                                                allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                                        </div>
                                    </div>
                                    <div class="video_clip hide" id="video01-3">
                                        <div class="video">
                                            <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                                            <!-- ws. 201122 미래인사혁신관 국가인재DB 국민추천제란-->
                                            <iframe
                                                src="https://www.youtube.com/embed/-uNhFC-qaBc?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0"
                                                frameborder="0"
                                                allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                                        </div>
                                    </div>
                                </div>
                                <div class="inner_content purple">
                                    <div>
                                        <div class="custom_tab video_trigger">
                                            <ul id="video01">
                                                <li><button class="selected"
                                                        data-video="video01-1">지연수<span>국립고궁박물관<br>전시홍보과장</span></button>
                                                </li>
                                                <li><button
                                                        data-video="video01-2">김희경<span>우정공무원<br>교육원장</span></button>
                                                </li>
                                                <li><button data-video="video01-3">오창수 / 이상욱<span>우정사업본부 /
                                                            관세청</span></button></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab07-2">
                                <div class="bg_white box_padding box_radius shadow_purple">
                                    <div class="inner_slider open">
                                        <div class="slider04">
                                            <ul class="swiper-wrapper">
                                                <li class="swiper-slide">
                                                    <img src="/static/mobile/img/innovation/ft-6-cont01.png" alt="">
                                                </li>
                                                <li class="swiper-slide">
                                                    <img src="/static/mobile/img/innovation/ft-6-cont02.png" alt="">
                                                </li>
                                                <li class="swiper-slide">
                                                    <img src="/static/mobile/img/innovation/ft-6-cont03.png" alt="">
                                                </li>
                                                <li class="swiper-slide">
                                                    <img src="/static/mobile/img/innovation/ft-6-cont04.png" alt="">
                                                </li>
                                            </ul>
                                            <div class="dots purple"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="inner_content wide slider_wrap purple">
                                    <div>
                                        <div class="inner_title">
                                            <h3 class="purple">정부 헤드헌팅<br>사례 살펴보기</h3>
                                        </div>
                                        <div class="inner_slider open">
                                            <div class="slider05">
                                                <ul class="swiper-wrapper">
                                                    <li class="swiper-slide">
                                                        <img src="/static/mobile/img/innovation/fr-5-cont01.png" alt="">
                                                    </li>
                                                    <li class="swiper-slide">
                                                        <img src="/static/mobile/img/innovation/fr-5-cont02.png" alt="">
                                                    </li>
                                                    <li class="swiper-slide">
                                                        <img src="/static/mobile/img/innovation/fr-5-cont03.png" alt="">
                                                    </li>
                                                    <li class="swiper-slide">
                                                        <img src="/static/mobile/img/innovation/fr-5-cont04.png" alt="">
                                                    </li>
                                                    <li class="swiper-slide">
                                                        <img src="/static/mobile/img/innovation/fr-5-cont05.png" alt="">
                                                    </li>
                                                </ul>
                                                <div class="dots purple"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section quiz_wrap"
                style="background-image: url(/static/mobile/img/innovation/fr_6-bg01.png); background-position: bottom; background-size: contain; background-repeat: no-repeat; padding-bottom: 60%;">
                <div class="quiz gray">
                    <img src="/static/mobile/img/quiz/1-1.png" alt="">
                    <img src="/static/mobile/img/quiz//1-2.png" alt="">
                </div>
                <div class="quiz_btn gray">
                	 <a href="https://docs.google.com/forms/d/e/1FAIpQLSeuB-vJP2P4Kkf2N6Zj_J_wG-FTwKIt_DS4GjxntqWSusokxg/viewform"
                        target="_blank" class="btn_quiz">퀴즈 풀기</a>
                </div>
                <div class="cont_wrap">
                    <img src="/static/mobile/img/innovation/fr_6_cont_ox.png" alt="">
                </div>
            </div>
        </div>
        <div class="modal_wrap">
            <button class="close_btn">X</button>
            <div class="img_cont"></div>
        </div>
        <div class="modal_slider" id="modal-2">
            <button class="close_btn">X</button>
            <div class="inner_slider">
                <div class="slider02">
                    <ul class="swiper-wrapper">
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-1.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-2.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-3.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-4.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-5.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-6.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-7.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-8.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-9.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-10.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-11.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-12.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-13.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-14.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-15.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-16.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-17.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-18.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-19.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-20.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-21.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-22.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-23.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-24.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-25.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-26.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider8-27.png" alt="">
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="modal_slider" id="modal-3">
            <button class="close_btn">X</button>
            <div class="inner_slider">
                <div class="slider03">
                    <ul class="swiper-wrapper">
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider9-1.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider9-2.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider9-3.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider9-4.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider9-5.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider9-6.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider9-7.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider9-8.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider9-9.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider9-10.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider9-11.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider9-12.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slider9-13.png" alt="">
                        </li>
                    </ul>
                    <div class="dots"></div>
                </div>
            </div>
        </div>
        <div class="modal_slider" id="modal-6">
            <button class="close_btn">X</button>
            <div class="inner_slider">
                <div class="slider06">
                    <ul class="swiper-wrapper">
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/2-cont02.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/2-cont03.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/2-cont04.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/2-cont05.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/2-cont06.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/2-cont07.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/2-cont08.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/2-cont09.png" alt="">
                        </li>
                    </ul>
                    <div class="dots"></div>
                </div>
            </div>
        </div>
        <div class="modal_slider" id="modal-7">
            <button class="close_btn">X</button>
            <div class="inner_slider">
                <div class="slider07">
                    <ul class="swiper-wrapper">
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slide01-1.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slide01-2.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slide01-3.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slide01-4.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slide01-5.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slide01-6.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slide01-7.png" alt="">
                        </li>
                        <li class="swiper-slide">
                            <img src="/static/mobile/img/innovation/slide01-8.png" alt="">
                        </li>
                    </ul>
                    <div class="dots"></div>
                </div>
            </div>
        </div>
        <!-- 푸터 생략 -->
    </div>
    <script src="/static/mobile/js/common.js"></script>
    <script>
        var innerSlider01 = new Swiper('.slider01', {
            loop: false,
            slideActiveClass: 'active',
            observer: true,
            observeParents: true,
            pagination: {
                el: '.slider01 .dots',
                type: 'bullets',
            },
        })

        var innerSlider02 = new Swiper('.slider02', {
            loop: false,
            slideActiveClass: 'active',
            observer: true,
            observeParents: true,
        })

        var innerSlider03 = new Swiper('.slider03', {
            loop: false,
            slideActiveClass: 'active',
            observer: true,
            observeParents: true,
        })

        var innerSlider04 = new Swiper('.slider04', {
            loop: false,
            slideActiveClass: 'active',
            observer: true,
            observeParents: true,
            pagination: {
                el: '.slider04 .dots',
                type: 'bullets',
            },
        })

        var innerSlider05 = new Swiper('.slider05', {
            loop: false,
            slideActiveClass: 'active',
            observer: true,
            observeParents: true,
            pagination: {
                el: '.slider05 .dots',
                type: 'bullets',
            },
        })

        var innerSlider06 = new Swiper('.slider06', {
            loop: false,
            slideActiveClass: 'active',
            observer: true,
            observeParents: true,
        })

        var innerSlider06 = new Swiper('.slider07', {
            loop: false,
            slideActiveClass: 'active',
            observer: true,
            observeParents: true,
        })

        $('.parent_tab ul li button,.inner_tab ul li button').click(function () {
            const sibling = $(this).parent().siblings()
            const target = $(this).attr('data-tab')
            sibling.each(function () {
                $(this).children().removeClass('selected')
            })
            $(this).addClass('selected')
            const wrapper = $(this).closest('div').children('.tab_cont,.inner_tab_cont').children().children()
            wrapper.each(function () {
                $(this).removeClass('open')
            })
            $('#' + target).addClass('open')
        })

        $('.video_trigger > ul > li > button').click(function () {
            const target = $(this).attr('data-video')
            const parent = $(this).parent().parent().attr('id')
            const wrapper = $('#' +parent+"-wrap")
            const child = wrapper.children()
            child.each(function(){
                $(this).addClass('hide')
            })
            $('#' + target).removeClass('hide')
        })

        $(document).ready(function () {
            $('a[href^="#"]').on('click', function (e) {
                e.preventDefault();

                var target = this.hash;
                $target = $(target);

                $('html, body').stop().animate({
                    'scrollTop': $target.offset().top
                }, 900, 'swing', function () {
                    window.location.hash = target;
                });
            });
        });
    </script>
</body>

</html>