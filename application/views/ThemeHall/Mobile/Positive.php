<!DOCTYPE html>
<html lang="ko">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>적극행정관</title>
    <meta name="description" content="적극행정관" />
    <meta name="keywords" content="적극행정관" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="" />
    <meta property="og:title" content="적극행정관" />
    <meta property="og:description" content="적극행정관" />
    <link rel="stylesheet" href="/static/mobile/css/style.css">
    <!--<link rel="stylesheet" href="/static/mobile/css/mobile.css">-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <script type="text/javascript" src="https://unpkg.com/swiper@5.4.5/js/swiper.min.js"></script>
</head>

<body>
    <div class="container">
        <!-- 헤더 생략 -->
        <div class="content">
            <div class="section top bp12" style="background-image: url(/static/mobile/img/positive/aao_bg_01.png);">
                <div class="title">
                    <img src="/static/mobile/img/positive/aao-1-cont-tt.png" alt="">
                </div>
                <div class="video_clip">
                    <div class="video">
                        <!-- 동영상은 반응형 대응을 위해 아래 소스에 식별코드만 변경해 주세요. -->
                        <!-- ws. 201122 적극행정관 메인  -->
                        <iframe
                            src="https://www.youtube.com/embed/9WcHr5FPTEQ?autoplay=0&amp;mute=1&amp;loop=1&amp;controls=0"
                            frameborder="0"
                            allow="clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                    </div>
                </div>
            </div>
            <div class="section" style="background-image: url(/static/mobile/img/positive/aao_bg_02.png);">
                <div class="title">
                    <img src="/static/mobile/img/positive/aao-2-cont-tt.png" alt="">
                </div>
                <div class="cont_wrap">
                    <div class="box_link">
                        <a href="#">
                            <img src="/static/mobile/img/positive/aao-2-cont01.png" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="section" style="background-color: #6D7BE2;">
                <div class="title">
                    <img src="/static/mobile/img/positive/aao-3-cont-tt.png" alt="">
                </div>
                <div class="cont_wrap">
                    <div class="frame">
                        <div class="card_slider">
                            <div class="slide01">
                                <h3 class="purple">카드뉴스 중앙부처편</h3>
                                <ul class="swiper-wrapper">
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn-01.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn-02.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn-03.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn-04.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn-05.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn-06.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn-07.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn-08.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn-09.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn-010.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn-011.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn-012.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn-014.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn-015.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn-016.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn-017.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn-018.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn-019.png" alt="">
                                    </li>
                                </ul>
                                <div class="dots purple"></div>
                            </div>
                        </div>
                    </div>
                    <div class="frame">
                        <div class="card_slider">
                            <div class="slide01">
                                <h3 class="yellow">카드뉴스 지자체편</h3>
                                <ul class="swiper-wrapper">
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn2-01.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn2-02.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn2-03.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn2-04.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn2-05.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn2-06.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn2-07.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn2-08.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn2-09.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn2-10.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn2-11.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn2-12.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn2-13.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn2-14.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn2-15.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn2-15-1.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn2-16.png" alt="">
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/static/mobile/img/positive/aao-cn2-17.png" alt="">
                                    </li>
                                </ul>
                                <div class="dots yellow"></div>
                            </div>
                        </div>
                    </div>
                    <div class="btn_wrap mt10">
                        <a href="#" class="round_link">적극행정 ON</a>
                    </div>
                </div>
            </div>
            <div class="section quiz_wrap" style="background-image: url(/static/mobile/img/positive/aao_bg_04.png);">
                <div class="quiz yellow">
                    <img src="/static/mobile/img/quiz/2-1.png" alt="">
                    <img src="/static/mobile/img/quiz//2-2.png" alt="">
                </div>
                <div class="quiz_btn yellow">
                 <a href="https://docs.google.com/forms/d/e/1FAIpQLSeuB-vJP2P4Kkf2N6Zj_J_wG-FTwKIt_DS4GjxntqWSusokxg/viewform"
                    target="_blank" class="btn_quiz">퀴즈 풀기</a>
                </div>
                <div class="cont_wrap">
                    <img src="/static/mobile/img/positive/aao-5-cont01.png" alt="">
                </div>
            </div>
        </div>
        <!-- 푸터 생략 -->
    </div>
    <script src="/static/mobile/js/common.js"></script>
    <script>
        var wideSlider01 = new Swiper('.slide01', {
            loop: false,
            slideActiveClass: 'active',
            navigation: {
                nextEl: '.slide01 .next',
                prevEl: '.slide01 .prev',
            },
            observer: true,
            observeParents: true,
            pagination: {
                el: '.slide01 .dots',
                type: 'bullets',
            },
        })

        var wideSlider02 = new Swiper('.slide02', {
            loop: false,
            slideActiveClass: 'active',
            navigation: {
                nextEl: '.slide02 .next',
                prevEl: '.slide02 .prev',
            },
            observer: true,
            observeParents: true,
            pagination: {
                el: '.slide02 .dots',
                type: 'bullets',
            },
        })
    </script>
</body>

</html>