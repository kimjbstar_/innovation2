<!DOCTYPE html>
<html lang="ko">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>균형인사관</title>
    <meta name="description" content="균형인사관" />
    <meta name="keywords" content="균형인사관" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="" />
    <meta property="og:title" content="균형인사관" />
    <meta property="og:description" content="균형인사관" />
    <link rel="stylesheet" href="/static/css/style.css">
    <!--<link rel="stylesheet" href="/static/css/mobile.css">-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <script type="text/javascript" src="https://unpkg.com/swiper@5.4.5/js/swiper.min.js"></script>
</head>

<body>
    <div class="container">
        <div id="header">
            <div class="top">
                <div class="inner">
                    <a href="#" class="upper"><span class="blind">인사혁신처</span></a>
                    <a href="#" class="btn_login">회원가입ㆍ로그인</a>
                </div>
            </div>
            <div class="bottom">
                <div class="inner">
                    <a href="#" class="logo"><span class="blind">2020 온라인 공직박람회</span></a>
                    <ul>
                        <li>
                            <a href="#">개막관</a>
                        </li>
                        <li>
                            <a href="#">주제관</a>
                        </li>
                        <li>
                            <a href="#">채용정보관</a>
                        </li>
                        <li>
                            <a href="#">참여 프로그램</a>
                        </li>
                        <li>
                            <a href="#">소통ㆍ이벤트</a>
                        </li>
                        <li>
                            <a href="#">인재혁신세미나</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="personnel_wrap">
                <div class="section"
                    style="background-image: url(/static/img/personnel/1-bg02.png); background-color: #8D6BC3;">
                    <h2>균형인사관</h2>
                    <div class="video_clip large">
                        <div class="video"></div>
                    </div>
                    <div class="cont_wrap">
                        <div class="clip_tab">
                            <ul>
                                <li onclick="playthis('WOZVKTnnXkQ')" class="selected"><button>균형인사 포럼</button></li>
                                <li onclick="playthis('HQrhcilccWE')"><button>균형인사<br />성과공유대회</button></li>
                                <li onclick="window.open('http://www.mpm.go.kr/mpm/comm/pblcateList/;jsessionid=K5yDrVPjx-pNL3AQDlQUb3Qj.node06?boardId=bbs_0000000000000036&mode=view&cntId=866&category=&pageIdx=')"><button>균형인사 연차보고서</button></li><!--ws.1125수정완료-->
                                <li onclick="playthis('FpbEVX4wEF0')"><button>중증장애인<br />공무원 인터뷰</button></li>
                            </ul>
                        </div>
                        <img src="/static/img/personnel/1-cont05.png" alt="" class="pc" />
                    </div>
                </div>
                <div class="section"
                    style="background-image: url(/static/img/personnel/2-bg02.png); background-color: #E7D9FE;">
                    <h2 class="dark">양성평등</h2>
                    <div class="cont_wrap">
                        <div class="frame">
                            <img src="/static/img/personnel/2-cont01.png" alt="" class="pc" />
                            <div class="qna_wrap">
                                <span></span>
                                <h3>FAQ(자주 하는 질문)</h3>
                                <ul>
                                    <li>
                                        <em>Q. 양성평등 채용목표제는 모든 공무원 채용 시험에 적용되는 것인지요?</em>
                                        <p>인사혁신처에서 시행하는 5·7·9급 공개경쟁채용시험(교정·보호직렬은 제외), 외교관후보자 선발시험 등이 양성평등채용목표제 적용대상이며,
                                            선발예정인원이 5명 이상인 모집단위에 한해 적용됩니다. 인사혁신처가 실시하는 경력경쟁채용시험에 대해서는 필요시 시험계획 공고문에 명시하여
                                            적용하고 있습니다.</p>
                                    </li>
                                    <li>
                                        <em>Q. 양성평등채용목표제를 적용해서 여성에게 합격 우선권을 주면 남성이 손해를 보는데 공채시험의 취지에 맞지 않다고 봅니다.</em>
                                        <p>양성평등채용목표제는 여성 또는 남성이 시험실시단계별로 선발예정인원의 일정비율(30%) 이상이 될 수 있도록 선발예정인원을 초과하여 여성
                                            또는 남성을 추가 합격시키는 제도입니다. 여성만을 우대하는 것은 제도가 아니며, 한쪽 성의 응시자를 추가 선발하더라도 기존 합격선에 든
                                            다른성 합격자를 탈락시키는 것은 아니므로 특정성이 손해를 보는 것은 아닙니다. </p>
                                    </li>
                                    <li>
                                        <em>Q. 양성평등채용목표제 적용으로 여성 1명을 추가 합격시킬 수 있는 경우, 추가 합격선에 해당하는 여성 응시자 중 동점자가 2명 이상일
                                            때에는 어떻게 합격자를 결정하나요?</em>
                                        <p>양성평등채용목표제 적용으로 필기시험에서 여성 추가합격 가능인원이 1명인데, 추가 합격이 가능한 합격선에 동점인 여성 응시자가 2명 존재할
                                            경우에는 균형인사지침에 따라 2명 모두를 추가 합격자로 결정합니다.
                                            참고로, 공무원임용시험령 제25조제6항(면접시험 응시포기 발생으로 면접시험 응시인원이 선발예정인원에 미달할 것으로 예상되는 경우, 당초의
                                            필기시험 합격인원 범위 내에서 필기시험 추가 합격자 결정 가능)에 따라 실시하는 필기시험 추가 합격자 결정시에는 양성평등채용목표제를
                                            적용함에 있어 해당 성의 동점자 발생 등으로 당초 필기시험 합격인원을 초과하게 되는 경우에는 양성평등채용목표제를 적용하지 않습니다.
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="frame">
                            <img src="/static/img/personnel/2-cont02.png" alt="" class="pc" />
                        </div>
                    </div>
                </div>
                <div class="section" style="background-image: url(/static/img/personnel/3-bg01.png);">
                    <h2>장애인</h2>
                    <div class="cont_wrap">
                        <div class="frame">
                            <img src="/static/img/personnel/3-cont01.png" alt="" class="pc" />
                            <div class="qna_wrap">
                                <span></span>
                                <h3>FAQ(자주 하는 질문)</h3>
                                <ul>
                                    <li>
                                        <em>Q. 7ㆍ9급 공채에서 장애인 구분모집이 아닌 일반모집에도 응시가 가능한가요?</em>
                                        <p>장애인 및 상이등급에 해당하는 수험생은 장애인 구분모집 이외에 일반모집 단위로도 시험 응시가 가능합니다.</p>
                                    </li>
                                    <li>
                                        <em>Q. 7ㆍ9급 공채 장애인 구분모집 공고는 어디에서 확인할 수 있나요? 그리고 장애정도(중증/경증)에 관계없이 응시가
                                            가능한가요?</em>
                                        <p>인사혁신처 홈페이지 및 사이버국가고시센터에 매년 1월에 공고되는 「국가공무원 공개경쟁채용시험 계획」을 통해 장애인 구분모집 직렬과
                                            선발예정인원을 확인할 수 있습니다.
                                            <br />또한, 원서접수마감일 기준으로 장애인 또는 상이등급자로 유효하게 등록되어 있다면 장애의 종류 및 정도에 관계없이 장애인
                                            구분모집에 응시할 수 있습니다. </p>
                                    </li>
                                    <li>
                                        <em>Q. 응시원서 접수 당시에는 장애등록이 되어 있었으나, 이후 재판정을 받아 장애인 등록이 취소되었습니다. 이 경우 장애인 구분모집
                                            응시자격이 박탈되는 것인지요?</em>
                                        <p>응시원서 접수마감일 기준으로 관계법령에 따라 장애인으로 유효하게 등록되어 있었다면 응시원서 접수 이후 장애 재판정을 통해 장애인 등록이
                                            취소되었더라도 장애인 구분모집 응시자격은 그대로 유지됩니다. 덧붙여, 최종합격 후 공무원으로 임용된 이후 장애인 등록이 취소되더라도
                                            공무원 임용 사실에는 아무런 영향이 없습니다. </p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="frame">
                            <img src="/static/img/personnel/3-cont02.png" alt="" class="pc" />
                            <div class="qna_wrap">
                                <span></span>
                                <h3>FAQ(자주 하는 질문)</h3>
                                <ul>
                                    <li>
                                        <em>Q. 응시자격요건의 소지 여부 판단 기준은 어떻게 되나요?</em>
                                        <p>경력의 계산, 학위 또는 자격증의 소지 여부는 최종시험 예정일을 기준으로 판단합니다. 최종시험예정일 기준으로 유효기간 경과 등 효력을
                                            상실한 자격증은 인정하지 않습니다.</p>
                                    </li>
                                    <li>
                                        <em>Q. 장애유형에 관계없이 응시가 가능한가요? 또 면접위원이 장애유형 등을 미리 알고 면접을 진행하는 것인지요?</em>
                                        <p>원서접수마감일 기준으로 장애인 또는 상이등급자로 유효하게 등록되어 있다면 장애의 유형에 관계없이 응시할 수 있습니다. 또한 면접시험
                                            평가과정에서 수험생 개개인의 장애유형 등이 선입견으로 작용될 우려가 있기 때문에 원칙적으로 면접시험 위원에게 일체의 정보를 사전에
                                            제공하지 않습니다. 다만, 시험 위원이 응시자의 장애특성 등에 대해 적합한 고려를 해야 하는 경우도 있기 때문에 희망하는 수험생에 한해
                                            면접위원에게 장애유형 등을 미리 알려드리고 있습니다.</p>
                                    </li>
                                    <li>
                                        <em>Q. 예를 들어, 중증 청각장애의 경우 현행규정상 신체검사 결과 불합격 대상자에 해당하나요?</em>
                                        <p>공무원으로 임용되기 위해서는 「공무원채용신체검사규정」에 따른 채용 신체검사를 받아야 합니다. 그러나 동 법령 제5조에 근거하여
                                            「장애인복지법」 제2조에 따른 장애인은 신체검사 결과 불합격 판정기준에 해당하더라도 직무수행에 지장이 없다고 인정될 경우에 공무원으로
                                            임용이 가능합니다.</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="frame">
                            <img src="/static/img/personnel/3-cont03.png" alt="" class="pc" />
                            <div class="qna_wrap">
                                <span></span>
                                <h3>FAQ(자주 하는 질문)</h3>
                                <ul>
                                    <li>
                                        <em>Q. 보조공학기기를 신청하기 전에 미리 기기를 볼 수 있는 방법이 있나요?</em>
                                        <p>유튜브(Youtube)에서 한국장애인고용공단이 제공하는 <핸풋TV> 채널을 검색하면 다양한 보조공학기기를 미리 볼 수 있습니다. </p>
                                    </li>
                                    <li>
                                        <em>Q. 보조공학기기 지원* 선정이 된 이후, 이행보증증권 발급 방법 및 수수료는 어떻게 되나요? (* 지원가 100만원 이상의 경우에
                                            해당) </em>
                                        <p>소재지 인근 서울보증보험 방문 접수 또는 인터넷 접수를 통해 가능하며, 소요기간은 1~2일 정도입니다. 발급 시에는 해당 기기의
                                            0.596%(기본 산정 기준) 부담 수수료가 발생합니다. </p>
                                    </li>
                                    <li>
                                        <em>Q. 근로지원인을 활용하는 시간·장소 변경이나 근로지원인 변경이 가능한가요?</em>
                                        <p>필요시 소속기관과의 논의를 통해 지원 시간 및 장소의 변경이 가능합니다. 또한 정당한 사유가 발생하면 근로지원인 변경도 가능합니다.</p>
                                    </li>
                                    <li>
                                        <em>Q. 장애인활동지원사를 근로지원인으로 활용해도 되나요?</em>
                                        <p>근로지원인의 경우, 필수 교육시간을 이수하여야 근로지원인으로 활동이 가능하며, 기존의 장애인활동지원사를 근로지원인으로 중복하여 활용할 수
                                            없습니다.</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section"
                    style="background-image: url(/static/img/personnel/4-bg01.png); background-color: #E7E7FF;">
                    <h2 class="dark"><span>지역ㆍ지방인재</span></h2>
                    <div class="cont_wrap">
                        <div class="frame">
                            <img src="/static/img/personnel/4-cont01.png" alt="" class="pc" />
                            <div class="qna_wrap">
                                <span></span>
                                <h3>FAQ(자주 하는 질문)</h3>
                                <ul>
                                    <li>
                                        <em>Q. 선발시험에 합격 후 다음연도 수습시작 전까지 졸업하지 못할 경우 합격이 취소되나요?</em>
                                        <p>그렇습니다. 졸업예정자의 경우 수습근무를 시작할 때까지는 졸업할 수 있어야 하며, 만약 그렇지 않을 경우 합격의 효력이 상실됩니다.
                                            <br />또한, 학업을 이유로 수습근무 유예는 불가능하며, 수습근무 유예는 병역의무의 수행, 질병의 치료 등 불가피한 사유로 수습근무가
                                            어려운 경우에만 가능합니다.</p>
                                    </li>
                                    <li>
                                        <em>Q. 편입생도 학교의 추천이 가능한가요?</em>
                                        <p>가능합니다. 다만, 편입생은 편입한 대학에서 최소한 4학기를 이수한 경우에 해당 대학에서 추천이 가능합니다.(계절학기는 학기에 미포함)
                                            성적 또한 편입한 학교의 성적만이 반영됩니다.</p>
                                    </li>
                                    <li>
                                        <em>Q. 휴학생도 졸업예정자로서 학교의 추천이 가능한가요?</em>
                                        <p>추천 당시 각 대학이 정하는 졸업 학점의 3/4 이상을 취득한 자는 졸업예정자로서 학교의 추천을 받을 수 있습니다. 이에 해당한다면 휴학
                                            여부와 관계없이 학교의 추천이 가능합니다.
                                            <br />휴학생의 경우 졸업석차 비율은 휴학당시 석차를 기준으로 하나, 학교 전산시스템상 휴학당시 석차를 알 수 없는 경우는 추천시
                                            석차를 기준으로 하되 학교별로 일관된 기준을 적용하여야 합니다.</p>
                                    </li>
                                    <li>
                                        <em>Q. 부처배치는 어떤 방식으로 이루어지나요?</em>
                                        <p>부처배치는 각 부처의 결원, 인력운영 사정과 수요 및 수습직원의 전공분야, 경력, 적성, 희망, 필기시험 등을 고려하여 부처별 선발기준과
                                            수습직원의 희망 부처 순위(1~3순위)를 매칭하는 맞춤형 부처배치를 통해 이루어집니다. </p>
                                    </li>
                                    <li>
                                        <em>Q. 수습근무 도중에 부처를 옮기는 것이 가능한가요?</em>
                                        <p>수습직원은 하나의 중앙행정기관 등에서 수습근무를 하고 해당 기관에 임용되는 것을 원칙으로 합니다. 다만, 기구 개편, 직제 및 정원의 변경
                                            등으로 인력 재배치의 필요성이 있는 경우에 한해서만 소속장관과 협의하여 수습기관을 변경할 수 있습니다.</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="frame">
                            <img src="/static/img/personnel/4-cont02.png" alt="" class="pc" />
                            <div class="qna_wrap">
                                <span></span>
                                <h3>FAQ(자주 하는 질문)</h3>
                                <ul>
                                    <li>
                                        <em>Q. 보통교과와 전문교과 성적은 어떤 방식으로 산출하나요?</em>
                                        <p>(보통교과) 과목별 단위수로 가중 평균한 값*이 3.5등급 이내여야 합니다.
                                            <br />* 평균석차등급 = [∑(과목별 단위수×과목별 등급)] ÷ ∑과목별 단위수
                                            <br />※ 석차등급이 산출되지 않는 과목(예:음악, 미술, 체육 등)은 제외
                                            <br />(전문교과)
                                            <br />① 성취도 평균 B 이상 = ∑(성취도 환산점수*) ÷ 총 이수과목수 ≥ 0
                                            <br />* 성취도 환산점수 : A= 1점, B= 0점, C= -1점, D= -2점, E= -3점
                                            <br />② 성취도 A비율 = (성취도 A 이수과목수 ÷ 총 이수과목수) × 100
                                            <br />※ 첨부파일로 제공한 ‘학과성적 계산‘ 엑셀파일(사이버국가고시센터)을 활용하되, 해당 자료는 참고용으로만 활용하여 주시기 바람
                                        </p>
                                    </li>
                                    <li>
                                        <em>Q. 지원하고자 하는 직류 관련 전문교과를 50%이상 이수하지 못하면 지원이 불가능한가요?</em>
                                        <p>전문교과 이수요건을 충족하지 못했을 경우 지원하고자 하는 직류 관련 자격증을 취득하여 지원이 가능합니다. 단, 자격증을 2개 이상
                                            취득하여도 가산점은 부여되지 않습니다.
                                            <br />※ 자격증은 응시원서 접수일 전까지 취득하여야 함 </p>
                                    </li>
                                    <li>
                                        <em>Q. 자격증 취득 시 구체적인 가산방법은 어떻게 되나요?</em>
                                        <p>필기시험에서 각 과목 40점 이상 득점자가 전문교과 이수요건을 갖춘 경우 지원 직류 관련 자격증 1개당 각 과목별 만점의 2%, 최대
                                            4%까지 점수를 가산합니다.
                                            <br />※ 행정직군 : 전산회계운용사 자격증만 인정되므로 취득 시 2% 가산
                                            <br />※ 기술직군 : 종목이 다른 자격증일 경우 최대 2개(4%)까지 가산</p>
                                    </li>
                                    <li>
                                        <em>Q. 일반고에서 전학 온 학생도 추천 가능한가요?</em>
                                        <p>가능합니다. 다만, 보통교과 성적은 일반고에서 이수한 성적도 포함하여 산출해야 합니다.
                                            <br />※ 전문교과 성적은 직업계고로 전학한 이후 이수한 성적 산출</p>
                                    </li>
                                    <li>
                                        <em>Q. 학과별 정원에 따라 추천 상한인원이 정해져 있는데 졸업생도 졸업 전 학과의 추천인원에 포함 되나요?</em>
                                        <p>학과별 추천 상한인원*은 재학생과 졸업생을 모두 포함한 인원입니다.
                                            <br />* 100명 이하 : 최대 3명 / 101명 이상 : 최대 4명 / 학교당 최대 7명 추천</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="frame">
                            <div class="fake_link">
                                <a href="https://www.gosi.kr/receipt/infoSelfTest.do" target="_blank"></a>
                                <a href="/static/img/personnel/fake_link.pdf"></a>
                            </div>
                            <img src="/static/img/personnel/4-cont03.png" alt="" class="pc" />
                            <div class="qna_wrap">
                                <span></span>
                                <h3>FAQ(자주 하는 질문)</h3>
                                <ul>
                                    <li>
                                        <em>Q. 지방인재채용목표제 적용 신청은 언제 하나요? 제출해야 하는 서류는 무엇인가요?</em>
                                        <p>응시원서 접수 시에 지방인재 해당하는지 여부와 본인의 최종학력 사항을 입력하시면 지방인재 채용목표제 적용을 받을 수 있습니다. 증빙서류는
                                            졸업(재학)증명서와 학력기술서이며, 필기시험 합격자 발표일에 안내하는 기간 동안 제출하여야 합니다.</p>
                                    </li>
                                    <li>
                                        <em>Q. 각 대학별 분교는 지방학교인가요?</em>
                                        <p>균형인사지침에 따르면 고등교육법 제24조 상의 분교인 경우에 한해 분교의 소재지를 기준으로 지방학교 여부를 판단합니다. 본인이 해당하는
                                            학교가 분교인지 캠퍼스인지를 확인하시고 지방인재로 신청하시기 바랍니다.
                                            <br />참고로, 2020년 기준 고등교육법 제24조 상의 분교는 연세대학교(원주), 한양대학교(안산), 건국대학교(충주),
                                            고려대학교(세종), 동국대학교(경주)입니다.</p>
                                    </li>
                                    <li>
                                        <em>Q. 면접시험에서 지방인재라는 사실이 불리하게 작용하나요?</em>
                                        <p>면접시험 전에 면접위원들에게 지방인재채용목표제의 의의와 내용을 설명하긴 하나, 어떤 수험생이 지방인재에 해당하는 지에 대한 정보는 일정
                                            공개하지 않은 상태에서 면접을 진행합니다.</p>
                                    </li>
                                    <li>
                                        <em>Q. 동점자가 다수 발생하여 추가합격 상한 비율(5급 공채 10%, 7급 공채 5%)을 넘는 경우 동점자 합격 처리는 어떻게
                                            하나요?</em>
                                        <p>동점자 발생시 채용목표(5급 공채 20%, 7급 공채 30%)를 넘는 경우에는 동점자 전원을 합격시키나, 추가합격 상한 비율을 초과하는
                                            경우에는 동점자는 합격 처리하지 않습니다. </p>
                                    </li>
                                    <li>
                                        <em>Q. 필기시험 합격자 결정시 지방인재채용목표제와 양성평등채용목표제가 경합될 경우 어떤 제도가 우선 적용되나요?</em>
                                        <p>이 경우 지방인재채용목표제를 우선 적용하고 양성평등채용목표제를 후순위로 적용합니다.</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section" style="background-image: url(/static/img/personnel/5-bg01.png);">
                    <h2>저소득층</h2>
                    <div class="cont_wrap">
                        <div class="frame">
                            <img src="/static/img/personnel/5-cont01.png" alt="" class="pc" />
                            <div class="qna_wrap">
                                <span></span>
                                <h3>FAQ(자주 하는 질문)</h3>
                                <ul>
                                    <li>
                                        <em>Q. 부모님이 기초생활수급자인데, 본인이 저소득층 모집에 응시가 가능하나요?</em>
                                        <p>저소득층 구분모집에 응시하기 위해서는 응시자 본인이 응시 자격에 해당하여야 합니다. 따라서 부모님이 수급자에 해당하는 지 여부와 관계없이
                                            응시자 본인이 응시 자격을 갖추지 않는다면 응시할 수 없습니다. </p>
                                    </li>
                                    <li>
                                        <em>Q. 저소득층 구분모집에 응시하고 싶은데 어떻게 확인하나요?</em>
                                        <p>국민기초생활보장사업은 보건복지부에서, 한부모가족지원사업은 여성가족부에서 각 지방자치단체를 통해 시행하고 있습니다. 따라서 본인의 주민등록상
                                            주소지의 시·군·구청·주민센터에 가시면 본인이 수급자(지원대상자)에 해당하는지 또 수급(지원)기간이 어떻게 되는지 확인할 수 있습니다.
                                        </p>
                                    </li>
                                    <li>
                                        <em>Q. 시험 응시 이후에 응시자격을 상실하는 경우에 불이익이 있나요?</em>
                                        <p>저소득층 구분모집의 경우 응시원서 접수일 또는 접수마감일까지 계속하여 2년 이상 자격을 유지한 경우에 응시가 가능하며, 접수한 이후에
                                            응시자격을 상실하는 경우 접수한 당해연도 시험에 불이익이 없음을 알려드립니다.</p>
                                    </li>
                                    <li>
                                        <em>Q. 차상위 계층도 저소득층 구분모집에 응시원서를 낼 수 있나요?</em>
                                        <p>9급 공채시험에서 저소득층 구분모집 응시대상은 「국민기초생활보장법」에 따른 수급자와 「한부모가족지원법」에 따른 지원대상자만 해당됩니다.
                                            원칙상 차상위계층은 「국민기초생활보장법」에 따른 수급권자가 아니기에 저소득층 구분모집에 응시할 수 없습니다. 다만 「국민기초생활보장법」
                                            제14조의 2에 따라 차상위 계층이라도 예외적으로 수급권자로 인정되는 경우에는 응시자격을 갖춘 후 응시가 가능합니다.</p>
                                    </li>
                                    <li>
                                        <em>Q. 한부모가족지원법에 따른 지원대상자 자격이 사라지고, 기초생활수급자 기준이 충족하여 수급자 자격을 취득했습니다. 이 때 이전의
                                            지원대상자 기간도 합산하여 계산이 가능한가요?</em>
                                        <p>해당법률에 의한 수급기간 및 지원기간이 합산하여 연속하여 2년 이상인 경우에는 저소득층 구분모집 응시자격이 인정되므로, 각 증명기간이
                                            중복되는지 여부를 사전에 확인 부탁드립니다.</p>
                                    </li>
                                    <li>
                                        <em>Q. 군 입대 전까지 수급자 급여를 받다가 군대를 갔습니다. 전역 이후 수급자 결정이 된 경우 저소득층 전형으로 응시가
                                            가능한가요?</em>
                                        <p>군복무 또는 교환학생으로 해외에 체류하는 경우 이로 인하여 그 기간에 급여 대상자에 제외된 경우에도 가구주가 그 기간에 계속하여 수급자로
                                            있었다면 응시자도 수급에 해당한다고 보며 응시자격(2년)을 갖추면 응시가 가능합니다. 다만 군복무 또는 교환학생으로 인한 해외체류 종료
                                            후 다시 수급자(지원대상자)로 결정되어야 기간의 계속성을 인정하며 이 경우 급여(지원)의 신청을 기간 종료 후 2개월 내에 하거나
                                            급여(지원)의 결정이 기간 종료 후 2개월 내어야 합니다.</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="section" style="background-image: url(/static/img/personnel/6-bg01.png);">
                    <div class="cont_wrap">
                        <div class="horz_wrap">
                            <img src="/static/img/personnel/6-cont01.png" alt="" />
                            <img src="/static/img/personnel/6-cont02.png" alt="" />
                        </div>
                        <div class="quiz purple">
                            <a href="https://docs.google.com/forms/d/e/1FAIpQLSeuB-vJP2P4Kkf2N6Zj_J_wG-FTwKIt_DS4GjxntqWSusokxg/viewform" target="_blank" class="btn_quiz">퀴즈 풀기</a>
                        </div>
                        <img src="/static/img/personnel/6-cont03.png" alt="" class="pc" />
                    </div>
                </div>
            </div>
        </div>
        <div id="footer">
            <div class="inner">
                <div class="top">
                    <a href="#" class="footer_logo"><span class="blind">인사혁신처</span></a>
                    <ul>
                        <li class="fb">
                            <a href="#"><span class="blind">facebook</span></a>
                        </li>
                        <li class="tw">
                            <a href="#"><span class="blind">twitter</span></a>
                        </li>
                        <li class="ig">
                            <a href="#"><span class="blind">instagram</span></a>
                        </li>
                        <li class="yt">
                            <a href="#"><span class="blind">youtube</span></a>
                        </li>
                    </ul>
                </div>
                <div class="bottom">
                    <ul>
                        <li><a href="#">개인정보 처리방침</a></li>
                        <li><a href="#">영상정보처리기 운영ㆍ관리방침</a></li>
                        <li><a href="#">저작권보호 및 이용정책</a></li>
                    </ul>
                    <p>30102 세종특별자치시 한누리대로 499(어진동) 세종포스트빌딩 6층~12층</p>
                    <p>copyright© Ministry of Personnel Management. All Rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
    <script src="/static/js/common.js"></script>
    <script>
        $('.clip_tab button').click(function () {
            $('.clip_tab li').removeClass('selected');
            $(this).parent().addClass('selected')
        })

        $('.qna_wrap ul li em').click(function () {
            if ($(this).parent().hasClass('selected')) {
                $('.qna_wrap ul li').removeClass('selected');
            } else {
                $('.qna_wrap ul li').removeClass('selected');
                $(this).parent().addClass('selected')
            }
        })





        function playthis(playurl){
                    var play_video = '<iframe width="1280" height="720" src="https://www.youtube.com/embed/'+playurl+'?rel=0&amp; autoplay=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                        $($('.video')[0]).html(play_video)
                    // location.href="#playhere";
                }

        document.addEventListener('DOMContentLoaded', function(){

             var play_video = '<iframe width="1280" height="720" src="https://www.youtube.com/embed/WOZVKTnnXkQ?rel=0&amp; autoplay=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                $($('.video')[0]).html(play_video)



        });/*ws 1125 수정완료 메인영상





    </script>
</body>

</html>