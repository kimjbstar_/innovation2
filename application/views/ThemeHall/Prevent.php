<!DOCTYPE html>
<html lang="ko">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>K-방역관</title>
    <meta name="description" content="K-방역관" />
    <meta name="keywords" content="K-방역관" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="" />
    <meta property="og:title" content="K-방역관" />
    <meta property="og:description" content="K-방역관" />
    <link rel="stylesheet" href="/static/css/style.css">
    <!--<link rel="stylesheet" href="/static/css/mobile.css">-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <script type="text/javascript" src="https://unpkg.com/swiper@5.4.5/js/swiper.min.js"></script>
</head>

<body>
    <div class="container">
        <div id="header">
            <div class="top">
                <div class="inner">
                    <a href="#" class="upper"><span class="blind">인사혁신처</span></a>
                    <a href="#" class="btn_login">회원가입ㆍ로그인</a>
                </div>
            </div>
            <div class="bottom">
                <div class="inner">
                    <a href="#" class="logo"><span class="blind">2020 온라인 공직박람회</span></a>
                    <ul>
                        <li>
                            <a href="#">개막관</a>
                        </li>
                        <li>
                            <a href="#">주제관</a>
                        </li>
                        <li>
                            <a href="#">채용정보관</a>
                        </li>
                        <li>
                            <a href="#">참여 프로그램</a>
                        </li>
                        <li>
                            <a href="#">소통ㆍ이벤트</a>
                        </li>
                        <li>
                            <a href="#">인재혁신세미나</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="prevent_wrap">
                <div class="section" style="background-image: url(/static/img/prevent/1-bg.png);">
                    <h2>K-시험방역관</h2>
                    <div class="video_clip large">
                        <div class="video"></div>
                    </div>
                </div>
                <div class="section n_p" style="background-color: #008ba6;">
                    <div class="pre_bg_con"></div>
                    <div class="pre_inner_con">
                        <h2 class="green"><span>수험생의 안전을 확보하라!</span></h2>
                        <div class="cont_wrap">
                            <div class="box_tab green" id="tab_01">
                                <ul>
                                    <li>
                                        <button onclick="playthis(1,'uxcLxzFIdd4')" class="selected" data-tab="tab_01_tab1">안전한 채용을 위한 인사혁신처의 노력</button>
                                    </li>
                                    <li>
                                        <button onclick="playthis(2,'Sg5AM3TAjEA')" data-tab="tab_01_tab2">공시생 3년차의 D-day</button>
                                    </li>
                                </ul>
                                <div class="tab_cont video m_t_p_30">
                                    <div>
                                        <div class="open " id="tab_01_tab1">
                                            <div class="video_clip large">
                                                <div class="video"></div>
                                            </div>
                                        </div>
                                        <div id="tab_01_tab2">
                                            <div class="video_clip large">
                                                <div class="video"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h2 class="green both_p"><span>K-시험방역 어떤 노력들이 있었나요??</span></h2>
                        <img src="/static/img/prevent/rail2.png" class="self_100" alt="" />
                        <div class="cont_wrap">
                            <img src="/static/img/prevent/p-cont03.png" alt="" style="margin-top: 200px;" />
                        </div>
                        <h2 class="green" style="margin: 200px 0 50px;"></h2>
                        <div class="cont_wrap">
                            <img src="/static/img/prevent/p-cont04.png" alt="" />
                        </div>
                        <h2 class="green" style="margin: 200px 0 50px;"><span>방역 노하우 공유로 채용시장에 활기를!</span></h2>
                        <div class="cont_wrap">
                            <div class="cont_tab col4" id="tab_02">
                                <div class="tab_bg green"></div>
                                <ul class="pre">
                                    <li>
                                        <button class="selected" data-tab="tab_02_tab1">
                                            <div></div>
                                            <span>현장견학 지원</span>
                                        </button>
                                    </li>
                                    <li>
                                        <button data-tab="tab_02_tab2">
                                            <div></div>
                                            <span>방역<br />메뉴얼 제공</span>
                                        </button>
                                    </li>
                                    <li>
                                        <button data-tab="tab_02_tab3">
                                            <div></div>
                                            <span>방역관련<br />컨설팅 진행</span>
                                        </button>
                                    </li>
                                    <li>
                                        <button data-tab="tab_02_tab4">
                                            <div></div>
                                            <span>온라인<br />국제 세미나 참여</span>
                                        </button>
                                    </li>
                                </ul>
                                <div class="tab_cont">
                                    <div>
                                        <div class="open" id="tab_02_tab1">
                                            <img src="/static/img/prevent/tab-cont01.png" alt="">
                                        </div>
                                        <div id="tab_02_tab2">
                                            <img src="/static/img/prevent/tab-cont02.png" alt="">
                                        </div>
                                        <div id="tab_02_tab3">
                                            <img src="/static/img/prevent/tab-cont03.png" alt="">
                                        </div>
                                        <div id="tab_02_tab4">
                                            <img src="/static/img/prevent/tab-cont04.png" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pre_bg_con_end"></div>
                </div>

                <div class="section" style="background-color: #008ba6;">
                    <div class="cont_wrap">
                        <div class="horz_wrap">
                            <img src="/static/img/prevent/2-cont01.png" alt="" />
                            <img src="/static/img/prevent/2-cont02.png" alt="" />
                        </div>
                        <div class="quiz light">
                            <a href="https://docs.google.com/forms/d/e/1FAIpQLSeuB-vJP2P4Kkf2N6Zj_J_wG-FTwKIt_DS4GjxntqWSusokxg/viewform"
                                target="_blank" class="btn_quiz">퀴즈 풀기</a>
                        </div>
                        <img src="/static/img/prevent/2-cont03.png" alt="" class="pc" />
                    </div>
                </div>
            </div>
        </div>
        <div id="footer">
            <div class="inner">
                <div class="top">
                    <a href="#" class="footer_logo"><span class="blind">인사혁신처</span></a>
                    <ul>
                        <li class="fb">
                            <a href="#"><span class="blind">facebook</span></a>
                        </li>
                        <li class="tw">
                            <a href="#"><span class="blind">twitter</span></a>
                        </li>
                        <li class="ig">
                            <a href="#"><span class="blind">instagram</span></a>
                        </li>
                        <li class="yt">
                            <a href="#"><span class="blind">youtube</span></a>
                        </li>
                    </ul>
                </div>
                <div class="bottom">
                    <ul>
                        <li><a href="#">개인정보 처리방침</a></li>
                        <li><a href="#">영상정보처리기 운영ㆍ관리방침</a></li>
                        <li><a href="#">저작권보호 및 이용정책</a></li>
                    </ul>
                    <p>30102 세종특별자치시 한누리대로 499(어진동) 세종포스트빌딩 6층~12층</p>
                    <p>copyright© Ministry of Personnel Management. All Rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
    <script src="/static/js/common.js"></script>
    <script>
        $('.box_tab ul li button,.cont_tab ul li button').click(function () {
            const sibling = $(this).parent().siblings()
            const target = $(this).attr('data-tab')
            sibling.each(function () {
                $(this).children().removeClass('selected')
            })
            $(this).addClass('selected')
            const wrapper = $(this).closest('div').children('.tab_cont').children().children()
            wrapper.each(function () {
                $(this).removeClass('open')
            })
            $('#' + target).addClass('open')
        })





        function playthis(which,playurl){
                    var play_video = '<iframe width="1280" height="720" src="https://www.youtube.com/embed/'+playurl+'?rel=0&amp; autoplay=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                        $($('.video')[1]).html(play_video)
                    // location.href="#playhere";
                }

        document.addEventListener('DOMContentLoaded', function(){

            var play_video = '<iframe width="1280" height="720" src="https://www.youtube.com/embed/FiCieG0nr5k?rel=0&amp; autoplay=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                $($('.video')[0]).html(play_video)


            var play_video = '<iframe width="1280" height="720" src="https://www.youtube.com/embed/uxcLxzFIdd4?rel=0&amp; autoplay=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                $($('.video')[1]).html(play_video)



        });






    </script>
</body>

</html>