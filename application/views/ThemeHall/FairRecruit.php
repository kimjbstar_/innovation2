<!DOCTYPE html>
<html lang="ko">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>공정채용관</title>
    <meta name="description" content="공정채용관" />
    <meta name="keywords" content="공정채용관" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="" />
    <meta property="og:title" content="공정채용관" />
    <meta property="og:description" content="공정채용관" />
    <link rel="stylesheet" href="/static/css/style.css">
    <!--<link rel="stylesheet" href="/static/css/mobile.css">-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <script type="text/javascript" src="https://unpkg.com/swiper@5.4.5/js/swiper.min.js"></script>
</head>

<body>
    <div class="container">
        <div id="header">
            <div class="top">
                <div class="inner">
                    <a href="#" class="upper"><span class="blind">인사혁신처</span></a>
                    <a href="#" class="btn_login">회원가입ㆍ로그인</a>
                </div>
            </div>
            <div class="bottom">
                <div class="inner">
                    <a href="#" class="logo"><span class="blind">2020 온라인 공직박람회</span></a>
                    <ul>
                        <li>
                            <a href="#">개막관</a>
                        </li>
                        <li>
                            <a href="#">주제관</a>
                        </li>
                        <li>
                            <a href="#">채용정보관</a>
                        </li>
                        <li>
                            <a href="#">참여 프로그램</a>
                        </li>
                        <li>
                            <a href="#">소통ㆍ이벤트</a>
                        </li>
                        <li>
                            <a href="#">인재혁신세미나</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="employ_wrap">
                <div class="section" style="background-image: url(/static/img/employ/bg01.png); height: 1100px;">
                    <h2>공정채용관</h2>
                    <div class="video_clip large">
                        <div class="video"></div>
                    </div>
                </div>
                <div class="section">
                    <h2 class="dark"><span>채용단계별 공정성 확보 방안</span></h2>
                    <p class="sub dark">각 아이콘을 클릭해 단계별 세부사항을 확인해보세요!</p>
                    <div class="cont_wrap">
                        <div class="cont_tab" id="tab_01">
                            <div class="tab_bg"></div>
                            <ul class="step">
                                <li><button data-tab="tab_01_tab1"></button></li>
                                <li><button data-tab="tab_01_tab2"></button></li>
                                <li><button data-tab="tab_01_tab3"></button></li>
                                <li><button data-tab="tab_01_tab4"></button></li>
                                <li><button data-tab="tab_01_tab5"></button></li>
                            </ul>
                            <div class="tab_cont">
                                <div>
                                    <div class="open" id="tab_01_tab1">
                                        <img src="/static/img/employ/1-cont01-1.png" alt="">
                                    </div>
                                    <div id="tab_01_tab2">
                                        <img src="/static/img/employ/1-cont01-2.png" alt="">
                                    </div>
                                    <div id="tab_01_tab3">
                                        <img src="/static/img/employ/1-cont01-3.png" alt="">
                                    </div>
                                    <div id="tab_01_tab4">
                                        <img src="/static/img/employ/1-cont01-4.png" alt="">
                                    </div>
                                    <div id="tab_01_tab5">
                                        <img src="/static/img/employ/1-cont01-5.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section" style="background-image: url(/static/img/employ/2-bg.jpg);">
                    <div class="cont_wrap">
                        <img src="/static/img/employ/2-cont00-1.png" alt="">
                        <div class="cont_tab" id="tab_02">
                            <ul class="button">
                                <li><button data-tab="tab_02_tab1"></button></li>
                                <li><button data-tab="tab_02_tab2"></button></li>
                                <li><button data-tab="tab_02_tab3"></button></li>
                                <li><button data-tab="tab_02_tab4"></button></li>
                                <li><button data-tab="tab_02_tab5"></button></li>
                                <li><button data-tab="tab_02_tab6"></button></li>
                                <li><button data-tab="tab_02_tab7"></button></li>
                            </ul>
                            <div class="tab_cont">
                                <div>
                                    <div class="open" id="tab_02_tab1">
                                        <img src="/static/img/employ/2-cont02-1.png" alt="">
                                    </div>
                                    <div id="tab_02_tab2">
                                        <img src="/static/img/employ/2-cont02-2.png" alt="">
                                    </div>
                                    <div id="tab_02_tab3">
                                        <img src="/static/img/employ/2-cont02-3.png" alt="">
                                    </div>
                                    <div id="tab_02_tab4">
                                        <img src="/static/img/employ/2-cont02-4.png" alt="">
                                    </div>
                                    <div id="tab_02_tab5">
                                        <img src="/static/img/employ/2-cont02-5.png" alt="">
                                    </div>
                                    <div id="tab_02_tab6">
                                        <img src="/static/img/employ/2-cont02-6.png" alt="">
                                    </div>
                                    <div id="tab_02_tab7">
                                        <img src="/static/img/employ/2-cont02-7.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h2 class="t_p_200"><img src="/static/img/employ/2-cont03.png" alt=""></h2>
                    <div class="video_clip large">
                        <div class="video"></div>
                    </div>
                    <h2 class="t_p_200"><img src="/static/img/employ/2-cont04.png" alt=""></h2>
                    <div class="cont_wrap">
                        <div class="box_tab col5 dblue" id="tab_03">
                            <ul>
                                <li>
                                    <button class="selected" data-tab="tab_03_tab1">시험문제 출제</button>
                                </li>
                                <li>
                                    <button data-tab="tab_03_tab2">시험문제 선정</button>
                                </li>
                                <li>
                                    <button data-tab="tab_03_tab3">시험문제 유출방지</button>
                                </li>
                                <li>
                                    <button data-tab="tab_03_tab4">필기 시험 실시</button>
                                </li>
                                <li>
                                    <button data-tab="tab_03_tab5">답안지 채점</button>
                                </li>
                            </ul>
                            <div class="tab_cont">
                                <div class="padding">
                                    <div class="open" id="tab_03_tab1">
                                        <div class="qna_wrap stalone">
                                            <ul>
                                                <li>
                                                    <em>Q. 시험문제 출제위원은 공정하게 선정되는 건가요?</em>
                                                    <div class="col2">
                                                        <div>
                                                            <img src="/static/img/employ/2-cont04-1.jpg" alt="">
                                                        </div>
                                                        <div>
                                                            <p>최근 3년 간 학원강의 또는 학교 고시반을 담당하거나 수험서 발간경력이 있는 자는 시험위원에서 배제하고, 위촉된 시험위원은 교육 및 보안서약서를 징구하는 등 위촉된 사실에 대해 반드시 비밀을 유지하도록 하고 있습니다.</p>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div id="tab_03_tab2">
                                        <div class="qna_wrap stalone">
                                            <ul>
                                                <li>
                                                    <em>Q. 특정위원이 출제한 문제가 시험문제로 많이 선정되는 것은 아닌가요?</em>
                                                    <p>문제은행에 문제를 입고하는 ‘출제위원’과 최종적으로 문제를 선정하는 ‘선정위원’을 구분하여 특정위원이 출제한 문제가 최종 시험문제에 다수 포함되는 상황을 사전에 방지하고 있습니다. 또한 선정위원 본인이 문제은행에 입고한 문제는 당해 시험에 실제 사용할 문제로 선정할 수 없으며, 같은 과목 내에서 동일대학 출제위원의 문제가 일정 수를 넘어 선정되지 않도록 제한하고 있습니다.</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div id="tab_03_tab3">
                                        <div class="qna_wrap stalone">
                                            <ul>
                                                <li>
                                                    <em>Q. 출제 과정에서 시험문제가 외부인에 의해 유출될 가능성은 없나요?</em>
                                                    <div class="col2">
                                                        <div>
                                                            <img src="/static/img/employ/2-cont04-2.jpg" alt="">
                                                        </div>
                                                        <div>
                                                            <p>문제선정 및 검토 등 모든 출제과정은 외부와의 접촉이 철저히 차단된 보안시설에서 이루어지고 있습니다. 해당 시설은 경비인력에 의한 외부인 출입통제, CCTV 설치 등을 통해 엄격히 보안이 관리되고 있으며, 출제위원 및 관련 직원들은 통신장비를 보안시설 입소시에 제출하고 시험 전까지 시설 내에서 합숙을 하고 있습니다. 보안시설 내에서도 시험관련 자료는 심사실 이외의 장소(숙소 등)로의 무단 반출을 철저히 금지하고, 출제 기간 동안에는 어떠한 물건도 출제 보안시설 안에서 밖으로 나갈 수 없도록 철저하게 관리하고 있습니다.</p>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <em>Q. 시험문제 인쇄 과정에서의 시험문제 유출 가능성은 없나요?</em>
                                                    <div class="col2">
                                                        <div>
                                                            <img src="/static/img/employ/2-cont04-3.jpg" alt="">
                                                        </div>
                                                        <div>
                                                            <p>시험문제 인쇄시 외부와의 접촉이 철저히 차단된 보안시설이 갖추어진 인쇄업체를 선정하고, 내외부 CCTV 점검 및 출입문·창문 봉인, 인쇄 종사자 통신장비 수거 등 보안관련 사항을 철저히 점검하고 있습니다.</p>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <em>Q. 인쇄된 문제는 시험장소(학교 등)로 안전하게 전달되는 건가요?</em>
                                                    <div class="col2">
                                                        <div>
                                                            <img src="/static/img/employ/2-cont04-4.jpg" alt="">
                                                        </div>
                                                        <div>
                                                            <p>문제지는 인쇄 후부터 해당 시험실로 들어가기 직전까지 봉인되어 관리되며, 인사혁신처 담당 공무원 등의 감독 하에 보안경비업체의 전문 경비인력이 보안차량을 이용하여 안전하게 시험장소로 문제지를 전달하고 있습니다.</p>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div id="tab_03_tab4">
                                        <div class="qna_wrap stalone">
                                            <ul>
                                                <li>
                                                    <em>Q. 특정 수험생에게 유리하게 시험장이 배정될 가능성은 없나요?</em>
                                                    <div class="col2">
                                                        <div>
                                                            <img src="/static/img/employ/2-cont04-5.jpg" alt="">
                                                        </div>
                                                        <div>
                                                            <p>응시번호를 무작위로 부여하여 응시번호를 통해 나이·성별 등을 식별할 수 없도록 하고, 응시번호에 따라 시험장(학교 등) 및 시험실을 배정하여 부정행위를 방지하고 있습니다.</p>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <em>Q. 시험장에서 시험시간 이전에 수험생에게 시험문제가 유출될 가능성은 없나요?</em>
                                                    <p>시험문제는 감독관에 의해 시험시작 약 5분전에 각 시험실로 전달되며, 시험문제가 시험실로 들어가게 된 이후에는 수험생의 출입을 통제하여 시험문제가 사전에 유출될 가능성을 차단하고 있습니다. 또한 시험시작 전에는 문제지를 열람하는 행위를 금지하여 모든 수험생이 동일한 시간 동안 문제를 풀 수 있도록 관리하고 있습니다.</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div id="tab_03_tab5">
                                        <div class="qna_wrap stalone">
                                            <ul>
                                                <li>
                                                    <em>Q. 제출한 답안지는 안전하게 채점하는 곳으로 전달되는 건가요?</em>
                                                    <div class="col2">
                                                        <div>
                                                            <img src="/static/img/employ/2-cont04-7.jpg" alt="">
                                                        </div>
                                                        <div>
                                                            <p>답안지가 분실, 누락되지 않도록 각 시험장별로 8회 이상에 걸쳐 매수를 확인하고 있습니다. 또한 시험문제가 시험장소에 전달될 때와 같이, 보안경비업체의 전문 경비인력, 인사혁신처 담당 직원 등이 보안차량을 이용하여 답안지를 안전하게 보안시설로 전달하고 있습니다.</p>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <em>Q. 채점 과정에서 외부인 침입에 의하여 답안지가 변경·훼손될 우려는 없는 건가요?</em>
                                                    <p>경비인력에 의한 외부인 출입통제, CCTV 설치 등 외부인이 침입할 수 없도록 안전하게 관리되는 보안시설에서 답안지가 보관되며 채점 작업이 이루어집니다.</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section" style="background-image: url(/static/img/employ/3-bg.jpg);">
                    <h2><img src="/static/img/employ/3-cont01.png" alt=""></h2>
                    <p class="sub dark" style="margin-top: -20px;">"본 동영상은 인사혁신처가 각 기관 채용담당 교육을 위해 제작한 온라인 교육 프로그램의 일부입니다."</p>
                    <div class="cont_wrap">
                        <div class="class_tab">
                            <ul>
                                <li><button onclick="playthis('0l8zMuOyOgQ');" class="selected">1차시</button></li>
                                <li><button onclick="playthis('sALX1QlluHU');" >2차시</button></li>
                                <li><button onclick="playthis('W5BqWPtVsN4');" >3차시</button></li>
                            </ul>
                            <div class="desktop_cont">
                                <div class="bg_desktop"></div>
                                <div class="video_clip">
                                    <div class="video"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="section" style="background-image: url(/static/img/employ/4-bg.jpg);">
                    <div class="cont_wrap">
                        <div class="horz_wrap">
                            <img src="/static/img/employ/6-cont01.png" alt="" />
                            <img src="/static/img/employ/6-cont02.png" alt="" />
                        </div>
                        <div class="quiz blue">
                            <a href="https://docs.google.com/forms/d/e/1FAIpQLSeuB-vJP2P4Kkf2N6Zj_J_wG-FTwKIt_DS4GjxntqWSusokxg/viewform"
                                target="_blank" class="btn_quiz">퀴즈 풀기</a>
                        </div>
                        <img src="/static/img/employ/6-cont03.png" alt="" class="pc" />
                        <img src="/static/img/mobile/employ/6-cont03.png" alt="" class="mobile" />
                    </div>
                </div>
            </div>
        </div>
        <div id="footer">
            <div class="inner">
                <div class="top">
                    <a href="#" class="footer_logo"><span class="blind">인사혁신처</span></a>
                    <ul>
                        <li class="fb">
                            <a href="#"><span class="blind">facebook</span></a>
                        </li>
                        <li class="tw">
                            <a href="#"><span class="blind">twitter</span></a>
                        </li>
                        <li class="ig">
                            <a href="#"><span class="blind">instagram</span></a>
                        </li>
                        <li class="yt">
                            <a href="#"><span class="blind">youtube</span></a>
                        </li>
                    </ul>
                </div>
                <div class="bottom">
                    <ul>
                        <li><a href="#">개인정보 처리방침</a></li>
                        <li><a href="#">영상정보처리기 운영ㆍ관리방침</a></li>
                        <li><a href="#">저작권보호 및 이용정책</a></li>
                    </ul>
                    <p>30102 세종특별자치시 한누리대로 499(어진동) 세종포스트빌딩 6층~12층</p>
                    <p>copyright© Ministry of Personnel Management. All Rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
    <script src="/static/js/common.js"></script>
    <script>
        var wideSlider01 = new Swiper('.slider01 .wide_slider', {
            loop: false,
            slideActiveClass: 'active',
            navigation: {
                nextEl: '.slider01 .next',
                prevEl: '.slider01 .prev',
            },
            observer: true,
            observeParents: true,
            pagination: {
                el: '.slider01 .dots',
                type: 'bullets',
            },
        })

        $('.box_tab ul li button,.cont_tab ul li button,.class_tab ul li button').click(function () {
            const sibling = $(this).parent().siblings()
            const target = $(this).attr('data-tab')
            sibling.each(function () {
                $(this).children().removeClass('selected')
            })
            $(this).addClass('selected')
            const wrapper = $(this).closest('div').children('.tab_cont,.inner_tab_cont').children().children()
            wrapper.each(function () {
                $(this).removeClass('open')
            })
            $('#' + target).addClass('open')
        })

        $('.qna_wrap ul li em').click(function () {
            if ($(this).parent().hasClass('selected')) {
                $('.qna_wrap ul li').removeClass('selected');
            } else {
                $('.qna_wrap ul li').removeClass('selected');
                $(this).parent().addClass('selected')
            }
        })





        function playthis(playurl){
                    var play_video = '<iframe width="778" height="447.6" src="https://www.youtube.com/embed/'+playurl+'?rel=0&amp; autoplay=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                        $($('.video')[2]).html(play_video)
                    // location.href="#playhere";
                }

        document.addEventListener('DOMContentLoaded', function(){

            var play_video = '<iframe width="1280" height="720" src="https://www.youtube.com/embed/TPbsMZpoPeA?rel=0&amp; autoplay=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                $($('.video')[0]).html(play_video)


            var play_video = '<iframe width="1280" height="720" src="https://www.youtube.com/embed/gFQZNPH6f5g?rel=0&amp; autoplay=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                $($('.video')[1]).html(play_video)

            var play_video = '<iframe width="778" height="447.6" src="https://www.youtube.com/embed/0l8zMuOyOgQ?rel=0&amp; autoplay=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                $($('.video')[2]).html(play_video)



        });




    </script>
</body>

</html>