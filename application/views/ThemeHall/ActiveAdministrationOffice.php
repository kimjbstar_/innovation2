<!DOCTYPE html>
<html lang="ko">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>적극행정관</title>
    <meta name="description" content="적극행정관" />
    <meta name="keywords" content="적극행정관" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="" />
    <meta property="og:title" content="적극행정관" />
    <meta property="og:description" content="적극행정관" />
    <link rel="stylesheet" href="/static/css/style.css">
    <!--<link rel="stylesheet" href="/static/css/mobile.css">-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <script type="text/javascript" src="https://unpkg.com/swiper@5.4.5/js/swiper.min.js"></script>
</head>

<body>
    <div class="container">
        <div id="header">
            <div class="top">
                <div class="inner">
                    <a href="#" class="upper"><span class="blind">인사혁신처</span></a>
                    <a href="#" class="btn_login">회원가입ㆍ로그인</a>
                </div>
            </div>
            <div class="bottom">
                <div class="inner">
                    <a href="#" class="logo"><span class="blind">2020 온라인 공직박람회</span></a>
                    <ul>
                        <li>
                            <a href="#">개막관</a>
                        </li>
                        <li>
                            <a href="#">주제관</a>
                        </li>
                        <li>
                            <a href="#">채용정보관</a>
                        </li>
                        <li>
                            <a href="#">참여 프로그램</a>
                        </li>
                        <li>
                            <a href="#">소통ㆍ이벤트</a>
                        </li>
                        <li>
                            <a href="#">인재혁신세미나</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="positive_wrap">
                <div class="section" style="background-image: url(/static/img/positive/1-bg01.png);">
                    <h2>적극행정관</h2>
                    <div class="video_clip large">
                        <div class="video"></div>
                    </div>
                </div>
                <div class="section" style="background-image: url(/static/img/positive/2-bg01.png); margin-bottom: -2px;">
                    <h2 class="dark"><span>적극행정제도</span></h2>
                    <div class="cont_wrap">
                        <a href="http://designcrepas.com/out/in5/2020_proactive2/index.html" target="_blank"><img src="/static/img/positive/2-cont01.png" alt=""></a>
                    </div>
                </div>
                <div class="section" style="background-image: url(/static/img/positive/3-bg01.png);">
                    <h2 class="bg" style="background-image: url(/static/img/positive/3-cont01.png);"><span>적극행정 우수사례</span>
                    </h2>
                    <div class="cont_wrap">
                        <div class="frame">
                            <h3 class="s_tit blue">카드뉴스 중앙부처편</h3>
                            <div class="slider_wrap slider01">
                                <div class="nav">
                                    <button class="prev"><span class="blind">이전</span></button>
                                    <button class="next"><span class="blind">다음</span></button>
                                </div>
                                <div class="wide_slider ">
                                    <ul class="swiper-wrapper">
                                        <li class="swiper-slide"><img src="/static/img/positive/slider01-1.jpg" alt=""></li>
                                        <li class="swiper-slide"><img src="/static/img/positive/slider01-2.jpg" alt=""></li>
                                        <li class="swiper-slide"><img src="/static/img/positive/slider01-3.jpg" alt=""></li>
                                        <li class="swiper-slide"><img src="/static/img/positive/slider01-4.jpg" alt=""></li>
                                        <li class="swiper-slide"><img src="/static/img/positive/slider01-5.jpg" alt=""></li>
                                        <li class="swiper-slide"><img src="/static/img/positive/slider01-6.jpg" alt=""></li>
                                        <li class="swiper-slide"><img src="/static/img/positive/slider01-7.jpg" alt=""></li>
                                        <li class="swiper-slide"><img src="/static/img/positive/slider01-8.jpg" alt=""></li>
                                        <li class="swiper-slide"><img src="/static/img/positive/slider01-9.jpg" alt=""></li>
                                    </ul>
                                </div>
                                <div class="dots"></div>
                            </div>
                            <h3 class="s_tit yellow top_p">카드뉴스 지자체편</h3>
                            <div class="slider_wrap slider02">
                                <div class="nav">
                                    <button class="prev"><span class="blind">이전</span></button>
                                    <button class="next"><span class="blind">다음</span></button>
                                </div>
                                <div class="wide_slider ">
                                    <ul class="swiper-wrapper">
                                        <li class="swiper-slide"><img src="/static/img/positive/cn1.jpg" alt=""></li>
                                        <li class="swiper-slide"><img src="/static/img/positive/cn2.jpg" alt=""></li>
                                        <li class="swiper-slide"><img src="/static/img/positive/cn3.jpg" alt=""></li>
                                        <li class="swiper-slide"><img src="/static/img/positive/cn4.jpg" alt=""></li>
                                        <li class="swiper-slide"><img src="/static/img/positive/cn5.jpg" alt=""></li>
                                        <li class="swiper-slide"><img src="/static/img/positive/cn6.jpg" alt=""></li>
                                        <li class="swiper-slide"><img src="/static/img/positive/cn7.jpg" alt=""></li>
                                        <li class="swiper-slide"><img src="/static/img/positive/cn8.jpg" alt=""></li>
                                        <li class="swiper-slide"><img src="/static/img/positive/cn9.jpg" alt=""></li>
                                    </ul>
                                </div>
                                <div class="dots"></div>
                            </div>
                        </div>
                        <div class="option_btn">
                            <a href="http://www.mpm.go.kr/proactivePublicService/recommand/intro/">적극행정 ON</a>
                        </div>
                    </div>
                </div>

                <div class="section" style="background-image: url(/static/img/positive/2-bg00.png);">
                    <div class="cont_wrap">
                        <div class="horz_wrap yellow">
                            <img src="/static/img/positive/4-cont01.png" alt="" />
                            <img src="/static/img/positive/4-cont02.png" alt="" />
                        </div>
                        <div class="quiz yellow">
                            <a href="https://docs.google.com/forms/d/e/1FAIpQLSeuB-vJP2P4Kkf2N6Zj_J_wG-FTwKIt_DS4GjxntqWSusokxg/viewform"
                                target="_blank" class="btn_quiz">퀴즈 풀기</a>
                        </div>
                        <img src="/static/img/positive/4-cont03.png" alt="" class="pc" />
                        <img src="/static/img/mobile/positive/4-cont03.png" alt="" class="mobile" />
                    </div>
                </div>
            </div>
        </div>
        <div id="footer">
            <div class="inner">
                <div class="top">
                    <a href="#" class="footer_logo"><span class="blind">인사혁신처</span></a>
                    <ul>
                        <li class="fb">
                            <a href="#"><span class="blind">facebook</span></a>
                        </li>
                        <li class="tw">
                            <a href="#"><span class="blind">twitter</span></a>
                        </li>
                        <li class="ig">
                            <a href="#"><span class="blind">instagram</span></a>
                        </li>
                        <li class="yt">
                            <a href="#"><span class="blind">youtube</span></a>
                        </li>
                    </ul>
                </div>
                <div class="bottom">
                    <ul>
                        <li><a href="#">개인정보 처리방침</a></li>
                        <li><a href="#">영상정보처리기 운영ㆍ관리방침</a></li>
                        <li><a href="#">저작권보호 및 이용정책</a></li>
                    </ul>
                    <p>30102 세종특별자치시 한누리대로 499(어진동) 세종포스트빌딩 6층~12층</p>
                    <p>copyright© Ministry of Personnel Management. All Rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
    <script src="/static/js/common.js"></script>
    <script>
        var wideSlider01 = new Swiper('.slider01 .wide_slider', {
            loop: false,
            slideActiveClass: 'active',
            navigation: {
                nextEl: '.slider01 .next',
                prevEl: '.slider01 .prev',
            },
            observer: true,
            observeParents: true,
            pagination: {
                el: '.slider01 .dots',
                type: 'bullets',
            },
        })

        var wideSlider02 = new Swiper('.slider02 .wide_slider', {
            loop: false,
            slideActiveClass: 'active',
            navigation: {
                nextEl: '.slider02 .next',
                prevEl: '.slider02 .prev',
            },
            observer: true,
            observeParents: true,
            pagination: {
                el: '.slider02 .dots',
                type: 'bullets',
            },
        })




        document.addEventListener('DOMContentLoaded', function(){

            var play_video = '<iframe width="1280" height="720" src="https://www.youtube.com/embed/9WcHr5FPTEQ?rel=0&amp; autoplay=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                $($('.video')[0]).html(play_video)




        });










    </script>
</body>

</html>