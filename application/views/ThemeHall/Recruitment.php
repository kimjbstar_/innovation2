<!DOCTYPE html>
<html lang="ko">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>공개경력채용관</title>
    <meta name="description" content="공개경력채용관" />
    <meta name="keywords" content="공개경력채용관" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="" />
    <meta property="og:title" content="공개경력채용관" />
    <meta property="og:description" content="공개경력채용관" />
    <link rel="stylesheet" href="/static/css/style.css">
    <!--<link rel="stylesheet" href="/static/css/mobile.css">-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <script type="text/javascript" src="https://unpkg.com/swiper@5.4.5/js/swiper.min.js"></script>
</head>

<body>
    <div class="container">
        <div id="header">
            <div class="top">
                <div class="inner">
                    <a href="#" class="upper"><span class="blind">인사혁신처</span></a>
                    <a href="#" class="btn_login">회원가입ㆍ로그인</a>
                </div>
            </div>
            <div class="bottom">
                <div class="inner">
                    <a href="#" class="logo"><span class="blind">2020 온라인 공직박람회</span></a>
                    <ul>
                        <li>
                            <a href="#">개막관</a>
                        </li>
                        <li>
                            <a href="#">주제관</a>
                        </li>
                        <li>
                            <a href="#">채용정보관</a>
                        </li>
                        <li>
                            <a href="#">참여 프로그램</a>
                        </li>
                        <li>
                            <a href="#">소통ㆍ이벤트</a>
                        </li>
                        <li>
                            <a href="#">인재혁신세미나</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="recruit_wrap">
                <div class="section"
                    style="background-image: url(/static/img/recruitment/bg01.png); background-color: #5DB47A;">
                    <h2>공개경력채용관</h2>
                    <div class="sec1_cont2">
                        <img src="/static/img/recruitment/1-cont02.png" alt="" class="pc" />
                        <img src="/static/img/recruitment/1-cont01.png" alt="" class="pc" />
                    </div>
                </div>
                <div class="section">
                    <h2 class="dark m_small"><span>인사혁신처 주관</span>
                        <br />2020년 국가공무원 <span>공채ㆍ경채 시험</span> 일정</h2>
                    <div class="cont_wrap">
                        <img src="/static/img/recruitment/2-cont01.png" alt="" class="pc" />
                    </div>
                    <div class="cont_wrap">
                        <img src="/static/img/recruitment/re_01.png" alt="" class="pc" />
                    </div>
                </div>
                <div class="section" style="background-image: url(/static/img/recruitment/3-bg01.png);">
                    <h2><span>공개경쟁채용제도</span></h2>
                    <div class="video_clip large b_p_100">
                        <div class="video"></div>
                    </div>
                    <div class="cont_wrap">
                        <div class="box_tab yellow" id="tab_01">
                            <ul>
                                <li>
                                    <button class="selected" data-tab="tab_01_tab1">국가공무원 5급 공개경쟁채용시험</button>
                                </li>
                                <li>
                                    <button data-tab="tab_01_tab2">국가공무원 7·9급 공개경쟁채용시험</button>
                                </li>
                            </ul>
                            <div class="tab_cont">
                                <div class="padding">
                                    <div class="open" id="tab_01_tab1">
                                        <img src="/static/img/recruitment/3-cont07.png" alt="" class="pc" />
                                    </div>
                                    <div id="tab_01_tab2">
                                        <img src="/static/img/recruitment/3-cont06.png" alt="" class="pc" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img style="margin-top: 100px;" src="/static/img/recruitment/3-cont01.png" alt="" class="pc" />
                    </div>
                </div>
                <div class="section" style="background-color: #FFE382;">
                    <h2 class="dark"><span>경력경쟁채용제도</span></h2>
                    <div class="cont_wrap">
                        <img style="margin-top: 100px;" src="/static/img/recruitment/4-cont01.png" alt="" class="pc" />
                    </div>
                    <div class="video_clip large">
                        <div class="video"></div>
                    </div>
                    <div class="cont_wrap t_p_300 pc">
                        <div class="box_tab blue col4" id="tab_02">
                            <ul>
                                <li>
                                    <button class="selected" data-tab="tab_02_tab1">민경채 도입 취지 및 소개</button>
                                </li>
                                <li>
                                    <button data-tab="tab_02_tab2">경채와 공채의 다른 점</button>
                                </li>
                                <li>
                                    <button data-tab="tab_02_tab3">합격자 통계</button>
                                </li>
                                <li>
                                    <button data-tab="tab_02_tab4">합격자 사례 소개</button>
                                </li>
                            </ul>
                            <div class="tab_cont">
                                <div>
                                    <div class="open" id="tab_02_tab1">
                                        <img src="/static/img/recruitment/tab_cont04.png" alt="">
                                    </div>
                                    <div id="tab_02_tab2">
                                        <img src="/static/img/recruitment/tab_cont03.png" alt="">
                                    </div>
                                    <div id="tab_02_tab3">
                                        <img src="/static/img/recruitment/tab_cont01.png" alt="">
                                    </div>
                                    <div id="tab_02_tab4">
                                        <img src="/static/img/recruitment/tab_cont02.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section" style="background-image: url(/static/img/recruitment/1-bg01.png);">
                    <h2 class="dark m_mid m_t_p_30"><span>시험 중 이런 실수하면 안되겠죠?</span></h2>
                    <div class="cont_wrap">
                        <div class="box_tab yellow col3" id="tab_03">
                            <ul>
                                <li>
                                    <button onclick="playthis(2,'Hl_Wj_213Zc')" class="selected" data-tab="tab_03_tab1">나는 네가 공고문을<br />확인하지 않은 것을 알고
                                        있다</button>
                                </li>
                                <li>
                                    <button onclick="playthis(2,'Y8N2XmQUh7Q')"  data-tab="tab_03_tab2">수험생 고민상담소<br />파란라이트를 켜줘</button>
                                </li>
                                <li>
                                    <button onclick="playthis(2,'tY1U3BKgpT0')"  data-tab="tab_03_tab3">부정행위,<br />악몽의 시나리오</button>
                                </li>
                            </ul>
                            <div class="tab_cont video m_t_p_30">
                                <div>
                                    <div class="open m_n_p" id="tab_03_tab1">
                                        <h4 class="m_tit yellow">나는 네가 공고문을<br />확인하지 않은 것을 알고
                                            있다</h4>
                                        <div class="video_clip large">
                                            <div class="video"></div>
                                        </div>
                                    </div>
                                    <div class="m_n_p" id="tab_03_tab2">
                                        <h4 class="m_tit yellow">수험생 고민상담소<br />파란라이트를 켜줘</h4>
                                        <div class="video_clip large">
                                            <div class="video"></div>
                                        </div>
                                    </div>
                                    <div class="m_n_p" id="tab_03_tab3">
                                        <h4 class="m_tit yellow">부정행위,<br />악몽의 시나리오</h4>
                                        <div class="video_clip large">
                                            <div class="video"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <span class="sep"></span>
                        <img src="/static/img/recruitment/5-cont03.png" alt="" class="pc" />
                        <div class="relative_box">
                            <img src="/static/img/recruitment/5-cont01.png" alt="" class="pc" />
                            <img src="/static/img/recruitment/Group366.png" alt="" class="abslt pc"
                                style="bottom: -30px; right: -26%;" />
                        </div>
                        <div class="relative_box">
                            <img src="/static/img/recruitment/5-cont02.png" alt="" class="pc" />
                            <img src="/static/img/recruitment/Frame.png" alt="" class="abslt pc"
                                style="bottom: -5px; right: -26%;" />
                        </div>
                    </div>
                </div>
                <div class="section" style="background-image: url(/static/img/recruitment/6-bg01.png);">
                    <div class="cont_wrap">
                        <div class="horz_wrap green">
                            <img src="/static/img/recruitment/6-cont01.png" alt="" />
                            <img src="/static/img/recruitment/6-cont02.png" alt="" />
                        </div>
                        <div class="quiz green">
                            <a href="https://docs.google.com/forms/d/e/1FAIpQLSeuB-vJP2P4Kkf2N6Zj_J_wG-FTwKIt_DS4GjxntqWSusokxg/viewform"
                                target="_blank" class="btn_quiz">퀴즈 풀기</a>
                        </div>
                        <img src="/static/img/recruitment/6-cont03.png" alt="" />
                    </div>
                </div>
            </div>
        </div>
        <div id="footer">
            <div class="inner">
                <div class="top">
                    <a href="#" class="footer_logo"><span class="blind">인사혁신처</span></a>
                    <ul>
                        <li class="fb">
                            <a href="#"><span class="blind">facebook</span></a>
                        </li>
                        <li class="tw">
                            <a href="#"><span class="blind">twitter</span></a>
                        </li>
                        <li class="ig">
                            <a href="#"><span class="blind">instagram</span></a>
                        </li>
                        <li class="yt">
                            <a href="#"><span class="blind">youtube</span></a>
                        </li>
                    </ul>
                </div>
                <div class="bottom">
                    <ul>
                        <li><a href="#">개인정보 처리방침</a></li>
                        <li><a href="#">영상정보처리기 운영ㆍ관리방침</a></li>
                        <li><a href="#">저작권보호 및 이용정책</a></li>
                    </ul>
                    <p>30102 세종특별자치시 한누리대로 499(어진동) 세종포스트빌딩 6층~12층</p>
                    <p>copyright© Ministry of Personnel Management. All Rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
    <script src="/static/js/common.js"></script>
    <script>
        var innerSlider01 = new Swiper('.slider01', {
            loop: false,
            slideActiveClass: 'active',
            navigation: {
                nextEl: '.slider01 .next',
                prevEl: '.slider01 .prev',
            },
            observer: true,
            observeParents: true,
        })

        var innerSlider02 = new Swiper('.slider02', {
            loop: false,
            slideActiveClass: 'active',
            navigation: {
                nextEl: '.slider02 .next',
                prevEl: '.slider02 .prev',
            },
            observer: true,
            observeParents: true,
        })

        $('.box_tab ul li button').click(function () {
            const sibling = $(this).parent().siblings()
            const target = $(this).attr('data-tab')
            sibling.each(function () {
                $(this).children().removeClass('selected')
            })
            $(this).addClass('selected')
            const wrapper = $(this).closest('div').children('.tab_cont').children().children()
            wrapper.each(function () {
                $(this).removeClass('open')
            })
            $('#' + target).addClass('open')
        })






        function playthis(which,playurl){
                    var play_video = '<iframe width="1280" height="720" src="https://www.youtube.com/embed/'+playurl+'?rel=0&amp; autoplay=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                        $($('.video')[which]).html(play_video)
                    // location.href="#playhere";
                }

        document.addEventListener('DOMContentLoaded', function(){

            var play_video = '<iframe width="1280" height="720" src="https://www.youtube.com/embed/Kaxb3ta30L4?rel=0&amp; autoplay=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                $($('.video')[0]).html(play_video)


            var play_video = '<iframe width="1280" height="720" src="https://www.youtube.com/embed/G_gS4XcqZMk?rel=0&amp; autoplay=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                $($('.video')[1]).html(play_video)

            var play_video = '<iframe width="1280" height="720" src="https://www.youtube.com/embed/Hl_Wj_213Zc?rel=0&amp; autoplay=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                $($('.video')[2]).html(play_video)



        });






    </script>
</body>

</html>