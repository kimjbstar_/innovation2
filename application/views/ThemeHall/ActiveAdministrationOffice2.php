<html lang="en"><head><meta charset="utf-8"><link rel="icon" href="/favicon.ico"><meta name="viewport" content="width=device-width,initial-scale=1"><meta name="theme-color" content="#000000"><meta name="description" content="Web site created using create-react-app"><link rel="apple-touch-icon" href="/logo192.png"><link rel="manifest" href="/manifest.json"><title>React App</title><link href="/static/css/main.8003005e.chunk.css" rel="stylesheet"></head><body><div id="root"><div class="container"><div class="section_one"><h1 class="title_one">적극행정관</h1><video class="video_one"></video></div><div class="section_two"><h1 class="title_two">적극행정 제도</h1><div class="sectionTwoSliderWrapper"><a href="http://designcrepas.com/out/in5/2020_proactive2/index.html" target="_blank"><div class="sectionTwoSlider"></div></a></div></div><div class="section_three"><div class="SectionThreeTitle"><div>적극행정 우수사례</div></div><div class="SectionThreeFrame"><div class="SectionThreeSlider"><div class="SectionThreeSliderTitle1"></div><div class="InnerSlider"><div class="InnerLeftBtn"></div><div class="InnerContent"><img src="/images/카드뉴스_중앙부처1_(순위없음).jpg" alt="content1"></div><div class="InnerRightBtn"></div></div><div class="idxButtons"><div class="activeButton"></div><div class="inactiveButton"></div><div class="inactiveButton"></div><div class="inactiveButton"></div><div class="inactiveButton"></div><div class="inactiveButton"></div><div class="inactiveButton"></div><div class="inactiveButton"></div><div class="inactiveButton"></div></div></div><div class="SectionThreeSlider"><div class="SectionThreeSliderTitle2"></div><div class="InnerSlider"><div class="InnerLeftBtn"></div><div class="InnerContent"><img src="/images/카드뉴스_지자체3_(순위없음).jpg" alt="content3"></div><div class="InnerRightBtn"></div></div><div class="idxButtons"><div class="inactiveButton"></div><div class="inactiveButton"></div><div class="activeButton2"></div><div class="inactiveButton"></div><div class="inactiveButton"></div><div class="inactiveButton"></div><div class="inactiveButton"></div><div class="inactiveButton"></div><div class="inactiveButton"></div></div></div></div><a href="http://www.mpm.go.kr/proactivePublicService/recommand/intro/" target="_blank"><div class="OnButton">적극행정 ON</div></a></div><article><div class="rectangle"><div></div></div><a href="https://forms.gle/18zhUAVZFvfuij7Q9" target="_blank"><div class="SolveBtn">퀴즈 풀기</div></a><div class="SolveExpUpper">하나, 오른쪽의 “퀴즈풀기” 버튼을 눌러 링크를 연다.<br>둘, 주제관 카테고리 내에 7개 관의 하단에 있는 OX퀴즈를 푼다!<br>셋, 링크 내에서 문제를 풀고 제출하면 이벤트 응모 완료!</div><div class="SolveExpBottom">* 퀴즈풀기 버튼은 모든 주제관이 동일하니 한번접속 후 띄워놔 주세요.<br>띄워놓은 퀴즈풀기 링크에서 7개 문제를 모두 푸시고 제출해주세요!</div></article></div></div><script>!function(e){function r(r){for(var n,a,i=r[0],c=r[1],l=r[2],f=0,s=[];f<i.length;f++)a=i[f],Object.prototype.hasOwnProperty.call(o,a)&&o[a]&&s.push(o[a][0]),o[a]=0;for(n in c)Object.prototype.hasOwnProperty.call(c,n)&&(e[n]=c[n]);for(p&&p(r);s.length;)s.shift()();return u.push.apply(u,l||[]),t()}function t(){for(var e,r=0;r<u.length;r++){for(var t=u[r],n=!0,i=1;i<t.length;i++){var c=t[i];0!==o[c]&&(n=!1)}n&&(u.splice(r--,1),e=a(a.s=t[0]))}return e}var n={},o={1:0},u=[];function a(r){if(n[r])return n[r].exports;var t=n[r]={i:r,l:!1,exports:{}};return e[r].call(t.exports,t,t.exports,a),t.l=!0,t.exports}a.e=function(e){var r=[],t=o[e];if(0!==t)if(t)r.push(t[2]);else{var n=new Promise((function(r,n){t=o[e]=[r,n]}));r.push(t[2]=n);var u,i=document.createElement("script");i.charset="utf-8",i.timeout=120,a.nc&&i.setAttribute("nonce",a.nc),i.src=function(e){return a.p+"static/js/"+({}[e]||e)+"."+{3:"6b042e2d"}[e]+".chunk.js"}(e);var c=new Error;u=function(r){i.onerror=i.onload=null,clearTimeout(l);var t=o[e];if(0!==t){if(t){var n=r&&("load"===r.type?"missing":r.type),u=r&&r.target&&r.target.src;c.message="Loading chunk "+e+" failed.\n("+n+": "+u+")",c.name="ChunkLoadError",c.type=n,c.request=u,t[1](c)}o[e]=void 0}};var l=setTimeout((function(){u({type:"timeout",target:i})}),12e4);i.onerror=i.onload=u,document.head.appendChild(i)}return Promise.all(r)},a.m=e,a.c=n,a.d=function(e,r,t){a.o(e,r)||Object.defineProperty(e,r,{enumerable:!0,get:t})},a.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},a.t=function(e,r){if(1&r&&(e=a(e)),8&r)return e;if(4&r&&"object"==typeof e&&e&&e.__esModule)return e;var t=Object.create(null);if(a.r(t),Object.defineProperty(t,"default",{enumerable:!0,value:e}),2&r&&"string"!=typeof e)for(var n in e)a.d(t,n,function(r){return e[r]}.bind(null,n));return t},a.n=function(e){var r=e&&e.__esModule?function(){return e.default}:function(){return e};return a.d(r,"a",r),r},a.o=function(e,r){return Object.prototype.hasOwnProperty.call(e,r)},a.p="/",a.oe=function(e){throw console.error(e),e};var i=this.webpackJsonpapp=this.webpackJsonpapp||[],c=i.push.bind(i);i.push=r,i=i.slice();for(var l=0;l<i.length;l++)r(i[l]);var p=c;t()}([])</script><script src="/static/js/2.8d614659.chunk.js"></script><script src="/static/js/main.2f46fc6c.chunk.js"></script></body></html>










<!-- 


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
document.addEventListener('DOMContentLoaded', function(){







    var imagemap = '<div class="nav"><div class="nav_top"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHoAAAAqCAYAAAB8108TAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAA+RSURBVHgB7VwJWFTX2X6HWdmRVQUsKCoKAiooGrdoJJrffY1/E2uNRhu3+EebPs2f2mgSo5ImxhixaaLEVnFNtGpEVBCNC7gCIgoCCsgqO8Mwa79zx0EGL8NgIE9b5/UZkTPnnHvueb/93KtAR4AF/+3QWcGC5wIWop8TWIh+TiBCO4O5/AalmvspkYggtLLI0r8DfjbRdfIG3HlQjAtX7yI9qwB5pVUora4jorXo5GALbxdH9PJyw7DwPvD36cy1WfDLQ/AsUTcbkPuwDHuOXMCJS+lIvZOHqtp6cgQC6ERCEh+SH4EA0GgANX1Iq21lEvTs7IzhA/0wf8aLCOrpSd0FsOAXga7NRDNCt++NR9SeU3hQUQstkcuI1FkJ4azVILimGsOrK9BTIYe7UgkRTS8XCpErkyHV2g6XrO1R7toJsye9gOWvj0U3D2dY0OFoG9H5JRVYujYaxy+kQy00EGwF34YGLCzKx+RHpfBQKaHV6bWe1LpxLPuXiFrV5LKTbB3wD2d35A0IxJefLEJwLy9Y0KEwn+ikWzl4bdVXyCqpIsaEXJsNmd6V+bmYXVaMzg1KqOiPFmTC6W+A9SGzTYG9ADLuY4CQCNcQ85nWUmzw74e5nyzB7LEDYUGHwTyiM3IKETpjDeSPCWawp2Exd1IxmMy0QqCAMKgHrMePgzgkACJfT8CaiKWgTJ1XCNXNDNQfOw71rXsQaK2JdH0MyLRcKtDgjx7eGBq9Dq9HhMKCDkHrROcWlGHq0s24ScGXAW4UZH2TdQuDpSoIRwyA3duLIBkaog/AWoJWC+W125Dv3I36Xcegq1YT0XrBkZLm7+zhh+E/fIawQF9Y0O4wTbSiQY05q7biyOV06qnPh6XUPTorDaPttbDf/gmkEcM4X90WqJJTUPXOWijP36KwX6JvIxk5PDwM077/DJ7O9rCgXWG61r33x0uITb7TSDLDxKpHGOMhg1PsLkjHjWgzyQzisCC4HN0Jm4XTaG6Vvo3EbeLFK7gWtReWU5b2R4ss1dQpsHHnCSi0T7ZdQtq8HDVwOBgFkX8P/BwIHOzguHkNrOdNYLE41yZT6eDzTQyKb9+DBe2LFolOSM7A7fzSphkSwmurELpoOsR9/dAeEFARxWnrWrIMg4hsLafJLjklEOw/wWqpZs9TXV2NvAcPUPjwIVQqldF3Ssrl40+fQWpqKu9YhUKB5t6rgdLF+NOnUVJSwjsmMeEsNq7/BMVFRU999+D+fZyhsVVVVbxj8/PzedvvZGTgbEICd+3muJ2ejgP79vFe78b169y41sBbAtXSjR8+e4PyoCdywLZitFAFybyZaE8IKDp32v4RSsNmQFdSRz6bQrSEJOhWv8F91xrOJyYiZk8MigofQiqVIXzoEEydPh1eXvrcvKSoGFu/+AIRlBH069fPaKxarcbemBjMmDkTtrZPSrP5eXnY8vlmvPPu7+Hu7s61XfjpJ6Sl6IUlMzMTOdnZkMqklFxYw87BHhMnTYK9vT3OnTuHAzF7se1vX8PR0dHoemxeJiDvrfkTPDw8jL47HXeKxiYiKCiI7kOKmpoaqEhIGZIuX8YPh75HL39/iMVirs2G1sv67fnHblSTUI0cNQqmwEt0QXEFLt9sZj4pog4dFAgrZye0N4TdusLuvTdRvSKSi8RVF29AnZkLcZC/yXEJZ85g21fb0D8kBLPnvIriwiIcOXIEuTk52BAZSZVYEQRUuRNLJBAKnzZeF3+6gONH/okhJBy9evXGk1sVQCKVUPjxZExubi7u3r1LBzUSiMQi+HbvjtLSUigVDbB3cEBxcTHq6+uhonoCu15zMDKitkVx/b7bsRMrV73Dra9xD6h6KBFLuH2Wy+X4YM0a3M24w31nbW0NGxsbrHnvfaoq04ERudO3li/FuPHjuTkM5JsCL9G5BaXILi5/qt2mx6/QUbCeNg6167ZDVyaHTqGB6ka6SaLramsR++MJ9CEpX7DoTbi6uVEGp4UdadWu6GjcvHEDA0NbzstZ3/OkQSLapKvJV42I5sPMWbMwjSzF5UuXcOSHwyh/9AiBpH1Tp01FV09PRG7ahKtJyZwg8OH0qVPIvncPPfz8kJyUxM3zwrBhvH1lVC6eOXs2dw2GW6lpuHb1Kub8+n9JqPQZSUBgINoCXh+dV1yJeqXauJGkqKiTI9qKGrkCp5Ju4/O/xyHyu1icuJiGyhr5U/2EXdwgi3iB+7eAlqVOyTI5r6JBgYdkrt3IBLq4unJtTAMD+gVyG1VaWmZyPPOlGbczSEM0SIg/g9bqRkxr0sjPf/XlVhImO7wycQIJ03V8SpaD+XkmCEuWL0P/0KcrfFWVVTh+9Bh8fX2wirkDD3fs2hmN8ooK3mux+xg8eDDGv/IKRo8Z0+i3lcoGvDxuHNferVs3tAW8RGfmlzxV/GC//lRWi7bgSMIN+I9/F2PnrsfKv+zD6s0HMf6NTej58ipsP3CWfKTmSWcyXbLpY6EvmwqgLSw3GZDZ2drBy9ML98lMNw18rl65QkencnTp0sXk2rjAh3zgxCmTUVtbh7iTJ2F8vwIucLqVdouIquTa4s/Ew9nZGR+sW8cRu3zlSjwigbp5nVwNBYHM/PKdyG35YjNUFA8sXLyYGz9/wQIugDxIARazLKbA7ic1JQWd6X7OnU1EJa2FBZhlZWXchwmqOeAluqa+gbfKdfBcCuQKJczB0cSbmLFiCwqpv8DRFgLyawIqoQrsbfBIo8Pv6HDkw22HjcaIB5I5EumvqyPfB03LmyAlrZ06YzqyKSha+6c1+PHYMWwnH7h719/h7e2N4JDgFsfW1dWRNt9G7969MX3GDAq43HA2Pt6oDyMt+tsdeHvpMlyhzWawpuCLuQwD8Q8LCrh+UvLnzJxHbtiIpEuXjeY5l3gWqTdTMDYiAj4+PlxbSP/+GDFqJOJiT5KQXG9xnUwIWITPLMBv5s+DnNZ9MjZWb8ZnzsLrr87hgkJzwEu0mNW0eZSpqLIW727cQ5poWgqT03Iw5/dRUMskLXeiA42Po2Op75OFCijoEHrrNVHAnWnDJJgPZhGshAiI3rEDV5KTMe6V8fh/ajMFpiH3c+9jzNiXODM/aHA4Mu9mkvamNfZhmvLrua9j3fqPEBCg94dTpk3nAqNFCxbirUWLseObbzk/PYDWsfoP7+Lg4R8weeoUoxTPliyPb4/umEZCaQAzzXPnzcPLFEx1cnFpcZ0sXWTuYsTIkRSNB6N3H39OIDt37oz1Gzfgow3rG7OL1sAbjHV2Yg6fMd1sp0nLvzt5BT27d8X8acNhZ2Oc/mjJj6dk5mP2yq2oMyMNVlMk/NmOE9j96Vv6Bqb1DjbctZnmM3PeGvr27YsPP/6YM4UsELKzs2u2Ji238VqNfkHMn7Jo3cXZhRvLMGzEcHx/8CD56vjGIIf5bKaB4UOGNM7l6eWJNWs/4AKrRxQoMfM9eEh4Y9TLtJulOWzz7R+vY8DAgejTpw9HLptT8NhSsij6jYULGudmgsXWaYgV2E+W/7OIngk0GxdBgvF55KeckI58cRTX59CBQ0/VDvjAS3Tvbh7cIQTfRtcoVVj92X6cunQLS1+LQF/frrCiboV0fPn96auIPpSIAqqqwcynR+KuZZKF1j55tkyjL5wIu3dpdWwJpSpfbtmC/5kwwYiQpnB396BUaxOcnPRpYR7lstnZOVxKZQjivImYgIAALn1i+WtLYNp14cIFCKkkzAQqKysTd+5kGPVhZLM6xKDw8Ma2/fv2U1pVhGUrVnAWhA8symbWyJB7V1ZUIpHMfmhoGHx89Qc9LMdmvpoVZJjpbwt4if6VpwvcHe1Qwh4Pag4iUEWm/ejFdBw9nwYZaaWI2mop2ACriUtEZpPMwMZVVMvh6kQaoFJDSzm8wEoH0aCAVsfWk3ZmZ91DmYkImxU1WErDwDTgFAVdcnkdQsPCGrWLrXfC5En4dOMmxMXFISSY379XUtB3PyeXBJHfXLHZKinCZhWsyVOmNBZhigoLUUD+3FTg5UZxAvsYcDL2BKorqzFy9IuN62RCMonm/fbrr5FCfj8oOAjmgpdoH0837kG+EvK1LY8Uch+F4XehCX9sAkK6CRup3vTpqmugKSuHqIcXxP49zRrPNkGr0xqZPT4w06lWqzjT7UnRukFLDAgicj3I90klLd/H0KFDEd5EU/lwgLSXuQHjRerXydaoVJoOZpkbYGvMzMyCVzcv+PkZl5v79w8hgXDHvXtZP59oGW38hGH9kJia3SbtfBYEeLrChgIzhobzyVQCtYJ4QF8Iu7q3OlZHGsJ82/F/HqVS6DnAxLlXdzrvXvzW77Bk2TIuamblyqZgG/zR+o9hTb6zpUiWmWVhK3GDQfCaQkNWq7zsET78YK3J7dRQjPPO6lVcasjWqqV7a349R3JBLBd3c3NDW8BLNFvLrPGDsP7b46hoXjhpR7BS3m+n6KtDOiodNpy8SFImgmzyKLOOP506dcKYiLG0IdoWzakBXbt25X6ygM3NnV+IbB6bWjFF/A7kK5/lmXRXN1cubZNJpY1tIQMGwoWIaa0ow76XSqScsLg+jh/40LRGwARWpzOdBTGYfPBg/vvfIDo2mc6MO0ar3UiTM4+th4OtNTQPClA6eBaZbW+4xO0060CDoS0PsQrMvA82J6s3szqytAlh5oBZGPZpWgrtiDUawMw8u17TQxkemH7w4I8LJ8CbpVod8MKljqpi//faWI5khvrDp6Ctq4b9n5ebTTID2xhzP22Z0/bx6VBbwR1ONPPzHbFGA1iA1grJHEwS7Udp1ud/mANrtDPI1L7U3w9L5ozhftXVylEb+VfYr5wP6UtDYUH7w6ynQKNizmBF5F6orAQ/OzjTkZkZ5OeJg5uXwcvDGTrKyyvmroROpYTznq0QSMSwoN2hM+vdq8WvjoaCCHn/i0OoFVB151lenCN5siJNHtTLG7s2LuJIZppdGxkFbXkZXGL+aiG5A2E2Y2/PfRl7/rIEvd2dIFDpD7/NBUuDbMkSzB7dH9Eb30QPb3fy0WrI9x2FKiMTLvv/BoGzAyzoOLT53asHReWI2n2aK4HeyC6kIoBa/8gRy/cMVp0JATuCJIIdbWUYFuyH31BtfOKoEMhIa3VVNaiL2gcrZxvY/HZW45sfFnQYdM/2NiWNKCqvRhIVVBKI8PT7RSh+WA5lg4oz6/Yu9uhJ1bWwQB9EDA+Gn5cbcWnFDdQUlEB57hrEYYEQ+XnDgl8Ez0Z0c6hIe9lbluxwwurxK7I2fEeUrJZdXUua7NjhFTcLjNA+RFvwbw/L/0r0vMBC9HOCdv/Pav7D8Ly4Ld2/ALHvXVc2tunKAAAAAElFTkSuQmCC"><div><a href="javascript:alert(\'회원가입 준비 중입니다.\');">회원가입</a> ･ <a href="javascript:alert(\'로그인 준비 중입니다.\');">로그인</a></div></div><div class="nav_bottom"><a href="http://publicservicefair.kr/Home"><img src="/static/media/image5.d0ec5abb.png"></a><ul><a href="javascript:alert(\'오픈 준비 중입니다.\');"><li>영상관</li></a><a href="javascript:alert(\'오픈 준비 중입니다.\');"><li>주제관</li></a><a href="javascript:alert(\'오픈 준비 중입니다.\');"><li>채용정보관</li></a><a href="http://publicservicefair.kr/Participate"><li>참여관</li></a><a href="http://publicservicefair.kr/Seminar"><li>세미나관</li></a><a href="javascript:alert(\'오픈 준비 중입니다.\');"><li>소통 ･ 이벤트</li></a></ul></div></div>';




    $('.nav').remove();

    var section_one = $('.section_one').html();
    section_one = '<div class="section_one">'+section_one+'</div>';
    $('.section_one').remove();

    var section_two = $('.section_two').html();
    section_two = '<div class="section_two">'+section_two+'</div>';
    $('.section_two').remove();

    
    var section_three = $('.section_three').html();
    section_three = '<div class="section_three">'+section_three+'</div>';
    $('.section_three').remove();

    
    var article = $('article').html();
    section_three = '<article">'+section_three+'</article>';
    $('article').remove();



    // $('.section6').remove();

    $(".container").html(imagemap);

    console.log(imagemap)

    $(".container").append(section_one);
    $(".container").append(section_two);
    $(".container").append(section_three);
    $(".container").append(article);





});
</script>


<style>

.nav &_bottom  img {
    position: absolute;
    bottom: 12.97%;
    left: 9.53%;
    height: 68px;
  }
    .nav &_bottom ul li {
      margin-left: 52px;
      list-style: none;
      font-family: Noto Sans CJK KR;
      font-style: normal;
      font-weight: 500;
      font-size: 20px;
      line-height: 22px;
      /* or 110% */

      color: #000000;
    }
  .nav &_bottom ul {
    display: flex;
    right: 11.67%;
    position: absolute;
  }
.nav &_bottom {
  display: flex;
  align-items: center;
  position: relative;
  height: 114px;
}
.nav .nav_top div a{
        color:white;
}
  .nav &_top img {
    display: inline-block;
    width: 122px;
    height: 42px;
    right: 22.19%;
    position: absolute;
  }
.nav &_top {
  position: relative;
  display: flex;
  align-items: center;
  border-bottom: 1px solid #d8d8d8;
  height: 70px;
}


.nav &_top div {
    position: absolute;
    right: 11.67%;
    background: #0e0c34;
    border-radius: 236px;
    width: 189px;
    height: 41px;
    font-family: Noto Sans CJK KR;
    font-style: normal;
    font-weight: 500;
    font-size: 20px;
    line-height: 22px;
    /* or 110% */

    display: flex;
    align-items: center;
    text-align: center;
    justify-content: center;
    margin-left: 14px;
    color: #ffffff;
  }

.nav {

-ms-user-select: none; 
-moz-user-select: -moz-none;
-khtml-user-select: none;
-webkit-user-select: none;
user-select: none;



background-color:white;
position: fixed;
z-index: 100000;
top: 0;
width: 1920px;
height: 185px;





}
.nav a{
    
    text-decoration : none; 
}
</style> -->







    