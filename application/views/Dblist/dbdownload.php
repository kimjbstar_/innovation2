<nav class="page-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#"><?=$text['menu_1'];?></a></li>
                    <li class="breadcrumb-item active" aria-current="page"><?=$text['menu_2'];?></li>
                </ol>
            </nav>

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-title"><?=$text['menu_2'];?></h6>
                            <div class="table-responsive">
                                <table id="dataTableExample" class="table">
                                    <thead>
                                        <tr>
                                            <th>그룹명</th>
                                            <th>오늘 추출 수량</th>
                                            <th>전체 추출 수량</th>
                                            <th>다운로드</th>
                                            <th>키워드</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                    foreach ($db as $datum) {
                                        // var_dump($datum);
                                        ?>
                                        <tr  style='background-color:#<?=$datum['dbColor'];?>'>
                                            <td><?=$datum['dbLandName'];?></td>
                                            <td><?=$datum['foundToday'];?></td>
                                            <td><?=$datum['foundTotal'];?></td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">다운로드</button>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="Keywordcontrol/downloadGroupToday?groupnum=<?=$datum['dbIdx'];?>&groupname=<?=$datum['dbLandName'];?>">오늘자</a>
                                                        <a class="dropdown-item" href="Keywordcontrol/downloadGroupYesterday?groupnum=<?=$datum['dbIdx'];?>&groupname=<?=$datum['dbLandName'];?>">어제자</a>
                                                        <a class="dropdown-item" href="Keywordcontrol/downloadGroupSevenDays?groupnum=<?=$datum['dbIdx'];?>&groupname=<?=$datum['dbLandName'];?>">최근 1주일</a>
                                                    </div>
                                                </div>                                            
                                            </td>
                                            <td style="width:20px;overflow:hidden; text-overflow:ellipsis; white-space:nowrap;"><?=$datum['dbHistory'];?></td>
                                        </tr>
                                        <?php
                                    }

                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>