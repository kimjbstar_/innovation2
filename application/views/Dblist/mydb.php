            <nav class="page-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#"><?=$text['menu_1'];?></a></li>
                    <li class="breadcrumb-item active" aria-current="page"><?=$text['menu_2'];?></li>
                </ol>
            </nav>

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-title"><?=$text['menu_2'];?></h6>
                            <div class="table-responsive">
                                <table id="dataTableExample" class="table">
                                    <thead>
                                        <tr>
                                            <th>성함</th>
                                            <th>광고</th>
                                            <th>랜딩</th>
                                            <th>전화번호</th>
                                            <th>유입</th>
                                            <th>최근 상태변화</th>
                                            <th>상태</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                    foreach ($db as $datum) {
                                        // var_dump($datum);
                                        ?>
                                        <tr ondblclick="getDBPopup(<?=$datum['dbIdx'];?>);" style='background-color:#<?=$datum['dbColor'];?>'>
                                            <td><?=$datum['dbName'];?></td>
                                            <td><?=$datum['dbAdvName'];?></td>
                                            <td><?=$datum['dbLandName'];?></td>
                                            <td><?=$datum['dbPhone'];?></td>
                                            <td><?=$datum['dbGet'];?></td>
                                            <td><?=$datum['dbUpdated'];?></td>
                                            <td><?=$datum['dbFlag'];?></td>
                                        </tr>
                                        <?php
                                    }

                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>