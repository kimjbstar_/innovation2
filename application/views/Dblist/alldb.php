<nav class="page-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#"><?=$text['menu_1'];?></a></li>
                    <li class="breadcrumb-item active" aria-current="page"><?=$text['menu_2'];?></li>
                </ol>
            </nav>

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-title"><?=$text['menu_2'];?></h6>
                            <div class="table-responsive">
                                <table id="dataTableExample" class="table">
                                    <thead>
                                        <tr>
                                            <th>성함</th>
                                            <th>디비 종류</th>
                                            <th>전화번호</th>
                                            <th>유입</th>
                                            <th>상태</th>
                                            <th>분배</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ondblclick="getDBPopup(1);" style='background-color:#ccccff;'>
                                            <td>고승일</td>
                                            <td>통합론</td>
                                            <td>010-2534-9557</td>
                                            <td>2020-08-01 10:00</td>
                                            <td>신규</td>
                                            <td style="display:inline-block;">						
                                                <select class="form-control" style="display:inline;">
                                                    <option selected="" disabled="">배분</option>
                                                    <option>김종훈B</option>
                                                    <option>김미혜AZ</option>
                                                    <option>고승일E</option>
                                                </select>
                                                <button class="file-upload-browse btn btn-primary" type="button" style="display:inline;">배치</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Garrett Winters</td>
                                            <td>Accountant</td>
                                            <td>Tokyo</td>
                                            <td>63</td>
                                            <td>$170,750</td>
                                            <td style="display:inline-block;">						
                                                <select class="form-control" style="display:inline;">
                                                    <option selected="" disabled="">배분</option>
                                                    <option>김종훈B</option>
                                                    <option>김미혜AZ</option>
                                                    <option>고승일E</option>
                                                </select>
                                                <button class="file-upload-browse btn btn-primary" type="button" style="display:inline;">배치</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Ashton Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td>San Francisco</td>
                                            <td>66</td>
                                            <td>$86,000</td>
                                            <td style="display:inline-block;">						
                                                <select class="form-control" style="display:inline;">
                                                    <option selected="" disabled="">배분</option>
                                                    <option>김종훈B</option>
                                                    <option>김미혜AZ</option>
                                                    <option>고승일E</option>
                                                </select>
                                                <button class="file-upload-browse btn btn-primary" type="button" style="display:inline;">배치</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Cedric Kelly</td>
                                            <td>Senior Javascript Developer</td>
                                            <td>Edinburgh</td>
                                            <td>22</td>
                                            <td>$433,060</td>
                                            <td style="display:inline-block;">						
                                                <select class="form-control" style="display:inline;">
                                                    <option selected="" disabled="">배분</option>
                                                    <option>김종훈B</option>
                                                    <option>김미혜AZ</option>
                                                    <option>고승일E</option>
                                                </select>
                                                <button class="file-upload-browse btn btn-primary" type="button" style="display:inline;">배치</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>