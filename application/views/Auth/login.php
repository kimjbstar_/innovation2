			<!-- <div class="page-content d-flex align-items-center justify-content-center"> -->

            <div class="row w-100 mx-0 auth-page">
					<div class="col-md-8 col-xl-6 mx-auto">
						<div class="card">
							<div class="row">
                                <div class="col-md-12 pl-md-0">
                                    <div class="auth-form-wrapper px-4 py-5">
                                        <a href="#" class="noble-ui-logo d-block mb-2">Public Service Fair<span> .kr</span></a>
                                        <form class="forms-sample" method="post" action="<?=$url['login']?>">
                                            <div class="form-group">
                                                <label for="exampleIutEmail1">Login id</label>
                                                <input type="text" name="username" class="form-control" id="exampleIputEmail1" placeholder="ID" autocomplete="off">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputPassword1">Password</label>
                                                <input type="password" class="form-control" id="exampleInputPassword1" autocomplete="current-password" name="password" placeholder="Password" autocomplete="off">
                                            </div>
                                            <input type="hidden" value="<?=$data['csrf']['hash']?>" name="<?=$data['csrf']['name']?>">
                                            <div class="mt-3">
                                                <button type="submit" class="btn btn-primary mr-2 mb-2 mb-md-0">Login</button>
                                            </div>
                                            <!-- <a href="register.html" class="d-block mt-3 text-muted">Not a user? Sign up</a> -->
                                        </form>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>



