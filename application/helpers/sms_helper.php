<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\RequestException;

if (! (function_exists('sendSMS'))) {
    function sendSMS(string $title, string $message, array $receiverPhoneList): object
    {
        return json_decode((new Client([
            'headers' => [
                'cache-control' => 'no-cache',
                'content-type'  => 'application/json; charset=utf-8'
            ],
            'timeout' => 10.0,
        ]))->request('POST', 'https://directsend.co.kr/index.php/api_v2/sms_change_word', [
            'body' => json_encode([
                'key' => getenv('EMAIL_API_KEY'),
                'message' => str_replace(' ', ' ', trim($message)),
                'sender' => '01025349557',
                'title' => $title,
                'username' => 'jackkor',
                'receiver' => array_map(function ($elem) {
                    return [
                            'mobile' => $elem,
                            'name'   => '',
                            'note1'  => '',
                            'note2'  => '',
                            'note3'  => '',
                            'note4'  => '',
                            'note5'  => '',
                        ];
                }, $receiverPhoneList),
            ])
        ])->getBody()->getContents());
    }
}
