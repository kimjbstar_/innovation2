<?php
if(! function_exists('calcUseableMileage')) {
    function calcUseableMileage(int $currentMileage): string
    {
        $UNIT = 10000;
        return number_format(($currentMileage <= $UNIT) ? 0 : (int)floor($currentMileage / 1000) * 1000);
    }
}