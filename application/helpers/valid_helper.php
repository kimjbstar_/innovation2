<?php

if (! class_exists('ValidPost')) {
    defined('BASEPATH') or exit('No direct script access allowed');

    class ValidPost
    {
        private $data = null;
        private $rules = null;
        private $ci = null;
        public function __construct(array $postData)
        {
            $this->data = $postData;
            $this->ci =& get_instance();
            /**
             *
             * rule
             * date
             *    ㄴ 컬럼에 해당하는 벨류가 반드시 date 형태여야 합니다.
             * numeric
             *    ㄴ 컬럼에 해당하는 벨류가 반드시 숫자여야합니다
             *  unique
             *    ㄴ 컬럼이름, 테이블이름 이렇게 유니크한지 안한지 검사함
             *    ㄴ ex)unique:member_id,members -> members테이블에서 member_id값과 해당 벨류가 중복인지 아닌지
             *  require
             *    ㄴ 빈값이 아니며 무조건 있어야함
             *   email
             *    ㄴ 이메일이야함
             *  min
             *    ㄴ n글자 이상이여야 함
             *    ㄴ ex)min:6 -> 6글자 이상
             *  max
             *    ㄴ n글자 이하여야함
             *    ㄴ ex)max:10 -> 10글자 이하
             *  same
             *    ㄴ 특정키에 대한 값과 일치해야함
             *    ㄴ ex)same:test -> test라는 키를 가진 벨류와 해당 벨류외 일치해야함
             *  contain
             *    ㄴ 조건에 해당하는 글자가 있어야함
             *    ㄴ number
             *      ㄴ 숫자를 포함해야함
             *    ㄴ alpha
             *       ㄴ 영어 대문자 또는 소문자를 포함해야함
             * @example [
             *     'userName' => 'require|unique:member_id,members|min:6|max:18|contain:number,alpha',
             *     'email' => 'require|email|unique:member_email,members',
             *     'phone' => 'require|numeric',
             *     'password' => 'require|min:6|max:10',
             *     'passwordConfirm' => 'require|same:password|min:6|max:10',
             * ]
             */
        }
        public function setRules(array $rules): ValidPost
        {
            $this->rules = $this->parseRules($rules);
            return $this;
        }
        public function run(): array
        {
            foreach ($this->rules as $rules) {
                foreach ($rules as $rule) {
                    foreach ($rule as $value) {
                        if (count($value) === 2) {
                            if ($this->{$value[0]}($this->data[key($rules)], $value[1]) === false) {
                                return [
                                    'stat' => false,
                                    'errKey' => key($rules),
                                    'message' => key($rules) . 'IsNot' . ucfirst($value[0])
                                ];
                            }
                        } else {
                            if ($this->{$value[0]}($this->data[key($rules)]) === false) {
                                return [
                                    'stat' => false,
                                    'errKey' => key($rules),
                                    'message' => key($rules) . 'IsNot' . ucfirst($value[0])
                                ];
                            }
                        }
                    }
                }
            }
            return [
                'stat' => true
            ];
        }
        private function alpha(string $value): bool
        {
            return preg_match('/[^A-Za-z]/', $value) !== 0;
        }

        private function number(string $value): bool
        {
            return preg_match('/[^0-9]/', $value) !== 0;
        }

        private function contain(string $value, string $condition): bool
        {
            foreach (explode(',', $condition) as $call) {
                if (method_exists($this, $call)) {
                    if ($this->{$call}($value)) {
                        return true;
                    }
                }
            }
            return false;
        }
        private function between(string $value, string $condition): bool
        {
            $data = explode(',', $condition);
            return $this->max($value, $data[1]) === false && $this->min($value, $data[0]) === false;
            # && $this->max($value, $data[1]));
        }
        private function min(string $value, string $condition): bool
        {
            return strlen($value) >= intval($condition);
        }
        private function max(string $value, string $condition): bool
        {
            return strlen($value) <= intval($condition);
        }
        private function unique(string $value, string $condition): bool
        {
            $data = explode(',', $condition);
            $this->ci->load->model('common/Unique');
            return $this->ci->Unique->isUnique([
                'colName' => $data[0],
                'tableName' => $data[1],
                'reqData' => $value
            ]);
        }
        private function same(string $before, string $after): bool
        {
            return $before !== $after;
        }
        private function parseRules(array $data): array
        {
            return array_map(function ($key, $val) {
                return [
                    $key => array_map(function ($elem) {
                        $data = explode(':', trim($elem));

                        return array_map(function ($cond) {
                            return trim($cond);
                        }, [array_shift($data), implode(':', $data)]);
                    }, explode('|', trim($val)))
                ];
            }, array_keys($data), $data);
        }
        private function numeric(string $data): bool
        {
            return is_numeric($data);
        }
        private function require(string $data = null): bool
        {
            return $data && $data !== '';
        }
        private function email(string $data): bool
        {
            return filter_var($data, FILTER_VALIDATE_EMAIL);
        }
    }
}
