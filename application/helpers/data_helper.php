<?php

use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Client;

if (! function_exists('clearSession')) {
    function clearSession(): void
    {
        $ci =& get_instance();
        foreach ($ci->session->all_userdata() as $key => $value) {
            if ($key !== 'session_id' && $key !== 'ip_address' && $key !== 'user_agent' && $key !== 'last_activity') {
                $ci->session->unset_userdata($key);
            }
        }
        $ci->session->sess_destroy();
    }
}
if (! function_exists('setSession')) {
    function setSession(array $data): void
    {
        $ci =& get_instance();
        $ci->session->set_userdata($data);
    }
}

if (! function_exists('_req')) {
    function _req(string $method, string $url, array $param, array $header = null): array
    {
        return strtolower($method) === 'get' ? (new Client($header))->requestAsync('GET', $url . '?' . http_build_query($param))
                                                ->then(
                                                    function (ResponseInterface $resp) {
                                                        try {
                                                            return [
                                                                'stat' => true,
                                                                'message' => $resp->getBody()->getContents(),
                                                            ];
                                                        } catch (Exception $e) {
                                                            return [
                                                                'stat' => true,
                                                                'message' => $resp->getBody()->getContents()
                                                            ];
                                                        }
                                                    },
                                                    function ($err) {
                                                        return [
                                                            'stat' => false,
                                                            'message' => $err->getResponse()->getBody()->getContents()
                                                        ];
                                                    }
                                                )
                                                ->wait() :
                                    (new Client($header))->requestAsync('POST', $url, [
                                                    'form_params' => $param
                                                ])
                                                ->then(
                                                    function (ResponseInterface $resp) {
                                                        try {
                                                            return [
                                                                'stat' => true,
                                                                'message' => $resp->getBody()->getContents(),
                                                            ];
                                                        } catch (Exception $e) {
                                                            return [
                                                                'stat' => true,
                                                                'message' => $resp->getBody()->getContents()
                                                            ];
                                                        }
                                                    },
                                                    function ($err) {
                                                        return [
                                                            'stat' => false,
                                                            'message' => $err->getResponse()->getBody()->getContents()
                                                        ];
                                                    }
                                                )
                                                ->wait();
    }
}
