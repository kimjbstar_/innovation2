<?php

if (! function_exists('getUserInfoByUserIdx')) {
    function getUserInfoByUserIdx()
    {
        #code...
    }
}
if (! function_exists('getUserInfoByUserId')) {
    function getUserInfoByUserId(string $userId): ?object
    {
        $ci =& get_instance();
        $ci->load->model('User/User');
        return $ci->User->getInfoByUserId($userId);
    }
}
