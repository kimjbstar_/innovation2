<?php

if (! function_exists('renderSlideMenu')) {
    function renderSlideMenu(): array
    {
        $ci =& get_instance();
        $config = $ci->config;
        $file = $ci->file;
        return [
            'userId' => $ci->session->userId,
            'userMail' => $ci->session->userEmail,
            'url' => [
                'main' => $config->site_url('Main'),
                'profile' => $config->site_url('Profile'),
                'logout' => $config->site_url('Main/Logout'),
                'addStudio' => $config->site_url('Studio/Add'),
                'orderlist' => $config->site_url('Orderlist'),
                // 'main' => $config->site_url(''),
                // 'main' => $config->site_url(''),
                // 'main' => $config->site_url(''),
            ],
            'img' => [
                'close' => $file->getIconUrl('close.svg'),
                'home' => $file->getIconUrl('home.svg'),
                'homeLight' => $file->getIconUrl('home-light.svg'),
                'avaterDark' => $file->getIconUrl('avatar-dark.svg'),
                'avater' => $file->getIconUrl('avatar.svg'),
                'historyLight' => $file->getIconUrl('history-light.svg'),
                'history' => $file->getIconUrl('history.svg'),
                'myAddressDark' => $file->getIconUrl('my-addresses-dark.svg'),
                'myAddress' => $file->getIconUrl('my-addresses.svg'),
                'settings' => $file->getIconUrl('settings.svg'),
                'settingsLight' => $file->getIconUrl('settings-light.svg'),
                'support' => $file->getIconUrl('support.svg'),
                'supportLight' => $file->getIconUrl('support-light.svg'),
                'logout' => $file->getIconUrl('logout.svg'),
                'logoutLight' => $file->getIconUrl('logout-light.svg'),
            ]
        ];
    }
}
