<?php


if (! function_exists('goBackAlert')) {
    function goBackAlert(string $message, string $url): void
    {
        $CI =& get_instance();
        $CI->load->library('file');

        $CI->load->view('common/alert', [
            'swal' => $CI->file->getAssetsUrl('js/swal.js'),
            'title' => $message,
            'url' => $url,
        ]);
    }
}

if (! function_exists('goBackAlertHTML')) {
    function goBackAlertHTML(string $message, string $html, string $url): void
    {
        $CI =& get_instance();
        $CI->load->library('file');

        $CI->load->view('common/alert_html', [
            'swal' => $CI->file->getAssetsUrl('js/swal.js'),
            'title' => $message,
            'html' => $html,
            'url' => $url,
        ]);
    }
}


if (! function_exists('goBackAlertHTMLOrder')) {
    function goBackAlertHTMLOrder(string $message, string $html, string $url): void
    {
        $CI =& get_instance();
        $CI->load->library('file');

        $CI->load->view('common/alert_html_order', [
            'swal' => $CI->file->getAssetsUrl('js/swal.js'),
            'title' => $message,
            'html' => $html,
            'url' => $url,
        ]);
    }
}



if (! function_exists('alert')) {
    function alert(string $message, string $url = ''): void
    {
        $CI =& get_instance();
        $CI->load->library('file');
        $CI->load->view('util/alert', [
            'swal' => $CI->file->getJsUrl('lib/swal.js'),
            'title' => $message,
            'url' => $url,
        ]);
    }
}
