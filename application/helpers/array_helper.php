<?php

if (! function_exists('insertAfter')) {
    function insertAfter(array $base, array $insertData, $position): array
    {
        $idx = array_search($position, array_keys($base)) + 1;
        return array_slice($base, 0, $idx, true) + $insertData + array_slice($base, $idx, count($base) - 1, true);
    }
    function trimArray(array $data)
    {
        return array_map(function ($e) {
            return is_string($e) ? trim($e) : $e;
        }, $data);
    }
}
