<?php

if (! function_exists('loadManagerCommonAssets')) {
    function loadManagerCommonAssets(): array
    {
        $CI =& get_instance();
        $CI->load->library('file');

        return [







            'vendors/core/core.css' => $CI->file->getAssetsUrl('vendors/core/core.css'),
            'vendors/bootstrap-datepicker/bootstrap-datepicker.min.css' => $CI->file->getAssetsUrl('vendors/bootstrap-datepicker/bootstrap-datepicker.min.css'),
            'fonts/feather-font/css/iconfont.css' => $CI->file->getAssetsUrl('fonts/feather-font/css/iconfont.css'),
            'css/demo_1/style.css' => $CI->file->getAssetsUrl('css/demo_1/style.css'),
            'vendors/cropperjs/cropper.min.css' => $CI->file->getAssetsUrl('vendors/cropperjs/cropper.min.css'),
            'vendors/owl.carousel/owl.carousel.min.css' => $CI->file->getAssetsUrl('vendors/owl.carousel/owl.carousel.min.css'),
            'vendors/owl.carousel/owl.theme.default.min.css' => $CI->file->getAssetsUrl('vendors/owl.carousel/owl.theme.default.min.css'),
            'vendors/animate.css/animate.min.css' => $CI->file->getAssetsUrl('vendors/animate.css/animate.min.css'),
            'vendors/sweetalert2/sweetalert2.min.css' => $CI->file->getAssetsUrl('vendors/sweetalert2/sweetalert2.min.css'),
            'vendors/fullcalendar/fullcalendar.min.css' => $CI->file->getAssetsUrl('vendors/fullcalendar/fullcalendar.min.css'),
            'vendors/morris.js/morris.css' => $CI->file->getAssetsUrl('vendors/morris.js/morris.css'),
            'vendors/select2/select2.min.css' => $CI->file->getAssetsUrl('vendors/select2/select2.min.css'),
            'vendors/simplemde/simplemde.min.css' => $CI->file->getAssetsUrl('vendors/simplemde/simplemde.min.css'),
            'vendors/jquery-tags-input/jquery.tagsinput.min.css' => $CI->file->getAssetsUrl('vendors/jquery-tags-input/jquery.tagsinput.min.css'),
            'vendors/dropzone/dropzone.min.css' => $CI->file->getAssetsUrl('vendors/dropzone/dropzone.min.css'),
            'vendors/dropify/dist/dropify.min.css' => $CI->file->getAssetsUrl('vendors/dropify/dist/dropify.min.css'),
            'vendors/bootstrap-colorpicker/bootstrap-colorpicker.min.css' => $CI->file->getAssetsUrl('vendors/bootstrap-colorpicker/bootstrap-colorpicker.min.css'),
            'vendors/font-awesome/css/font-awesome.min.css' => $CI->file->getAssetsUrl('vendors/font-awesome/css/font-awesome.min.css'),
            'vendors/tempusdominus-bootstrap-4/tempusdominus-bootstrap-4.min.css' => $CI->file->getAssetsUrl('vendors/tempusdominus-bootstrap-4/tempusdominus-bootstrap-4.min.css'),
            'vendors/jquery-steps/jquery.steps.css' => $CI->file->getAssetsUrl('vendors/jquery-steps/jquery.steps.css'),
            'vendors/flag-icon-css/css/flag-icon.min.css' => $CI->file->getAssetsUrl('vendors/flag-icon-css/css/flag-icon.min.css'),
            'vendors/mdi/css/materialdesignicons.min.css' => $CI->file->getAssetsUrl('vendors/mdi/css/materialdesignicons.min.css'),
            'vendors/datatables.net-bs4/dataTables.bootstrap4.css' => $CI->file->getAssetsUrl('vendors/datatables.net-bs4/dataTables.bootstrap4.css'),
            'vendors/prismjs/themes/prism.css' => $CI->file->getAssetsUrl('vendors/prismjs/themes/prism.css'),
            'vendors/core/core.css' => $CI->file->getAssetsUrl('vendors/core/core.css'),
            'vendors/bootstrap-datepicker/bootstrap-datepicker.min.css' => $CI->file->getAssetsUrl('vendors/bootstrap-datepicker/bootstrap-datepicker.min.css'),
            'fonts/feather-font/css/iconfont.css' => $CI->file->getAssetsUrl('fonts/feather-font/css/iconfont.css'),
            'css/demo_1/style.css' => $CI->file->getAssetsUrl('css/demo_1/style.css'),






            'vendors/core/core.js' => $CI->file->getAssetsUrl('vendors/core/core.js'),


            'vendors/jquery-ui/jquery-ui.min.js' => $CI->file->getAssetsUrl('vendors/jquery-ui/jquery-ui.min.js'),
            'js/jquery.flot.js' => $CI->file->getAssetsUrl('js/jquery.flot.js'),
            'vendors/jquery.flot/jquery.flot.pie.js' => $CI->file->getAssetsUrl('vendors/jquery.flot/jquery.flot.pie.js'),
            'vendors/jquery.flot/jquery.flot.categories.js' => $CI->file->getAssetsUrl('vendors/jquery.flot/jquery.flot.categories.js'),
            'vendors/jquery-mousewheel/jquery.mousewheel.js' => $CI->file->getAssetsUrl('vendors/jquery-mousewheel/jquery.mousewheel.js'),
            'vendors/jquery-sparkline/jquery.sparkline.min.js' => $CI->file->getAssetsUrl('vendors/jquery-sparkline/jquery.sparkline.min.js'),
            'vendors/peity/jquery.peity.min.js' => $CI->file->getAssetsUrl('vendors/peity/jquery.peity.min.js'),
            'vendors/jquery-validation/jquery.validate.min.js' => $CI->file->getAssetsUrl('vendors/jquery-validation/jquery.validate.min.js'),
            'vendors/inputmask/jquery.inputmask.bundle.min.js' => $CI->file->getAssetsUrl('vendors/inputmask/jquery.inputmask.bundle.min.js'),
            'vendors/jquery-tags-input/jquery.tagsinput.min.js' => $CI->file->getAssetsUrl('vendors/jquery-tags-input/jquery.tagsinput.min.js'),
            'vendors/jquery-steps/jquery.steps.min.js' => $CI->file->getAssetsUrl('vendors/jquery-steps/jquery.steps.min.js'),
            'vendors/datatables.net/jquery.dataTables.js' => $CI->file->getAssetsUrl('vendors/datatables.net/jquery.dataTables.js'),
            'vendors/jquery.flot/jquery.flot.js' => $CI->file->getAssetsUrl('vendors/jquery.flot/jquery.flot.js'),
            'vendors/jquery.flot/jquery.flot.resize.js' => $CI->file->getAssetsUrl('vendors/jquery.flot/jquery.flot.resize.js'),




            'vendors/cropperjs/cropper.min.js' => $CI->file->getAssetsUrl('vendors/cropperjs/cropper.min.js'),
            'js/file-upload.js' => $CI->file->getAssetsUrl('js/file-upload.js'),
            'js/cropper.js' => $CI->file->getAssetsUrl('js/cropper.js'),
            'vendors/owl.carousel/owl.carousel.min.js' => $CI->file->getAssetsUrl('vendors/owl.carousel/owl.carousel.min.js'),
            'js/carousel.js' => $CI->file->getAssetsUrl('js/carousel.js'),
            'vendors/sweetalert2/sweetalert2.min.js' => $CI->file->getAssetsUrl('vendors/sweetalert2/sweetalert2.min.js'),
            'vendors/promise-polyfill/polyfill.min.js' => $CI->file->getAssetsUrl('vendors/promise-polyfill/polyfill.min.js'),
            'js/sweet-alert.js' => $CI->file->getAssetsUrl('js/sweet-alert.js'),
            'vendors/moment/moment.min.js' => $CI->file->getAssetsUrl('vendors/moment/moment.min.js'),
            'vendors/fullcalendar/fullcalendar.min.js' => $CI->file->getAssetsUrl('vendors/fullcalendar/fullcalendar.min.js'),
            'js/fullcalendar.js' => $CI->file->getAssetsUrl('js/fullcalendar.js'),
            'js/chat.js' => $CI->file->getAssetsUrl('js/chat.js'),
            'js/apexcharts.js' => $CI->file->getAssetsUrl('js/apexcharts.js'),
            'vendors/chartjs/Chart.min.js' => $CI->file->getAssetsUrl('vendors/chartjs/Chart.min.js'),
            'js/chartjs.js' => $CI->file->getAssetsUrl('js/chartjs.js'),
            'vendors/raphael/raphael.min.js' => $CI->file->getAssetsUrl('vendors/raphael/raphael.min.js'),
            'vendors/morris.js/morris.min.js' => $CI->file->getAssetsUrl('vendors/morris.js/morris.min.js'),
            'js/morris.js' => $CI->file->getAssetsUrl('js/morris.js'),
            'js/peity.js' => $CI->file->getAssetsUrl('js/peity.js'),
            'js/sparkline.js' => $CI->file->getAssetsUrl('js/sparkline.js'),
            'vendors/select2/select2.min.js' => $CI->file->getAssetsUrl('vendors/select2/select2.min.js'),
            'vendors/simplemde/simplemde.min.js' => $CI->file->getAssetsUrl('vendors/simplemde/simplemde.min.js'),
            'js/email.js' => $CI->file->getAssetsUrl('js/email.js'),
            // 'vendors/bootstrap-maxlength/bootstrap-maxlength.min.js' => $CI->file->getAssetsUrl('vendors/bootstrap-maxlength/ootstrap-maxlength.min.js'),
            'vendors/typeahead.js/typeahead.bundle.min.js' => $CI->file->getAssetsUrl('vendors/typeahead.js/typeahead.bundle.min.js'),
            'vendors/dropzone/dropzone.min.js' => $CI->file->getAssetsUrl('vendors/dropzone/dropzone.min.js'),
            'vendors/dropify/dist/dropify.min.js' => $CI->file->getAssetsUrl('vendors/dropify/dist/dropify.min.js'),
            'vendors/bootstrap-colorpicker/bootstrap-colorpicker.min.js' => $CI->file->getAssetsUrl('vendors/bootstrap-colorpicker/bootstrap-colorpicker.min.js'),
            'vendors/tempusdominus-bootstrap-4/tempusdominus-bootstrap-4.js' => $CI->file->getAssetsUrl('vendors/tempusdominus-bootstrap-4/tempusdominus-bootstrap-4.js'),
            'js/form-validation.js' => $CI->file->getAssetsUrl('js/form-validation.js'),
            'js/bootstrap-maxlength.js' => $CI->file->getAssetsUrl('js/bootstrap-maxlength.js'),
            'js/inputmask.js' => $CI->file->getAssetsUrl('js/inputmask.js'),
            'js/select2.js' => $CI->file->getAssetsUrl('js/select2.js'),
            'js/typeahead.js' => $CI->file->getAssetsUrl('js/typeahead.js'),
            'js/tags-input.js' => $CI->file->getAssetsUrl('js/tags-input.js'),
            'js/dropzone.js' => $CI->file->getAssetsUrl('js/dropzone.js'),
            'js/dropify.js' => $CI->file->getAssetsUrl('js/dropify.js'),
            'js/bootstrap-colorpicker.js' => $CI->file->getAssetsUrl('js/bootstrap-colorpicker.js'),
            'js/timepicker.js' => $CI->file->getAssetsUrl('js/timepicker.js'),
            'js/file-upload.js' => $CI->file->getAssetsUrl('js/file-upload.js'),
            'vendors/tinymce/tinymce.min.js' => $CI->file->getAssetsUrl('vendors/tinymce/tinymce.min.js'),
            'vendors/ace-builds/src-min/ace.js' => $CI->file->getAssetsUrl('vendors/ace-builds/src-min/ace.js'),
            'vendors/ace-builds/src-min/theme-chaos.js' => $CI->file->getAssetsUrl('vendors/ace-builds/src-min/theme-chaos.js'),
            'js/tinymce.js' => $CI->file->getAssetsUrl('js/tinymce.js'),
            'js/simplemde.js' => $CI->file->getAssetsUrl('js/simplemde.js'),
            'js/ace.js' => $CI->file->getAssetsUrl('js/ace.js'),
            'js/wizard.js' => $CI->file->getAssetsUrl('js/wizard.js'),
            'vendors/datatables.net-bs4/dataTables.bootstrap4.js' => $CI->file->getAssetsUrl('vendors/datatables.net-bs4/dataTables.bootstrap4.js'),
            'js/data-table.js' => $CI->file->getAssetsUrl('js/data-table.js'),
            'vendors/prismjs/prism.js' => $CI->file->getAssetsUrl('vendors/prismjs/prism.js'),
            'vendors/clipboard/clipboard.min.js' => $CI->file->getAssetsUrl('vendors/clipboard/clipboard.min.js'),
            'vendors/chartjs/Chart.min.js' => $CI->file->getAssetsUrl('vendors/chartjs/Chart.min.js'),
            'vendors/bootstrap-datepicker/bootstrap-datepicker.min.js' => $CI->file->getAssetsUrl('vendors/bootstrap-datepicker/bootstrap-datepicker.min.js'),
            'vendors/apexcharts/apexcharts.min.js' => $CI->file->getAssetsUrl('vendors/apexcharts/apexcharts.min.js'),
            'vendors/progressbar.js/progressbar.min.js' => $CI->file->getAssetsUrl('vendors/progressbar.js/progressbar.min.js'),
            'vendors/feather-icons/feather.min.js' => $CI->file->getAssetsUrl('vendors/feather-icons/feather.min.js'),
            'js/template.js' => $CI->file->getAssetsUrl('js/template.js'),
            'js/dashboard.js' => $CI->file->getAssetsUrl('js/dashboard.js'),
            'js/datepicker.js' => $CI->file->getAssetsUrl('js/datepicker.js'),
            'js/demo.js' => $CI->file->getAssetsUrl('js/demo.js'),




            'images/favicon.png' => $CI->file->getAssetsUrl('images/favicon.png'),









        ];
    }
}





if (! function_exists('loadCommonAssets')) {
    function loadCommonAssets(): array
    {
        $CI =& get_instance();
        $CI->load->library('file');

        return [













            "plugins/jquery-sparkline/jquery.sparkline.js" => $CI->file->getAssetsUrl("plugins/jquery-sparkline/jquery.sparkline.js"),
            "plugins/turbo-slider/turbo.min.js" => $CI->file->getAssetsUrl("plugins/turbo-slider/turbo.min.js"),
            "plugins/turbo-slider/turbo.css" => $CI->file->getAssetsUrl("plugins/turbo-slider/turbo.css"),
            "plugins/jquery.flot.tooltip/js/jquery.flot.tooltip.js" => $CI->file->getAssetsUrl("plugins/jquery.flot.tooltip/js/jquery.flot.tooltip.js"),
            "plugins/smartphoto/js/smartphoto.min.js" => $CI->file->getAssetsUrl("plugins/smartphoto/js/smartphoto.min.js"),
            "plugins/smartphoto/css/smartphoto.min.css" => $CI->file->getAssetsUrl("plugins/smartphoto/css/smartphoto.min.css"),
            "plugins/flot/jquery.flot.stack.min.js" => $CI->file->getAssetsUrl("plugins/flot/jquery.flot.stack.min.js"),
            "plugins/flot/jquery.flot.html" => $CI->file->getAssetsUrl("plugins/flot/jquery.flot.html"),
            "plugins/flot/jquery.flot.resize.min.js" => $CI->file->getAssetsUrl("plugins/flot/jquery.flot.resize.min.js"),
            "plugins/flot/jquery.flot.pie.min.js" => $CI->file->getAssetsUrl("plugins/flot/jquery.flot.pie.min.js"),
            "plugins/flot/jquery.flot.time.min.js" => $CI->file->getAssetsUrl("plugins/flot/jquery.flot.time.min.js"),
            "plugins/flot/custom/horizontal.js" => $CI->file->getAssetsUrl("plugins/flot/custom/horizontal.js"),
            "plugins/flot/custom/donut.js" => $CI->file->getAssetsUrl("plugins/flot/custom/donut.js"),
            "plugins/flot/custom/stacked-vertical.js" => $CI->file->getAssetsUrl("plugins/flot/custom/stacked-vertical.js"),
            "plugins/flot/custom/pie.js" => $CI->file->getAssetsUrl("plugins/flot/custom/pie.js"),
            "plugins/flot/custom/scatter.js" => $CI->file->getAssetsUrl("plugins/flot/custom/scatter.js"),
            "plugins/flot/custom/stacked-area.js" => $CI->file->getAssetsUrl("plugins/flot/custom/stacked-area.js"),
            "plugins/flot/custom/line.js" => $CI->file->getAssetsUrl("plugins/flot/custom/line.js"),
            "plugins/flot/custom/area.js" => $CI->file->getAssetsUrl("plugins/flot/custom/area.js"),
            "plugins/flot/custom/combine-chart.js" => $CI->file->getAssetsUrl("plugins/flot/custom/combine-chart.js"),
            "plugins/flot/custom/vertical.js" => $CI->file->getAssetsUrl("plugins/flot/custom/vertical.js"),
            "plugins/flot/custom/realtime.js" => $CI->file->getAssetsUrl("plugins/flot/custom/realtime.js"),
            "plugins/flot/custom/combine-chart-compare.js" => $CI->file->getAssetsUrl("plugins/flot/custom/combine-chart-compare.js"),
            "plugins/flot/custom/stacked-horizontal.js" => $CI->file->getAssetsUrl("plugins/flot/custom/stacked-horizontal.js"),
            "plugins/flot/custom/rectangular-pie.js" => $CI->file->getAssetsUrl("plugins/flot/custom/rectangular-pie.js"),
            "plugins/flot/jquery.flot.tooltip.min.js" => $CI->file->getAssetsUrl("plugins/flot/jquery.flot.tooltip.min.js"),
            "plugins/flot/jquery.flot.min.js" => $CI->file->getAssetsUrl("plugins/flot/jquery.flot.min.js"),
            "plugins/sidebysideimproved/jquery.flot.orderBars.js" => $CI->file->getAssetsUrl("plugins/sidebysideimproved/jquery.flot.orderBars.js"),
            "plugins/jquery.flot.spline/jquery.flot.spline.js" => $CI->file->getAssetsUrl("plugins/jquery.flot.spline/jquery.flot.spline.js"),
            "plugins/c3-chart/c3.min.js" => $CI->file->getAssetsUrl("plugins/c3-chart/c3.min.js"),
            "plugins/c3-chart/c3.css" => $CI->file->getAssetsUrl("plugins/c3-chart/c3.css"),
            "plugins/c3-chart/d3.min.js" => $CI->file->getAssetsUrl("plugins/c3-chart/d3.min.js"),
            "plugins/c3-chart/c3.custom.js" => $CI->file->getAssetsUrl("plugins/c3-chart/c3.custom.js"),
            "images/.DS_Store" => $CI->file->getAssetsUrl("images/.DS_Store"),
            "js/swiper.min.js" => $CI->file->getAssetsUrl("js/swiper.min.js"),
            "js/global.script.js" => $CI->file->getAssetsUrl("js/global.script.js"),
            "js/jquery-3.2.1.min.js" => $CI->file->getAssetsUrl("js/jquery-3.2.1.min.js"),
            "js/app-charts.js" => $CI->file->getAssetsUrl("js/app-charts.js"),
            "css/global.style.css" => $CI->file->getAssetsUrl("css/global.style.css"),
            "css/cryptocoins.css" => $CI->file->getAssetsUrl("css/cryptocoins.css"),
            "css/animate.css" => $CI->file->getAssetsUrl("css/animate.css"),
            "css/font-awesome.min.css" => $CI->file->getAssetsUrl("css/font-awesome.min.css"),
            "css/intro-style.css" => $CI->file->getAssetsUrl("css/intro-style.css"),
            "css/bootstrap.min.css" => $CI->file->getAssetsUrl("css/bootstrap.min.css"),
            "css/swiper.min.css" => $CI->file->getAssetsUrl("css/swiper.min.css"),
            "fonts/fontawesome-webfont3e6e3e6e.eot" => $CI->file->getAssetsUrl("fonts/fontawesome-webfont3e6e3e6e.eot"),
            "fonts/fontawesome-webfont3e6e3e6e.svg" => $CI->file->getAssetsUrl("fonts/fontawesome-webfont3e6e3e6e.svg"),
            "fonts/cryptocoins.css" => $CI->file->getAssetsUrl("fonts/cryptocoins.css"),
            "fonts/fontawesome-webfontd41dd41d.eot" => $CI->file->getAssetsUrl("fonts/fontawesome-webfontd41dd41d.eot"),
            "fonts/fontawesome-webfont3e6e3e6e.ttf" => $CI->file->getAssetsUrl("fonts/fontawesome-webfont3e6e3e6e.ttf"),
            "fonts/cryptocoins.woff" => $CI->file->getAssetsUrl("fonts/cryptocoins.woff"),
            "fonts/cryptocoins.woff2" => $CI->file->getAssetsUrl("fonts/cryptocoins.woff2"),
            "fonts/fontawesome-webfont3e6e3e6e.woff2" => $CI->file->getAssetsUrl("fonts/fontawesome-webfont3e6e3e6e.woff2"),
            "fonts/fontawesome-webfont3e6e3e6e.woff" => $CI->file->getAssetsUrl("fonts/fontawesome-webfont3e6e3e6e.woff"),
            "fonts/cryptocoins.ttf" => $CI->file->getAssetsUrl("fonts/cryptocoins.ttf"),
            "img/content/ref1.png" => $CI->file->getAssetsUrl("img/content/ref1.png"),
            "img/content/ref-icon1.png" => $CI->file->getAssetsUrl("img/content/ref-icon1.png"),
            "img/content/ref2.png" => $CI->file->getAssetsUrl("img/content/ref2.png"),
            "img/content/ref-icon4.png" => $CI->file->getAssetsUrl("img/content/ref-icon4.png"),
            "img/content/ref-icon3.png" => $CI->file->getAssetsUrl("img/content/ref-icon3.png"),
            "img/content/coin6.png" => $CI->file->getAssetsUrl("img/content/coin6.png"),
            "img/content/avatar/avatar-3.png" => $CI->file->getAssetsUrl("img/content/avatar/avatar-3.png"),
            "img/content/avatar/avatar-1.png" => $CI->file->getAssetsUrl("img/content/avatar/avatar-1.png"),
            "img/content/avatar/avatar-2.png" => $CI->file->getAssetsUrl("img/content/avatar/avatar-2.png"),
            "img/content/avatar/user-1.png" => $CI->file->getAssetsUrl("img/content/avatar/user-1.png"),
            "img/content/avatar/user-4.png" => $CI->file->getAssetsUrl("img/content/avatar/user-4.png"),
            "img/content/avatar/avatar-5.png" => $CI->file->getAssetsUrl("img/content/avatar/avatar-5.png"),
            "img/content/avatar/user-2.png" => $CI->file->getAssetsUrl("img/content/avatar/user-2.png"),
            "img/content/avatar/user-3.png" => $CI->file->getAssetsUrl("img/content/avatar/user-3.png"),
            "img/content/avatar/avatar-4.png" => $CI->file->getAssetsUrl("img/content/avatar/avatar-4.png"),
            "img/content/onboard1.png" => $CI->file->getAssetsUrl("img/content/onboard1.png"),
            "img/content/notification2.png" => $CI->file->getAssetsUrl("img/content/notification2.png"),
            "img/content/coin5.png" => $CI->file->getAssetsUrl("img/content/coin5.png"),
            "img/content/p4.png" => $CI->file->getAssetsUrl("img/content/p4.png"),
            "img/content/coin3.png" => $CI->file->getAssetsUrl("img/content/coin3.png"),
            "img/content/crypto-wallet.png" => $CI->file->getAssetsUrl("img/content/crypto-wallet.png"),
            "img/content/icons/13.png" => $CI->file->getAssetsUrl("img/content/icons/13.png"),
            "img/content/icons/14.png" => $CI->file->getAssetsUrl("img/content/icons/14.png"),
            "img/content/icons/9.png" => $CI->file->getAssetsUrl("img/content/icons/9.png"),
            "img/content/icons/7.png" => $CI->file->getAssetsUrl("img/content/icons/7.png"),
            "img/content/icons/6.png" => $CI->file->getAssetsUrl("img/content/icons/6.png"),
            "img/content/icons/10.png" => $CI->file->getAssetsUrl("img/content/icons/10.png"),
            "img/content/icons/8.png" => $CI->file->getAssetsUrl("img/content/icons/8.png"),
            "img/content/icons/11.png" => $CI->file->getAssetsUrl("img/content/icons/11.png"),
            "img/content/icons/5.png" => $CI->file->getAssetsUrl("img/content/icons/5.png"),
            "img/content/icons/3.png" => $CI->file->getAssetsUrl("img/content/icons/3.png"),
            "img/content/icons/4.png" => $CI->file->getAssetsUrl("img/content/icons/4.png"),
            "img/content/icons/12.png" => $CI->file->getAssetsUrl("img/content/icons/12.png"),
            "img/content/icons/2.png" => $CI->file->getAssetsUrl("img/content/icons/2.png"),
            "img/content/icons/15.png" => $CI->file->getAssetsUrl("img/content/icons/15.png"),
            "img/content/icons/1.png" => $CI->file->getAssetsUrl("img/content/icons/1.png"),
            "img/content/crypto-buy.png" => $CI->file->getAssetsUrl("img/content/crypto-buy.png"),
            "img/content/ref3.png" => $CI->file->getAssetsUrl("img/content/ref3.png"),
            "img/content/headerbg-1.jpg" => $CI->file->getAssetsUrl("img/content/headerbg-1.jpg"),
            "img/content/coin2.png" => $CI->file->getAssetsUrl("img/content/coin2.png"),
            "img/content/coin1.png" => $CI->file->getAssetsUrl("img/content/coin1.png"),
            "img/content/trans-bg2.png" => $CI->file->getAssetsUrl("img/content/trans-bg2.png"),
            "img/content/coin8.png" => $CI->file->getAssetsUrl("img/content/coin8.png"),
            "img/content/s2.png" => $CI->file->getAssetsUrl("img/content/s2.png"),
            "img/content/onboard3.png" => $CI->file->getAssetsUrl("img/content/onboard3.png"),
            "img/content/exchange.png" => $CI->file->getAssetsUrl("img/content/exchange.png"),
            "img/content/ref-icon2.png" => $CI->file->getAssetsUrl("img/content/ref-icon2.png"),
            "img/content/crypto-sell.png" => $CI->file->getAssetsUrl("img/content/crypto-sell.png"),
            "img/content/p2.png" => $CI->file->getAssetsUrl("img/content/p2.png"),
            "img/content/b1-bg.png" => $CI->file->getAssetsUrl("img/content/b1-bg.png"),
            "img/content/onboard2.png" => $CI->file->getAssetsUrl("img/content/onboard2.png"),
            "img/content/s1.png" => $CI->file->getAssetsUrl("img/content/s1.png"),
            "img/content/coin7.png" => $CI->file->getAssetsUrl("img/content/coin7.png"),
            "img/content/3.png" => $CI->file->getAssetsUrl("img/content/3.png"),
            "img/content/p1.png" => $CI->file->getAssetsUrl("img/content/p1.png"),
            "img/content/qr.png" => $CI->file->getAssetsUrl("img/content/qr.png"),
            "img/content/trans-bg1.png" => $CI->file->getAssetsUrl("img/content/trans-bg1.png"),
            "img/content/curve.svg" => $CI->file->getAssetsUrl("img/content/curve.svg"),
            "img/content/dash-bg.jpg" => $CI->file->getAssetsUrl("img/content/dash-bg.jpg"),
            "img/content/coin4.png" => $CI->file->getAssetsUrl("img/content/coin4.png"),
            "img/content/s3.png" => $CI->file->getAssetsUrl("img/content/s3.png"),
            "img/content/b2-bg.png" => $CI->file->getAssetsUrl("img/content/b2-bg.png"),
            "img/content/2.png" => $CI->file->getAssetsUrl("img/content/2.png"),
            "img/content/notification1.png" => $CI->file->getAssetsUrl("img/content/notification1.png"),
            "img/content/1.png" => $CI->file->getAssetsUrl("img/content/1.png"),
            "img/content/p3.png" => $CI->file->getAssetsUrl("img/content/p3.png"),
            "img/avatar.png" => $CI->file->getAssetsUrl("img/avatar.png"),
            "img/intro.png" => $CI->file->getAssetsUrl("img/intro.png"),
            "img/iphone.png" => $CI->file->getAssetsUrl("img/iphone.png"),
            "img/QR_Code.png" => $CI->file->getAssetsUrl("img/QR_Code.png"),
            "img/icon.png" => $CI->file->getAssetsUrl("img/icon.png"),
            "img/up-down.svg" => $CI->file->getAssetsUrl("img/up-down.svg"),
            "img/assets/feature-2.svg" => $CI->file->getAssetsUrl("img/assets/feature-2.svg"),
            "img/assets/feature-4.svg" => $CI->file->getAssetsUrl("img/assets/feature-4.svg"),
            "img/assets/feature-3.svg" => $CI->file->getAssetsUrl("img/assets/feature-3.svg"),
            "img/assets/feature-1.svg" => $CI->file->getAssetsUrl("img/assets/feature-1.svg"),
            "img/assets/news-list-image-2.jpg" => $CI->file->getAssetsUrl("img/assets/news-list-image-2.jpg"),
            "img/assets/news-list-image-1.jpg" => $CI->file->getAssetsUrl("img/assets/news-list-image-1.jpg"),



        ];
    }
}









if (! function_exists('loadCommonUrl')) {
    function loadCommonUrl(): array
    {
        $CI =& get_instance();

        return [
            "index.html" => $CI->config->site_url("index"),
            "forms.html" => $CI->config->site_url("forms"),
            "legtree.html" => $CI->config->site_url("legtree"),
            "marketing.html" => $CI->config->site_url("marketing"),
            "present.html" => $CI->config->site_url("present"),
            "setting.html" => $CI->config->site_url("setting"),
            "profile.html" => $CI->config->site_url("profile"),
            "search_result.html" => $CI->config->site_url("search_result"),
            "trading.html" => $CI->config->site_url("trading"),
            "checkbox_list.html" => $CI->config->site_url("checkbox_list"),
            "tab_top.html" => $CI->config->site_url("tab_top"),
            "wallet.html" => $CI->config->site_url("wallet"),
            "contact.html" => $CI->config->site_url("contact"),
            "signup.html" => $CI->config->site_url("signup"),
            "forgot_password.html" => $CI->config->site_url("forgot_password"),
            "link_list_two_column.html" => $CI->config->site_url("link_list_two_column"),
            "buy_sell.html" => $CI->config->site_url("buy_sell"),
            "blank.html" => $CI->config->site_url("blank"),
            "popup.html" => $CI->config->site_url("popup"),
            "charts.html" => $CI->config->site_url("charts"),
            "wizard_default.html" => $CI->config->site_url("wizard_default"),
            "accordion.html" => $CI->config->site_url("accordion"),
            "login.html" => $CI->config->site_url("login"),
            "affiliate.html" => $CI->config->site_url("affiliate"),
            "wizard_fullscreen.html" => $CI->config->site_url("wizard_fullscreen"),
            "link_list.html" => $CI->config->site_url("link_list"),
            "tab_bottom.html" => $CI->config->site_url("tab_bottom"),






            // CRM.coderecipe.io
            "mydb" => $CI->config->site_url("MyDB"),
            "dbdownload" => $CI->config->site_url("Dbdownload"),
            "keywords" => $CI->config->site_url("Keywords"),
            "alldb" => $CI->config->site_url("AllDB"),
            "logout" => $CI->config->site_url("Logout/logout"),


            "admin_apply_consulting" => $CI->config->site_url("Richmango/Apply/Consulting"),
            "admin_apply_mentoring" => $CI->config->site_url("Richmango/Apply/Mentoring"),
            "admin_apply_mock_interview" => $CI->config->site_url("Richmango/Apply/MockInterview"),
            "admin_apply_seminar" => $CI->config->site_url("Richmango/Apply/Seminar"),

            "admin_boards_informations" => $CI->config->site_url("Richmango/Boards/Informations"),
            "admin_boards_notices" => $CI->config->site_url("Richmango/Boards/Notices"),
            
            "admin_contents_events" => $CI->config->site_url("Richmango/Contents/Events"),
            "admin_contents_eventresults" => $CI->config->site_url("Richmango/Contents/EventResult"),
            "admin_contents_faqs" => $CI->config->site_url("Richmango/Contents/Faqs"),
            "admin_contents_hire_infos" => $CI->config->site_url("Richmango/Contents/HireInfos"),
            
            "admin_links_buttons" => $CI->config->site_url("Richmango/Links/Buttons"),
            "admin_links_open_links" => $CI->config->site_url("Richmango/Links/OpenLinks"),
            "admin_links_youtube_links" => $CI->config->site_url("Richmango/Links/YoutubeLinks"),








            'customer' => $CI->config->site_url('Pages/Customer'),
            'manage_sales' => $CI->config->site_url('Pages/Manage_sales'),
            'manage_setting' => $CI->config->site_url('Pages/Manage_setting'),
            'manager/' => $CI->config->site_url('manager/product'),
            'dashboard_one' => $CI->config->site_url('M_dashboard'),
            'pages/email/inbox' => $CI->config->site_url('Pages/Email/Inbox'),
            'pages/email/read' => $CI->config->site_url('Pages/Email/Read'),
            'pages/email/compose' => $CI->config->site_url('Pages/Email/Compose'),
            'pages/apps/chat' => $CI->config->site_url('Pages/Apps/Chat'),
            'pages/apps/calendar' => $CI->config->site_url('Pages/Apps/Calendar'),
            'pages/ui_components/alerts' => $CI->config->site_url('Pages/Ui_components/Alerts'),
            'pages/ui_components/badges' => $CI->config->site_url('Pages/Ui_components/Badges'),
            'pages/ui_components/breadcrumbs' => $CI->config->site_url('Pages/Ui_components/Breadcrumbs'),
            'pages/ui_components/buttons' => $CI->config->site_url('Pages/Ui_components/Buttons'),
            'pages/ui_components/button_group' => $CI->config->site_url('Pages/Ui_components/Button_group'),
            'pages/ui_components/cards' => $CI->config->site_url('Pages/Ui_components/Cards'),
            'pages/ui_components/carousel' => $CI->config->site_url('Pages/Ui_components/Carousel'),
            'pages/ui_components/collapse' => $CI->config->site_url('Pages/Ui_components/Collapse'),
            'pages/ui_components/dropdowns' => $CI->config->site_url('Pages/Ui_components/Dropdowns'),
            'pages/ui_components/list_group' => $CI->config->site_url('Pages/Ui_components/List_group'),
            'pages/ui_components/media_object' => $CI->config->site_url('Pages/Ui_components/Media_object'),
            'pages/ui_components/modal' => $CI->config->site_url('Pages/Ui_components/Modal'),
            'pages/ui_components/navs' => $CI->config->site_url('Pages/Ui_components/Navs'),
            'pages/ui_components/navbar' => $CI->config->site_url('Pages/Ui_components/Navbar'),
            'pages/ui_components/pagination' => $CI->config->site_url('Pages/Ui_components/Pagination'),
            'pages/ui_components/popover' => $CI->config->site_url('Pages/Ui_components/Popover'),
            'pages/ui_components/progress' => $CI->config->site_url('Pages/Ui_components/Progress'),
            'pages/ui_components/scrollbar' => $CI->config->site_url('Pages/Ui_components/Scrollbar'),
            'pages/ui_components/scrollspy' => $CI->config->site_url('Pages/Ui_components/Scrollspy'),
            'pages/ui_components/spinners' => $CI->config->site_url('Pages/Ui_components/Spinners'),
            'pages/ui_components/tooltips' => $CI->config->site_url('Pages/Ui_components/Tooltips'),
            'pages/advanced_ui/cropper' => $CI->config->site_url('Pages/Advanced_ui/Cropper'),
            'pages/advanced_ui/owl_carousel' => $CI->config->site_url('Pages/Advanced_ui/Owl_carousel'),
            'pages/advanced_ui/sweet_alert' => $CI->config->site_url('Pages/Advanced_ui/Sweet_alert'),
            'pages/forms/basic_elements' => $CI->config->site_url('Pages/Forms/Basic_elements'),
            'pages/forms/advanced_elements' => $CI->config->site_url('Pages/Forms/Advanced_elements'),
            'pages/forms/editors' => $CI->config->site_url('Pages/Forms/Editors'),
            'pages/forms/wizard' => $CI->config->site_url('Pages/Forms/Wizard'),
            'pages/charts/apex' => $CI->config->site_url('Pages/Charts/Apex'),
            'pages/charts/chartjs' => $CI->config->site_url('Pages/Charts/Chartjs'),
            'pages/charts/flot' => $CI->config->site_url('Pages/Charts/Flot'),
            'pages/charts/morrisjs' => $CI->config->site_url('Pages/Charts/Morrisjs'),
            'pages/charts/peity' => $CI->config->site_url('Pages/Charts/Peity'),
            'pages/charts/sparkline' => $CI->config->site_url('Pages/Charts/Sparkline'),
            'pages/tables/basic_table' => $CI->config->site_url('Pages/Tables/Basic_table'),
            'pages/tables/data_table' => $CI->config->site_url('Pages/Tables/Data_table'),
            'pages/icons/feather_icons' => $CI->config->site_url('Pages/Icons/Feather_icons'),
            'pages/icons/flag_icons' => $CI->config->site_url('Pages/Icons/Flag_icons'),
            'pages/icons/mdi_icons' => $CI->config->site_url('Pages/Icons/Mdi_icons'),
            'pages/general/blank_page' => $CI->config->site_url('Pages/General/Blank_page'),
            'pages/general/faq' => $CI->config->site_url('Pages/General/Faq'),
            'pages/general/invoice' => $CI->config->site_url('Pages/General/Invoice'),
            'pages/general/profile' => $CI->config->site_url('Pages/General/Profile'),
            'pages/general/pricing' => $CI->config->site_url('Pages/General/Pricing'),
            'pages/general/timeline' => $CI->config->site_url('Pages/General/Timeline'),
            'pages/general/profile' => $CI->config->site_url('Pages/General/Profile'),
            'pages/auth/login' => $CI->config->site_url('Pages/Auth/Login'),
            'pages/auth/register' => $CI->config->site_url('Pages/Auth/Register'),
            'pages/error/404' => $CI->config->site_url('Pages/Error/404'),
            'pages/error/500' => $CI->config->site_url('Pages/Error/500'),






        ];
    }
}


function loadFooterAssets(): array
{
    $CI =& get_instance();
    $CI->load->library('file');

    return [


        'vendors/core/core.js' => $CI->file->getAssetsUrl('vendors/core/core.js'),
        'vendors/chartjs/Chart.min.js' => $CI->file->getAssetsUrl('vendors/chartjs/Chart.min.js'),
        'vendors/jquery.flot/jquery.flot.js' => $CI->file->getAssetsUrl('vendors/jquery.flot/jquery.flot.js'),
        'vendors/jquery.flot/jquery.flot.resize.js' => $CI->file->getAssetsUrl('vendors/jquery.flot/jquery.flot.resize.js'),
        'vendors/bootstrap-datepicker/bootstrap-datepicker.min.js' => $CI->file->getAssetsUrl('vendors/bootstrap-datepicker/bootstrap-datepicker.min.js'),
        'vendors/apexcharts/apexcharts.min.js' => $CI->file->getAssetsUrl('vendors/apexcharts/apexcharts.min.js'),
        'vendors/progressbar.js/progressbar.min.js' => $CI->file->getAssetsUrl('vendors/progressbar.js/progressbar.min.js'),
        'vendors/feather-icons/feather.min.js' => $CI->file->getAssetsUrl('vendors/feather-icons/feather.min.js'),
        'js/template.js' => $CI->file->getAssetsUrl('js/template.js'),
        'js/dashboard.js' => $CI->file->getAssetsUrl('js/dashboard.js'),
        'js/datepicker.js' => $CI->file->getAssetsUrl('js/datepicker.js'),
        'vendors/datatables.net-bs4/dataTables.bootstrap4.js' => $CI->file->getAssetsUrl('vendors/datatables.net-bs4/dataTables.bootstrap4.js'),








    ];
}





if (! function_exists('getDirContents')) {
    function getDirContents(string $path): ?array
    {
        $realPath = realpath($path);
        if ($realPath) {
            $files = [];
            foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($realPath)) as $file) {
                if ($file->isDir()) {
                    continue;
                }
                $files[] = $file->getPathname();
            }
            return $files;
        } else {
            throw new \Exception("Path is not invalid : " . $path, 1);
        }
    }
}
