<?php

if (! class_exists('CheckPermission')) {
    class CheckPermission
    {
        /**
         * 로그인 관련 퍼미션을 체크해주는 클래스 입니다
         *
         * @copyright  2020 DBAD
         * @version    Release: @2.0.0@
         * @since      Class available since Release 2.0.0
         */
        private $CI = null;
        private $session = null;
        private $config = null;
        public function __construct()
        {
            $this->CI =& get_instance();
            $this->session = $this->CI->session;
            $this->config = $this->CI->config;
            $this->CI->load->helper('url');
        }
        public function init(): void
        {
            $segment = strtolower($this->CI->uri->segment(1));
            $secSegment = strtolower($this->CI->uri->segment(2));

            $query = http_build_query(array_map(function ($e) {
                return $this->CI->security->xss_clean(trim($e));
            }, $_GET));

            // var_dump($this->CI->session->isLogin);
            // var_dump($segment);
            if ($segment) { // 세그먼트가 있을때
                $filter = $this->keyToLower([
                    'Verify' => true,
                    'Phone' => true,
                    'Auth' => true,
                    'Home' => true,
                    'Home2' => true,
                    'PracticeTest' => true,
                    'Seminar' => true,
                    'Participate' => true,
                    'PSAT' => true,
                    'ThemeHall' => true,
                    'RecruitInfo' => true,
                    'OpeningHall2' => true,
                    'OpeningHall' => true,
                    'CommunicationEvent' => true,
                    

                ]);

                if (isset($filter[$segment])) { // 로그인 비허용 지역에 접근 시
                    if ($this->CI->session->isLogin) { // 로그인 중일때
                        // var_dump($this->CI->session->isLogin);
                        redirect($this->CI->config->site_url('Richmango/Links/Buttons'), 'refresh'); // 메인으로 이동
                        return;
                    }

                } else {
                    if (! $this->CI->session->isLogin) { // 로그인중이 아닐때
                        // var_dump($this->CI->session->isLogin);
                        redirect($this->CI->config->site_url('Home'), 'refresh'); // 로그인 사이트로 이동
                        return;
                    }
                }
            } else { // 세그먼트가 없을 시 (디폹트 컨트롤러일때)

                if ($this->CI->session->isLogin) { // 로그인 되있으면
                    redirect($this->CI->config->site_url('Richmango/Links/Buttons'), 'refresh'); // 메인으로 이동
                    return;
                } else {
                    redirect($this->CI->config->site_url('Home'), 'refresh'); // 로그인 사이트로 이동
                    return;
                }
            }
        }

        private function arrToLower(array $datas): array
        {
            return array_map(function ($data) {
                return trim(strtolower($data));
            }, $datas);
        }


        private function keyToLower(array $filters): array
        {
            $rltArr = [];
            foreach ($filters as $filter => $isTrue) {
                $rltArr[strtolower($filter)] = $isTrue;
            }
            return $rltArr;
        }
    }
}
