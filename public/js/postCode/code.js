// $(function(){
//     $("iframe").one("load", function() {
//         alert("test");
//         console.log($("iframe").contents());
//         $("iframe").contents().find(".daum_popup .inner_desc .emph_post").remove();
//         $("iframe").contents().find(".daum_popup .inner_desc a").remove();
//         $("iframe").contents().find(".daum_popup .inner_body").remove();
//       });
// })

// $(function(){
//     alert("test");
//     console.log($("#__daum__viewerFrame_1"));
// })


// $("#__daum__viewerFrame_1").on("load", function() {

//     $('html > iframe > iframe#__daum__viewerFrame_1').remove();
//     alert("test");
//     let head = $("html > iframe > iframe#__daum__viewerFrame_1").contents().find("head");
//     let css = '<style>*{color:red}.info_body{display:none}</style>';
//     $(head).append(css);
//   });



function execDaumPostCode(elemId) {
    const elem = document.getElementById(elemId);
    function initLayerPosition() {
        // let width = 300; //우편번호서비스가 들어갈 element의 width
        // let height = 400; //우편번호서비스가 들어갈 element의 height
        // let borderWidth = 5; //샘플에서 사용하는 border의 두께

        // // 위에서 선언한 값들을 실제 element에 넣는다.
        // elem.style.width = width + 'px';
        // elem.style.height = height + 'px';
        // elem.style.border = borderWidth + 'px solid';
        // // 실행되는 순간의 화면 너비와 높이 값을 가져와서 중앙에 뜰 수 있도록 위치를 계산한다.
        // elem.style.left = (((window.innerWidth || document.documentElement.clientWidth) - width) / 2 - borderWidth) + 'px';
        // elem.style.top = (((window.innerHeight || document.documentElement.clientHeight) - height) / 2 - borderWidth) + 'px';




        // 위에서 선언한 값들을 실제 element에 넣는다.
        elem.style.width = '95%';
        elem.style.height = '80%';
        elem.style.border = borderWidth + 'px solid';
        // 실행되는 순간의 화면 너비와 높이 값을 가져와서 중앙에 뜰 수 있도록 위치를 계산한다.
        elem.style.left = '3%';
        elem.style.top = '10%';


    }
    return {
        close: function() {
            elem.style.display = 'none';
        },
        onresize : function(size) {
            element_wrap.style.height = size.height+'px';
        },
        postCode: function(_cb) {
            new daum.Postcode({
                oncomplete: function (data) {
                    var addr = ''; // 주소 변수
                    var extraAddr = ''; // 참고항목 변수
    
                    //사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
                    if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                        addr = data.roadAddress;
                    } else { // 사용자가 지번 주소를 선택했을 경우(J)
                        addr = data.jibunAddress;
                    }
    
                    // 사용자가 선택한 주소가 도로명 타입일때 참고항목을 조합한다.
                    if (data.userSelectedType === 'R') {
                        // 법정동명이 있을 경우 추가한다. (법정리는 제외)
                        // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
                        if (data.bname !== '' && /[동|로|가]$/g.test(data.bname)) {
                            extraAddr += data.bname;
                        }
                        // 건물명이 있고, 공동주택일 경우 추가한다.
                        if (data.buildingName !== '' && data.apartment === 'Y') {
                            extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                        }
                        // 표시할 참고항목이 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
                        if (extraAddr !== '') {
                            extraAddr = ' (' + extraAddr + ')';
                        }
                    }
                    // TODO: 상세주소까지 다 입력이 완료가 된 다음 display:none 시켜야함. 
                    elem.style.display = 'none';





                    // $('#addressInput').val(data.roadAddress);
                    // $('#saveForm').append([
                    //     ['studio_bname', data.bname],
                    //     ['studio_jibun_address',data.jibunAddress],
                    //     ['studio_city',data.sido],
                    //     ['studio_local',data.sigungu],
                    // ].map(v => `<input class="locateHidden" type="hidden" value="${v[1]}" name="${v[0]}">`).join(''));
                    // $('#explain').focus();
                    _cb(data);
                },
                width: '100%',
                height: '100%',
                maxSuggestItems: 5,
            }).embed(elem);
            elem.style.display = 'block';
            initLayerPosition();
        }
    }
}