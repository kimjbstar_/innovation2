window.addEventListener('load', function (e) {
    let isSubmut = true;
    document.getElementById('destination-input').addEventListener('keypress', function (e) {
        if (e.keyCode === 13 && this.value.trim()) {
            if (isSubmut) {
                isSubmut = false;
                e.preventDefault();
                let query = this.value.trim();
                UTIL.submit({
                    url: `${this.dataset.search}`,
                    method: 'get',
                    data: {
                        query: query
                    }
                });
            }
        }
    });
})