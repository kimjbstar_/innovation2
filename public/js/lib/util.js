'use strict';
const UTIL = {
    validate : {
        phoneNumValidate : function(num) {
            return /(^02.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/g.test(num)
        },
        emailValidate : function(emailAddress) {
            return /^[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[@]{1}[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[.]{1}[A-Za-z]{1,5}$/g.test(emailAddress);
        },
    },
    isScrollBottom: function (callback) {
        if (typeof(callback) === 'function') {
            window.addEventListener('scroll', async function(){
                if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight - 200) {
                    callback();
                }
            });
        }
    },
    submit: function({method, url, data}) {
        const objToInput = (data) => {
            let rlt = '';
            for (let i in data) {
                rlt += `<input type="hidden" name="${i}" value="${data[i]}">`
            }
            return rlt;
        };
        const form = document.createElement('form');
        form.method = method.toUpperCase();
        form.action = url;
        form.innerHTML = objToInput(data);
        document.body.appendChild(form);
        form.submit();
        
    },
    dateIsoFormat: function(_date) {
        return new Date(_date).toISOString().split('T')[0];
    },
    isMobilePhoneNum : function(str) {
        return /^[0-9]{2,3}[0-9]{3,4}[0-9]{4}$/.test(str);
    },
    extractNumberFromString : function(str) {
        return Number(str.replace(/\D/g, ''));
    },
    isNumeric: function(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    },
    inputVaildateToClassNext: function(className) {
        let tag = document.getElementsByClassName(className);
        for( let i = 0, tagLen = tag.length; i < tagLen - 1; i++) {
            tag[i].addEventListener('keyup', function(e){
                if(this.value.length === this.maxLength) {
                    this.nextElementSibling.focus();
                }
            });
        }
    },
    req : async function({url, formData = null, method}) {
        const buildQuery = (data) => Object.keys(data).map(v => `${encodeURIComponent(v)}=${encodeURIComponent(data[v])}`).join('&');
        const toFormData = (data) => {
            const f = new FormData();
            for (let key in data) {
                f.append(key, data[key]);
            }
        };
        let _req = null;
        if (method.toLowerCase() === 'get') {
            url = `${url}?${buildQuery(formData)}`
            _req = await fetch(url, {
                method: method
            });
        } else {
            formData = toFormData(formData);
            _req = await fetch(url, {
                method : method,
                body: formData
            });
        }
        
        let rlt = await _req.text();
        try {
            return JSON.parse(rlt);
        } catch (e) {
            return rlt;
        }
    },
    inputVaildateToClassBefore: function(className) {
        let tag = document.getElementsByClassName(className);
        for( let i = 0, tagLen = tag.length; i < tagLen; i++) {
            tag[i].addEventListener('keyup', function(e) {
                if(e.keyCode === 8) {
                    if (this.previousElementSibling) {
                        this.previousElementSibling.value = '';
                        this.previousElementSibling.focus();
                    }
                }
            });
        }
    },
    formIsEmpty : function(formData) {
        for (const iterator of formData.values()) {
            if (! iterator) {
                return false
            }
        }
        return true;
    },
    confirm : async function(message) {
        const {value} = await Swal.fire({
            title: message,
            showCancelButton: true,
            confirmButtonColor : '#ea5b11',
            allowOutsideClick: false,
        });
        return value;
    },
    alert : function(message, cb = null) {
        try {

            Swal.fire({
                title: message,
                confirmButtonColor : '#ea5b11',
                allowOutsideClick: false,
                onAfterClose: () => {
                    if (cb) {
                        cb();
                    }
                },
            }).then((v) => {
                // if (v.value && cb) {
                //     setTimeout(() => {
                //         cb();
                //     }, 10);
                // }
                // if (cb) {
                //     cb();
                // }
            });
        } catch (e) {
            console.warn('Must include Swal library');
        }
    },
    multiAlert : function(title, firstMessage, secondMessage) {
        try {
            Swal.fire({
                title: title,
                confirmButtonText: firstMessage,
                showCancelButton : true,
                cancelButtonText : secondMessage,
                confirmButtonColor : '#ea5b11',
                allowOutsideClick: false,
            });
        } catch (e) {
            console.warn('Must include Swal library');
        }
    },
    numberFormat : function(num) {
        return Number(num).toLocaleString('kr');
    },
    formControlButton : function(formId, buttonId, callback) {
        let tag = document.getElementById(buttonId);
        if(tag) {
            tag.addEventListener('click', function(e) {
                e.preventDefault();
                if(callback) {
                    callback();
                } else {
                    try {
                        document.getElementById(formId).submit();
                    } catch(e) {
                        console.log(e);
                    }
                }
            });
        }
    },
    getUrlVars : function() {
        if (location.search) {
            let rlt = {};
            let query = location.search
                           .slice(1, location.search.length)
                           .split('&')
                           .map(v => v.split('='));
            for (let i in query) {
                rlt[query[i][0]] = decodeURI(query[i][1]);
            }
            return rlt;
        }
    }
};
