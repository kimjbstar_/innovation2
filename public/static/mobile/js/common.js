$('.popup_trigger,.modal_wrap .close_btn').click(function(){
    $('.modal_wrap').toggleClass('open');
    if($('.modal_wrap').hasClass('open')) {
        const img = $(this).attr('data-popup')
        $('.img_cont').append('<img src="' + img +'" alt="팝업" >')
    } else {
        $('.img_cont').children().remove()
    }
})

$('.slider_trigger').click(function(){
    var target = $(this).attr('data-popup')
    $('#modal-' + target ).addClass('open')
})

$('.modal_slider .close_btn').click(function(){
    $('.modal_slider').removeClass('open')
})