import React, { useState } from "react";
import "./HumanDev.scss";

const HumanDev = () => {
  const [isOnFirst, setIsOnFirst] = useState(true);
  return (
    <div className="HumanDev">
      <div className="head-title" />
      <div className="simple-line" />
      <div className="vedio-tab-container">
        <div
          className={`video-tab1-${isOnFirst ? "color" : "normal"}`}
          onClick={() => setIsOnFirst(!isOnFirst)}
        />
        <div
          className={`video-tab2-${isOnFirst ? "normal" : "color"}`}
          onClick={() => setIsOnFirst(!isOnFirst)}
        />
      </div>
      <div className="main-content" />
    </div>
  );
};

export default HumanDev;
