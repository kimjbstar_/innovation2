import React from "react";
import HumanDev from "./pages/HumanDev";
import CountryMan from "./pages/CountryMan";
import FutureMan from "./pages/FutureMan";
import "./App.scss";

function App() {
  return (
    <div className="App">
      <div className="top-image" />
      <HumanDev />
      <CountryMan />
      <FutureMan />
      <div className="quiz" />
      <div className="footer">
        <a href="https://forms.gle/18zhUAVZFvfuij7Q9">
          <div className="quiz-button" />
        </a>
      </div>
    </div>
  );
}

export default App;
